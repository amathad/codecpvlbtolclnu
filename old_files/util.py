#import few packages
import sys, os
import tensorflow as tf
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # Do not use GPU for tensor flow
import numpy as np
import pandas as pd

#amplitf
home = os.getenv('HOME')
sys.path.append(home+"/Packages/AmpliTF/")
import amplitf.interface as atfi
import amplitf.kinematics as atfk
import amplitf.dynamics as atfd
from amplitf.phasespace.rectangular_phasespace import RectangularPhaseSpace

#tfa2
sys.path.append(home+"/Packages/TFA2/")
import tfa.optimisation as tfo
import tfa.toymc as tft

#get current directory
fit_dir=os.path.dirname(os.path.abspath(__file__))

########### Make some common functions
def MakeFourVector(pmag, costheta, phi, msq): 
    """Make 4-vec given magnitude, cos(theta), phi and mass"""
    sintheta = atfi.sqrt(1. - costheta**2) #theta 0 to pi => atfi.sin always pos
    px = pmag * sintheta * atfi.cos(phi)
    py = pmag * sintheta * atfi.sin(phi)
    pz = pmag * costheta
    E  = atfi.sqrt(msq + pmag**2)
    return atfk.lorentz_vector(atfk.vector(px, py, pz), E)

def pvecMag(Msq, m1sq, m2sq): 
    """Momentum mag of (1 or 2) in M rest frame"""
    kallen = atfi.sqrt(Msq**2 + m1sq**2 + m2sq**2 - 2.*(Msq*m1sq + Msq*m2sq + m1sq*m2sq))
    return kallen/(2.*atfi.sqrt(Msq))

def InvRotateAndBoostFromRest(m_p4mom_p1, m_p4mom_p2, gm_phi_m, gm_ctheta_m, gm_p4mom_m):
    """
    Function that gives 4momenta of the particles in the grand mother helicity frame

    m_p4mom_p1: particle p1 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
    m_p4mom_p2: particle p2 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
    gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
    gm_4mom_m: m 4mom in gm's helicity frame
    --------------
    Checks done such as 
        - (lc_p4mom_p+lc_p4mom_k == lc_p4mom_r), 
        - Going in reverse i.e. boost lc_p4mom_p into R rest frame (using lc_p4mom_r) and rotating into R helicity frame gives back r_p4mom_p 
            - i.e. lc_p4mom_p_opp = atfk.rotate_lorentz_vector(atfk.boost_to_rest(lc_p4mom_p, lc_p4mom_r), -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)) where lc_p4mom_p_opp == r_p4mom_p
        - Rotate and boost, instead of boost and rotate should give the same answer, checked (Does not agree with TFA implementation though)
            -i.e. lc_p4mom_prot  = atfk.rotate_lorentz_vector(lc_p4mom_p, -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)
            -i.e. lc_p4mom_krot  = atfk.rotate_lorentz_vector(lc_p4mom_k, -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)
            -i.e. lc_p4mom_p2    = atfk.boost_to_rest(lc_p4mom_prot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_p2 == r_p4mom_p
            -i.e. lc_p4mom_k2    = atfk.boost_to_rest(lc_p4mom_krot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_k2 == r_p4mom_k
    """
    #First: Rotate particle p 3mom defined in mother m helicity frame such that z now points along z in grand mother gm helicity frame 
    #(i.e interpreted as gm mom in it's grand-grand-mother ggm rest frame)
    m_p4mom_p1_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p1, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
    m_p4mom_p2_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p2, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
    #Second: Boost particle p 4mom from mother m's rest frame to gm helicity frame. This is done using boost vector from gm_p4mom_m  [checked: E(m_p4mom_p1_zmmom + m_p4mom_p2_zmmom) == Mass_m]
    gm_p4mom_p1            = atfk.boost_from_rest(m_p4mom_p1_zgmmom, gm_p4mom_m)
    gm_p4mom_p2            = atfk.boost_from_rest(m_p4mom_p2_zgmmom, gm_p4mom_m)
    return gm_p4mom_p1, gm_p4mom_p2

def make_q2_cthl(dt_np, Lb4moml, Lc4moml, Mu4moml, Nu4moml):
    """Get q^2 and cos(theta) given 4 momenta of all particles"""
    Lb4mom = atfk.lorentz_vector(atfk.vector(dt_np[Lb4moml[1]],dt_np[Lb4moml[2]],dt_np[Lb4moml[3]]), dt_np[Lb4moml[0]])
    Lc4mom = atfk.lorentz_vector(atfk.vector(dt_np[Lc4moml[1]],dt_np[Lc4moml[2]],dt_np[Lc4moml[3]]), dt_np[Lc4moml[0]])
    Mu4mom = atfk.lorentz_vector(atfk.vector(dt_np[Mu4moml[1]],dt_np[Mu4moml[2]],dt_np[Mu4moml[3]]), dt_np[Mu4moml[0]])
    Nu4mom = atfk.lorentz_vector(atfk.vector(dt_np[Nu4moml[1]],dt_np[Nu4moml[2]],dt_np[Nu4moml[3]]), dt_np[Nu4moml[0]])
    #Nu4mom = Nu4mom + (Lb4mom-(Lc4mom+Mu4mom+Nu4mom))
    #Boost Lb frame
    Lb4mom_Lb  = atfk.boost_to_rest(Lb4mom, Lb4mom)
    Lc4mom_Lb  = atfk.boost_to_rest(Lc4mom, Lb4mom)
    Mu4mom_Lb  = atfk.boost_to_rest(Mu4mom, Lb4mom)
    Nu4mom_Lb  = atfk.boost_to_rest(Nu4mom, Lb4mom)
    W4mom_Lb   = atfk.boost_to_rest(Mu4mom+Nu4mom, Lb4mom)
    #Q2
    Q2_var     = tf.square(atfk.mass(Mu4mom+Nu4mom))
    #Cthl
    Lb4mom_W  = atfk.boost_to_rest(Lb4mom_Lb, W4mom_Lb)
    Mu4mom_W  = atfk.boost_to_rest(Mu4mom_Lb, W4mom_Lb)
    Cthl_var  =-atfk.scalar_product(atfk.spatial_components(Mu4mom_W), atfk.spatial_components(Lb4mom_W))/(P(Mu4mom_W) * P(Lb4mom_W))
    #clthl    = atfk.scalar_product(atfk.spatial_components(Mu4mom_W), atfk.spatial_components(W4mom_Lb))/(P(Mu4mom_W) * P(W4mom_Lb))
    return Q2_var, Cthl_var

def HelAngles3Body(pa, pb, pc):
    """Get three-body helicity angles"""
    theta_r  = atfi.acos(-atfk.z_component(pc) / atfk.norm(atfk.spatial_components(pc)))
    phi_r    = atfi.atan2(-atfk.y_component(pc), -atfk.x_component(pc))
    pa_prime = atfk.rotate_lorentz_vector(pa, -phi_r, -theta_r, phi_r)
    pb_prime = atfk.rotate_lorentz_vector(pb, -phi_r, -theta_r, phi_r)
    pa_prime2= atfk.boost_to_rest(pa_prime, pa_prime+pb_prime)
    theta_a  = atfi.acos(atfk.z_component(pa_prime2) / atfk.norm(atfk.spatial_components(pa_prime2)))
    phi_a    = atfi.atan2(atfk.y_component(pa_prime2), atfk.x_component(pa_prime2))
    return (theta_r, phi_r, theta_a, phi_a)

def RotateAndBoostToRest(gm_p4mom_p1, gm_p4mom_p2, gm_phi_m, gm_ctheta_m):
    """
    gm_p4mom_p1: particle p1 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
    gm_p4mom_p2: particle p2 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
    gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
    """
    #First:  Rotate particle p 3mom defined in grand mother gm helicity frame such that z now points along z of mother m in gm rest frame.
    gm_p4mom_p1_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p1, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
    gm_p4mom_p2_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p2, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
    #Second: Boost particle p 4mom to mother m i.e. p1p2 rest frame
    gm_p4mom_m             = gm_p4mom_p1_zmmom+gm_p4mom_p2_zmmom
    m_p4mom_p1             = atfk.boost_to_rest(gm_p4mom_p1_zmmom, gm_p4mom_m)
    m_p4mom_p2             = atfk.boost_to_rest(gm_p4mom_p2_zmmom, gm_p4mom_m)
    return m_p4mom_p1, m_p4mom_p2
########

#define the class for Lb->Lclnu decay
class LbToLclNu_Model:
    """Class that defines the Lb->Lclnu decay"""

    def __init__(self, MLb, MLc, Mlep, wc_floated_names = ['CVR'], ff_floated_names = ['None']):
        """initialise some variables"""
        #Masses of particles involved 
        self.MLb     = MLb
        self.MLc     = MLc
        self.Mlep    = Mlep
        #print(self.MLb, self.MLc, self.Mlep)
    
        #Constants expressed in GeV
        self.GF      = 1.166378e-5 #GeV^-2
        self.Vcb     = 4.22e-2     #avg of incl and excl
        #print(self.GF, self.Vcb)
        
        #Define function defined in lattice QCD here related to form factors
        self.Tf_plus = {} #mf_pole = M_BC + delta_f and Tf_plus = mf_pole**2
        self.Tf_plus['fplus']      = np.power(6.276 + 56e-3         , 2)#GeV
        self.Tf_plus['fperp']      = np.power(6.276 + 56e-3         , 2)#GeV
        self.Tf_plus['f0']         = np.power(6.276 + 449e-3        , 2)#GeV
        self.Tf_plus['gplus']      = np.power(6.276 + 492e-3        , 2)#GeV
        self.Tf_plus['gperp']      = np.power(6.276 + 492e-3        , 2)#GeV
        self.Tf_plus['g0']         = np.power(6.276 + 0.            , 2)#GeV
        self.Tf_plus['hplus']      = np.power(6.276 + 6.332 - 6.276 , 2)#GeV
        self.Tf_plus['hperp']      = np.power(6.276 + 6.332 - 6.276 , 2)#GeV
        self.Tf_plus['htildeplus'] = np.power(6.276 + 6.768 - 6.276 , 2)#GeV
        self.Tf_plus['htildeperp'] = np.power(6.276 + 6.768 - 6.276 , 2)#GeV
        #print(self.Tf_plus)
        
        #Map to relate W* resonance helicity to the sign in the amplitude (used in building the amplitde)
        self.eta    = {'t': 1.,  0 : -1.,  -1: -1., 1 : -1.}
        #print(self.eta)
        
        #Map to relate WC to the sign in the amplitude (used in building the amplitde)
        self.signWC = {'V': 1., 'A': -1., 'S': 1., 'PS': -1., 'T': 1., 'PT': -1.}
        #print(self.signWC)

        #make SM wc values
        self.wc_sm = {}
        self.wc_sm['CVL'] = 0.
        self.wc_sm['CVL'] = 0.
        self.wc_sm['CVR'] = 0.
        self.wc_sm['CSR'] = 0.
        self.wc_sm['CSL'] = 0.
        self.wc_sm['CT']  = 0.
        #print(self.wc_sm)
        
        #make SM FF mean and covariance values 
        #ff mean
        self.ff_mean  = {}
        f     = open(fit_dir+"/FF_cov/LambdabLambdac_results.dat", "r")
        for l in f.readlines(): self.ff_mean[l.split()[0]] = float(l.split()[1])
        f.close()
        #print(self.ff_mean)

        #ff covariant
        self.ff_cov = {}
        for l in list(self.ff_mean.keys()): self.ff_cov[l] = {}
        f  = open(fit_dir+"/FF_cov/LambdabLambdac_covariance.dat", "r")
        for l in f.readlines(): self.ff_cov[l.split()[0]][l.split()[1]] = float(l.split()[2])
        f.close()
        #print(self.ff_cov)
        
        #Lists that will serve as index for incoherent sum of amplitudes (defined in units of 1/2 except for lw used in leptonic case that does not need this)
        self.lLbs = [1, -1]
        self.lLcs = [1, -1]
        self.lls  = [1, -1]
        #print(self.lLbs, self.lLcs, self.lls)
        
        #Lists that will serve as index for coherent sum of amplitudes (defined in units of 1/2 except for lw used in leptonic case that does not need this)
        self.lws  = ['t', 0, 1, -1]
        self.WCs  = ['V', 'A', 'S', 'PS', 'T', 'PT']
        self.FFs  = ['a0f0', 'a0fplus', 'a0fperp', 'a0g0', 'a0gplus', 'a0hplus', 'a0hperp', 'a0htildeplus']
        self.FFs += ['a1f0', 'a1fplus', 'a1fperp', 'a1g0', 'a1gplus', 'a1gperp', 'a1hplus', 'a1hperp', 'a1htildeplus', 'a1htildeperp']
        self.FFs += ['a0gperp', 'a0htildeperp']
        #print(self.lws, self.WCs, self.FFs)
        
        #Map defining WC -> FF (Used to turn off FF floating if WC is not floating)
        self.map_wc_ff = {}
        self.map_wc_ff['SM']   = ['a0f0', 'a0fplus', 'a0fperp', 'a1f0', 'a1fplus', 'a1fperp', 'a0g0', 'a0gplus', 'a0gperp', 'a1g0', 'a1gplus', 'a1gperp']
        self.map_wc_ff['CVL']  = ['a0f0', 'a0fplus', 'a0fperp', 'a1f0', 'a1fplus', 'a1fperp', 'a0g0', 'a0gplus', 'a0gperp', 'a1g0', 'a1gplus', 'a1gperp']
        self.map_wc_ff['CVR']  = ['a0f0', 'a0fplus', 'a0fperp', 'a1f0', 'a1fplus', 'a1fperp', 'a0g0', 'a0gplus', 'a0gperp', 'a1g0', 'a1gplus', 'a1gperp']
        self.map_wc_ff['CSL']  = ['a0f0', 'a1f0', 'a0g0', 'a1g0']
        self.map_wc_ff['CSR']  = ['a0f0', 'a1f0', 'a0g0', 'a1g0']
        self.map_wc_ff['CT']   = ['a0hplus', 'a0hperp', 'a0htildeperp', 'a0htildeplus', 'a1hplus', 'a1hperp', 'a1htildeplus', 'a1htildeperp']
        #print(self.map_wc_ff)
    
        #get wilson coefficients (wc)
        self.wc_params     = self.get_wc_params()
        #get form factor (ff) parameters
        self.ff_params     = self.get_ff_params()
        #define dictionary with all parms (fixed and floated)
        self.tot_params    = {**self.wc_params, **self.ff_params}
        #define limits and rectangular phase space i.e. (qsq, costhmu) space
        phsp_limits  = [(self.Mlep**2, (self.MLb - self.MLc)**2)] 
        phsp_limits += [(-1., 1.)]
        self.phase_space = RectangularPhaseSpace(ranges=phsp_limits)

        #float only the necesarry wilson coefficient
        self.fix_or_float_params(wc_floated_names)
        self.wc_floated = [p for p in list(self.wc_params.values()) if p.floating()]
        self.wc_floated_names = [p.name for p in self.wc_floated]
        if len(self.wc_floated) == 0:
            self.wc_floated = None
            self.wc_floated_names = None

        #float only the necesarry ff params
        self.ff_floated = None
        self.ff_floated_names = None
        self.ff_floated_mean  = None
        self.ff_floated_cov   = None
        if ff_floated_names[0] == 'None':
            print('All form factors parameters are fixed!')
        elif ff_floated_names[0] == 'All':
            print('All form factors parameters related to the WC are floated!')
            self.fix_or_float_params(self.map_wc_ff['SM'])
            if self.wc_floated_names is not None:
                if 'CT' in self.wc_floated_names:
                    self.fix_or_float_params(self.map_wc_ff['CT'])

            self.ff_floated = [p for p in list(self.ff_params.values()) if p.floating()]
            self.ff_floated_names = [p.name for p in self.ff_floated]
            self.ff_floated_mean, self.ff_floated_cov = self.get_FF_mean_cov()
        else:
            self.fix_or_float_params(ff_floated_names)
            self.ff_floated = [p for p in list(self.ff_params.values()) if p.floating()]
            self.ff_floated_names = [p.name for p in self.ff_floated]
            self.ff_floated_mean, self.ff_floated_cov = self.get_FF_mean_cov()

        #define a free parameter independent integral to cache everything
        self.fp_indp_terms = None
        self.fp_indp_terms_norm = None

    def get_wc_params(self):
        """Make WC parameters"""
        Wlcoef = {}
        Wlcoef['CVL'] = tfo.FitParameter("CVL" , 0.0, -2., 2., 0.08)
        Wlcoef['CVR'] = tfo.FitParameter("CVR" , 0.1, -2., 2., 0.08)
        Wlcoef['CSR'] = tfo.FitParameter("CSR" , 0.0, -2., 2., 0.08)
        Wlcoef['CSL'] = tfo.FitParameter("CSL" , 0.0, -2., 2., 0.08)
        Wlcoef['CT']  = tfo.FitParameter("CT"  , 0.0, -2., 2., 0.08)
        for k in list(Wlcoef.keys()): Wlcoef[k].fix() #Fix all the wcs
        print('Setting WCs to SM value of zero and allowed to vary in the fit b/w [-2., 2.]')
        return Wlcoef

    def get_ff_params(self):
        """Make FF parameters"""
        #make dictionary of ff params
        ffact = {}
        print('Setting FF to LQCD central value and allowed to vary b/w [lqcd_val - 100 * lqcd_sigma, lqcd_val + 100 * lqcd_sigma]. So this corresponds to...')
        for FF in self.FFs[:-2]: 
            ff_mn  = self.ff_mean[FF]
            ff_sg  = np.sqrt(self.ff_cov[FF][FF])
            nsigma_ff = 100.
            ff_l   = ff_mn - nsigma_ff*ff_sg
            ff_h   = ff_mn + nsigma_ff*ff_sg
            print('Setting', FF, 'to SM value:', ff_mn, 'with sigma', ff_sg, ', allowed to vary in fit b/w [', ff_l, ff_h, ']')
            ffact[FF] = tfo.FitParameter(FF, ff_mn, ff_l, ff_h, 0.08)
            ffact[FF].fix() #fix all the ff params
            
        return ffact

    def fix_or_float_params(self, fp_list, fix = False, verbose = True):
        """Given a list of parmeter names float of fix the paramters"""
        for p in list(self.tot_params.values()):
            if p.name in fp_list:
                if fix:
                    if verbose: print('Fixing', p.name)
                    p.fix()
                else:
                    if verbose: print('Floating', p.name)
                    p.float()

    def get_FF_mean_cov(self):
        """get the FF mean and covariant for FF parameters""" 
    
        #define a check for pos definite covariant matrix
        def _is_symm_pos_def(A):

            def _check_symmetric(a, rtol=1e-05, atol=1e-08): 
                return np.allclose(a, a.T, rtol=rtol, atol=atol)
        
            if _check_symmetric(A):
                try:
                    np.linalg.cholesky(A)
                    return True
                except np.linalg.LinAlgError:
                    return False
            else:
                return False
    
        if self.ff_floated_names is None:
            raise Exception("Nothing is floated to cannot get mean and covariance for floated FF params.")

        #get FF parameter  that are floated
        #get mean
        mean_list = [self.ff_mean[n] for n in self.ff_floated_names]
        mean_list = np.array(mean_list)
        #get cov
        cov_list  = [[self.ff_cov[l1][l2] for l2 in self.ff_floated_names] for l1 in self.ff_floated_names]
        cov_list  = np.array(cov_list)
        #check if symmetric and positive definite
        if not _is_symm_pos_def(cov_list): 
            print('Not symmetric positive definite cov matrix, exiting')
            exit(0)
        else:
            print('Cov is symmetric and positive definite')
    
        return mean_list, cov_list

    def get_freeparams(self):
        """Make dictionary of free parameters i.e. WC * FF"""

        #define function for change of basis
        def _wc_basis(basis_name):
            """Change the WC basis e.g. 1+CVL+CVR -> V"""
            wcp = None
            if basis_name == 'V':
                wcp =  atfi.const(1.) + self.wc_params['CVL']() + self.wc_params['CVR']()
            elif basis_name == 'A':
                wcp =  atfi.const(1.) + self.wc_params['CVL']() - self.wc_params['CVR']()
            elif basis_name == 'S':
                wcp =  self.wc_params['CSL']() + self.wc_params['CSR']()
            elif basis_name == 'PS':
                wcp =  self.wc_params['CSL']() - self.wc_params['CSR']()
            elif basis_name == 'T' or basis_name == 'PT':
                wcp =  self.wc_params['CT']()
            else:
                raise Exception('The passed basis_name', basis_name, 'not recognised')
    
            return wcp

        def _ff_common(ff_name):
            """Set a0gplus = a0gperp and a0htildeplus = a0htildeperp"""
            ffp = None
            if ff_name == 'a0gperp':
                ffp = self.ff_params['a0gplus']()
            elif ff_name == 'a0htildeperp':
                ffp = self.ff_params['a0htildeplus']()
            else:
                ffp = self.ff_params[ff_name]()

            return ffp

        #Define free parameters dictionary
        free_params = {}
        for WC in self.WCs: 
            for FF in self.FFs:
                free_params[( WC, FF)] = _wc_basis(WC) * _ff_common(FF)
    
        #print(len(free_params.keys()))                        #6*20 = 120
        return free_params

    def prepare_data(self,x):
        """Function that calculates most of the variables for phase space variables"""
        Vars = {}
        #Store Lc phase_space Varsiables and make sure volume element stays as dq2*dcthlc*dcthl*dphl*dm2pk
        q2            = x[:,0]
        w_ctheta_l    = x[:,1] #This because in MC it is like this
        lb_ctheta_lc  = atfi.ones(x[:,0])  #Lb polarisation is zero (shape same as integrating or fixing)
        lb_phi_lc     = atfi.zeros(x[:,0]) #always zeros
        w_phi_l       = atfi.zeros(x[:,0]) #Lb polarisation is zero (shape same as integrating or fixing)
    
        #Store info of other particles
        lb_ctheta_w   = -lb_ctheta_lc
        lb_phi_w      =  lb_phi_lc+atfi.pi()
        w_ctheta_nu   = -w_ctheta_l
        w_phi_nu      =  w_phi_l+atfi.pi()
    
        #Lc and W 4mom in Lb rest frame     (zLb = pbeam_lab x pLb_lab, yLb = zLb x lb_p3vec_lc, x = y x z => r, theta, phi = lb_p3mag_lc,lb_ctheta_lc, 0)
        lb_p3mag_lc  = pvecMag(atfi.const(self.MLb)**2, atfi.const(self.MLc)**2, q2)
        lb_p4mom_lc  = MakeFourVector(lb_p3mag_lc,     lb_ctheta_lc, lb_phi_lc, atfi.const(self.MLc)**2)
        lb_p4mom_w   = MakeFourVector(lb_p3mag_lc,     lb_ctheta_w,  lb_phi_w , q2)
        #l and nu 4mom in W helicity frame (zw = lb_p3vec_w, yw = yLb = zLb x lb_p3vec_lc , xw = yw x zw => r, theta, phi => w_p3mag_l, w_ctheta_l, w_phl_l)
        w_p3mag_l    = pvecMag(q2, atfi.const(self.Mlep)**2, 0.)
        w_p4mom_l    = MakeFourVector(w_p3mag_l,    w_ctheta_l , w_phi_l , atfi.const(self.Mlep)**2)
        w_p4mom_nu   = MakeFourVector(w_p3mag_l,    w_ctheta_nu, w_phi_nu, 0.)
    
        #Get everything is Lb rest frame
        lb_p4mom_l , lb_p4mom_nu = InvRotateAndBoostFromRest(w_p4mom_l,   w_p4mom_nu, lb_phi_w,  lb_ctheta_w,  lb_p4mom_w)
    
        #Store Varss
        Vars['q2']            = q2
        Vars['El']            = atfk.time_component(lb_p4mom_l)
        Vars['m_Lbmu']        = atfk.mass(lb_p4mom_lc+lb_p4mom_l)
        #lnu angles
        Vars['lb_ctheta_w']   = lb_ctheta_w
        Vars['lb_phi_w']      = lb_phi_w
        Vars['w_ctheta_l']    = w_ctheta_l
        Vars['w_phi_l']       = w_phi_l
        #3mom mag 
        Vars['lb_p3mag_lc']   = lb_p3mag_lc
        Vars['w_p3mag_l']     = w_p3mag_l
    
        return Vars

    def d_phi_twobody(self,mijsq, misq, mjsq):
        """Two body phase space element"""
        return 1./(2.**4 * atfi.pi()**2) * pvecMag(mijsq, misq, mjsq)/atfi.sqrt(mijsq)

    def get_phasespace_term(self, Obs):
        """Two body phase space factors along with the element"""
        #d(lb_ctheta_lc) * d(q2) * d(w_ctheta_l) * d(w_phi_l)
        phsspace  = 1./(2. * atfi.const(self.MLb) * 2. * atfi.pi()) * self.d_phi_twobody(atfi.const(self.MLb)**2, atfi.const(self.MLc)**2, Obs['q2'])   #Lb -> Lc W
        phsspace *= self.d_phi_twobody(Obs['q2'], atfi.const(self.Mlep)**2, 0.)   #W  ->  l nu
        return phsspace

    def get_lb_ampls(self, Obs):
        """
        Some important comments about the hadronic currents:
        V currents:
            - aV, bV and cV are form factor parameter independent (unlike in our paper)
            - When summing over lwd only one term is needed. This term is taken as 't' (b'cos eta[t] = 1). Make sure Leptonic V and A current lwd='t'.
            - The ffterms func return corrent q2 dependent terms pertaining to FF parameter a0 and a1.
            - Hadr(lb, lc, lw, 't', 'V', 'a0') have 16 comb out of which 12 survive and 4 are zero
        S currents:
            - Fake lw and lwd index (see above for reason). Fixed to zero, make sure Leptonic S, PS currents also have lw = lwd = 't'.
            - Hadr(lb, lc, 't', 't', 'S', 'a0') have 4 terms all which are nonzero.
        A currents: 
            - Like in Leptonic current these are NOT equal to V currents.
            - Same comments as V currents.
        PS currents: 
            - Like in Leptonic current these are NOT equal to S currents.
            - Same comments as S currents.
        T currents:
            - Hadr(lb, lc, lw, lwd, 'T', 'a0') have 64 terms out of which 32 are nonzero.
        PT currents:
            - Like in Leptonic current these are NOT equal to T currents.
            - same comments as T currents.
        Func returns:
            Dict with index as tuples Hadr[(lb, lc, lw, lwd, wc, ff)]
        """
        #q2 and angular terms: Used sin(x/2) = sqrt((1-cthlc)/2) and cos(x/2)=sqrt((1+cthlc)/2).Since x/2 ranges from 0 to pi/2 both sine and cosine are pos.
        q2          =  Obs['q2']
        Mpos        = atfi.const(self.MLb) + atfi.const(self.MLc)
        Mneg        = atfi.const(self.MLb) - atfi.const(self.MLc)
        sqrtQp      = atfi.sqrt(atfi.pow(Mpos,2) - q2)
        sqrtQn      = atfi.sqrt(atfi.pow(Mneg,2) - q2)
        sqrtQpMn    = sqrtQp * Mneg
        sqrtQnMp    = sqrtQn * Mpos
        aV          = sqrtQpMn/atfi.sqrt(q2)
        bV          = sqrtQnMp/atfi.sqrt(q2)
        cV          = atfi.sqrt(np.array(2.)) * sqrtQn
        aA          = bV
        bA          = aV
        cA          = atfi.sqrt(np.array(2.)) * sqrtQp
        mb          = atfi.const(4.18       ) #GeV
        mc          = atfi.const(1.275      ) #GeV
        aS          = sqrtQpMn/(mb - mc)
        aP          = sqrtQnMp/(mb + mc)
        aT          = sqrtQn
        bT          = atfi.sqrt(np.array(2.)) * bV
        cT          = atfi.sqrt(np.array(2.)) * aV
        dT          = sqrtQp
        cthlc       = -Obs['lb_ctheta_w'] #ONE Lb polarisation ignored
        costhlchalf = atfi.sqrt((1+cthlc)/2.)  #One Lb polarisation ignored
        sinthlchalf = atfi.sqrt((1-cthlc)/2.)  #ZERO Lb polarisation ignored, so commented out the terms
        t0          = atfi.pow(Mneg,2)
    
        #get only q2 dependent terms in FF expansion i.e. ones pertaining to a0 and a1.
        ffterms = {}
        for ff in ['fplus', 'fperp', 'f0', 'gplus', 'gperp', 'g0', 'hplus', 'hperp', 'htildeperp', 'htildeplus']:
            cf = 1./(1. - q2/atfi.const(self.Tf_plus[ff]))
            zf = (atfi.sqrt(atfi.const(self.Tf_plus[ff]) - q2) - atfi.sqrt(atfi.const(self.Tf_plus[ff]) - t0))/(atfi.sqrt(atfi.const(self.Tf_plus[ff]) - q2) + atfi.sqrt(atfi.const(self.Tf_plus[ff]) - t0))
            ffterms['a0'+ff] =  cf
            ffterms['a1'+ff] =  cf * zf
    
        #define hadronic amplitudes
        Hadr = {}
        #f0 terms: contains V and S currents
        for a in ['a0f0', 'a1f0']:
            Hadr[( 1, 1,'t', 't', 'V', a)] =  costhlchalf * aV * ffterms[a]
            Hadr[(-1,-1,'t', 't', 'V', a)] =  costhlchalf * aV * ffterms[a]
            Hadr[( 1, 1,'t', 't', 'S', a)] =  costhlchalf * aS * ffterms[a]
            Hadr[(-1,-1,'t', 't', 'S', a)] =  costhlchalf * aS * ffterms[a]
            #Hadr[( 1,-1,'t', 't', 'V', a)] = -sinthlchalf * aV * ffterms[a]
            #Hadr[(-1, 1,'t', 't', 'V', a)] =  sinthlchalf * aV * ffterms[a]
            #Hadr[( 1,-1,'t', 't', 'S', a)] = -sinthlchalf * aS * ffterms[a]
            #Hadr[(-1, 1,'t', 't', 'S', a)] =  sinthlchalf * aS * ffterms[a]
        
        #fplus terms
        for a in ['a0fplus', 'a1fplus']:
            Hadr[( 1, 1,  0, 't', 'V', a)] =  costhlchalf  * bV * ffterms[a]
            Hadr[(-1,-1,  0, 't', 'V', a)] =  costhlchalf  * bV * ffterms[a]
            #Hadr[( 1,-1,  0, 't', 'V', a)] = -sinthlchalf  * bV * ffterms[a]
            #Hadr[(-1, 1,  0, 't', 'V', a)] =  sinthlchalf  * bV * ffterms[a]
        
        #fperp terms
        for a in ['a0fperp', 'a1fperp']:
            Hadr[( 1,-1, -1, 't', 'V', a)] = -costhlchalf  * cV * ffterms[a]
            Hadr[(-1, 1,  1, 't', 'V', a)] = -costhlchalf  * cV * ffterms[a]
            #Hadr[(-1,-1, -1, 't', 'V', a)] = -sinthlchalf  * cV * ffterms[a]
            #Hadr[( 1, 1,  1, 't', 'V', a)] =  sinthlchalf  * cV * ffterms[a]
        
        #g0 terms: contains A and PS currents
        for a in ['a0g0','a1g0']:
            Hadr[( 1, 1,'t', 't', 'A', a)] =  costhlchalf * aA * ffterms[a]
            Hadr[(-1,-1,'t', 't', 'A', a)] = -costhlchalf * aA * ffterms[a]
            Hadr[( 1, 1,'t', 't','PS', a)] = -costhlchalf * aP * ffterms[a]
            Hadr[(-1,-1,'t', 't','PS', a)] =  costhlchalf * aP * ffterms[a]
            #Hadr[( 1,-1,'t', 't', 'A', a)] =  sinthlchalf * aA * ffterms[a]
            #Hadr[(-1, 1,'t', 't', 'A', a)] =  sinthlchalf * aA * ffterms[a]
            #Hadr[( 1,-1,'t', 't','PS', a)] = -sinthlchalf * aP * ffterms[a]
            #Hadr[(-1, 1,'t', 't','PS', a)] = -sinthlchalf * aP * ffterms[a]
        
        #gplus terms
        for a in ['a0gplus','a1gplus']:
            Hadr[( 1, 1,  0, 't', 'A', a)] =  costhlchalf * bA * ffterms[a]
            Hadr[(-1,-1,  0, 't', 'A', a)] = -costhlchalf * bA * ffterms[a]
            #Hadr[( 1,-1,  0, 't', 'A', a)] =  sinthlchalf * bA * ffterms[a]
            #Hadr[(-1, 1,  0, 't', 'A', a)] =  sinthlchalf * bA * ffterms[a]
        
        #gperp terms
        for a in ['a0gperp','a1gperp']:
            Hadr[( 1,-1, -1, 't', 'A', a)] =  costhlchalf * cA * ffterms[a]
            Hadr[(-1, 1,  1, 't', 'A', a)] = -costhlchalf * cA * ffterms[a]
            #Hadr[(-1,-1, -1, 't', 'A', a)] =  sinthlchalf * cA * ffterms[a]
            #Hadr[( 1, 1,  1, 't', 'A', a)] =  sinthlchalf * cA * ffterms[a]
        
        #hplus terms: T and PT
        for a in ['a0hplus','a1hplus']:
            Hadr[( 1, 1,'t',  0, 'T', a)] =  costhlchalf * aT * ffterms[a]
            Hadr[(-1,-1,'t',  0, 'T', a)] =  costhlchalf * aT * ffterms[a]
            Hadr[( 1, 1,  0,'t', 'T', a)] = -costhlchalf * aT * ffterms[a]
            Hadr[(-1,-1,  0,'t', 'T', a)] = -costhlchalf * aT * ffterms[a]
            Hadr[( 1, 1,  1, -1, 'PT',a)] =  costhlchalf * aT * ffterms[a]
            Hadr[(-1,-1,  1, -1, 'PT',a)] =  costhlchalf * aT * ffterms[a]
            Hadr[( 1, 1, -1,  1, 'PT',a)] = -costhlchalf * aT * ffterms[a]
            Hadr[(-1,-1, -1,  1, 'PT',a)] = -costhlchalf * aT * ffterms[a]
            #Hadr[( 1,-1,'t',  0, 'T', a)] = -sinthlchalf * aT * ffterms[a]
            #Hadr[(-1, 1,'t',  0, 'T', a)] =  sinthlchalf * aT * ffterms[a]
            #Hadr[( 1,-1,  0,'t', 'T', a)] =  sinthlchalf * aT * ffterms[a]
            #Hadr[(-1, 1,  0,'t', 'T', a)] = -sinthlchalf * aT * ffterms[a]
            #Hadr[( 1,-1,  1, -1, 'PT',a)] = -sinthlchalf * aT * ffterms[a]
            #Hadr[(-1, 1,  1, -1, 'PT',a)] =  sinthlchalf * aT * ffterms[a]
            #Hadr[( 1,-1, -1,  1, 'PT',a)] =  sinthlchalf * aT * ffterms[a]
            #Hadr[(-1, 1, -1,  1, 'PT',a)] = -sinthlchalf * aT * ffterms[a]
        
        #hperp terms: T and PT
        for a in ['a0hperp','a1hperp']:
            Hadr[(-1, 1,'t',  1, 'T', a)] = -costhlchalf * bT *  ffterms[a]
            Hadr[( 1,-1,'t', -1, 'T', a)] = -costhlchalf * bT *  ffterms[a]
            Hadr[(-1, 1,  1,'t', 'T', a)] =  costhlchalf * bT *  ffterms[a]
            Hadr[( 1,-1, -1,'t', 'T', a)] =  costhlchalf * bT *  ffterms[a]
            Hadr[( 1,-1,  0, -1, 'PT',a)] = -costhlchalf * bT *  ffterms[a]
            Hadr[( 1,-1, -1,  0, 'PT',a)] =  costhlchalf * bT *  ffterms[a]
            Hadr[(-1, 1,  0,  1, 'PT',a)] =  costhlchalf * bT *  ffterms[a]
            Hadr[(-1, 1,  1,  0, 'PT',a)] = -costhlchalf * bT *  ffterms[a]
            #Hadr[(-1,-1,'t', -1, 'T', a)] = -sinthlchalf * bT *  ffterms[a]
            #Hadr[( 1, 1,'t',  1, 'T', a)] =  sinthlchalf * bT *  ffterms[a]
            #Hadr[(-1,-1, -1,'t', 'T', a)] =  sinthlchalf * bT *  ffterms[a]
            #Hadr[( 1, 1,  1,'t', 'T', a)] = -sinthlchalf * bT *  ffterms[a]
            #Hadr[(-1,-1,  0, -1, 'PT',a)] = -sinthlchalf * bT *  ffterms[a]
            #Hadr[( 1, 1,  1,  0, 'PT',a)] =  sinthlchalf * bT *  ffterms[a]
            #Hadr[(-1,-1, -1,  0, 'PT',a)] =  sinthlchalf * bT *  ffterms[a]
            #Hadr[( 1, 1,  0,  1, 'PT',a)] = -sinthlchalf * bT *  ffterms[a]
        
        #htildeperp terms: T and PT
        for a in ['a0htildeperp','a1htildeperp']:
            Hadr[(-1, 1,  0,  1, 'T', a)] = -costhlchalf * cT * ffterms[a]
            Hadr[( 1,-1,  0, -1, 'T', a)] = -costhlchalf * cT * ffterms[a]
            Hadr[(-1, 1,  1,  0, 'T', a)] =  costhlchalf * cT * ffterms[a]
            Hadr[( 1,-1, -1,  0, 'T', a)] =  costhlchalf * cT * ffterms[a]
            Hadr[( 1,-1,'t', -1, 'PT',a)] = -costhlchalf * cT * ffterms[a]
            Hadr[(-1, 1,'t',  1, 'PT',a)] =  costhlchalf * cT * ffterms[a]
            Hadr[( 1,-1, -1,'t', 'PT',a)] =  costhlchalf * cT * ffterms[a]
            Hadr[(-1, 1,  1,'t', 'PT',a)] = -costhlchalf * cT * ffterms[a]
            #Hadr[(-1,-1,  0, -1, 'T', a)] = -sinthlchalf * cT * ffterms[a]
            #Hadr[( 1, 1,  0,  1, 'T', a)] =  sinthlchalf * cT * ffterms[a]
            #Hadr[(-1,-1, -1,  0, 'T', a)] =  sinthlchalf * cT * ffterms[a]
            #Hadr[( 1, 1,  1,  0, 'T', a)] = -sinthlchalf * cT * ffterms[a]
            #Hadr[( 1, 1,'t',  1, 'PT',a)] = -sinthlchalf * cT * ffterms[a]
            #Hadr[(-1,-1,'t', -1, 'PT',a)] = -sinthlchalf * cT * ffterms[a]
            #Hadr[( 1, 1,  1,'t', 'PT',a)] =  sinthlchalf * cT * ffterms[a]
            #Hadr[(-1,-1, -1,'t', 'PT',a)] =  sinthlchalf * cT * ffterms[a]
        
        #htildeplus terms: T and PT
        for a in ['a0htildeplus','a1htildeplus']:
            Hadr[( 1, 1,  1, -1, 'T', a)] = -costhlchalf * dT * ffterms[a]
            Hadr[(-1,-1,  1, -1, 'T', a)] =  costhlchalf * dT * ffterms[a]
            Hadr[( 1, 1, -1,  1, 'T', a)] =  costhlchalf * dT * ffterms[a]
            Hadr[(-1,-1, -1,  1, 'T', a)] = -costhlchalf * dT * ffterms[a]
            Hadr[( 1, 1,'t',  0, 'PT',a)] = -costhlchalf * dT * ffterms[a]
            Hadr[(-1,-1,'t',  0, 'PT',a)] =  costhlchalf * dT * ffterms[a]
            Hadr[( 1, 1,  0,'t', 'PT',a)] =  costhlchalf * dT * ffterms[a]
            Hadr[(-1,-1,  0,'t', 'PT',a)] = -costhlchalf * dT * ffterms[a]
            #Hadr[( 1,-1,  1, -1, 'T', a)] = -sinthlchalf * dT * ffterms[a]
            #Hadr[(-1, 1,  1, -1, 'T', a)] = -sinthlchalf * dT * ffterms[a]
            #Hadr[( 1,-1, -1,  1, 'T', a)] =  sinthlchalf * dT * ffterms[a]
            #Hadr[(-1, 1, -1,  1, 'T', a)] =  sinthlchalf * dT * ffterms[a]
            #Hadr[( 1,-1,'t',  0, 'PT',a)] = -sinthlchalf * dT * ffterms[a]
            #Hadr[(-1, 1,'t',  0, 'PT',a)] = -sinthlchalf * dT * ffterms[a]
            #Hadr[( 1,-1,  0,'t', 'PT',a)] =  sinthlchalf * dT * ffterms[a]
            #Hadr[(-1, 1,  0,'t', 'PT',a)] =  sinthlchalf * dT * ffterms[a]
    
        return Hadr

    def get_w_ampls(self, Obs):
        """
        Some important comments on all the leptonic currents
        V or A current:
            - Terms with WC index V and A are equal.
            - Since only one term appears in the ampl for lwd. We fix lwd to 't' as done in hadronic V, A currents. Index 't' choosen because eta['t'] = 1.
            - Therefore Lept(lw, 't', 'V', ll) has total 8 components out of which 1 is zero.
        S or PS current:
            - Terms with WC index S and PS are equal.
            - In this case both lw and lwd are fake indeces (see above for reason), choosen to be 't' (see above) as done in hadronic S, PS currents.
            - Lept('t', 't', 'S', ll) has total 2 components out of which 1 is zero.
        T or PT current:
            - Terms with WC index T and PT are equal.
            - Lept(lw,lwd,'T',l) has total 32 components out of which 8 are zero.
        Func returns:
            Dict with index as tuples Lept[(lw, lwd, wc, ll)]
        """
        
        #q2 and angular terms
        q2              = Obs['q2'] #real
        cthl            =-Obs['w_ctheta_l'] #real (our pdf is expressed a function of w_ctheta_nu not w_ctheta_l
        phl             = Obs['w_phi_l'] #ZERO Lb polarisation ignored as a result amplitude real
        v               = atfi.sqrt(1. - atfi.pow(atfi.const(self.Mlep),2)/q2) #real
        sinthl          = atfi.sqrt(1. - atfi.pow(cthl,2.)) #real
        expPlusOneIphl  = atfi.cos(phl)    #real and One since Lb polarisation ignored
        expMinusOneIphl = atfi.cos(-phl)   #real and One since Lb polarisation ignored
        expMinusTwoIphl = atfi.cos(-2.*phl)#real and One since Lb polarisation ignored
        OneMinuscthl    = (1. - cthl)
        OnePluscthl     = (1. + cthl)
        al              = 2. * atfi.const(self.Mlep) * v
        bl              = 2. * atfi.sqrt(q2)* v
        complexsqrttwo  = atfi.sqrt(np.array(2.))
        
        Lept = {}
        
        Lept[('t', 't', 'V',  1)] =  expMinusOneIphl * al
        Lept[(  0, 't', 'V',  1)] = -expMinusOneIphl * cthl * al
        Lept[(  1, 't', 'V',  1)] =  expMinusTwoIphl * sinthl * al/complexsqrttwo
        Lept[( -1, 't', 'V',  1)] = -sinthl * al/complexsqrttwo
        Lept[(  0, 't', 'V', -1)] =  sinthl * bl
        Lept[(  1, 't', 'V', -1)] =  expMinusOneIphl * OnePluscthl  * bl/complexsqrttwo
        Lept[( -1, 't', 'V', -1)] =  expPlusOneIphl  * OneMinuscthl * bl/complexsqrttwo
        
        Lept[('t', 't', 'S', 1)]  =  expMinusOneIphl * bl
        
        Lept[('t',  0, 'T', -1)]  =  sinthl * al
        Lept[(  1, -1, 'T', -1)]  =  sinthl * al
        Lept[(  0, -1, 'T', -1)]  =  expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
        Lept[('t', -1, 'T', -1)]  =  expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
        Lept[('t',  1, 'T', -1)]  =  expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
        Lept[(  0,  1, 'T', -1)]  = -expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
        Lept[('t',  0, 'T',  1)]  = -expMinusOneIphl * cthl * bl
        Lept[(  1, -1, 'T',  1)]  = -expMinusOneIphl * cthl * bl
        Lept[('t',  1, 'T',  1)]  =  expMinusTwoIphl * sinthl * bl/complexsqrttwo
        Lept[(  0,  1, 'T',  1)]  = -expMinusTwoIphl * sinthl * bl/complexsqrttwo
        Lept[('t', -1, 'T',  1)]  = -sinthl * bl/complexsqrttwo
        Lept[(  0, -1, 'T',  1)]  = -sinthl * bl/complexsqrttwo
        Lept[(  0,'t', 'T', -1)]  = -sinthl * al
        Lept[( -1,  1, 'T', -1)]  = -sinthl * al
        Lept[( -1,  0, 'T', -1)]  = -expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
        Lept[( -1,'t', 'T', -1)]  = -expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
        Lept[(  1,'t', 'T', -1)]  = -expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
        Lept[(  1,  0, 'T', -1)]  =  expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
        Lept[(  0,'t', 'T',  1)]  =  expMinusOneIphl * cthl * bl
        Lept[( -1,  1, 'T',  1)]  =  expMinusOneIphl * cthl * bl
        Lept[(  1,'t', 'T',  1)]  = -expMinusTwoIphl * sinthl * bl/complexsqrttwo
        Lept[(  1,  0, 'T',  1)]  =  expMinusTwoIphl * sinthl * bl/complexsqrttwo
        Lept[( -1,'t', 'T',  1)]  =  sinthl * bl/complexsqrttwo
        Lept[( -1,  0, 'T',  1)]  =  sinthl * bl/complexsqrttwo
    
        return Lept

    def get_dynamic_term(self, obsv):
        """Get the dynamic density term in Lb->Lclnu decay"""
    
        #define a helpful function
        def _replace_wc(strg):
            """Helpful function when for leptonic amplitudes in definition of density function"""
            strg_wc = strg.replace('A' , 'V')
            strg_wc = strg_wc.replace('PS', 'S')
            strg_wc = strg_wc.replace('PT', 'T')
            return strg_wc
    
        #get Lb and w amplitudes
        free_params   = self.get_freeparams()
        Lbampl = self.get_lb_ampls(obsv)  #LbDecay_lLb_lLc_lw_lwd_WC_FF
        Wampl  = self.get_w_ampls(obsv)   #WDecay_lw_lwd_WC_ll
        #print(len(LbDecay_lLb_lLc_lw_lwd_WC_FF.keys()))    #192 reduces to 92 if Lb is unpolarised
        #print(len(WDecay_lw_lwd_WC_ll.keys()))             #32
        #print(len(fp_wc_ff.keys()))                        #6*20 = 120

        dens = atfi.const(0.)
        for lLb in self.lLbs:
            for lLc in self.lLcs:
                for ll in self.lls:
                    ampls_coherent = atfi.const(0.)
                    for lw in self.lws:
                        for lwd in self.lws:
                            for WC in self.WCs:
                                for FF in self.FFs:
                                    lbindx = (lLb,lLc,lw,lwd,WC,FF)
                                    windx  = (lw,lwd,_replace_wc(WC),ll)
                                    fpindx = (WC, FF)
                                    cond   = lbindx not in Lbampl or windx not in Wampl
                                    if cond: continue
                                    amp  = atfi.sqrt(np.array(2.))*atfi.const(self.GF)*atfi.const(self.Vcb)*atfi.const(self.signWC[WC])*atfi.const(self.eta[lw])*atfi.const(self.eta[lwd])
                                    amp *= Lbampl[lbindx]*Wampl[windx]*free_params[fpindx]
                                    #print(amp)
                                    ampls_coherent+=amp
    
                    dens += atfd.density(ampls_coherent)
    
        return dens

    def get_unbinned_model(self, ph, method= '1', normalisation_sample = False):
        """Define the unbinned model for Lb->Lclnu"""
        #if method == '1' and normalisation_sample:
        #    raise Exception('Method', method, 'picked, but normalisation_sample is set to True. Check!')

        model = None
        if method == '1':
            obsv              = self.prepare_data(ph) 
            dGdO_dynm         = self.get_dynamic_term(obsv)
            dGdO_phsp         = self.get_phasespace_term(obsv)
            model             = dGdO_phsp*dGdO_dynm
        else:
            raise Exception("Method not recognised")

        return model

    def generate_unbinned_data(self, size, seed = None, chunks = 1000000):
        """Generate unbinned data"""
        if seed is not None:
            atfi.set_seed(seed)
            np.random.seed(seed)

        maximum = tft.maximum_estimator(self.get_unbinned_model, self.phase_space, 100000) * 1.5
        print("Maximum = ", maximum)
        sample  = tft.run_toymc(self.get_unbinned_model, self.phase_space, size, maximum, chunk=chunks)
        print('Sample shape', sample.shape)
        return sample

###########  tests
#MLb     = 5619.49997776e-3    #GeV
#MLc     = 2286.45992749e-3    #GeV
#Mlep    = 105.6583712e-3      #GeV Mu
##Mlep   = 1.77686             #GeV Tau
#
#md       = LbToLclNu_Model(MLb, MLc, Mlep)
#phsp_arr = np.array([[0.1, 0.2], 
#                     [1.3, 0.1],
#                     [5.3,-0.1]])
#
#pdfa = md.get_unbinned_model(phsp_arr, method= '1')
#print(pdfa)
#np_pdfa = pdfa.numpy()
#print(np_pdfa/np_pdfa.sum())
#pdfb = md.get_unbinned_model(phsp_arr, method= '2')

#md.get_unbinned_model(phsp_arr, method= '1')
#md.wc_params['CVR'].update(0.5)
#md.get_unbinned_model(phsp_arr, method= '1')

#md.get_weights_wrt_sm(phsp_arr, fname = './weights.npy')

#md.get_normalised_pdf_values(phsp_arr, {'CVR': 1.0})

#making a callable function is better in tf2 compared to md.generate_binned_data which isnt below
#bmd = md.get_binned_model()
#print(bmd())
#md.tot_params['a0fplus'].update(0.6)
#print(bmd())

#np_data_binned = md.generate_binned_data(int(7.5e6), bmd)
#print(np_data_binned)
#md.tot_params['a0fplus'].update(0.6)
#np_data_binned = md.generate_binned_data(int(7.5e6), bmd)
#print(np_data_binned)

#md.randomise_ff_params()
#md.randomise_wc_params()

#md.generate_unbinned_data(100000, store_file = True, fname = './test.root')
#md.import_unbinned_data(fname = './test.root')

#md.generate_binned_data_alternate(1000, 'Scheme0')

#nl = md.gaussian_constraint_ffparams()
#print(nl)
#md.ff_params['a0fplus'].update(0.6)
#nl = md.gaussian_constraint_ffparams()
#print(nl)

#nl = md.binned_nll(np_data_binned, bmd, gauss_constraint = True)
#print(nl(md.tot_params))
#md.tot_params['a0fplus'].update(0.6)
#print(nl(md.tot_params))

#parms = md.tot_params
#oldmd = Old_Model(MLb, MLc, Mlep)
#pdfb  = oldmd._unnormalized_pdf(phsp_arr, parms)
#print(pdfb)
#np_pdfb = pdfb.numpy()
#print(np_pdfb/np_pdfb.sum())
########### 
