import math 
import sys 
import operator 
import itertools 
import numpy as np
import tensorflow as tf 
import pandas as pd 

import amplitf.kinematics as kinematics 
import amplitf.dynamics as dynamics 
from amplitf import interface as atfi

""" Amplitude for K* resonance - 1 decay """

""" Define a complex number function in terms of phase and magnitude """
def complex_phase_mag(r, a):
    #r = magnitude, a = phase
    x = r * tf.cos(a)
    y = r * tf.sin(a)
    return tf.complex(x, y)

""" For decay A to BC - Function to determine the angular momentum of L(BC) """
#use J(A) = J(B) +/- J(C) +/- L(BC)
#where total angular momenta are definened in units of 1/2
def l_bc( ja , jb , jc ):
    l_bc1 = - ( ja/2 - jb/2 - jc/2 )
    l_bc2 = ja/2 - jb/2 + jc/2
    l_bc = np.min( [l_bc1 , l_bc2] )
    return np.max( [l_bc, 0.] )

""" Constants """

#mass in GeV
MLc     = 2.28646    
Mp      = 0.938      
Mk      = 0.497    
Mpi     = 0.140   
M_K_star = tf.constant(0.892, dtype=tf.float64) #GeV  

#angular momenta - units 1/2
Jlam_c = tf.constant(1., dtype=tf.float64)
Jp = tf.constant(1., dtype=tf.float64)
Jk_star = tf.constant(2., dtype=tf.float64)
Jpi = tf.constant(0., dtype=tf.float64)
Jk = tf.constant(0., dtype=tf.float64)
#l_lam_c = 1. # not units of 1/2
#l_k_star = 1.
l_lam_c = l_bc( Jlam_c , Jk_star, Jp )
l_k_star = l_bc( Jk_star, Jk, Jpi )

#helicities 
#units of 1/2
h_p1 = tf.constant(1., dtype=tf.float64)
h_p2 = -tf.constant(1., dtype=tf.float64)
h_k_star1 = tf.constant(0., dtype=tf.float64)
h_k_star2 = tf.constant(2., dtype=tf.float64)
h_k_star3 = -tf.constant(2., dtype=tf.float64)
#finite widths - can be varied to find optimal
d_k_star = tf.constant(1.5, dtype=tf.float64) #GeV
d_Lambda_c = tf.constant(5. , dtype=tf.float64) #GeV
#linewidth K*
T = tf.constant(0.051, dtype=tf.float64) #GeV
#angles 
Cos_theta1_lc = 0.05
Phi1_lc = 1.5

""" Propagator term for the Resonanace """
@atfi.function
def BW_lineshape(m_sq, m_0, T_0, ma, mb, mc, md, d_r, d_d, l_r, l_d):
    return dynamics.breit_wigner_lineshape(m_sq, m_0, T_0, ma, mb, mc, md, d_r, d_d, l_r, l_d, barrier_factor=True, ma0=None, md0=None )

""" Amplitude Resonance = K* """
@atfi.function
def Ampl_k_star( phi_r_lamc, theta_r_lamc, phi_r_res, theta_r_res, J_p, h_p, J_res, h_res, J_lam_c, m_sq, m_r, T_0, ma, mb, mc, md, d_r, d_d, l_r, l_d, r, a , J_lam_c_proj):
    e_lp = ( -1 )**( J_p - h_p )
    A = BW_lineshape( m_sq , m_r , T_0 , ma , mb , mc , md , d_r , d_d , l_r , l_d )
    A *= kinematics.wigner_capital_d( phi_r_lamc , theta_r_lamc , 0 , J_lam_c , J_lam_c_proj , h_res - h_p ) 
    A *= kinematics.wigner_capital_d( phi_r_res , theta_r_res , 0 , J_res , h_res , 0 ) 
    A *= tf.complex(e_lp, tf.constant(0., dtype=tf.float64))
    A *= complex_phase_mag(r, a)
    return A

""" Test Amplitude K*(892)"""
#try m(Kpi) = 800 GeV - need distribution 
#for h_p = +1/2 - h_kstar = 0, 1
#for h_p = -1/2 - h_kstar = 0, -1

def call_ampl(  J_lam_c_proj, h_p , h_res ):
    theta = tf.acos(Cos_theta1_lc)
    r = tf.constant(0.5, dtype=tf.float64)
    a = tf.constant(0., dtype=tf.float64)
    m2 = tf.constant(0.8**2, dtype=tf.float64)
    return Ampl_k_star( phi_r_lamc = Phi1_lc , theta_r_lamc = theta, phi_r_res = Phi1_lc , theta_r_res = theta, J_p = Jp, h_p = h_p, J_res = 2. , h_res = h_res , J_lam_c = 1., m_sq = m2, m_r = M_K_star , T_0 = T , ma = Mk , mb = Mpi, mc = Mp , md = MLc, d_r = d_k_star , d_d = d_Lambda_c, l_r = l_k_star, l_d =l_lam_c, r = r, a = a , J_lam_c_proj = J_lam_c_proj)

def prob_density( J_lam_c_proj, h_p , h_res ):
    return dynamics.density(call_ampl(  J_lam_c_proj, h_p , h_res ))

"""
print( call_ampl( 1., 1. , 2.) )
print( call_ampl( 1., -1. , 2.) )

print( call_ampl( 1., 1. , -2.) )
print( call_ampl( 1., -1. , -2.) )

print( call_ampl( 1., 1. , 0.) )
print( call_ampl( 1., -1. , 0.) )


"""

print( call_ampl( -1., 1. , 2.) )
print( call_ampl( -1., -1. , 2.) )
print( call_ampl( -1., 1. , 0.) )
print( call_ampl( -1., -1. , 0.) )
print( call_ampl( -1., 1. , -2.) )
print( call_ampl( -1., -1. , -2.) )
