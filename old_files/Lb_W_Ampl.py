import sys, os
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # Do not use GPU
sys.path.append(os.path.abspath(os.getcwd()))
from util import LbToLclNu_Model
import numpy as np
import matplotlib.pyplot as plt

MLb     = 5619.49997776e-3    #GeV
MLc     = 2286.45992749e-3    #GeV
Mlep    = 105.6583712e-3      #GeV Mu

md       = LbToLclNu_Model(MLb, MLc, Mlep)
smpl     = md.generate_unbinned_data(50000, seed = 0).numpy()
_ = plt.hist(smpl[:, 0], bins=50)
plt.show()

phsp_arr = np.array([[0.1, 0.2], 
                     [1.3, 0.1],
                     [5.3,-0.1]])

pdfa = md.get_unbinned_model(phsp_arr, method= '1')
print(pdfa)
