#import few packages
import pandas as pd
import sys, os
import tensorflow as tf
from tensorflow.python.ops.array_ops import ones, zeros
from tensorflow.python.ops.gen_array_ops import ones_like, zeros_like
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # Do not use GPU for tensor flow
import numpy as np
import matplotlib.pyplot as plt
import uproot

#from corrected_Lc_decay_3res import LcTopKpi_Model
from old_pdf import LcTopKpi_Model_OLD

#amplitf
home = os.getenv('HOME')
sys.path.append(home+"/Packages/AmpliTF/")
import amplitf.interface as atfi
import amplitf.kinematics as atfk
import amplitf.dynamics as atfd
from amplitf.phasespace.rectangular_phasespace import RectangularPhaseSpace

#tfa2
sys.path.append(home+"/Packages/TFA2/")
import tfa.optimisation as tfo
import tfa.toymc as tft
import tfa.plotting as tfp


########### import files
#file = uproot.open("LcMuNu_gen.root:MCDecayTreeTuple/MCDecayTree;1")

class Correction_to_Lc_decay:
    def __init__(self, MLb, MLc, Mp, Mk, Mpi, file, res):
        """initialise some variables"""
        #Masses of particles involved 
        self.MLb       = MLb
        self.MLc       = MLc
        self.Mp        = Mp
        self.Mk        = Mk
        self.Mpi       = Mpi

        self.file      = file
        self.res       = res
    
    def prepare_data_id(self):
        #conditions on Lc, mu and neutrino from Lb 
        cond = tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122), \
                    tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["nu_mu~_MC_MOTHER_ID"].array())), 5122),\
                                             tf.equal(tf.abs(tf.convert_to_tensor(self.file["muminus_MC_MOTHER_ID"].array())), 5122)) ) 
        if self.res == "Kstar_kpi(892)":
            #Kstar -> Kpi 
            keys_res_p           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())), 313) , cond) )

            keys_res_pi          = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 313),  \
                                   tf.logical_and(cond, \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())), 4122) )))
            keys_res_k           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())), 313),  \
                                   tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122), \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())), 4122) ) ) )

            keys_Lc_p            = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())), 4122), \
                                   tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 313), \
                                   cond) ))
            keys_Lc_k            = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())), 4122), \
                                   cond ))
            keys_Lc_pi           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 4122), \
                                   cond ))

        if self.res == "D(1232)":
            #D -> ppi
            keys_res_p           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())), 2224),  \
                                   tf.logical_and(cond, tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())), 4122) )))
            keys_res_pi          = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 2224),  \
                                   tf.logical_and(cond, tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())), 4122) )))
            keys_res_k           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())), 2224),  \
                                   cond ))

            keys_Lc_p            = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())), 4122), \
                                   cond))
            keys_Lc_pi           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 4122), \
                                   cond))
            keys_Lc_k            = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())), 4122), \
                                   tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 2224), \
                                   cond) ) )
        if self.res == "L(1520)":
            #L -> pK
            keys_res_p           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())), 3124),  \
                                   tf.logical_and(cond, tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 4122) )))
            keys_res_pi          = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 3124), cond))
            keys_res_k           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())), 3124),  \
                                   tf.logical_and(cond, tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 4122) )))
            
            keys_Lc_p            = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())), 4122), cond))
            keys_Lc_pi           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 4122), \
                                   tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())), 3124), cond) ) )
            keys_Lc_k            = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())), 4122), cond))
            
        if self.res == 'phsp':
            keys_res_p           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())) , 4122) , cond ))
            keys_res_pi          = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())) , 4122), cond ))
            keys_res_k           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())) , 4122), cond ))
            keys_Lc_p            = keys_res_p
            keys_Lc_pi           = keys_res_pi
            keys_Lc_k            = keys_res_k

        keys_Lb_Lc           = tf.where( tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122) )

        def concat(x, keys_1, keys_Lc1):
            x1            = tf.gather(x, keys_1[:,-1])
            nans          = np.nan * tf.ones_like( x )
            x_update_1    = tf.tensor_scatter_nd_update( nans, keys_1, x1 )
            empty = np.empty(0)
           
            if np.array_equal(x1.numpy() , empty) == False:
                return x_update_1

            else:
                x3  = tf.gather(x, keys_Lc1[:,-1])
                nans2          = np.nan * tf.ones_like( x )
                x_update_3    = tf.tensor_scatter_nd_update( nans2, keys_Lc1, x3 )
                return x_update_3
        
        particle_lab_info = {}
        
        #p+
        particle_lab_info['p_px_lab']    = concat(tf.convert_to_tensor(self.file["pplus_TRUEP_X"].array()), keys_res_p, keys_Lc_p) / 1000
        particle_lab_info['p_py_lab']    = concat(tf.convert_to_tensor(self.file["pplus_TRUEP_Y"].array()), keys_res_p, keys_Lc_p) / 1000
        particle_lab_info['p_pz_lab']    = concat(tf.convert_to_tensor(self.file["pplus_TRUEP_Z"].array()), keys_res_p, keys_Lc_p) / 1000
        particle_lab_info['p_E_lab']     = concat(tf.convert_to_tensor(self.file["pplus_TRUEP_E"].array()), keys_res_p, keys_Lc_p) / 1000
        #K-
        particle_lab_info['k_px_lab']    = concat(tf.convert_to_tensor(self.file["Kminus_TRUEP_X"].array()), keys_res_k, keys_Lc_k) / 1000
        particle_lab_info['k_py_lab']    = concat(tf.convert_to_tensor(self.file["Kminus_TRUEP_Y"].array()), keys_res_k, keys_Lc_k) / 1000
        particle_lab_info['k_pz_lab']    = concat(tf.convert_to_tensor(self.file["Kminus_TRUEP_Z"].array()), keys_res_k, keys_Lc_k) / 1000
        particle_lab_info['k_E_lab']     = concat(tf.convert_to_tensor(self.file["Kminus_TRUEP_E"].array()), keys_res_k, keys_Lc_k) / 1000
        #pi+
        particle_lab_info['pi_px_lab']   = concat(tf.convert_to_tensor(self.file["piplus_TRUEP_X"].array()), keys_res_pi, keys_Lc_pi) / 1000
        particle_lab_info['pi_py_lab']   = concat(tf.convert_to_tensor(self.file["piplus_TRUEP_Y"].array()), keys_res_pi, keys_Lc_pi) / 1000
        particle_lab_info['pi_pz_lab']   = concat(tf.convert_to_tensor(self.file["piplus_TRUEP_Z"].array()), keys_res_pi, keys_Lc_pi) / 1000
        particle_lab_info['pi_E_lab']    = concat(tf.convert_to_tensor(self.file["piplus_TRUEP_E"].array()), keys_res_pi, keys_Lc_pi) / 1000
        #Lb-0
        particle_lab_info['Lb_px_lab']   = tf.convert_to_tensor(self.file["Lambda_b0_TRUEP_X"].array()) / 1000
        particle_lab_info['Lb_py_lab']   = tf.convert_to_tensor(self.file["Lambda_b0_TRUEP_Y"].array()) / 1000
        particle_lab_info['Lb_pz_lab']   = tf.convert_to_tensor(self.file["Lambda_b0_TRUEP_Z"].array()) / 1000
        particle_lab_info['Lb_E_lab']    = tf.convert_to_tensor(self.file["Lambda_b0_TRUEP_E"].array()) / 1000
        #Lc+
        particle_lab_info['Lc_px_lab']   = concat(tf.convert_to_tensor(self.file["Lambda_cplus_TRUEP_X"].array()), keys_Lb_Lc, keys_Lb_Lc) / 1000
        particle_lab_info['Lc_py_lab']   = concat(tf.convert_to_tensor(self.file["Lambda_cplus_TRUEP_Y"].array()), keys_Lb_Lc, keys_Lb_Lc) / 1000
        particle_lab_info['Lc_pz_lab']   = concat(tf.convert_to_tensor(self.file["Lambda_cplus_TRUEP_Z"].array()), keys_Lb_Lc, keys_Lb_Lc) / 1000
        particle_lab_info['Lc_E_lab']    = concat(tf.convert_to_tensor(self.file["Lambda_cplus_TRUEP_E"].array()), keys_Lb_Lc, keys_Lb_Lc) / 1000
        
        return particle_lab_info


    ########### Make some common functions
    def MakeFourVector(self, pmag, costheta, phi, msq): 
        """Make 4-vec given magnitude, cos(theta), phi and mass"""
        sintheta = atfi.sqrt(1. - costheta**2) #theta 0 to pi => atfi.sin always pos
        px = pmag * sintheta * atfi.cos(phi)
        py = pmag * sintheta * atfi.sin(phi)
        pz = pmag * costheta
        E  = atfi.sqrt(msq + pmag**2)
        return atfk.lorentz_vector(atfk.vector(px, py, pz), E)

    def pvecMag(self, Msq, m1sq, m2sq): 
        """Momentum mag of (1 or 2) in M rest frame"""
        kallen = atfi.sqrt(Msq**2 + m1sq**2 + m2sq**2 - 2.*(Msq*m1sq + Msq*m2sq + m1sq*m2sq))
        const  = tf.cast(2. * atfi.sqrt(Msq), dtype = tf.float64)
        return  kallen / const 
    
    def HelAngles3Body(self, pa, pb, pc):
        """Get three-body helicity angles"""
        theta_r  = atfi.acos(-atfk.z_component(pc) / atfk.norm(atfk.spatial_components(pc)))
        phi_r    = atfi.atan2(-atfk.y_component(pc), -atfk.x_component(pc))
        pa_prime = atfk.rotate_lorentz_vector(pa, -phi_r, -theta_r, phi_r)
        pb_prime = atfk.rotate_lorentz_vector(pb, -phi_r, -theta_r, phi_r)
        pa_prime2= atfk.boost_to_rest(pa_prime, pa_prime+pb_prime)
        theta_a  = atfi.acos(atfk.z_component(pa_prime2) / atfk.norm(atfk.spatial_components(pa_prime2)))
        phi_a    = atfi.atan2(atfk.y_component(pa_prime2), atfk.x_component(pa_prime2))
        return (theta_r, phi_r, theta_a, phi_a)
    
    def RotateAndBoostToRest(self, gm_p4mom_p1, gm_p4mom_p2, gm_phi_m, gm_ctheta_m):
        """
        gm_p4mom_p1: particle p1 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
        gm_p4mom_p2: particle p2 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
        gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
        """
        #First:  Rotate particle p 3mom defined in grand mother gm helicity frame such that z now points along z of mother m in gm rest frame.
        gm_p4mom_p1_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p1, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
        gm_p4mom_p2_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p2, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
        #Second: Boost particle p 4mom to mother m i.e. p1p2 rest frame
        gm_p4mom_m             = gm_p4mom_p1_zmmom+gm_p4mom_p2_zmmom
        m_p4mom_p1             = atfk.boost_to_rest(gm_p4mom_p1_zmmom, gm_p4mom_m)
        m_p4mom_p2             = atfk.boost_to_rest(gm_p4mom_p2_zmmom, gm_p4mom_m)
        return m_p4mom_p1, m_p4mom_p2

    def InvRotateAndBoostFromRest(self, m_p4mom_p1, m_p4mom_p2, gm_phi_m, gm_ctheta_m, gm_p4mom_m):
        """
        Function that gives 4momenta of the particles in the grand mother helicity frame

        m_p4mom_p1: particle p1 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
        m_p4mom_p2: particle p2 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
        gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
        gm_4mom_m: m 4mom in gm's helicity frame
        --------------
        Checks done such as 
            - (lc_p4mom_p+lc_p4mom_k == lc_p4mom_r), 
            - Going in reverse i.e. boost lc_p4mom_p into R rest frame (using lc_p4mom_r) and rotating into R helicity frame gives back r_p4mom_p 
                - i.e. lc_p4mom_p_opp = atfk.rotate_lorentz_vector(atfk.boost_to_rest(lc_p4mom_p, lc_p4mom_r), -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)) where lc_p4mom_p_opp == r_p4mom_p
            - Rotate and boost, instead of boost and rotate should give the same answer, checked (Does not agree with TFA implementation though)
                -i.e. lc_p4mom_prot  = atfk.rotate_lorentz_vector(lc_p4mom_p, -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)
                -i.e. lc_p4mom_krot  = atfk.rotate_lorentz_vector(lc_p4mom_k, -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)
                -i.e. lc_p4mom_p2    = atfk.boost_to_rest(lc_p4mom_prot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_p2 == r_p4mom_p
                -i.e. lc_p4mom_k2    = atfk.boost_to_rest(lc_p4mom_krot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_k2 == r_p4mom_k
        """
        #First: Rotate particle p 3mom defined in mother m helicity frame such that z now points along z in grand mother gm helicity frame 
        #(i.e interpreted as gm mom in it's grand-grand-mother ggm rest frame)
        m_p4mom_p1_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p1, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
        m_p4mom_p2_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p2, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
        #Second: Boost particle p 4mom from mother m's rest frame to gm helicity frame. This is done using boost vector from gm_p4mom_m  [checked: E(m_p4mom_p1_zmmom + m_p4mom_p2_zmmom) == Mass_m]
        gm_p4mom_p1            = atfk.boost_from_rest(m_p4mom_p1_zgmmom, gm_p4mom_m)
        gm_p4mom_p2            = atfk.boost_from_rest(m_p4mom_p2_zgmmom, gm_p4mom_m)
        return gm_p4mom_p1, gm_p4mom_p2
    
    def InvRotateAndBoostFromRest_3particles(self, m_p4mom_p1, m_p4mom_p2, m_p4mom_p3, gm_phi_m, gm_ctheta_m, gm_p4mom_m):
        #First: Rotate particle p 3mom defined in mother m helicity frame such that z now points along z in grand mother gm helicity frame 
        #(i.e interpreted as gm mom in it's grand-grand-mother ggm rest frame)
        m_p4mom_p1_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p1, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
        m_p4mom_p2_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p2, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
        m_p4mom_p3_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p3, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
        #Second: Boost particle p 4mom from mother m's rest frame to gm helicity frame. This is done using boost vector from gm_p4mom_m  [checked: E(m_p4mom_p1_zmmom + m_p4mom_p2_zmmom) == Mass_m]
        gm_p4mom_p1            = atfk.boost_from_rest(m_p4mom_p1_zgmmom, gm_p4mom_m)
        gm_p4mom_p2            = atfk.boost_from_rest(m_p4mom_p2_zgmmom, gm_p4mom_m)
        gm_p4mom_p3            = atfk.boost_from_rest(m_p4mom_p3_zgmmom, gm_p4mom_m)
        return gm_p4mom_p1, gm_p4mom_p2, gm_p4mom_p3

    def RotateAndBoostToRest_3particles(self, gm_p4mom_p1, gm_p4mom_p2, gm_p4mom_p3, gm_phi_m, gm_ctheta_m):
        """
        gm_p4mom_p1: particle p1 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
        gm_p4mom_p2: particle p2 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
        gm_p4mom_p3: particle p3 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
        gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
        """
        #First:  Rotate particle p 3mom defined in grand mother gm helicity frame such that z now points along z of mother m in gm rest frame.
        gm_p4mom_p1_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p1, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
        gm_p4mom_p2_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p2, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
        gm_p4mom_p3_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p3, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)

        #Second: Boost particle p 4mom to mother m i.e. p1p2p3 rest frame
        gm_p4mom_m             = gm_p4mom_p1_zmmom + gm_p4mom_p2_zmmom + gm_p4mom_p3_zmmom 
        m_p4mom_p1             = atfk.boost_to_rest(gm_p4mom_p1_zmmom, gm_p4mom_m)
        m_p4mom_p2             = atfk.boost_to_rest(gm_p4mom_p2_zmmom, gm_p4mom_m)
        m_p4mom_p3             = atfk.boost_to_rest(gm_p4mom_p3_zmmom, gm_p4mom_m)
        return m_p4mom_p1, m_p4mom_p2, m_p4mom_p3 
    
    def RotateAndBoostToRest_Lb(self, p1_p4mom_lab, lamb_p4mom_lab, lamb_phi_lab, lamb_theta_lab):
        """
        p1_p4mom_lab: particle p1 4mom in lab helicity frame (i.e z points along Lb momentum in lab frame).
        lamb_phi_lab, lamb_theta_lab: Angles phi and ctheta of Lb in lab frame (i.e z points along Lb momentum in lab frame).
        """
        #First:  Rotate particle p 3mom defined in lab frame such that z now points along z of Lb in Lab frame.
        #p1_p4mom_lab_zmmom      = atfk.rotate_lorentz_vector(p1_p4mom_lab, -lamb_phi_lab, -lamb_theta_lab, lamb_phi_lab)

        #Second: Boost particle p 4mom to mother Lb
        p1_p4mom_Lb             = atfk.boost_to_rest(p1_p4mom_lab, lamb_p4mom_lab)
  
        return p1_p4mom_Lb

    ########### Make functions to determine the 5 phase space variables for the Lc -> pKpi decay 
    def p4mom(self):
        """p, K. pi 4-momenta in lab, lb and lc rest frames"""
        
        lab_info = self.prepare_data_id()
        #proton 3-mom and energy in lab 
        p_px_lab    = lab_info['p_px_lab']
        p_py_lab    = lab_info['p_py_lab']
        p_pz_lab    = lab_info['p_pz_lab']
        p_E_lab     = lab_info['p_E_lab']
        #kaon 3-mom and energy in lab 
        k_px_lab    = lab_info['k_px_lab']
        k_py_lab    = lab_info['k_py_lab']
        k_pz_lab    = lab_info['k_pz_lab']
        k_E_lab     = lab_info['k_E_lab']
        #pion 3-mom and energy in lab 
        pi_px_lab   = lab_info['pi_px_lab']
        pi_py_lab   = lab_info['pi_py_lab']
        pi_pz_lab   = lab_info['pi_pz_lab']
        pi_E_lab    = lab_info['pi_E_lab']
        #Lambda_c 3-mom and energy in lab 
        Lc_px_lab   = lab_info['Lc_px_lab']
        Lc_py_lab   = lab_info['Lc_py_lab']
        Lc_pz_lab   = lab_info['Lc_pz_lab']
        Lc_E_lab    = lab_info['Lc_E_lab']
        #Lambda_b 3-mom and energy in lab 
        Lb_px_lab   = lab_info['Lb_px_lab']
        Lb_py_lab   = lab_info['Lb_py_lab']
        Lb_pz_lab   = lab_info['Lb_pz_lab']    
        Lb_E_lab    = lab_info['Lb_E_lab']    

        # 4 momenta - LAB FRAME
        p_p4mom_lab     = atfk.lorentz_vector(atfk.vector(p_px_lab, p_py_lab, p_pz_lab   ), p_E_lab)
        k_p4mom_lab     = atfk.lorentz_vector(atfk.vector(k_px_lab, k_py_lab, k_pz_lab   ), k_E_lab)
        pi_p4mom_lab    = atfk.lorentz_vector(atfk.vector(pi_px_lab, pi_py_lab, pi_pz_lab), pi_E_lab)
        Lc_p4mom_lab    = atfk.lorentz_vector(atfk.vector(Lc_px_lab, Lc_py_lab, Lc_pz_lab), Lc_E_lab)
        Lb_p4mom_lab    = atfk.lorentz_vector(atfk.vector(Lb_px_lab, Lb_py_lab, Lb_pz_lab), Lb_E_lab)
        #Lb momenta spherical angles in lab frame 
        Lb_theta_lab    =  atfk.spherical_angles(Lb_p4mom_lab)[0]
        Lb_phi_lab      =  atfk.spherical_angles(Lb_p4mom_lab)[1]
        Lb_ctheta_lab   =  atfi.cos(Lb_theta_lab)
        
        # 4 momenta - Lb FRAME
        p_p4mom_Lb      = self.RotateAndBoostToRest_Lb(p_p4mom_lab , Lb_p4mom_lab, Lb_phi_lab, Lb_theta_lab)
        k_p4mom_Lb      = self.RotateAndBoostToRest_Lb(k_p4mom_lab , Lb_p4mom_lab, Lb_phi_lab, Lb_theta_lab)
        pi_p4mom_Lb     = self.RotateAndBoostToRest_Lb(pi_p4mom_lab, Lb_p4mom_lab, Lb_phi_lab, Lb_theta_lab)
        Lc_p4mom_Lb     = self.RotateAndBoostToRest_Lb(Lc_p4mom_lab, Lb_p4mom_lab, Lb_phi_lab, Lb_theta_lab)
        #Lc momenta spherical angles in Lb frame 
        Lc_theta_Lb     =  atfk.spherical_angles(Lc_p4mom_Lb)[0]
        Lc_phi_Lb       =  atfk.spherical_angles(Lc_p4mom_Lb)[1]
        Lc_ctheta_Lb    =  atfi.cos(Lc_theta_Lb)

        # 4 momenta - Lc FRAME
        p_p4mom_Lc      = self.RotateAndBoostToRest_3particles( p_p4mom_Lb , k_p4mom_Lb, pi_p4mom_Lb, Lc_phi_Lb, Lc_ctheta_Lb )[0]
        k_p4mom_Lc      = self.RotateAndBoostToRest_3particles( p_p4mom_Lb , k_p4mom_Lb, pi_p4mom_Lb, Lc_phi_Lb, Lc_ctheta_Lb )[1]
        pi_p4mom_Lc     = self.RotateAndBoostToRest_3particles( p_p4mom_Lb , k_p4mom_Lb, pi_p4mom_Lb, Lc_phi_Lb, Lc_ctheta_Lb )[2]

        #####Checks
        #p_p4mom_Lc_check = self.RotateAndBoostToRest_Lc(p_p4mom_Lb , Lc_p4mom_Lb, Lc_phi_Lb, Lc_theta_Lb)
        #k_p4mom_Lc       = self.RotateAndBoostToRest_Lc(k_p4mom_Lb , Lc_p4mom_Lb, Lc_phi_Lb, Lc_theta_Lb)
        #pi_p4mom_Lc      = self.RotateAndBoostToRest_Lc(pi_p4mom_Lb, Lc_p4mom_Lb, Lc_phi_Lb, Lc_theta_Lb) 

        #print(p_p4mom_Lc)
        #print(p_p4mom_Lc_check)

        ''' Checks done:
        1. Lb_p4mom_Lb   approx = 0.
        2. Lc_p4mom_Lb     =  p_p4mom_Lb + k_p4mom_Lb + pi_p4mom_Lb 
        -small differeneces, approximately same 
        3. invrotboost = self.InvRotateAndBoostFromRest_3particles(p_p4mom_Lb, pi_p4mom_Lb, k_p4mom_Lb, Lc_phi_Lb, Lc_ctheta_Lb, Lc_p4mom_Lb)
        print(invrotboost_lb[0])
        print(p_p4mom_Lb)
        print(invrotboost_lb[1])
        print(pi_p4mom_Lb)
        print(invrotboost_lb[2])
        print(k_p4mom_Lb)
        returns p_p4mom_Lb, pi_p4mom_Lb, pi_p4mom_Lb
        -not exact but roughly equal 
        '''
        p4mom = {}
        p4mom['p_p4mom_lab']     = p_p4mom_lab
        p4mom['k_p4mom_lab']     = k_p4mom_lab
        p4mom['pi_p4mom_lab']    = pi_p4mom_lab
        p4mom['p_p4mom_Lb']      = p_p4mom_Lb
        p4mom['k_p4mom_Lb']      = k_p4mom_Lb
        p4mom['pi_p4mom_Lb']     = pi_p4mom_Lb

        p4mom['p_p4mom_Lc']      = p_p4mom_Lc
        p4mom['k_p4mom_Lc']      = k_p4mom_Lc
        p4mom['pi_p4mom_Lc']     = pi_p4mom_Lc

        return p4mom
    
    def phsp_vars(self):
        particle_4mom = self.p4mom()
        """5 phase space variables for Lc -> pKpi decay from p, pi, k 4-momenta"""
        p_p4mom_lab       = particle_4mom['p_p4mom_lab']
        k_p4mom_lab       = particle_4mom['k_p4mom_lab']
        pi_p4mom_lab      = particle_4mom['pi_p4mom_lab']
        
        p_p4mom_Lc        = particle_4mom['p_p4mom_Lc']
        k_p4mom_Lc        = particle_4mom['k_p4mom_Lc']
        pi_p4mom_Lc       = particle_4mom['pi_p4mom_Lc']  
 
        #m2_kpi            = (k_p4mom_lab[:,3] + pi_p4mom_lab[:,3])**2 - ( (k_p4mom_lab[:,0]+pi_p4mom_lab[:,0])**2 + (k_p4mom_lab[:,1]+pi_p4mom_lab[:,1])**2 + (k_p4mom_lab[:,2]+pi_p4mom_lab[:,2])**2 )
        #m2_ppi            = (p_p4mom_lab[:,3] + pi_p4mom_lab[:,3])**2 - ( (p_p4mom_lab[:,0]+pi_p4mom_lab[:,0])**2 + (p_p4mom_lab[:,1]+pi_p4mom_lab[:,1])**2 + (p_p4mom_lab[:,2]+pi_p4mom_lab[:,2])**2 )
        #m2_pK             = (p_p4mom_lab[:,3] + k_p4mom_lab[:,3])**2 - ( (p_p4mom_lab[:,0]+k_p4mom_lab[:,0])**2 + (p_p4mom_lab[:,1]+k_p4mom_lab[:,1])**2 + (p_p4mom_lab[:,2]+k_p4mom_lab[:,2])**2 )
        
        m2_kpi            = atfk.mass_squared(pi_p4mom_Lc+k_p4mom_Lc)
        m2_ppi            = atfk.mass_squared(p_p4mom_Lc+pi_p4mom_Lc)
        m2_pK             = atfk.mass_squared(p_p4mom_Lc+k_p4mom_Lc)

        kstar_theta_Lc    =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[0]
        kstar_phi_Lc      =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[1]
        k_theta_kstar     =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[2]
        k_phi_kstar       =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[3]

        # Angle for Delta and Lambda * 
        #Delta resonance angles 
        delta_theta_lc    =  self.HelAngles3Body(p_p4mom_Lc, pi_p4mom_Lc, k_p4mom_Lc)[0]
        delta_phi_lc      =  self.HelAngles3Body(p_p4mom_Lc, pi_p4mom_Lc, k_p4mom_Lc)[1]
        p_theta_delta     =  self.HelAngles3Body(p_p4mom_Lc, pi_p4mom_Lc, k_p4mom_Lc)[2]
        p_phi_delta       =  self.HelAngles3Body(p_p4mom_Lc, pi_p4mom_Lc, k_p4mom_Lc)[3]
        #pi_theta_delta    =  self.HelAngles3Body(pi_p4mom_Lc, p_p4mom_Lc, k_p4mom_Lc)[2]
        #pi_phi_delta      =  self.HelAngles3Body(pi_p4mom_Lc, p_p4mom_Lc, k_p4mom_Lc)[3]

        #Lambda* resonance angles 
        lambda_theta_lc  =  self.HelAngles3Body(p_p4mom_Lc, k_p4mom_Lc, pi_p4mom_Lc)[0]
        lambda_phi_lc    =  self.HelAngles3Body(p_p4mom_Lc, k_p4mom_Lc, pi_p4mom_Lc)[1]
        p_theta_lambda   =  self.HelAngles3Body(p_p4mom_Lc, k_p4mom_Lc, pi_p4mom_Lc)[2]
        p_phi_lambda     =  self.HelAngles3Body(p_p4mom_Lc, k_p4mom_Lc, pi_p4mom_Lc)[3]

        '''Checks:
        1.  Mass:
            m2_pK2        = (p_p4mom_Lc[:,3] + k_p4mom_Lc[:,3])**2 - ( (p_p4mom_Lc[:,0]+k_p4mom_Lc[:,0])**2 + (p_p4mom_Lc[:,1]+k_p4mom_Lc[:,1])**2 + (p_p4mom_Lc[:,2]+k_p4mom_Lc[:,2])**2 )
            m2_kpi        =  self.Mk**2 + self.Mpi**2 + 2 * ( pi_p4mom_lab[:,3]*k_p4mom_lab[:,3] -  (pi_p4mom_lab[:,0]*k_p4mom_lab[:,0] +  pi_p4mom_lab[:,1]*k_p4mom_lab[:,1] + pi_p4mom_lab[:,2]*k_p4mom_lab[:,2] ) )
            m2_ppi        =  self.Mp**2 + self.Mpi**2 + 2 * ( pi_p4mom_lab[:,3]*p_p4mom_lab[:,3] -  (pi_p4mom_lab[:,0]*p_p4mom_lab[:,0] +  pi_p4mom_lab[:,1]*p_p4mom_lab[:,1] + pi_p4mom_lab[:,2]*p_p4mom_lab[:,2] ) )
            m2_pK         =  self.Mk**2 + self.Mp**2 + 2 * ( p_p4mom_lab[:,3]*k_p4mom_lab[:,3] -  (p_p4mom_lab[:,0]*k_p4mom_lab[:,0] +  p_p4mom_lab[:,1]*k_p4mom_lab[:,1] + p_p4mom_lab[:,2]*k_p4mom_lab[:,2] ) )
        2. Angles: 
            k_star_p4mom_Lc   =  k_p4mom_Lc + pi_p4mom_Lc
            kstar_theta_Lc    =  atfk.spherical_angles(k_star_p4mom_Lc)[0]
            kstar_phi_Lc      =  atfk.spherical_angles(k_star_p4mom_Lc)[1]
            kstar_ctheta_Lc   =  atfi.cos(kstar_theta_Lc)

            k_p4mom_kstar     =  self.RotateAndBoostToRest(k_p4mom_Lc, pi_p4mom_Lc, kstar_phi_Lc, kstar_ctheta_Lc)[0]
            #k_p4mom_kstar    =  self.RotateAndBoostToRest_Lc(k_p4mom_Lc, k_star_p4mom_Lc, kstar_phi_Lc, kstar_theta_Lc)
            k_theta_kstar     =  atfk.spherical_angles(k_p4mom_kstar)[0]
            k_phi_kstar       =  atfk.spherical_angles(k_p4mom_kstar)[1]

            kstar_theta_Lc2    =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[0]
            kstar_phi_Lc2      =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[1]
            k_theta_kstar2     =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[2]
            k_phi_kstar2       =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[3]

            print(kstar_theta_Lc)
            print(kstar_theta_Lc2)
            print(kstar_phi_Lc)
            print(kstar_phi_Lc2)
            print(k_theta_kstar)
            print(k_theta_kstar2)
            print(k_phi_kstar)
            print(k_phi_kstar2)

            kstar_theta_Lc2    =  atfk.helicity_angles_3body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[0]
            kstar_phi_Lc2      =  atfk.helicity_angles_3body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[1]
            k_theta_kstar2     =  atfk.helicity_angles_3body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[2]
            k_phi_kstar2       =  atfk.helicity_angles_3body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[3]
        '''

        Vars = {}
        #kstar resonance variables 
        Vars['m2_kpi']                = m2_kpi
        Vars['kstar_phi_lc']          = kstar_phi_Lc
        Vars['kstar_theta_lc']        = kstar_theta_Lc 
        Vars['k_phi_kstar']           = k_phi_kstar 
        Vars['k_theta_kstar']         = k_theta_kstar
        #delta and lambda * resonance variables 
        Vars['m2_ppi']                = m2_ppi
        Vars['m2_pk']                 = m2_pK
        #Delta resonance variables
        Vars['delta_theta_lc']        = delta_theta_lc
        Vars['delta_phi_lc']          = delta_phi_lc
        Vars['p_theta_delta']         = p_theta_delta
        Vars['p_phi_delta']           = p_phi_delta 
        #Vars['pi_theta_delta']        = pi_theta_delta
        #Vars['pi_phi_delta']          = pi_phi_delta 

        #Lambda* resonance variables
        Vars['lambda_theta_lc']       = lambda_theta_lc
        Vars['lambda_phi_lc']         = lambda_phi_lc
        Vars['p_theta_lambda']        = p_theta_lambda
        Vars['p_phi_lambda']          = p_phi_lambda

        return Vars
    
    def concat_phsp_vars(self):
        """Make an array of the 5 phase space variables calculated from the 3-momenta"""
        Variables       = self.phsp_vars()
        m2_kpi          = tf.reshape(Variables['m2_kpi'], [len(Variables['m2_kpi']), 1])
        kstar_phi_lc    = tf.reshape(Variables['kstar_phi_lc'], [len(Variables['kstar_phi_lc']), 1])
        kstar_theta_lc  = tf.reshape(Variables['kstar_theta_lc'], [len(Variables['kstar_theta_lc']), 1])
        k_phi_kstar     = tf.reshape(Variables['k_phi_kstar'], [len(Variables['k_phi_kstar']), 1])
        k_theta_kstar   = tf.reshape(Variables['k_theta_kstar'], [len(Variables['k_theta_kstar']), 1])
        t1              = tf.concat([m2_kpi, kstar_phi_lc], axis = 1)
        t2              = tf.concat([t1, kstar_theta_lc], axis = 1)
        t3              = tf.concat([t2, k_phi_kstar], axis = 1)
        t4              = tf.concat([t3, k_theta_kstar], axis = 1)
        return t4
    
    def remove_nans(self, sample):
        c1   = tf.logical_and(tf.equal(tf.math.is_nan(sample[:,0]), False), tf.equal(tf.math.is_nan(sample[:,1]), False) )
        c2   = tf.logical_and(c1, tf.equal(tf.math.is_nan(sample[:,2]), False))
        c3   = tf.logical_and(c2, tf.equal(tf.math.is_nan(sample[:,3]), False))
        c4   = tf.logical_and(c3, tf.equal(tf.math.is_nan(sample[:,4]), False))
        keys = tf.where(tf.equal(c4, True))

        m2kpi               = tf.gather(sample[:,0], keys[:,-1])
        kstar_phi_Lc        = tf.gather(sample[:,1], keys[:,-1])
        kstar_theta_Lc      = tf.gather(sample[:,2], keys[:,-1])
        k_phi_kstar         = tf.gather(sample[:,3], keys[:,-1])
        k_theta_kstar       = tf.gather(sample[:,4], keys[:,-1])

        m2kpi_1             = tf.reshape(m2kpi, [len(m2kpi), 1])
        kstar_phi_lc_1      = tf.reshape(kstar_phi_Lc, [len(kstar_phi_Lc), 1])
        kstar_theta_lc_1    = tf.reshape(kstar_theta_Lc, [len(kstar_theta_Lc), 1])
        k_phi_kstar_1       = tf.reshape(k_phi_kstar, [len(k_phi_kstar), 1])
        k_theta_kstar_1     = tf.reshape(k_theta_kstar, [len(k_theta_kstar), 1])

        t1                  = tf.concat([m2kpi_1  , kstar_phi_lc_1 ], axis = 1)
        t2                  = tf.concat([t1, kstar_theta_lc_1 ], axis = 1)
        t3                  = tf.concat([t2, k_phi_kstar_1 ], axis = 1)
        t4                  = tf.concat([t3, k_theta_kstar_1 ], axis = 1)
        return t4

    def norm(self, m):
        sum_weights = float( len(m) )
        numerators = np.ones_like( m )
        return( numerators / sum_weights )
    
    def plots(self, data, data2, i):
        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure
        label = [r"$m^2(K\pi)$", r"$\phi^{[\Lambda_c]}_{K*}$", r"$\theta^{[\Lambda_c]}_{K*}$", r"$\phi^{[K*]}_K$", r"$\theta^{[K*]}_K$", r"$m^2(p\pi)$", r"$m^2(pK)$", r"$\theta^{[\Lambda_*]}_{p}$", r"$\theta^{[\Delta]}_{p}$"]
        #label = [r"$m^2(K\pi)$", r"$\theta^{[K*]}_K$"]
       
        tfp.plot_distr1d(
            data2,
            bins= 70,
            range=(np.min(data2), np.max(data2)),
            ax=ax,
            label = label[i],
            units="",
            weights = self.norm(data2),
            color = 'b',
            ) 
        
        tfp.plot_distr1d(
            data,
            bins=70,
            range=(np.min(data), np.max(data)),
            ax=ax,
            label = label[i],
            units="",
            weights = self.norm(data),
            color = 'r',
            ) 
        
        plt.legend(['MC - generated', 'MC - old'], fontsize = 'x-small')
        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
        return plt.show()  
 

def main():
    MLb = 5619.49997776e-3
    MLc = 2.28646
    Mp  = 0.938272046
    Mk  = 0.493677
    Mpi = 0.13957018

    #res_list = ["Kstar_kpi(892)"]
    #res_list = ["D(1232)"]
    res_list = ["L(1520)"]
    #res_list = ["phsp"]

    file_new = uproot.open("LcMuNu_gen.root:MCDecayTreeTuple/MCDecayTree;1")
    cn = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file_new, res_list[0])
    
    #retrieve MC old phsp variables
    sample_wnans        = cn.concat_phsp_vars()
    sample              = cn.remove_nans(sample_wnans)

    #retrieve generated 
    old_model  = LcTopKpi_Model_OLD(MLc, Mp, Mk, Mpi, res_list, float_params = [])
    mc_old     = old_model.MC(10000)
    mc_old2    = old_model.prepare_data(mc_old)
    
    cn.plots(sample[:, 0], mc_old[:, 0], 0)
    cn.plots(sample[:, 1], mc_old[:, 1], 1)
    cn.plots(sample[:, 2], mc_old[:, 2], 2)
    cn.plots(sample[:, 3], mc_old[:, 3], 3)
    cn.plots(sample[:, 4], mc_old[:, 4], 4)

    ''' Plot m2(pK), theta_p_Lam, m2(ppi), theta_p_D '''
    phspvar             = cn.phsp_vars()
    m2pk                = phspvar['m2_pk']
    m2ppi               = phspvar['m2_ppi']
    theta_p_Lam         = phspvar['p_theta_lambda']
    theta_p_D           = phspvar['p_theta_delta']

    keys_m2          = tf.where(tf.equal(tf.math.is_nan(m2ppi), False))
    keys_m3          = tf.where(tf.equal(tf.math.is_nan(m2pk), False))
    keys_theta1      = tf.where(tf.equal(tf.math.is_nan(theta_p_Lam), False))
    keys_theta2      = tf.where(tf.equal(tf.math.is_nan(theta_p_D), False))
    m2ppi_plot       = tf.gather(m2ppi, keys_m2[:,-1])
    m2pk_plot        = tf.gather(m2pk, keys_m3[:,-1])
    theta_p_Lam_plot = tf.gather(theta_p_Lam, keys_theta1[:,-1])
    theta_p_D_plot   = tf.gather(theta_p_D, keys_theta2[:,-1])

    #Plot m2(pK), theta_p_Lam
    cn.plots( m2pk_plot, mc_old2['m2_pK'], 6)
    cn.plots( theta_p_Lam_plot, mc_old2['p_theta_lambda'], 7)

    #Plot m2(ppi), theta_p_D 
    #cn.plots( m2ppi_plot, mc_old2['m2_ppi'], 5)
    #cn.plots( theta_p_D_plot, mc_old2['p_theta_delta'], 8)
  
   
if __name__ == '__main__':
    main()
