#Import things
import tensorflow as tf
import sys, os
home = os.getenv('HOME')
sys.path.append(home+"/Packages/TensorFlowAnalysisnew")
os.environ["CUDA_VISIBLE_DEVICES"] = "1"  # Do not use GPU
from TensorFlowAnalysis import *
import time
start = time.time()
from math import pi
import pprint
#global vars and function
from util import * 

worktype = sys.argv[1]

#Define the rectangular phsp space dq2*dcthlc*dcthl*dphl*dm2pk*dr_ctheta_p*dr_phi_p*dlc_ctheta_pi*dlc_phi_pi
limits        = [(-1., 1.)]
limits       += [(-1., 1.)]
limits       += [(0., np.pi)]
limits       += [(Mlep**2, (MLb - MLc)**2)]
limits       += [(-1., 1.)]
limits       += [(0., np.pi)]
limits       += [((Mp+Mk)**2  , (MLc - Mpi)**2)]
limits       += [(-1., 1.)]
limits       += [( 0., np.pi)]
vol           = 1.
for r in limits: vol *= (r[1] - r[0])
print(limits)
print(vol)
phsp              = RectangularPhaseSpace(ranges=limits)

#Define names of variables and placeholders to get later after importing from stored model
Polname           = 'pol'
FFnames           = FFs 
wcMag_names       = [WC+'_Mag' for WC in WCs[:-1]] #Do not get PT since is the same as T
wcPhs_names       = [WC+'_Phs' for WC in WCs[:-1]] #Do not get PT since is the same as T
resnMag_names     = [resn+'_Mag_'+str(i) for resn in resns for i in range(1,3)] + ["Kstar-kpi_Mag_3", "Kstar-kpi_Mag_4"]
resnPhs_names     = [resn+'_Phs_'+str(i) for resn in resns for i in range(1,3)] + ["Kstar-kpi_Phs_3", "Kstar-kpi_Phs_4"]
flv_name          = 'flv'
swtcPH_names      = [resn+'_switch' for resn in resns]
dataPH_name       = 'data_ph'
PhspOpname        = 'dGdO_phsp'
DynmOpname        = 'dGdO_dynm'

tf.random.set_random_seed(200)
sample            = phsp.UnfilteredSampleGraph(size=100000)

if worktype == 'save':
    print(worktype)

    #Make tf.Variable: resonances, ffparams, wilson coeff, polarisation, Make sure the name is passed into tf.VariableClass
    from freeparam_resns import get_resonances, get_ffparams, get_wilsonparams
    FFact   = get_ffparams()
    Resns   = get_resonances(resns)
    Wilcoef = get_wilsonparams(WCs)
    pol     = FitParameter(Polname,  0., -1., 1., 0.01)  
    flv     = tf.Variable(1., name=flv_name, dtype = tf.float64, trainable=False)

    #Make tf.placeholder: switches, data, flavour
    switches          = get_switches(resns)
    #data_ph           = phsp.Placeholder(name=dataPH_name)
    data_ph           = phsp.UnfilteredSampleGraph(size=1)

    #Build model with 
    obsv              = Prepare_data(data_ph) 
    dGdO_phsp         = tf.multiply(getPhasespaceTerm(obsv), 1., name = PhspOpname)
    dGdO_dynm         = tf.multiply(dGdO_phsp , getDynamicTerm(data_ph, obsv, flv, Wilcoef, Resns, FFact, pol, switches), name = DynmOpname)

    #Make session and initialise all variables and save model
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver()
    #print(sess.run(sample))
    #print(sess.run(dGdO_dynm,{data_ph:sess.run(sample)}))
    saver.save(sess, 'savemodel/my_model')
elif worktype == 'generate':
    print(worktype)

    #load model graph and checkpoint
    sess=tf.Session()
    saver = tf.train.import_meta_graph('savemodel/my_model.meta')
    saver.restore(sess,tf.train.latest_checkpoint('./savemodel/'))
    graph = tf.get_default_graph()

    #get variables
    flv       = [v for v in tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES) if v.name == flv_name+':0'][0]
    wcMags    = [v for v in tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES) for wcMag_name in wcMag_names if v.name == wcMag_name+':0']
    wcPhss    = [v for v in tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES) for wcPhs_name in wcPhs_names if v.name == wcPhs_name+':0']
    resnMags  = [v for v in tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES) for resnMag_name in resnMag_names if v.name == resnMag_name+':0']
    resnPhss  = [v for v in tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES) for resnPhs_name in resnPhs_names if v.name == resnPhs_name+':0']

    #store freeparam in a dict
    freeparamdict = {}
    for wcMag,wcPhs in zip(wcMags, wcPhss): 
        freeparamdict[wcMag.name[:-2]] = wcMag
        freeparamdict[wcPhs.name[:-2]] = wcPhs
        for resnMag,resnPhs in zip(resnMags, resnPhss): 
            freeparamdict[resnMag.name[:-2]] = resnMag
            freeparamdict[resnPhs.name[:-2]] = resnPhs

    #set every resonance coupling to zero
    for k in list(freeparamdict.keys()):
        #if 'L1670-pk_Mag' in k or 'L1520-pk_Mag' in k or 'Kstar-kpi_Mag' in k: sess.run(freeparamdict[k].assign(0.))
        if 'L1670-pk' in k or 'L1520-pk' in k or 'Kstar-kpi' in k: sess.run(freeparamdict[k].assign(0.))
    #print(sess.run(freeparamdict))
    pprint.pprint(sess.run(freeparamdict))

    #get placeholder
    sws       = [graph.get_tensor_by_name(swtcPH_name+':0') for swtcPH_name in swtcPH_names] #Default = 1
    data_ph   = graph.get_tensor_by_name(dataPH_name+':0')
    dGdO_dynm = graph.get_tensor_by_name(DynmOpname+':0')

    smpl        = sess.run(sample)
    obsv        = Prepare_data(data_ph) 

    #activate two of them
    sess.run(freeparamdict['Kstar-kpi_Mag_1'].assign(1.0))
    sess.run(freeparamdict['Kstar-kpi_Phs_1'].assign(0.0))
    sess.run(freeparamdict['V_Mag'].assign(1.03))
    sess.run(freeparamdict['V_Phs'].assign(np.pi/4.))
    sess.run(freeparamdict['A_Mag'].assign(0.))
    sess.run(freeparamdict['A_Phs'].assign(0.))
    #sess.run(freeparamdict['Kstar-kpi_Mag_1'].assign(1.0))
    #sess.run(freeparamdict['Kstar-kpi_Phs_1'].assign(0.))
    #sess.run(freeparamdict['Kstar-kpi_Mag_2'].assign(1.5))
    #sess.run(freeparamdict['Kstar-kpi_Phs_2'].assign(np.pi/2.))
    #sess.run(freeparamdict['Kstar-kpi_Mag_3'].assign(1.4))
    #sess.run(freeparamdict['Kstar-kpi_Phs_3'].assign(-np.pi/4.))
    #sess.run(freeparamdict['Kstar-kpi_Mag_4'].assign(2.2))
    #sess.run(freeparamdict['Kstar-kpi_Phs_4'].assign(np.pi/6.))
    #sess.run(freeparamdict['V_Mag'].assign(1.03))
    #sess.run(freeparamdict['V_Phs'].assign( np.pi/3))
    #sess.run(freeparamdict['A_Mag'].assign(0.97))
    #sess.run(freeparamdict['A_Phs'].assign(-2*np.pi/8))
    #print(sess.run(freeparamdict))
    pprint.pprint(sess.run(freeparamdict))
    genmodel(sess, dGdO_dynm, data_ph, smpl, phsp, sws, 1234, resns, 'toy_Lb.root', 10000, obsv)

    sess.run(flv.assign(-1.))
    #print(sess.run(freeparamdict))
    pprint.pprint(sess.run(freeparamdict))
    genmodel(sess, dGdO_dynm, data_ph, smpl, phsp, sws, 1234, resns, 'toy_Lbbar.root', 10000, obsv)

##Calculate integral to normalize PDF: NB this is for a fixed free parameter value (remove if fitting)
#fixparam_par_intg_val = vol * FixParam_Integral_val(model_intg=Integral(dGdO), mcsize=2000000, niter=50, x=data_ph, sess=sess, phsp=phsp)
#print(fixparam_par_intg_val)
#mypdf         = 1./fixparam_par_intg_val * dGdO
#pdfunirom_val = 1./vol
#weights_model = mypdf/pdfunirom_val
#
#unifrm_sampl      = sess.run(phsp.UnfilteredSampleGraph(size=1000000))
#weights_model_val = sess.run(weights_model, feed_dict={data_ph:unifrm_sampl}) 
#wdict             = Res_weights(wgts=weights_model, x=data_ph, sampl=unifrm_sampl, v=weights_model_val)
#
#df                = pd.DataFrame.from_dict(sess.run(obsv, feed_dict={data_ph:unifrm_sampl}))
#df['w_tot']       = weights_model_val
#for res in resns: df['w_'+res.replace('-','_')] = wdict[res]
#print(df)
#exit(0)
#to_root(df, 'out.root', key='mytree')
#
#data          = sess.run(phsp.UnfilteredSampleGraph(size=1000000))
#print(sess.run(dens, feed_dict={data_ph:data}))
#fit_sample  = RunToyMC(sess, dGdO, data_ph, phsp, 100000, majorant, chunk = 1000000, switches = sws , seed = 200, interferences = False)
#df          = pd.DataFrame(fit_sample, columns=['lb_ctheta_lc','lc_ctheta_pi','lc_phi_pi','q2','w_ctheta_l','w_phi_l','m2pk','r_ctheta_p','r_phi_p', 'kstw', 'Lstw'])

end = time.time()
print("Time taken: ", (end - start)/60.)
