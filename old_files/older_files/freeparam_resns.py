import tensorflow as tf
import sys, os
home = os.getenv('HOME')
sys.path.append(home+"/Packages/TensorFlowAnalysisnew")
from TensorFlowAnalysis import *
pi = np.pi

def get_wilsonparams(wcs):
    Wilcoef = {}
    Wilcoef['V' ]  = [FitParameter("V_Mag" , 1.0, 0., 20., 0.01), FitParameter("V_Phs" , 0., -pi, pi, 0.01)]
    Wilcoef['A' ]  = [FitParameter("A_Mag" , 0.0, 0., 20., 0.01), FitParameter("A_Phs" , 0., -pi, pi, 0.01)]
    Wilcoef['S' ]  = [FitParameter("S_Mag" , 0.0, 0., 20., 0.01), FitParameter("S_Phs" , 0., -pi, pi, 0.01)]
    Wilcoef['PS']  = [FitParameter("PS_Mag", 0.0, 0., 20., 0.01), FitParameter("PS_Phs", 0., -pi, pi, 0.01)]
    Wilcoef['T' ]  = [FitParameter("T_Mag" , 0.0, 0., 20., 0.01), FitParameter("T_Phs" , 0., -pi, pi, 0.01)]
    Wilcoef['PT']  = Wilcoef['T']
    return Wilcoef

def get_ffparams():
    FFact = {}
    f     = open("FF_cov/LambdabLambdac_results.dat", "r")
    for l in f.readlines(): 
        name        = l.split()[0]
        val         = float(l.split()[1])
        lowval      = val - 0.5 * abs(val)
        highval     = val + 0.5 * abs(val)
        FFact[name] = FitParameter(name, val, lowval, highval, 0.01)

    f.close()
    return FFact

def get_resonances(resns):
    resonances = {}
    for res in resns:
        if res == "Kstar-kpi":
            resonances["Kstar-kpi"] = {
                                        "lineshape" : BreitWignerLineShape, 
                                        "mass"      : Const(0.892), 
                                        "width"     : Const(0.051), 
                                        "spin"      :  2, 
                                        "parity"    : -1, 
                                        "coupl"     : [              
                                                        FitParameter(res+"_Mag_1", 1.0    ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_1", 0.     ,-pi, pi, 0.01), 
                                                        FitParameter(res+"_Mag_2", 0.     ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_2", 0.     ,-pi, pi, 0.01), 
                                                        FitParameter(res+"_Mag_3", 0.     ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_3", 0.     ,-pi, pi, 0.01), 
                                                        FitParameter(res+"_Mag_4", 0.     ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_4", 0.     ,-pi, pi, 0.01), 
                                                        #FitParameter(res+"_Mag_1", 1.0    ,   0., 20.  , 0.01), 
                                                        #FitParameter(res+"_Phs_1", 0.     ,-pi, pi, 0.01), 
                                                        #FitParameter(res+"_Mag_2", 2.0    ,   0., 20.  , 0.01), 
                                                        #FitParameter(res+"_Phs_2", pi/3.,-pi, pi, 0.01), 
                                                        #FitParameter(res+"_Mag_3", 4.0    ,   0., 20.  , 0.01), 
                                                        #FitParameter(res+"_Phs_3",-pi/6.,-pi, pi, 0.01), 
                                                        #FitParameter(res+"_Mag_4", 1.2    ,   0., 20.  , 0.01), 
                                                        #FitParameter(res+"_Phs_4",-pi/4.,-pi, pi, 0.01), 
                                                      ]                                
                                       }
        elif res == "L1405-pk":
            resonances["L1405-pk"] = {
                                        "lineshape" : SubThresholdBreitWignerLineShape, 
                                        "mass"      : Const(1.405), 
                                        "width"     : Const(0.050), 
                                        "spin"      :  1, 
                                        "parity"    : -1, 
                                        "coupl"     : [
                                                        FitParameter(res+"_Mag_1", 1.0    ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_1", pi/7.,-pi, pi, 0.01), 
                                                        FitParameter(res+"_Mag_2", 2.0    ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_2", pi/3.,-pi, pi, 0.01), 
                                                      ]
                                      }
            
        elif res == "L1520-pk":
            resonances["L1520-pk"] = {
                                        "lineshape" : BreitWignerLineShape, 
                                        "mass"      : Const(1.5195), 
                                        "width"     : Const(0.0156), 
                                        "spin"      :  3, 
                                        "parity"    : -1, 
                                        "coupl"     : [
                                                        FitParameter(res+"_Mag_1", 1.0    ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_1", pi/7.,-pi, pi, 0.01), 
                                                        FitParameter(res+"_Mag_2", 2.0    ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_2", pi/3.,-pi, pi, 0.01), 
                                                      ]
                                      }

        elif res == "L1670-pk":
            resonances["L1670-pk"] = {
                                        "lineshape" : BreitWignerLineShape, 
                                        "mass"      : Const(1.670), 
                                        "width"     : Const(0.035), 
                                        "spin"      :  1, 
                                        "parity"    : -1, 
                                        "coupl"     : [
                                                        FitParameter(res+"_Mag_1", 1.0    ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_1", pi/7.,-pi, pi, 0.01), 
                                                        FitParameter(res+"_Mag_2", 2.0    ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_2", pi/3.,-pi, pi, 0.01), 
                                                      ]
                                      }
            
        elif res == "L1690-pk":
            resonances["L1690-pk"] = {
                                        "lineshape" : BreitWignerLineShape, 
                                        "mass"      : Const(1.690), 
                                        "width"     : Const(0.060), 
                                        "spin"      :  3, 
                                        "parity"    : -1, 
                                        "coupl"     : [
                                                        FitParameter(res+"_Mag_1", 1.0    ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_1", pi/7.,-pi, pi, 0.01), 
                                                        FitParameter(res+"_Mag_2", 2.0    ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_2", pi/3.,-pi, pi, 0.01), 
                                                      ]
                                      }
            
        elif res == "D1232-ppi":
            resonances["D1232-ppi"] = {
                                        "lineshape" : BreitWignerLineShape, 
                                        "mass"      : Const(1.232), 
                                        "width"     : Const(0.100), 
                                        "spin"      :  3, 
                                        "parity"    :  1, 
                                        "coupl"     : [
                                                        FitParameter(res+"_Mag_1", 1.0    ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_1", pi/7.,-pi, pi, 0.01), 
                                                        FitParameter(res+"_Mag_2", 2.0    ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_2", pi/3.,-pi, pi, 0.01), 
                                                      ]
                                      }
            
        elif res == "D1600-ppi":
            resonances["D1600-ppi"] = {
                                        "lineshape" : BreitWignerLineShape, 
                                        "mass"      : Const(1.600), 
                                        "width"     : Const(0.275), 
                                        "spin"      :  3, 
                                        "parity"    :  1, 
                                        "coupl"     : [
                                                        FitParameter(res+"_Mag_1", 1.0    ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_1", pi/7.,-pi, pi, 0.01), 
                                                        FitParameter(res+"_Mag_2", 2.0    ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_2", pi/3.,-pi, pi, 0.01), 
                                                      ]
                                      }
            
        elif res == "D1620-ppi":
            resonances["D1620-ppi"] = {
                                        "lineshape" : BreitWignerLineShape, 
                                        "mass"      : Const(1.620), 
                                        "width"     : Const(0.130), 
                                        "spin"      :  1, 
                                        "parity"    : -1, 
                                        "coupl"     : [
                                                        FitParameter(res+"_Mag_1", 1.0    ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_1", pi/7.,-pi, pi, 0.01), 
                                                        FitParameter(res+"_Mag_2", 2.0    ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_2", pi/3.,-pi, pi, 0.01), 
                                                      ]
                                      }
        
        elif res == "D1700-ppi":
            resonances["D1700-ppi"] = {
                                        "lineshape" : BreitWignerLineShape, 
                                        "mass"      : Const(1.650), 
                                        "width"     : Const(0.230), 
                                        "spin"      : 3, 
                                        "parity"    : -1, 
                                        "couplings" : [
                                                        FitParameter(res+"_Mag_1", 1.0    ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_1", pi/7.,-pi, pi, 0.01), 
                                                        FitParameter(res+"_Mag_2", 2.0    ,   0., 20.  , 0.01), 
                                                        FitParameter(res+"_Phs_2", pi/3.,-pi, pi, 0.01), 
                                                      ]
                                      }
    return resonances
