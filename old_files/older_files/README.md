# CodeCPVLbToLclnu
In presense of new physics, the full 9D phase space of for $$\Lambda_b \rightarrow \Labmda_c (\rightarrow) l \nu$$ and its CP conjugate.
Based on TensorFlowAnalysis package (NB: Before running the following needs to done to TFA master branch, for getting variables from saved graph).
```python
-        VariableClass.__init__(self, init_value, dtype=FPType())
+        VariableClass.__init__(self, init_value, dtype=FPType(), name = name)
```

# How to save or generate the model

To save

```python
python LbToLclnu_CPV.py save
```

To generate

```python
python LbToLclnu_CPV.py generate
```

To-do:

- [x] Save the model graph instead of building all the time. 
- Each amplitude is indexed by contributing particle helicities which are implemented as dictionaries. Speed gains from replacing these amplitudes with tf.Variable tensors? Can benefit from tf.einsum but amplitudes corresponding to most of the indices are zero. Guess have to just try it out sometime.
- Cache all the integrals independent of form-factor parameters, wilson coefficients and helicity couplings. This can be done due to the linear structure of the total amplitude with regard to these parameters. If we move into fitting stage and if masses and widths are floated then trouble town!!. For this point, the 2nd point to make this happen is very relevant.
-
