def dPhitwo(Msq, m1sq, m2sq):
    pvec = pvecMag(Msq, m1sq, m2sq)
    1./(tf.pow(Const(2.),4)*tf.pow(Pi(),2)) * pvec/Sqrt(Msq)


#angularterms
costhlchalf = Sqrt((1+cthlc)/2.)
sinthlchalf = Sqrt((1-cthlc)/2.)

lLbs = [1, -1]
lLcs = [1, -1]
lls  = [1, -1]
lws  = ['t', 0, 1, -1]

HV = {}; HA = {}; HS = {}; HPS= {}; HT = {}; HPT= {}
#initialise
for lLb in lLbs:
    for lLc in lLcs:
        HS[(lLb, lLc)]  = CastComplex(0.)
        HPS[(lLb, lLc)] = CastComplex(0.)
        for lw in lws:
            HV[(lLb, lLc, lw)] = CastComplex(0.)
            HA[(lLb, lLc, lw)] = CastComplex(0.)
            for lwd in lws:
                HT[(lLb, lLc, lw, lwd)] = CastComplex(0.)
                HPT[(lLb,lLc, lw, lwd)] = CastComplex(0.)

Hadr = {}
Hadr[('SM', lLb, lLc, lw)]
Hadr[('VL', lLb, lLc, lw)]
Hadr[('VR', lLb, lLc, lw)]
Hadr[('SL', lLb, lLc)]
Hadr[('SR', lLb, lLc)]
Hadr[('TL', lLb, lLc, lw, lwd)] = Ctl * (HT[(lLb, lLc, lw, lwd)] - HPT[(lLb, lLc, lw, lwd)])

#Below -cthlc are all sin terms (NB: Bosth sin(x/2) and cos(x/2) lie in 1st quad as x/2 is b/w 0 to pi/2. Hence always pos)
#lb, lc, lw : 4 comb are zero
HV[( 1, 1,'t')] =  costhlchalf  * aV
HV[(-1,-1,'t')] =  costhlchalf  * aV
HV[( 1,-1,'t')] = -sinthlchalf  * aV
HV[(-1, 1,'t')] =  sinthlchalf  * aV
HV[( 1, 1,  0)] =  costhlchalf  * bV
HV[(-1,-1,  0)] =  costhlchalf  * bV
HV[( 1,-1,  0)] = -sinthlchalf  * bV
HV[(-1, 1,  0)] =  sinthlchalf  * bV
HV[( 1,-1, -1)] = -costhlchalf  * cV
HV[(-1, 1,  1)] = -costhlchalf  * cV
HV[(-1,-1, -1)] = -sinthlchalf  * cV
HV[( 1, 1,  1)] =  sinthlchalf  * cV
#HA: lb, lc, lw: 4 comb are zero
HA[( 1, 1,'t')] =  costhlchalf * aA
HA[(-1,-1,'t')] = -costhlchalf * aA
HA[( 1,-1,'t')] =  sinthlchalf * aA
HA[(-1, 1,'t')] =  sinthlchalf * aA
HA[( 1, 1,  0)] =  costhlchalf * bA
HA[(-1,-1,  0)] = -costhlchalf * bA
HA[( 1,-1,  0)] =  sinthlchalf * bA
HA[(-1, 1,  0)] =  sinthlchalf * bA
HA[( 1,-1, -1)] =  costhlchalf * cA
HA[(-1, 1,  1)] = -costhlchalf * cA
HA[(-1,-1, -1)] =  sinthlchalf * cA
HA[( 1, 1,  1)] =  sinthlchalf * cA
#HS: lb, lc
HS[( 1, 1)]     =  costhlchalf * aS
HS[(-1,-1)]     =  costhlchalf * aS
HS[( 1,-1)]     = -sinthlchalf * aS
HS[(-1, 1)]     =  sinthlchalf * aS
#HPS: lb, lc
HPS[( 1, 1)]    = -costhlchalf  * aP
HPS[(-1,-1)]    =  costhlchalf  * aP
HPS[( 1,-1)]    = -sinthlchalf  * aP
HPS[(-1, 1)]    = -sinthlchalf  * aP
#HT: lb, lc, lw1, lw2: 32 comb are zero
HT[( 1, 1,'t',  0)] =  costhlchalf * aT
HT[(-1,-1,'t',  0)] =  costhlchalf * aT
HT[( 1,-1,'t',  0)] = -sinthlchalf * aT
HT[(-1, 1,'t',  0)] =  sinthlchalf * aT
HT[( 1, 1,  0,'t')] = -costhlchalf * aT
HT[(-1,-1,  0,'t')] = -costhlchalf * aT
HT[( 1,-1,  0,'t')] =  sinthlchalf * aT
HT[(-1, 1,  0,'t')] = -sinthlchalf * aT
HT[(-1, 1,'t',  1)] = -costhlchalf * bT
HT[( 1,-1,'t', -1)] = -costhlchalf * bT
HT[(-1,-1,'t', -1)] = -sinthlchalf * bT
HT[( 1, 1,'t',  1)] =  sinthlchalf * bT
HT[(-1, 1,  1,'t')] =  costhlchalf * bT
HT[( 1,-1, -1,'t')] =  costhlchalf * bT
HT[(-1,-1, -1,'t')] =  sinthlchalf * bT
HT[( 1, 1,  1,'t')] = -sinthlchalf * bT
HT[(-1,-1,  0, -1)] = -sinthlchalf * cT
HT[( 1, 1,  0,  1)] =  sinthlchalf * cT
HT[(-1, 1,  0,  1)] = -costhlchalf * cT
HT[( 1,-1,  0, -1)] = -costhlchalf * cT
HT[(-1,-1, -1,  0)] =  sinthlchalf * cT
HT[( 1, 1,  1,  0)] = -sinthlchalf * cT
HT[(-1, 1,  1,  0)] =  costhlchalf * cT
HT[( 1,-1, -1,  0)] =  costhlchalf * cT
HT[( 1,-1,  1, -1)] = -sinthlchalf * dT
HT[(-1, 1,  1, -1)] = -sinthlchalf * dT
HT[( 1, 1,  1, -1)] = -costhlchalf * dT
HT[(-1,-1,  1, -1)] =  costhlchalf * dT
HT[( 1,-1, -1,  1)] =  sinthlchalf * dT
HT[(-1, 1, -1,  1)] =  sinthlchalf * dT
HT[( 1, 1, -1,  1)] =  costhlchalf * dT
HT[(-1,-1, -1,  1)] = -costhlchalf * dT
#HPT: lb, lc, lw1, lw2: 32 comb are zero
HPT[( 1, 1,  1, -1)] =  costhlchalf * aT
HPT[(-1,-1,  1, -1)] =  costhlchalf * aT
HPT[( 1,-1,  1, -1)] = -sinthlchalf * aT
HPT[(-1, 1,  1, -1)] =  sinthlchalf * aT
HPT[( 1, 1, -1,  1)] = -costhlchalf * aT
HPT[(-1,-1, -1,  1)] = -costhlchalf * aT
HPT[( 1,-1, -1,  1)] =  sinthlchalf * aT
HPT[(-1, 1, -1,  1)] = -sinthlchalf * aT
HPT[( 1,-1,  0, -1)] = -costhlchalf * bT
HPT[(-1,-1,  0, -1)] = -sinthlchalf * bT
HPT[( 1,-1, -1,  0)] =  costhlchalf * bT
HPT[(-1,-1, -1,  0)] =  sinthlchalf * bT
HPT[( 1, 1,  0,  1)] = -sinthlchalf * bT
HPT[(-1, 1,  0,  1)] =  costhlchalf * bT
HPT[( 1, 1,  1,  0)] =  sinthlchalf * bT
HPT[(-1, 1,  1,  0)] = -costhlchalf * bT
HPT[( 1, 1,'t',  1)] = -sinthlchalf * cT
HPT[(-1,-1,'t', -1)] = -sinthlchalf * cT
HPT[( 1,-1,'t', -1)] = -costhlchalf * cT
HPT[(-1, 1,'t',  1)] =  costhlchalf * cT
HPT[( 1, 1,  1,'t')] =  sinthlchalf * cT
HPT[(-1,-1, -1,'t')] =  sinthlchalf * cT
HPT[( 1,-1, -1,'t')] =  costhlchalf * cT
HPT[(-1, 1,  1,'t')] = -costhlchalf * cT
HPT[( 1,-1,'t',  0)] = -sinthlchalf * dT
HPT[(-1, 1,'t',  0)] = -sinthlchalf * dT
HPT[( 1, 1,'t',  0)] = -costhlchalf * dT
HPT[(-1,-1,'t',  0)] =  costhlchalf * dT
HPT[( 1,-1,  0,'t')] =  sinthlchalf * dT
HPT[(-1, 1,  0,'t')] =  sinthlchalf * dT
HPT[( 1, 1,  0,'t')] =  costhlchalf * dT
HPT[(-1,-1,  0,'t')] = -costhlchalf * dT


#angular terms
sinthl          = Sqrt(1-tf.pow(cthl,2.))
expPlusOneIphl  = Exp( 1j*phl)
expMinusOneIphl = Exp(-1j*phl)
expMinusTwoIphl = Exp(-2.*1j*phl)
sqrttwo         = Sqrt(2.)
OneMinuscthl    = (1. - cthl)
OnePluscthl     = (1. + cthl)

LeptSM = {}; LeptSL = {}; LeptTL = {}
#Initialise
for ll in lls:
    LeptSL[(ll,)] = CastComplex(0.)
    for lw in lws:
        LeptSM[(lw, ll)] = CastComplex(0.)
        for lwd in lws:
            LeptTL[(lw, lwd, ll)] = CastComplex(0.)

#(*SM=VL=VR*)(*index helicity: (w,l)*): 1 zero comp
LeptSM[('t',  1)]      =  expMinusOneIphl * al
LeptSM[(  0,  1)]      = -expMinusOneIphl * cthl * al
LeptSM[(  1,  1)]      =  expMinusTwoIphl * sinthl * al/sqrttwo
LeptSM[( -1,  1)]      = -sinthl * al/sqrttwo
LeptSM[(  0, -1)]      =  sinthl * bl
LeptSM[(  1, -1)]      =  expMinusOneIphl * OnePluscthl  * bl/sqrttwo, 
LeptSM[( -1, -1)]      =  expPlusOneIphl  * OneMinuscthl * bl/sqrttwo,
#(*SL=SR*) (*index helicity: (l)*): 1 zero comp
LeptSL[(1,)]           =  expMinusOneIphl * bl
#(*TL=T=PT*) (*index helicity: (w1,w2,l)*):  8 zeros comps 
LeptTL[('t',  0, -1)]  =  sinthl * al
LeptTL[(  1, -1, -1)]  =  sinthl * al
LeptTL[(  0, -1, -1)]  =  expPlusOneIphl  * OneMinuscthl * al/sqrttwo
LeptTL[('t', -1, -1)]  =  expPlusOneIphl  * OneMinuscthl * al/sqrttwo
LeptTL[('t',  1, -1)]  =  expMinusOneIphl * OnePluscthl  * al/sqrttwo
LeptTL[(  0,  1, -1)]  = -expMinusOneIphl * OnePluscthl  * al/sqrttwo
LeptTL[('t',  0,  1)]  = -expMinusOneIphl * cthl * bl
LeptTL[(  1, -1,  1)]  = -expMinusOneIphl * cthl * bl
LeptTL[('t',  1,  1)]  =  expMinusTwoIphl * sinthl * bl/sqrttwo,
LeptTL[(  0,  1,  1)]  = -expMinusTwoIphl * sinthl * bl/sqrttwo,
LeptTL[('t', -1,  1)]  = -sinthl * bl/sqrttwo
LeptTL[(  0, -1,  1)]  = -sinthl * bl/sqrttwo
LeptTL[(  0,'t', -1)]  = -sinthl * al
LeptTL[( -1,  1, -1)]  = -sinthl * al
LeptTL[( -1,  0, -1)]  = -expPlusOneIphl  * OneMinuscthl * al/sqrttwo
LeptTL[( -1,'t', -1)]  = -expPlusOneIphl  * OneMinuscthl * al/sqrttwo
LeptTL[(  1,'t', -1)]  = -expMinusOneIphl * OnePluscthl  * al/sqrttwo
LeptTL[(  1,  0, -1)]  =  expMinusOneIphl * OnePluscthl  * al/sqrttwo
LeptTL[(  0,'t',  1)]  =  expMinusOneIphl * cthl * bl
LeptTL[( -1,  1,  1)]  =  expMinusOneIphl * cthl * bl
LeptTL[(  1,'t',  1)]  = -expMinusTwoIphl * sinthl * bl/sqrttwo
LeptTL[(  1,  0,  1)]  =  expMinusTwoIphl * sinthl * bl/sqrttwo
LeptTL[( -1,'t',  1)]  =  sinthl * bl/sqrttwo
LeptTL[( -1,  0,  1)]  =  sinthl * bl/sqrttwo
