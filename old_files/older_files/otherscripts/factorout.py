import tensorflow as tf
import sys, os
sys.path.append("TensorFlowAnalysis")
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # Do not use GPU
from TensorFlowAnalysis import *

############ - Global
#Define masses
mLc = Const(2.286); mp  = Const(0.938); mk  = Const(0.497); mpi = Const(0.140) #GeV
#Define Lc and Res radii
rLc = Const(5.); rR  = Const(1.5) #GeV
#Define all helicities but expressed in units of 1/2
jLc= 1; lK = 0  ; lpi = 0
#get resonances
js    = ["L1520-pk", "Delta-ppi", "Kstar-kpi"]
resns = get_resonances(js, isCPV = True)  
############

lLcs = [1, -1]
lps  = [1, -1]
lpds = [1, -1]
lRs  = [2, 1, 0, -1, 2] 
exit(0)

###########
def MakeFourVector(pmag, costheta, phi, msq): 
    sintheta = Sqrt(1 - costheta**2) #theta 0 to pi => Sin always pos
    px = pmag * sintheta * Cos(phi)
    py = pmag * sintheta * Sin(phi)
    pz = pmag * costheta
    E  = Sqrt(msq + pmag**2)
    return LorentzVector(Vector(px, py, pz), E)

def pvecMag(Msq, m1sq, m2sq): #Mag of (1 or 2) in M rest frame
    kallen = Sqrt(Msq**2 + m1sq**2 + m2sq**2 - 2.*(Msq*m1sq + Msq*m2sq + m1sq*m2sq))
    return kallen/(2.*Sqrt(Msq))

def get_amp_dict(lLcs_l, lps_l, lpds_l, js_l, lRs_l, obs, Resns):
    Lcdict = {}
    #initialise to zero
    for lLc in lLcs_l:
        for lp in lps_l:
            for lpd in lpds_l: 
                for j in js_l: 
                    for lR in lRs_l: Lcdict[(lLc, lp, lpd, j, lR)] = Scalar(0.)

    #fill only relavant ones
    for lLc in lLcs_l:
        for lp in lps_l:
            for j in js_l:
                if "kpi" in j:
                    #kpi Channel
                    Lcdict[(lLc, lp, 1,j, 0)] = Amp_lLc_lp_lpd_j_lR(lLc, lp,  1, j,  0, obs) #coup0, lR= 0 when lpd=1
                    Lcdict[(lLc, lp, 1,j, 2)] = Amp_lLc_lp_lpd_j_lR(lLc, lp,  1, j,  2, obs) #coup1, lR= 1 when lpd=1 
                    Lcdict[(lLc, lp,-1,j, 0)] = Amp_lLc_lp_lpd_j_lR(lLc, lp, -1, j,  0, obs) #coup3, lR= 0 when lpd =-1
                    Lcdict[(lLc, lp,-1,j,-2)] = Amp_lLc_lp_lpd_j_lR(lLc, lp, -1, j, -2, obs) #coup4, lR=-1 when lpd =-1
                elif "ppi" in j or "pk" in j:
                    #ppi or pk Channel: For pK only lp=lpd terms survive. 
                    Lcdict[(lLc, lp, 1,j, 1)] = Amp_lLc_lp_lpd_j_lR(lLc, lp,  1, j,  1, obs) #coup0, lR= 1/2
                    Lcdict[(lLc, lp, 1,j,-1)] = Amp_lLc_lp_lpd_j_lR(lLc, lp,  1, j, -1, obs) #coup1, lR=-1/2
                    Lcdict[(lLc, lp,-1,j, 1)] = Amp_lLc_lp_lpd_j_lR(lLc, lp, -1, j,  1, obs) #sign*coup0, lR= 1/2
                    Lcdict[(lLc, lp,-1,j,-1)] = Amp_lLc_lp_lpd_j_lR(lLc, lp, -1, j, -1, obs) #sign*coup1, lR=-1/2

    return Lcdict

def delta_func(p4mom, mass):
    cond = tf.equal(mass - TimeComponent(p4mom)                    , Const(0.))
    cond = tf.logical_and(cond, tf.equal(XComponent(p4mom)         , Const(0.)))
    cond = tf.logical_and(cond, tf.equal(YComponent(p4mom)         , Const(0.)))
    cond = tf.logical_and(cond, tf.equal(ZComponent(p4mom)         , Const(0.)))
    return cond

# All the spins expressed in units of 1/2
def L_bw_ProtonAndPsuedoscalar(spin, parity) : 
    l1 = (spin-1)/2     # Lowest possible momentum
    p1 = 2*(l1 % 2)-1   # P=(-1)^(L1+1), e.g. P=-1 if L=0
    if p1 == parity : return l1
    return l1+1

# All the spins expressed in units of 1/2
def ParitySign(spin, parity): 
    jp =  1; jd =  0; pp =  1; pd = -1
    s = 2*(((jp+jd-spin)/2+1) % 2)-1
    s *= (pp*pd*parity)
    return s

def Amp_lLc_lp_lpd_j_lR(lLcval, lpval, lpdval, resname, lRval, Obs):
    res     = resns[resname]
    BW      = res["lineshape"]
    if "ppi" in resname: #D=Lc, R2=Rpip, A=p, B=pi, C=K; jlc >= abs(lRpip) => lRpip = 1/2,-1/2
        msqname    = "m2ppi";       ma      = mp;           mb      = mpi;              mc      = mk
        cthetaRname = "ppi_ctheta_r"; phiRname= "ppi_phi_r";  cthetaAname = "ppi_ctheta_p"; phiAname= "ppi_ctheta_p"
        lambdaA    = lpdval;            lambdaB = lpi;          lambdaC    = lK
        L_bw_Res_Bachelor   = (res["spin"]-1)/2  #L_bw_Res_Bachelor = Max(|jr - jLc - jK|,0) (or Max(|jLc + jK - jr|,0))
        L_bw_ResDaug        = L_bw_ProtonAndPsuedoscalar(res["spin"], res["parity"]) #L_bw_ResDaug = |jres - jp| (or |jp - jres|) with P_res = (-1)^(L_bw_ResDaug +1)
        rotname = "rot_angle_ppi"
    elif "kpi" in resname: #D=Lc, R1=RKpi, A=K, B=pi, C=p; jlc >= abs(lRKpi - lpdval) => lRKpi = 0,1 (when lpdval = 1/2) and lRKpi = 0,-1 (when lpdval = -1/2)
        msqname    = "m2kpi";       ma      = mk;           mb      = mpi;              mc      = mp
        cthetaRname = "kpi_ctheta_r"; phiRname= "kpi_phi_r";  cthetaAname = "kpi_ctheta_k"; phiAname= "kpi_ctheta_k"
        lambdaA    = lK;            lambdaB = lpi;          lambdaC    = lpdval
        L_bw_Res_Bachelor   = res["spin"]/2-1  #L_bw_Res_Bachelor = Max(|jr - jLc - jp|,0)
        L_bw_ResDaug        = res["spin"]/2    #L_bw_ResDaug = jr since lK=lpi=0
        rotname = "rot_angle_kpi"
    elif "pk" in resname: #D=Lc, R1=RKp,  A=p, B=K, C=pi; jlc >= abs(lRKp) => lRKp = 1/2,-1/2
        msqname    = "m2pK";       ma      = mp;           mb      = mk;              mc      = mpi
        cthetaRname = "pk_ctheta_r"; phiRname= "pk_phi_r";  cthetaAname = "pk_ctheta_p"; phiAname= "pk_ctheta_p"
        lambdaA    = lpdval;            lambdaB = lK;          lambdaC    = lpi
        L_bw_Res_Bachelor   = (res["spin"]-1)/2  #L_bw_Res_Bachelor = Max(|jr - jLc - jpi|,0) (or Max(|jLc + jpi - jr|,0))
        L_bw_ResDaug        = L_bw_ProtonAndPsuedoscalar(res["spin"], res["parity"]) #L_bw_ResDaug = |jres - jp| (or |jp - jres|) with P_res = (-1)^(L_bw_ResDaug +1)
        rotname = "rot_angle_pk"

    Amp  = Complex(Wignerd(Obs[rotname], 1, lpdval, lpval), Const(0.))  #proton hel defined in pK, so for pK Channel rot_angle is 0.
    Amp *= BW(m2=Obs[msqname], m0=res["mass"], gamma0=res["width"], ma=ma, mb=mb, mc=mc, md=mLc, dr=rR, dd=rLc, lr=L_bw_ResDaug, ld=L_bw_Res_Bachelor)
    Amp *= HelicityAmplitude3Body(thetaR=Acos(Obs[cthetaRname]), phiR=Obs['flav']*Obs[phiRname], thetaA=Acos(Obs[cthetaAname]), phiA=Obs['flav']*Obs[phiAname], spinD=jLc, spinR=res["spin"], mu=lLcval, lambdaR=lRval, lambdaA=lambdaA, lambdaB=lambdaB, lambdaC=lambdaC) 
    return Amp

def Prepare_data(x, flv):
    Vars = {}
    #Store Lc phsp Varsiables and 
    #make sure volume element stays as dm2pk * dpk_ctheta_p * dpk_phi_p * dlc_ctheta_pi * dlc_phi_pi
    m2pk          = x[:,0]
    pk_ctheta_p   = x[:,1]
    pk_phi_p      = x[:,2]
    lc_ctheta_pi  = x[:,3] #Should be -pk_ctheta_r 
    lc_phi_pi     = x[:,4] #Should be  pk_phi_r + pi
    #p and k 4mom in (pk) rest frame
    pk_p3mag_p   = pvecMag(m2pk, mp**2, mk**2)
    pk_p4mom_p   = MakeFourVector(pk_p3mag_p,    pk_ctheta_p, pk_phi_p      , mp**2)
    pk_p4mom_k   = MakeFourVector(pk_p3mag_p,-1.*pk_ctheta_p, pk_phi_p+Pi() , mk**2)
    #get all 4mom in Lc rest frame
    lc_p3mag_pi  = pvecMag(mLc**2, mpi**2, m2pk)
    p4R          = MakeFourVector(lc_p3mag_pi,-1.*lc_ctheta_pi, lc_phi_pi+Pi(), m2pk)
    p4pi         = MakeFourVector(lc_p3mag_pi,    lc_ctheta_pi, lc_phi_pi,      mpi**2)
    p4p          = BoostToRest(pk_p4mom_p, p4R)  #CHECK!!!
    p4k          = BoostToRest(pk_p4mom_k, p4R)
    #Store all helicity angles of kpi, pk and ppi systems
    kpi_theta_r, kpi_phi_r, kpi_theta_k, kpi_phi_k = HelicityAngles3Body(p4k, p4pi, p4p)
    pk_theta_r,   pk_phi_r,  _, _                  = HelicityAngles3Body(p4p, p4k, p4pi)
    ppi_theta_r, ppi_phi_r, ppi_theta_p, ppi_phi_p = HelicityAngles3Body(p4p, p4pi, p4k)
    #Store all rotation angles
    rot_angle_kpi = SpinRotationAngle( p4k, p4p, p4pi, 1) #return angle b/w Kpi & K in p rest, kpi Channel
    rot_angle_ppi = SpinRotationAngle( p4k, p4p, p4pi, 0) #return angle b/w K & pi in p rest,  ppi Channel
    rot_angle_pk  = Const(0.)
    #store Varss
    Vars['m2pk']          = m2pk
    Vars["m2kpi"]         = Mass(p4k + p4pi)**2
    Vars["m2ppi"]         = Mass(p4p + p4pi)**2
    Vars["kpi_ctheta_r"]  = Cos(kpi_theta_r)
    Vars["kpi_phi_r"  ]   = kpi_phi_r 
    Vars["kpi_ctheta_k"]  = Cos(kpi_theta_k)
    Vars["kpi_phi_k"  ]   = kpi_phi_k 
    Vars["pk_ctheta_r" ]  = Cos(pk_theta_r)
    Vars["pk_phi_r"   ]   = pk_phi_r 
    Vars['pk_ctheta_p']   = pk_ctheta_p
    Vars['pk_phi_p']      = pk_phi_p
    Vars["ppi_ctheta_r"]  = Cos(ppi_theta_r)
    Vars["ppi_phi_r"  ]   = ppi_phi_r 
    Vars["ppi_ctheta_p"]  = Cos(ppi_theta_p)
    Vars["ppi_phi_p"  ]   = ppi_phi_p 
    Vars["rot_angle_kpi"] = rot_angle_kpi 
    Vars["rot_angle_ppi"] = rot_angle_ppi 
    Vars["rot_angle_pk"]  = rot_angle_pk
    Vars["flav"]          = flv * Ones(m2pk)
    Vars['pk_p3mag_p']    = pk_p3mag_p
    Vars['lc_p3mag_pi']   = lc_p3mag_pi
    Vars['lc_phi_pi']     = lc_phi_pi
    return Vars

def get_coup_dict(lpds_l, js_l, lRs_l, flv):
    coupl_Lc    = {}
    #initialise to zero
    for lpd in lpds_l: 
        for j in js_l: 
            for lR in lRs_l: 
                coupl_Lc[(lpd, j, lR)]    = Complex(0., 0.)
    
    #Fill
    for j in js_l:
        coupls = resns[j]['coupl']
        if 'kpi' in j: 
            coupl_Lc[( 1, j, 0)]    = Complex(coupls[0] + flv * coupls[1], coupls[2]  + flv  * coupls[3])
            coupl_Lc[( 1, j, 2)]    = Complex(coupls[4] + flv * coupls[5], coupls[6]  + flv  * coupls[7])
            coupl_Lc[(-1, j, 0)]    = Complex(coupls[8] + flv * coupls[9], coupls[10] + flv  * coupls[11])
            coupl_Lc[(-1, j,-2)]    = Complex(coupls[12]+ flv * coupls[13],coupls[14] + flv  * coupls[15])
        elif 'ppi' in j or 'pk' in j:
            sign = ParitySign(resns[j]["spin"], resns[j]["parity"]) #Parity conserved in R decay, coup_{lr_lpd=-1} = sign * coup_{lr_lpd=1}, where sign = (-1)^(jp+jk-jr)*P_r*P_p*P_K
            coupl_Lc[( 1, j, 1)]    = Complex(coupls[0] + flv * coupls[1], coupls[2]  + flv  * coupls[3])
            coupl_Lc[( 1, j,-1)]    = Complex(coupls[4] + flv * coupls[5], coupls[6]  + flv  * coupls[7])
            coupl_Lc[(-1, j, 1)]    = sign * coupl_Lc[(   1, j, 1)]
            coupl_Lc[(-1, j,-1)]    = sign * coupl_Lc[(   1, j,-1)]

    return coupl_Lc

def get_Lc_ampconjamp(LcDecay_amp, lLc_l, lp_l, lpd_l, js_l, lR_l):
    LcDecay_ampconjamp = {}
    for lLc in lLc_l:
        for lp in lp_l:
            for lpd in lpd_l:
                for lpdd in lpd_l:
                    for j in js_l:
                        for jd in js_l:
                            for lR in lR_l:
                                for lRd in lR_l:
                                    LcDecay_ampconjamp[(lLc, lp, lpd, j, lR, lpdd, jd, lRd)]  = LcDecay_amp[(lLc, lp, lpd, j, lR)] * Conjugate(LcDecay_amp[(lLc, lp, lpdd, jd, lRd)])

    return LcDecay_ampconjamp

def get_coup_ampconjamp(coupl_Lc, lpd_l, js_l, lR_l):
    coupl_ampconjamp   = {}
    for lpd in lpd_l:
        for lpdd in lpd_l:
            for j in js_l:
                for jd in js_l:
                    for lR in lR_l:
                        for lRd in lR_l:
                            coupl_ampconjamp[(lpd, j, lR, lpdd, jd, lRd)] = coupl_Lc[(lpd, j, lR)] * Conjugate(coupl_Lc[(lpdd, jd, lRd)])

    return coupl_ampconjamp

def get_dens(LcDecay_ampconjamp, coupl_ampconjamp, lLc_l, lp_l, lpd_l, js_l, lR_l, pol):
    dens = {}
    for lLc in lLc_l:
        for lp in lp_l:
            dens1 = 0.
            for lpd in lpd_l:
                for lpdd in lpd_l:
                    for j in js_l:
                        for jd in js_l:
                            for lR in lR_l:
                                for lRd in lR_l:
                                    dens1 += LcDecay_ampconjamp[(lLc, lp, lpd, j, lR, lpdd, jd, lRd)]  * coupl_ampconjamp[(lpd, j, lR, lpdd, jd, lRd)]
    
            dens[(lLc, lp)] = dens1

    return 0.5 * (1. + pol) * (dens[(1, 1)] + dens[(1, -1)]) + 0.5 * (1. - pol) * (dens[(-1, 1)] + dens[(-1, -1)]) 

def EnergyMomConserv(dens, Vars):
    ##Energy-mom conservatioin - delta function (not needed for toys)
    pk_p4mom_p   = MakeFourVector(Vars['pk_p3mag_p'],     Vars['pk_ctheta_p'],  Vars['pk_phi_p']      , mp**2)
    pk_p4mom_k   = MakeFourVector(Vars['pk_p3mag_p'], -1.*Vars['pk_ctheta_p'],  Vars['pk_phi_p'] +Pi(), mk**2)
    p4R          = MakeFourVector(Vars['lc_p3mag_pi'],-1.*Vars['lc_ctheta_pi'], Vars['lc_phi_pi']+Pi(), m2pk)
    p4pi         = MakeFourVector(Vars['lc_p3mag_pi'],    Vars['lc_ctheta_pi'], Vars['lc_phi_pi']     , mpi**2)
    cond = delta_func(pk_p4mom_p+pk_p4mom_k, Sqrt(m2pk))
    cond = tf.logical_and(cond, delta_func(p4R+p4pi, mLc))
    dens = tf.where(cond, dens, Zeros(dens))
    return dens
###########

#Particle decay
obsLc                            = Prepare_data(x, flv= 1.)                           #flv option changes the sign of phi
LcDecay_lLc_lp_lpd_j_lR          = get_amp_dict(lLcs, lps, lpds, js, lRs, obsLc   )   #Get all coupling independent amplitudes 
LcCoupl_lpd_j_lR                 = get_coup_dict(lpds_l, js_l, lRs_l, flv= 1.)        #Params to fit, flv option changes sign of CPV coupling
dens                             = build_dens(LcDecay_lLc_lp_lpd_j_lR, LcCoupl_lpd_j_lR, lLcs, lps, lpds, js, lRs)        #Params to fit

def build_dens(amps, coups, pol, lLcs_l, lps_l, lpds_l, js_l, lRs_l):
    for lLc in lLc_l:
        for lp in lp_l:
            dens[(lLc, lp)] = {}
            amp_coherent = Complex(0., 0.)
            for lpd in lpd_l:
                for j in js_l:
                    for lR in lR_l:
                        amp_coherent += amps[(lLc, lp, lpd, j, lR)] * coups[(lpd, j, lR)]
    
            dens[(lLc, lp)] = Density(amp_coherent)

    return 0.5 * (1. + pol) * (dens[(1, 1)] + dens[(1, -1)]) + 0.5 * (1. - pol) * (dens[(-1, 1)] + dens[(-1, -1)]) 

#LcDecay_lLc_lp_lpdlpdd_jjd_lRlRd = get_Lc_ampconjamp(  LcDecay_lLc_lp_lpd_j_lR, lLcs, lps, lpds, js, lRs) 
#LcDecay_data                     = sess.run(LcDecay_lLc_lp_lpdlpdd_jjd_lRlRd, feed_dict={x,data})
#LcDecay_norm                     = sess.run(LcDecay_lLc_lp_lpdlpdd_jjd_lRlRd, feed_dict={x,norm})
#LcCoupl_lpd_j_lR                 = get_coup_dict(lpds_l, js_l, lRs_l, flv= 1.)        #Params to fit
#LcCoupl_lpdlpdd_jjd_lRlRd        = get_coup_ampconjamp(LcCoupl_lpd_j_lR, lpds, js, lRs)
#polr                             = FitParameter("pol",  0., -1., 1., 0.01)
#dens_data                        = get_dens(LcDecay_data, LcCoupl_lpdlpdd_jjd_lRlRd, lLcs, lps, lpds, js, lRs, polr)
#dens_norm                        = get_dens(LcDecay_norm, LcCoupl_lpdlpdd_jjd_lRlRd, lLcs, lps, lpds, js, lRs, polr)
#dens_d                           = EnergyMomConserv(dens_data, obsLc)
#dens_n                           = EnergyMomConserv(dens_norm, obsLc)
##Antiparticle decay
    #Now rotate and boost, instead of other way around, TFA implementation
    p4prot  = RotateLorentzVector(p4p, -lc_phi_R, Pi()-Acos(lc_ctheta_R), lc_phi_R)
    p4krot  = RotateLorentzVector(p4k, -lc_phi_R, Pi()-Acos(lc_ctheta_R), lc_phi_R)
    p4p2    = BoostToRest(p4prot, p4prot+p4krot)
    #print(sess.run(p4p2))
    #print(sess.run(pk_p4mom_p))
    #print(sess.run(p4k2))
    #print(sess.run(pk_p4mom_k))
    theta_a = Acos(-ZComponent(p4p2) / Norm( SpatialComponents(p4p2) ) )
    phi_a   = Atan2(YComponent(p4p2), XComponent(p4p2) )
    print(sess.run([Cos(theta_a), Sin(theta_a), pk_ctheta_p, Sin(Acos(pk_ctheta_p))]))
    print(sess.run([Cos(phi_a), Sin(phi_a), Cos(pk_phi_p), Sin(pk_phi_p)]))
    exit(1)
