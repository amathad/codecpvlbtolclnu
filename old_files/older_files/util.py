############################ - Import 
import tensorflow as tf
import sys, os
home = os.getenv('HOME')
sys.path.append(home+"/Packages/TensorFlowAnalysisnew")
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # Do not use GPU
from TensorFlowAnalysis import *
import pandas as pd
from root_pandas import to_root
############################

############################ - Make constants
#Masses 
MLb     = 5.61960    #GeV
MLc     = 2.28646    #GeV
Mp      = 0.938      #GeV
Mk      = 0.497      #GeV 
Mpi     = 0.140      #GeV
Mlep    = 105.66e-3  #GeV Mu
#Mlep   = 1.77686    #GeV Tau
#Global constants express all in GeV
Tau_Lc  = Const(1e9 * 2e-13/6.582e-16) #GeV^-1, where hcross = 6.582e-16 eV * sec 
GF      = Const(1.166378e-5) #GeV^-2
mb      = Const(4.18       ) #GeV
mc      = Const(1.275      ) #GeV
Vcb     = Const(4.22e-2    ) #avg of incl and excl
rLc     = Const(5.)  #GeV
rR      = Const(1.5) #GeV
jLc     = 1 
lK      = 0   
lpi     = 0
#derived constants
Mpos    = Const(MLb) + Const(MLc)
Mneg    = Const(MLb) - Const(MLc)
t0      = Pow(Mneg,2)
sqrttwo = Sqrt(np.array(2.)) #Used a alot and complains about tf.dtype, so define it here with np
#For FF parametrisation
Tf_plus = {} #mf_pole = M_BC + delta_f and tf_plus = mf_pole**2
Tf_plus['fplus']      = Pow(Const(6.276 + 56e-3         ), 2)#GeV
Tf_plus['fperp']      = Pow(Const(6.276 + 56e-3         ), 2)#GeV
Tf_plus['f0']         = Pow(Const(6.276 + 449e-3        ), 2)#GeV
Tf_plus['gplus']      = Pow(Const(6.276 + 492e-3        ), 2)#GeV
Tf_plus['gperp']      = Pow(Const(6.276 + 492e-3        ), 2)#GeV
Tf_plus['g0']         = Pow(Const(6.276 + 0.            ), 2)#GeV
Tf_plus['hplus']      = Pow(Const(6.276 + 6.332 - 6.276 ), 2)#GeV
Tf_plus['hperp']      = Pow(Const(6.276 + 6.332 - 6.276 ), 2)#GeV
Tf_plus['htildeplus'] = Pow(Const(6.276 + 6.768 - 6.276 ), 2)#GeV
Tf_plus['htildeperp'] = Pow(Const(6.276 + 6.768 - 6.276 ), 2)#GeV
#Define signs
eta    = {'t': 1.,  0 :-1.,  -1: -1., 1 : -1.}
signWC = {'V': 1., 'A':-1., 'S': 1.,'PS': -1., 'T': 1., 'PT':-1.}
#defile helicity list (NB: To build Lc amplitude defined everything in units of 1/2 except for lw used in leptonic case that does not need this)
#incoherent hel
lLbs = [1, -1]
lLcs = [1, -1]
lls  = [1, -1]
lps  = [1, -1]
#coherent hel
lws  = ['t', 0, 1, -1]
WCs  = ['V', 'A', 'S', 'PS', 'T', 'PT']
FFs  = ['a0f0', 'a0fplus', 'a0fperp', 'a0g0', 'a0gplus', 'a0gperp', 'a0hplus', 'a0hperp', 'a0htildeperp', 'a0htildeplus']
FFs += ['a1f0', 'a1fplus', 'a1fperp', 'a1g0', 'a1gplus', 'a1gperp', 'a1hplus', 'a1hperp', 'a1htildeperp', 'a1htildeplus']
resns= ["Kstar-kpi", "L1520-pk", "L1670-pk"]
lRs  = [2, 1, 0, -1, -2] 
############################

############################ - define functions
def get_switches(names): 
    swtch = {}
    for name in names: swtch[name] = tf.placeholder_with_default(Const(1.), shape = () , name = name+'_switch')
    return swtch

def MakeFourVector(pmag, costheta, phi, msq): 
    sintheta = Sqrt(1 - costheta**2) #theta 0 to pi => Sin always pos
    px = pmag * sintheta * Cos(phi)
    py = pmag * sintheta * Sin(phi)
    pz = pmag * costheta
    E  = Sqrt(msq + pmag**2)
    return LorentzVector(Vector(px, py, pz), E)

def pvecMag(Msq, m1sq, m2sq): #Mag of (1 or 2) in M rest frame
    kallen = Sqrt(Msq**2 + m1sq**2 + m2sq**2 - 2.*(Msq*m1sq + Msq*m2sq + m1sq*m2sq))
    return kallen/(2.*Sqrt(Msq))

def HelAngles3Body(pa, pb, pc):
    theta_r  = Acos(-ZComponent(pc) / Norm(SpatialComponents(pc)))
    phi_r    = Atan2(-YComponent(pc), -XComponent(pc))
    pa_prime = RotateLorentzVector(pa, -phi_r, -theta_r, phi_r)
    pb_prime = RotateLorentzVector(pb, -phi_r, -theta_r, phi_r)
    pa_prime2= BoostToRest(pa_prime, pa_prime+pb_prime)
    theta_a  = Acos(ZComponent(pa_prime2) / Norm(SpatialComponents(pa_prime2)))
    phi_a    = Atan2(YComponent(pa_prime2), XComponent(pa_prime2))
    return (theta_r, phi_r, theta_a, phi_a)

def InvRotateAndBoostFromRest(m_p4mom_p1, m_p4mom_p2, gm_phi_m, gm_ctheta_m, gm_p4mom_m):
    """
    m_p4mom_p1: particle p1 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
    m_p4mom_p2: particle p2 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
    gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
    gm_4mom_m: m 4mom in gm's helicity frame
    """
    #First:  Rotate particle p 3mom defined in mother m helicity frame such that z now points along z in grand mother gm helicity frame (i.e interpreted as gm mom in it's grand-grand-mother ggm rest frame)
    m_p4mom_p1_zgmmom      = RotateLorentzVector(m_p4mom_p1, -gm_phi_m, Acos(gm_ctheta_m), gm_phi_m)
    m_p4mom_p2_zgmmom      = RotateLorentzVector(m_p4mom_p2, -gm_phi_m, Acos(gm_ctheta_m), gm_phi_m)
    #Second: Boost particle p 4mom from mother m's rest frame to gm helicity frame. This is done using boost vector from gm_p4mom_m  [checked: E(m_p4mom_p1_zmmom + m_p4mom_p2_zmmom) == Mass_m]
    gm_p4mom_p1            = BoostFromRest(m_p4mom_p1_zgmmom, gm_p4mom_m)
    gm_p4mom_p2            = BoostFromRest(m_p4mom_p2_zgmmom, gm_p4mom_m)
    """
    Checks done such as 
        - (lc_p4mom_p+lc_p4mom_k == lc_p4mom_r), 
        - Going in reverse i.e. boost lc_p4mom_p into R rest frame (using lc_p4mom_r) and rotating into R helicity frame gives back r_p4mom_p 
            - i.e. lc_p4mom_p_opp = RotateLorentzVector(BoostToRest(lc_p4mom_p, lc_p4mom_r), -lc_phi_r, -Acos(lc_ctheta_r), lc_phi_r)) where lc_p4mom_p_opp == r_p4mom_p
        - Rotate and boost, instead of boost and rotate should give the same answer, checked (Does not agree with TFA implementation though)
            -i.e. lc_p4mom_prot  = RotateLorentzVector(lc_p4mom_p, -lc_phi_r, -Acos(lc_ctheta_r), lc_phi_r)
            -i.e. lc_p4mom_krot  = RotateLorentzVector(lc_p4mom_k, -lc_phi_r, -Acos(lc_ctheta_r), lc_phi_r)
            -i.e. lc_p4mom_p2    = BoostToRest(lc_p4mom_prot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_p2 == r_p4mom_p
            -i.e. lc_p4mom_k2    = BoostToRest(lc_p4mom_krot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_k2 == r_p4mom_k
    """
    return gm_p4mom_p1, gm_p4mom_p2

def RotateAndBoostToRest(gm_p4mom_p1, gm_p4mom_p2, gm_phi_m, gm_ctheta_m):
    """
    gm_p4mom_p1: particle p1 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
    gm_p4mom_p2: particle p2 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
    gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
    """
    #First:  Rotate particle p 3mom defined in grand mother gm helicity frame such that z now points along z of mother m in gm rest frame.
    gm_p4mom_p1_zmmom      = RotateLorentzVector(gm_p4mom_p1, -gm_phi_m, -Acos(gm_ctheta_m), gm_phi_m)
    gm_p4mom_p2_zmmom      = RotateLorentzVector(gm_p4mom_p2, -gm_phi_m, -Acos(gm_ctheta_m), gm_phi_m)
    #Second: Boost particle p 4mom to mother m i.e. p1p2 rest frame
    gm_p4mom_m             = gm_p4mom_p1_zmmom+gm_p4mom_p2_zmmom
    m_p4mom_p1             = BoostToRest(gm_p4mom_p1_zmmom, gm_p4mom_m)
    m_p4mom_p2             = BoostToRest(gm_p4mom_p2_zmmom, gm_p4mom_m)
    return m_p4mom_p1, m_p4mom_p2

def Prepare_data(x, Flv = 1.):
    Vars = {}
    #Store Lc phsp Varsiables and make sure volume element stays as dq2*dcthlc*dcthl*dphl*dm2pk*dr_ctheta_p*dr_phi_p*dlc_ctheta_pi*dlc_phi_pi
    lb_ctheta_lc  = x[:,0]
    lb_phi_lc     = Zeros(x[:,0])
    lc_ctheta_pi  = x[:,1] 
    lc_phi_pi     = x[:,2] 
    q2            = x[:,3]
    w_ctheta_l    = x[:,4]
    w_phi_l       = x[:,5]
    m2pk          = x[:,6]
    r_ctheta_p    = x[:,7]
    r_phi_p       = x[:,8]

    #Store info of other particles
    lb_ctheta_w   = -lb_ctheta_lc
    lb_phi_w      =  lb_phi_lc+Pi()
    lc_ctheta_r   = -lc_ctheta_pi
    lc_phi_r      =  lc_phi_pi+Pi()
    w_ctheta_nu   = -w_ctheta_l
    w_phi_nu      =  w_phi_l+Pi()
    r_ctheta_k    = -r_ctheta_p
    r_phi_k       =  r_phi_p+Pi()

    #Lc and W 4mom in Lb rest frame     (zLb = pbeam_lab x pLb_lab, yLb = zLb x lb_p3vec_lc, x = y x z => r, theta, phi = lb_p3mag_lc,lb_ctheta_lc, 0)
    lb_p3mag_lc  = pvecMag(Const(MLb)**2, Const(MLc)**2, q2)
    lb_p4mom_lc  = MakeFourVector(lb_p3mag_lc,     lb_ctheta_lc, lb_phi_lc, Const(MLc)**2)
    lb_p4mom_w   = MakeFourVector(lb_p3mag_lc,     lb_ctheta_w,  lb_phi_w , q2)
    #R and pi 4mom in Lc helicity frame (zlc = lb_p3vec_lc, ylc = zLb x zlc, xlc = ylc x xlc => r, theta, phi => lc_p3mag_pi, lc_ctheta_pi, lc_phi_pi)
    lc_p3mag_pi  = pvecMag(Const(MLc)**2, Const(Mpi)**2, m2pk)
    lc_p4mom_pi  = MakeFourVector(lc_p3mag_pi,    lc_ctheta_pi, lc_phi_pi,      Const(Mpi)**2)
    lc_p4mom_r   = MakeFourVector(lc_p3mag_pi,     lc_ctheta_r,  lc_phi_r,      m2pk)
    #p and k 4mom in R helicity frame   (zr = lc_p3vec_r, yr = zlc x zr, xr = yr x xr => r, theta, phi => r_p3mag_p, r_ctheta_p, r_phi_p)
    r_p3mag_p    = pvecMag(m2pk, Const(Mp)**2, Const(Mk)**2)
    r_p4mom_p    = MakeFourVector(r_p3mag_p,    r_ctheta_p, r_phi_p, Const(Mp)**2)
    r_p4mom_k    = MakeFourVector(r_p3mag_p,    r_ctheta_k, r_phi_k, Const(Mk)**2)
    #l and nu 4mom in W helicity frame (zw = lb_p3vec_w, yw = zlb x zlc , xw = yw x zw => r, theta, phi => w_p3mag_l, w_ctheta_l, w_phl_l)
    w_p3mag_l    = pvecMag(q2, Const(Mlep)**2, 0.)
    w_p4mom_l    = MakeFourVector(w_p3mag_l,    w_ctheta_l , w_phi_l , Const(Mlep)**2)
    w_p4mom_nu   = MakeFourVector(w_p3mag_l,    w_ctheta_nu, w_phi_nu, 0.)

    #Get everything is Lb rest frame
    lc_p4mom_p , lc_p4mom_k = InvRotateAndBoostFromRest(r_p4mom_p ,  r_p4mom_k , lc_phi_r,  lc_ctheta_r,  lc_p4mom_r)
    lb_p4mom_pi, lb_p4mom_r = InvRotateAndBoostFromRest(lc_p4mom_pi, lc_p4mom_r, lb_phi_lc, lb_ctheta_lc, lb_p4mom_lc)
    lb_p4mom_p , lb_p4mom_k = InvRotateAndBoostFromRest(lc_p4mom_p,  lc_p4mom_k, lb_phi_lc, lb_ctheta_lc, lb_p4mom_lc)
    lb_p4mom_l , lb_p4mom_nu= InvRotateAndBoostFromRest(w_p4mom_l,   w_p4mom_nu, lb_phi_w,  lb_ctheta_w,  lb_p4mom_w)

    #Store all helicity angles of kpi, pk and ppi systems: Checked following
    #a,b,c,d = HelAngles3Body(lb_p4mom_l, lb_p4mom_nu, lb_p4mom_lc)
    #print(sess.run([a,Cos(b),Sin(b),c,Cos(d), Sin(d)]))
    #print(sess.run([Acos(lb_ctheta_w), Cos(lb_phi_w), Sin(lb_phi_w), Acos(w_ctheta_l), Cos(w_phi_l), Sin(w_phi_l)])) - 3 phase space variables
    #a,b,c,d = HelAngles3Body(lc_p4mom_p, lc_p4mom_k, lc_p4mom_pi)
    #print(sess.run([a,Cos(b),Sin(b),c,Cos(d), Sin(d)]))
    #print(sess.run([Acos(lc_ctheta_r), Cos(lc_phi_r), Sin(lc_phi_r), Acos(r_ctheta_p), Cos(r_phi_p), Sin(r_phi_p)])) - 4 phase space variables
    #print(sess.run([Mass(lb_p4mom_p+lb_p4mom_k) , Sqrt(m2pk)])) - 1 phase space var
    #print(sess.run([Mass(lb_p4mom_l+lb_p4mom_nu), Sqrt(q2)]))   - 1 phase space var
    lc_theta_kpi, lc_phi_kpi, kpi_theta_k, kpi_phi_k = HelAngles3Body(lc_p4mom_k, lc_p4mom_pi, lc_p4mom_p)
    lc_theta_ppi, lc_phi_ppi, ppi_theta_p, ppi_phi_p = HelAngles3Body(lc_p4mom_p, lc_p4mom_pi, lc_p4mom_k)

    #Store all rotation angles, should be correct
    rot_angle_kpi = SpinRotationAngle( lc_p4mom_k, lc_p4mom_p, lc_p4mom_pi, 1) #return angle b/w Kpi & K in p rest, kpi Channel
    rot_angle_ppi = SpinRotationAngle( lc_p4mom_k, lc_p4mom_p, lc_p4mom_pi, 0) #return angle b/w K & pi in p rest,  ppi Channel
    rot_angle_pk  = Zeros(0.)

    #Store Varss
    #DP vars
    Vars['q2']            = q2
    Vars['m2pk']          = m2pk
    Vars["m2kpi"]         = Pow(Mass(lc_p4mom_k + lc_p4mom_pi),2)
    Vars["m2ppi"]         = Pow(Mass(lc_p4mom_p + lc_p4mom_pi),2)
    Vars['El']            = TimeComponent(lb_p4mom_l)
    #Vars['msqmiss']       = Pow(Mass(lb_p4mom_nu),2)
    #lnu angles
    Vars['lb_ctheta_w']   = lb_ctheta_w
    Vars['lb_phi_w']      = Flv * lb_phi_w
    Vars['w_ctheta_l']    = w_ctheta_l
    Vars['w_phi_l']       = Flv * w_phi_l
    #kpi angles
    Vars["lc_ctheta_kpi"] = Cos(lc_theta_kpi)
    Vars["lc_phi_kpi"  ]  = Flv * lc_phi_kpi 
    Vars["kpi_ctheta_k"]  = Cos(kpi_theta_k)
    Vars["kpi_phi_k"  ]   = Flv * kpi_phi_k 
    #pk angles
    Vars["lc_ctheta_r" ]  = lc_ctheta_r
    Vars["lc_phi_r"   ]   = Flv * lc_phi_r 
    Vars['r_ctheta_p']    = r_ctheta_p
    Vars['r_phi_p']       = Flv * r_phi_p
    #ppi angles
    Vars["lc_ctheta_ppi"] = Cos(lc_theta_ppi)
    Vars["lc_phi_ppi"  ]  = Flv * lc_phi_ppi 
    Vars["ppi_ctheta_p"]  = Cos(ppi_theta_p)
    Vars["ppi_phi_p"  ]   = Flv * ppi_phi_p 
    #rot angles
    Vars["rot_angle_kpi"] = rot_angle_kpi 
    Vars["rot_angle_ppi"] = rot_angle_ppi 
    Vars["rot_angle_pk"]  = rot_angle_pk
    #3mom mag 
    Vars['lb_p3mag_lc']   = lb_p3mag_lc
    Vars['lc_p3mag_pi']   = lc_p3mag_pi
    Vars['r_p3mag_p']     = r_p3mag_p
    Vars['w_p3mag_l']     = w_p3mag_l

    return Vars

# All the spins expressed in units of 1/2
def L_bw_ProtonAndPsuedoscalar(spin, parity) : 
    l1 = (spin-1)/2     # Lowest possible momentum
    p1 = 2*(l1 % 2)-1   # P=(-1)^(L1+1), e.g. P=-1 if L=0
    if p1 == parity : return l1
    return l1+1

def Amp_lLc_lp_lpd_res_lR(lLcval, lpval, lpdval, resname, lRval, Obs, Resns):
    Res     = Resns[resname]
    BW      = Res["lineshape"]
    if "ppi" in resname: #D=Lc, R2=Rpip, A=p, B=pi, C=K; jlc >= abs(lRpip) => lRpip = 1/2,-1/2
        msqname    = "m2ppi";       ma      = Const(Mp);           mb      = Const(Mpi);              mc      = Const(Mk)
        cthetaRname = "lc_ctheta_ppi"; phiRname= "lc_phi_ppi";  cthetaAname = "ppi_ctheta_p"; phiAname= "ppi_phi_p"
        lambdaA    = lpdval;            lambdaB = lpi;          lambdaC    = lK
        L_bw_Res_Bachelor   = (Res["spin"]-1)/2  #L_bw_Res_Bachelor = Max(|jr - jLc - jK|,0) (or Max(|jLc + jK - jr|,0))
        L_bw_ResDaug        = L_bw_ProtonAndPsuedoscalar(Res["spin"], Res["parity"]) #L_bw_ResDaug = |jres - jp| (or |jp - jres|) with P_res = (-1)^(L_bw_ResDaug +1)
        rotname = "rot_angle_ppi"
    elif "kpi" in resname: #D=Lc, R1=RKpi, A=K, B=pi, C=p; jlc >= abs(lRKpi - lpdval) => lRKpi = 0,1 (when lpdval = 1/2) and lRKpi = 0,-1 (when lpdval = -1/2)
        msqname    = "m2kpi";       ma      = Const(Mk);           mb      = Const(Mpi);              mc      = Const(Mp)
        cthetaRname = "lc_ctheta_kpi"; phiRname= "lc_phi_kpi";  cthetaAname = "kpi_ctheta_k"; phiAname= "kpi_phi_k"
        lambdaA    = lK;            lambdaB = lpi;          lambdaC    = lpdval
        L_bw_Res_Bachelor   = Res["spin"]/2-1  #L_bw_Res_Bachelor = Max(|jr - jLc - jp|,0)
        L_bw_ResDaug        = Res["spin"]/2    #L_bw_ResDaug = jr since lK=lpi=0
        rotname = "rot_angle_kpi"
    elif "pk" in resname: #D=Lc, R1=RKp,  A=p, B=K, C=pi; jlc >= abs(lRKp) => lRKp = 1/2,-1/2
        msqname    = "m2pk";       ma      = Const(Mp);           mb      = Const(Mk);              mc      = Const(Mpi)
        cthetaRname = "lc_ctheta_r"; phiRname= "lc_phi_r";  cthetaAname = "r_ctheta_p"; phiAname= "r_phi_p"
        lambdaA    = lpdval;            lambdaB = lK;          lambdaC    = lpi
        L_bw_Res_Bachelor   = (Res["spin"]-1)/2  #L_bw_Res_Bachelor = Max(|jr - jLc - jpi|,0) (or Max(|jLc + jpi - jr|,0))
        L_bw_ResDaug        = L_bw_ProtonAndPsuedoscalar(Res["spin"], Res["parity"]) #L_bw_ResDaug = |jres - jp| (or |jp - jres|) with P_res = (-1)^(L_bw_ResDaug +1)
        rotname = "rot_angle_pk"

    #print("msqname, ma, mb, mc, cthetaRname, phiRname, cthetaAname, phiAname, lambdaA, lambdaB, lambdaC, L_bw_Res_Bachelor, L_bw_ResDaug, rotname")
    Amp  = CastComplex(Wignerd(Obs[rotname], 1, lpdval, lpval))  #proton hel defined in pK, so for pK Channel rot_angle is 0.
    Amp *= BW(m2=Obs[msqname], m0=Res["mass"], gamma0=Res["width"], ma=ma, mb=mb, mc=mc, md=Const(MLc), dr=rR, dd=rLc, lr=L_bw_ResDaug, ld=L_bw_Res_Bachelor)
    Amp *= HelicityAmplitude3Body(thetaR=Acos(Obs[cthetaRname]), phiR=Obs[phiRname], thetaA=Acos(Obs[cthetaAname]), phiA=Obs[phiAname], spinD=jLc, spinR=Res["spin"], mu=lLcval, lambdaR=lRval, lambdaA=lambdaA, lambdaB=lambdaB, lambdaC=lambdaC) 
    return Amp

def get_Lc_ampls(Obs, Resns):
    Lcdict = {}

    #fill only relavant ones
    for lLc in lLcs:
        for lp in lps:
            for res in resns:
                if "kpi" in res:
                    #kpi Channel
                    Lcdict[(lLc, lp, 1,res, 0)] = Amp_lLc_lp_lpd_res_lR(lLc, lp,  1, res,  0, Obs, Resns) #coup0, lR= 0 when lpd=1
                    Lcdict[(lLc, lp, 1,res, 2)] = Amp_lLc_lp_lpd_res_lR(lLc, lp,  1, res,  2, Obs, Resns) #coup1, lR= 1 when lpd=1 
                    Lcdict[(lLc, lp,-1,res, 0)] = Amp_lLc_lp_lpd_res_lR(lLc, lp, -1, res,  0, Obs, Resns) #coup3, lR= 0 when lpd =-1
                    Lcdict[(lLc, lp,-1,res,-2)] = Amp_lLc_lp_lpd_res_lR(lLc, lp, -1, res, -2, Obs, Resns) #coup4, lR=-1 when lpd =-1
                elif "ppi" in res or "pk" in res:
                    #ppi or pk Channel: For pK only lp=lpd terms survive. For pk the rotation angle is zero should be taken take of by the small-wignerd
                    Lcdict[(lLc, lp, 1,res, 1)] = Amp_lLc_lp_lpd_res_lR(lLc, lp,  1, res,  1, Obs, Resns) #coup0, lR= 1/2
                    Lcdict[(lLc, lp, 1,res,-1)] = Amp_lLc_lp_lpd_res_lR(lLc, lp,  1, res, -1, Obs, Resns) #coup1, lR=-1/2
                    Lcdict[(lLc, lp,-1,res, 1)] = Amp_lLc_lp_lpd_res_lR(lLc, lp, -1, res,  1, Obs, Resns) #sign*coup0, lR= 1/2
                    Lcdict[(lLc, lp,-1,res,-1)] = Amp_lLc_lp_lpd_res_lR(lLc, lp, -1, res, -1, Obs, Resns) #sign*coup1, lR=-1/2

    return Lcdict

def get_Lb_ampls(Obs):
    """
    Some important comments about the hadronic currents:
    V currents:
        - aV, bV and cV are form factor parameter independent (unlike in our paper)
        - When summing over lwd only one term is needed. This term is taken as 't' (b'cos eta[t] = 1). Make sure Leptonic V and A current lwd='t'.
        - The ffterms func return corrent q2 dependent terms pertaining to FF parameter a0 and a1.
        - Hadr(lb, lc, lw, 't', 'V', 'a0') have 16 comb out of which 12 survive and 4 are zero
    S currents:
        - Fake lw and lwd index (see above for reason). Fixed to zero, make sure Leptonic S, PS currents also have lw = lwd = 't'.
        - Hadr(lb, lc, 't', 't', 'S', 'a0') have 4 terms all which are nonzero.
    A currents: 
        - Like in Leptonic current these are NOT equal to V currents.
        - Same comments as V currents.
    PS currents: 
        - Like in Leptonic current these are NOT equal to S currents.
        - Same comments as S currents.
    T currents:
        - Hadr(lb, lc, lw, lwd, 'T', 'a0') have 64 terms out of which 32 are nonzero.
    PT currents:
        - Like in Leptonic current these are NOT equal to T currents.
        - same comments as T currents.
    Func returns:
        Dict with index as tuples Hadr[(lb, lc, lw, lwd, wc, ff)]
    """
    #q2 and angular terms: Used sin(x/2) = sqrt((1-cthlc)/2) and cos(x/2)=sqrt((1+cthlc)/2).Since x/2 ranges from 0 to pi/2 both sine and cosine are pos.
    q2          =  Obs['q2']
    cthlc       = -Obs['lb_ctheta_w']
    sqrtQp      = Sqrt(Pow(Mpos,2) - q2)
    sqrtQn      = Sqrt(Pow(Mneg,2) - q2)
    sqrtQpMn    = sqrtQp * Mneg
    sqrtQnMp    = sqrtQn * Mpos
    aV          = sqrtQpMn/Sqrt(q2)
    bV          = sqrtQnMp/Sqrt(q2)
    cV          = sqrttwo * sqrtQn
    aA          = bV
    bA          = aV
    cA          = sqrttwo * sqrtQp
    aS          = sqrtQpMn/(mb - mc)
    aP          = sqrtQnMp/(mb + mc)
    aT          = sqrtQn
    bT          = sqrttwo * bV
    cT          = sqrttwo * aV
    dT          = sqrtQp
    costhlchalf = Sqrt((1+cthlc)/2.)
    sinthlchalf = Sqrt((1-cthlc)/2.)
    #get only q2 dependent terms in FF expansion i.e. ones pertaining to a0 and a1.
    ffterms = {}
    for ff in ['fplus', 'fperp', 'f0', 'gplus', 'gperp', 'g0', 'hplus', 'hperp', 'htildeperp', 'htildeplus']:
        cf = 1./(1. - q2/Tf_plus[ff] )
        zf = (Sqrt(Tf_plus[ff] - q2) - Sqrt(Tf_plus[ff] - t0))/(Sqrt(Tf_plus[ff] - q2) + Sqrt(Tf_plus[ff] - t0))
        ffterms['a0'+ff] =  cf
        ffterms['a1'+ff] =  cf * zf

    Hadr = {}

    #f0 terms: contains V and S currents
    for a in ['a0f0', 'a1f0']:
        Hadr[( 1, 1,'t', 't', 'V', a)] =  costhlchalf * aV * ffterms[a]
        Hadr[(-1,-1,'t', 't', 'V', a)] =  costhlchalf * aV * ffterms[a]
        Hadr[( 1,-1,'t', 't', 'V', a)] = -sinthlchalf * aV * ffterms[a]
        Hadr[(-1, 1,'t', 't', 'V', a)] =  sinthlchalf * aV * ffterms[a]
        Hadr[( 1, 1,'t', 't', 'S', a)] =  costhlchalf * aS * ffterms[a]
        Hadr[(-1,-1,'t', 't', 'S', a)] =  costhlchalf * aS * ffterms[a]
        Hadr[( 1,-1,'t', 't', 'S', a)] = -sinthlchalf * aS * ffterms[a]
        Hadr[(-1, 1,'t', 't', 'S', a)] =  sinthlchalf * aS * ffterms[a]
    
    #fplus terms
    for a in ['a0fplus', 'a1fplus']:
        Hadr[( 1, 1,  0, 't', 'V', a)] =  costhlchalf  * bV * ffterms[a]
        Hadr[(-1,-1,  0, 't', 'V', a)] =  costhlchalf  * bV * ffterms[a]
        Hadr[( 1,-1,  0, 't', 'V', a)] = -sinthlchalf  * bV * ffterms[a]
        Hadr[(-1, 1,  0, 't', 'V', a)] =  sinthlchalf  * bV * ffterms[a]
    
    #fperp terms
    for a in ['a0fperp', 'a1fperp']:
        Hadr[( 1,-1, -1, 't', 'V', a)] = -costhlchalf  * cV * ffterms[a]
        Hadr[(-1, 1,  1, 't', 'V', a)] = -costhlchalf  * cV * ffterms[a]
        Hadr[(-1,-1, -1, 't', 'V', a)] = -sinthlchalf  * cV * ffterms[a]
        Hadr[( 1, 1,  1, 't', 'V', a)] =  sinthlchalf  * cV * ffterms[a]
    
    #g0 terms: contains A and PS currents
    for a in ['a0g0','a1g0']:
        Hadr[( 1, 1,'t', 't', 'A', a)] =  costhlchalf * aA * ffterms[a]
        Hadr[(-1,-1,'t', 't', 'A', a)] = -costhlchalf * aA * ffterms[a]
        Hadr[( 1,-1,'t', 't', 'A', a)] =  sinthlchalf * aA * ffterms[a]
        Hadr[(-1, 1,'t', 't', 'A', a)] =  sinthlchalf * aA * ffterms[a]
        Hadr[( 1, 1,'t', 't','PS', a)] = -costhlchalf * aP * ffterms[a]
        Hadr[(-1,-1,'t', 't','PS', a)] =  costhlchalf * aP * ffterms[a]
        Hadr[( 1,-1,'t', 't','PS', a)] = -sinthlchalf * aP * ffterms[a]
        Hadr[(-1, 1,'t', 't','PS', a)] = -sinthlchalf * aP * ffterms[a]
    
    #gplus terms
    for a in ['a0gplus','a1gplus']:
        Hadr[( 1, 1,  0, 't', 'A', a)] =  costhlchalf * bA * ffterms[a]
        Hadr[(-1,-1,  0, 't', 'A', a)] = -costhlchalf * bA * ffterms[a]
        Hadr[( 1,-1,  0, 't', 'A', a)] =  sinthlchalf * bA * ffterms[a]
        Hadr[(-1, 1,  0, 't', 'A', a)] =  sinthlchalf * bA * ffterms[a]
    
    #gperp terms
    for a in ['a0gperp','a1gperp']:
        Hadr[( 1,-1, -1, 't', 'A', a)] =  costhlchalf * cA * ffterms[a]
        Hadr[(-1, 1,  1, 't', 'A', a)] = -costhlchalf * cA * ffterms[a]
        Hadr[(-1,-1, -1, 't', 'A', a)] =  sinthlchalf * cA * ffterms[a]
        Hadr[( 1, 1,  1, 't', 'A', a)] =  sinthlchalf * cA * ffterms[a]
    
    #hplus terms: T and PT
    for a in ['a0hplus','a1hplus']:
        Hadr[( 1, 1,'t',  0, 'T', a)] =  costhlchalf * aT * ffterms[a]
        Hadr[(-1,-1,'t',  0, 'T', a)] =  costhlchalf * aT * ffterms[a]
        Hadr[( 1,-1,'t',  0, 'T', a)] = -sinthlchalf * aT * ffterms[a]
        Hadr[(-1, 1,'t',  0, 'T', a)] =  sinthlchalf * aT * ffterms[a]
        Hadr[( 1, 1,  0,'t', 'T', a)] = -costhlchalf * aT * ffterms[a]
        Hadr[(-1,-1,  0,'t', 'T', a)] = -costhlchalf * aT * ffterms[a]
        Hadr[( 1,-1,  0,'t', 'T', a)] =  sinthlchalf * aT * ffterms[a]
        Hadr[(-1, 1,  0,'t', 'T', a)] = -sinthlchalf * aT * ffterms[a]
        Hadr[( 1, 1,  1, -1, 'PT',a)] =  costhlchalf * aT * ffterms[a]
        Hadr[(-1,-1,  1, -1, 'PT',a)] =  costhlchalf * aT * ffterms[a]
        Hadr[( 1,-1,  1, -1, 'PT',a)] = -sinthlchalf * aT * ffterms[a]
        Hadr[(-1, 1,  1, -1, 'PT',a)] =  sinthlchalf * aT * ffterms[a]
        Hadr[( 1, 1, -1,  1, 'PT',a)] = -costhlchalf * aT * ffterms[a]
        Hadr[(-1,-1, -1,  1, 'PT',a)] = -costhlchalf * aT * ffterms[a]
        Hadr[( 1,-1, -1,  1, 'PT',a)] =  sinthlchalf * aT * ffterms[a]
        Hadr[(-1, 1, -1,  1, 'PT',a)] = -sinthlchalf * aT * ffterms[a]
    
    #hperp terms: T and PT
    for a in ['a0hperp','a1hperp']:
        Hadr[(-1, 1,'t',  1, 'T', a)] = -costhlchalf * bT *  ffterms[a]
        Hadr[( 1,-1,'t', -1, 'T', a)] = -costhlchalf * bT *  ffterms[a]
        Hadr[(-1,-1,'t', -1, 'T', a)] = -sinthlchalf * bT *  ffterms[a]
        Hadr[( 1, 1,'t',  1, 'T', a)] =  sinthlchalf * bT *  ffterms[a]
        Hadr[(-1, 1,  1,'t', 'T', a)] =  costhlchalf * bT *  ffterms[a]
        Hadr[( 1,-1, -1,'t', 'T', a)] =  costhlchalf * bT *  ffterms[a]
        Hadr[(-1,-1, -1,'t', 'T', a)] =  sinthlchalf * bT *  ffterms[a]
        Hadr[( 1, 1,  1,'t', 'T', a)] = -sinthlchalf * bT *  ffterms[a]
        Hadr[( 1,-1,  0, -1, 'PT',a)] = -costhlchalf * bT *  ffterms[a]
        Hadr[(-1,-1,  0, -1, 'PT',a)] = -sinthlchalf * bT *  ffterms[a]
        Hadr[( 1,-1, -1,  0, 'PT',a)] =  costhlchalf * bT *  ffterms[a]
        Hadr[(-1,-1, -1,  0, 'PT',a)] =  sinthlchalf * bT *  ffterms[a]
        Hadr[( 1, 1,  0,  1, 'PT',a)] = -sinthlchalf * bT *  ffterms[a]
        Hadr[(-1, 1,  0,  1, 'PT',a)] =  costhlchalf * bT *  ffterms[a]
        Hadr[( 1, 1,  1,  0, 'PT',a)] =  sinthlchalf * bT *  ffterms[a]
        Hadr[(-1, 1,  1,  0, 'PT',a)] = -costhlchalf * bT *  ffterms[a]
    
    #htildeperp terms: T and PT
    for a in ['a0htildeperp','a1htildeperp']:
        Hadr[(-1,-1,  0, -1, 'T', a)] = -sinthlchalf * cT * ffterms[a]
        Hadr[( 1, 1,  0,  1, 'T', a)] =  sinthlchalf * cT * ffterms[a]
        Hadr[(-1, 1,  0,  1, 'T', a)] = -costhlchalf * cT * ffterms[a]
        Hadr[( 1,-1,  0, -1, 'T', a)] = -costhlchalf * cT * ffterms[a]
        Hadr[(-1,-1, -1,  0, 'T', a)] =  sinthlchalf * cT * ffterms[a]
        Hadr[( 1, 1,  1,  0, 'T', a)] = -sinthlchalf * cT * ffterms[a]
        Hadr[(-1, 1,  1,  0, 'T', a)] =  costhlchalf * cT * ffterms[a]
        Hadr[( 1,-1, -1,  0, 'T', a)] =  costhlchalf * cT * ffterms[a]
        Hadr[( 1, 1,'t',  1, 'PT',a)] = -sinthlchalf * cT * ffterms[a]
        Hadr[(-1,-1,'t', -1, 'PT',a)] = -sinthlchalf * cT * ffterms[a]
        Hadr[( 1,-1,'t', -1, 'PT',a)] = -costhlchalf * cT * ffterms[a]
        Hadr[(-1, 1,'t',  1, 'PT',a)] =  costhlchalf * cT * ffterms[a]
        Hadr[( 1, 1,  1,'t', 'PT',a)] =  sinthlchalf * cT * ffterms[a]
        Hadr[(-1,-1, -1,'t', 'PT',a)] =  sinthlchalf * cT * ffterms[a]
        Hadr[( 1,-1, -1,'t', 'PT',a)] =  costhlchalf * cT * ffterms[a]
        Hadr[(-1, 1,  1,'t', 'PT',a)] = -costhlchalf * cT * ffterms[a]
    
    #htildeplus terms: T and PT
    for a in ['a0htildeplus','a1htildeplus']:
        Hadr[( 1,-1,  1, -1, 'T', a)] = -sinthlchalf * dT * ffterms[a]
        Hadr[(-1, 1,  1, -1, 'T', a)] = -sinthlchalf * dT * ffterms[a]
        Hadr[( 1, 1,  1, -1, 'T', a)] = -costhlchalf * dT * ffterms[a]
        Hadr[(-1,-1,  1, -1, 'T', a)] =  costhlchalf * dT * ffterms[a]
        Hadr[( 1,-1, -1,  1, 'T', a)] =  sinthlchalf * dT * ffterms[a]
        Hadr[(-1, 1, -1,  1, 'T', a)] =  sinthlchalf * dT * ffterms[a]
        Hadr[( 1, 1, -1,  1, 'T', a)] =  costhlchalf * dT * ffterms[a]
        Hadr[(-1,-1, -1,  1, 'T', a)] = -costhlchalf * dT * ffterms[a]
        Hadr[( 1,-1,'t',  0, 'PT',a)] = -sinthlchalf * dT * ffterms[a]
        Hadr[(-1, 1,'t',  0, 'PT',a)] = -sinthlchalf * dT * ffterms[a]
        Hadr[( 1, 1,'t',  0, 'PT',a)] = -costhlchalf * dT * ffterms[a]
        Hadr[(-1,-1,'t',  0, 'PT',a)] =  costhlchalf * dT * ffterms[a]
        Hadr[( 1,-1,  0,'t', 'PT',a)] =  sinthlchalf * dT * ffterms[a]
        Hadr[(-1, 1,  0,'t', 'PT',a)] =  sinthlchalf * dT * ffterms[a]
        Hadr[( 1, 1,  0,'t', 'PT',a)] =  costhlchalf * dT * ffterms[a]
        Hadr[(-1,-1,  0,'t', 'PT',a)] = -costhlchalf * dT * ffterms[a]

    return Hadr

def get_W_ampls(Obs):
    """
    Some important comments on all the leptonic currents
    V or A current:
        - Terms with WC index V and A are equal.
        - Since only one term appears in the ampl for lwd. We fix lwd to 't' as done in hadronic V, A currents. Index 't' choosen because eta['t'] = 1.
        - Therefore Lept(lw, 't', 'V', ll) has total 8 components out of which 1 is zero.
    S or PS current:
        - Terms with WC index S and PS are equal.
        - In this case both lw and lwd are fake indeces (see above for reason), choosen to be 't' (see above) as done in hadronic S, PS currents.
        - Lept('t', 't', 'S', ll) has total 2 components out of which 1 is zero.
    T or PT current:
        - Terms with WC index T and PT are equal.
        - Lept(lw,lwd,'T',l) has total 32 components out of which 8 are zero.
    Func returns:
        Dict with index as tuples Lept[(lw, lwd, wc, ll)]
    """
    
    #q2 and angular terms
    q2              = Obs['q2'] #real
    phl             = Obs['w_phi_l'] #real
    v               = Sqrt(1. - Pow(Const(Mlep),2)/q2) #real
    cthl            = CastComplex(Obs['w_ctheta_l'])
    sinthl          = CastComplex(Sqrt(1.-Pow(cthl,2.)))
    I               = Complex(Const(0.), Const(1.))
    expPlusOneIphl  = Exp( I*CastComplex(phl))
    expMinusOneIphl = Exp(-I*CastComplex(phl))
    expMinusTwoIphl = Exp(-2.*I*CastComplex(phl))
    OneMinuscthl    = CastComplex((1. - cthl))
    OnePluscthl     = CastComplex((1. + cthl))
    al              = CastComplex(2. * Const(Mlep) * v)
    bl              = CastComplex(2. * Sqrt(q2)* v)
    complexsqrttwo  = CastComplex(sqrttwo)
    
    Lept = {}
    
    Lept[('t', 't', 'V',  1)] =  expMinusOneIphl * al
    Lept[(  0, 't', 'V',  1)] = -expMinusOneIphl * cthl * al
    Lept[(  1, 't', 'V',  1)] =  expMinusTwoIphl * sinthl * al/complexsqrttwo
    Lept[( -1, 't', 'V',  1)] = -sinthl * al/complexsqrttwo
    Lept[(  0, 't', 'V', -1)] =  sinthl * bl
    Lept[(  1, 't', 'V', -1)] =  expMinusOneIphl * OnePluscthl  * bl/complexsqrttwo
    Lept[( -1, 't', 'V', -1)] =  expPlusOneIphl  * OneMinuscthl * bl/complexsqrttwo
    
    Lept[('t', 't', 'S', 1)]  =  expMinusOneIphl * bl
    
    Lept[('t',  0, 'T', -1)]  =  sinthl * al
    Lept[(  1, -1, 'T', -1)]  =  sinthl * al
    Lept[(  0, -1, 'T', -1)]  =  expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
    Lept[('t', -1, 'T', -1)]  =  expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
    Lept[('t',  1, 'T', -1)]  =  expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
    Lept[(  0,  1, 'T', -1)]  = -expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
    Lept[('t',  0, 'T',  1)]  = -expMinusOneIphl * cthl * bl
    Lept[(  1, -1, 'T',  1)]  = -expMinusOneIphl * cthl * bl
    Lept[('t',  1, 'T',  1)]  =  expMinusTwoIphl * sinthl * bl/complexsqrttwo
    Lept[(  0,  1, 'T',  1)]  = -expMinusTwoIphl * sinthl * bl/complexsqrttwo
    Lept[('t', -1, 'T',  1)]  = -sinthl * bl/complexsqrttwo
    Lept[(  0, -1, 'T',  1)]  = -sinthl * bl/complexsqrttwo
    Lept[(  0,'t', 'T', -1)]  = -sinthl * al
    Lept[( -1,  1, 'T', -1)]  = -sinthl * al
    Lept[( -1,  0, 'T', -1)]  = -expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
    Lept[( -1,'t', 'T', -1)]  = -expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
    Lept[(  1,'t', 'T', -1)]  = -expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
    Lept[(  1,  0, 'T', -1)]  =  expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
    Lept[(  0,'t', 'T',  1)]  =  expMinusOneIphl * cthl * bl
    Lept[( -1,  1, 'T',  1)]  =  expMinusOneIphl * cthl * bl
    Lept[(  1,'t', 'T',  1)]  = -expMinusTwoIphl * sinthl * bl/complexsqrttwo
    Lept[(  1,  0, 'T',  1)]  =  expMinusTwoIphl * sinthl * bl/complexsqrttwo
    Lept[( -1,'t', 'T',  1)]  =  sinthl * bl/complexsqrttwo
    Lept[( -1,  0, 'T',  1)]  =  sinthl * bl/complexsqrttwo

    return Lept

# All the spins expressed in units of 1/2
def ParitySign(spin, parity): 
    jp =  1; jd =  0; pp =  1; Pd = -1
    s = 2*(((jp+jd-spin)/2+1) % 2)-1
    s *= (pp*Pd*parity)
    return s

def get_freeparams_dict(flv, Wilcoef, Resns, FFact):
    freeparam    = {}
    for WC in WCs: 
        wilcoef = Wilcoef[WC]
        for FF in FFs:
            for res in resns:
                rescoup = Resns[res]['coupl']
                if 'kpi' in res: 
                    freeparam[( 1, res, 0, WC, FF)] = Polar(FFact[FF] * rescoup[0] * wilcoef[0], rescoup[1] + flv * wilcoef[1])
                    freeparam[( 1, res, 2, WC, FF)] = Polar(FFact[FF] * rescoup[2] * wilcoef[0], rescoup[3] + flv * wilcoef[1])
                    freeparam[(-1, res, 0, WC, FF)] = Polar(FFact[FF] * rescoup[4] * wilcoef[0], rescoup[5] + flv * wilcoef[1])
                    freeparam[(-1, res,-2, WC, FF)] = Polar(FFact[FF] * rescoup[6] * wilcoef[0], rescoup[7] + flv * wilcoef[1])
                elif 'ppi' in res or 'pk' in res:
                    #Parity conserved in R decay, coup_{lr_lpd=-1} = sign * coup_{lr_lpd=1}, where sign = (-1)^(jp+jk-jr)*P_r*P_p*P_K
                    sign = ParitySign(Resns[res]["spin"], Resns[res]["parity"]) 
                    freeparam[( 1, res, 1, WC, FF)] = Polar(FFact[FF] * rescoup[0] * wilcoef[0], rescoup[1] + flv * wilcoef[1])
                    freeparam[( 1, res,-1, WC, FF)] = Polar(FFact[FF] * rescoup[2] * wilcoef[0], rescoup[3] + flv * wilcoef[1])
                    freeparam[(-1, res, 1, WC, FF)] = sign * freeparam[( 1, res, 1, WC, FF)]
                    freeparam[(-1, res,-1, WC, FF)] = sign * freeparam[( 1, res,-1, WC, FF)]

    return freeparam

def get_dens(Lbampl, Wampl, Lcampl, freeparams, polr, switches):
    Dens = {}
    i = 0
    for lLb in lLbs:
        for ll in lls:
            for lp in lps:
                ampls_coherent = CastComplex(0.)
                for lLc in lLcs:
                    for lw in lws:
                        for lwd in lws:
                            for WC in WCs:
                                for FF in FFs:
                                    for lpd in lps:
                                        for resn in resns:
                                            for lR in lRs:
                                                lbindx = (lLb,lLc,lw,lwd,WC,FF)
                                                windx  = (lw,lwd,ReplaceWC(WC),ll)
                                                lcindx = (lLc,lp,lpd,resn,lR)
                                                fpindx = (lpd, resn, lR, WC, FF)
                                                if lbindx not in Lbampl or windx not in Wampl or lcindx not in Lcampl: 
                                                    continue

                                                amp  = CastComplex(sqrttwo*GF*Vcb*switches[resn]*signWC[WC]*eta[lw]*eta[lwd])
                                                amp *= CastComplex(Lbampl[lbindx]) * Wampl[windx] * Lcampl[lcindx]
                                                amp *= freeparams[fpindx]
                                                ampls_coherent += amp
                                                i   += 1
                                                if i % 1000 == 0: print('Getting term', i)

                Dens[(lLb, ll, lp)] = Density(ampls_coherent)

    Lb_poshel = 0.5 * (1. + polr) * (Dens[( 1,1,1)] + Dens[( 1,-1,-1)] + Dens[( 1,-1, 1)] + Dens[( 1, 1, -1)])
    Lb_neghel = 0.5 * (1. - polr) * (Dens[(-1,1,1)] + Dens[(-1,-1,-1)] + Dens[(-1,-1, 1)] + Dens[(-1, 1, -1)])
    return Lb_poshel + Lb_neghel

def ReplaceWC(strg):
    strg_wc = strg.replace('A' , 'V')
    strg_wc = strg_wc.replace('PS', 'S')
    strg_wc = strg_wc.replace('PT', 'T')
    return strg_wc
    
def delta_func(p4mom, mass, atol):
    cond = tf.less(mass - TimeComponent(p4mom)                    , Const(atol))
    cond = tf.logical_and(cond, tf.less(XComponent(p4mom)         , Const(atol)))
    cond = tf.logical_and(cond, tf.less(YComponent(p4mom)         , Const(atol)))
    cond = tf.logical_and(cond, tf.less(ZComponent(p4mom)         , Const(atol)))
    return cond

def EnergyMomConserv(dens, Vars, atol):
    lb_p4mom_w  = MakeFourVector(Vars['lb_p3mag_lc'],    Vars['lb_ctheta_w'],  Vars['lb_phi_w']        , Vars['q2'])
    lb_p4mom_lc = MakeFourVector(Vars['lb_p3mag_lc'],-1.*Vars['lb_ctheta_w'],  Vars['lb_phi_w']+Pi()   , Const(MLc)**2)
    lc_p4mom_r  = MakeFourVector(Vars['lc_p3mag_pi'],    Vars['lc_ctheta_r'],  Vars['lc_phi_r']        , Vars['m2pk'])
    lc_p4mom_pi = MakeFourVector(Vars['lc_p3mag_pi'],-1.*Vars['lc_ctheta_r'],  Vars['lc_phi_r']+Pi()   , Const(Mpi)**2)
    r_p4mom_p   = MakeFourVector(Vars['r_p3mag_p'],      Vars['r_ctheta_p'],   Vars['r_phi_p']         , Const(Mp)**2)
    r_p4mom_k   = MakeFourVector(Vars['r_p3mag_p'],  -1.*Vars['r_ctheta_p'],   Vars['r_phi_p']+Pi()    , Const(Mk)**2)
    w_p4mom_l   = MakeFourVector(Vars['w_p3mag_l'],      Vars['w_ctheta_l'],   Vars['w_phi_l']         , Const(Mlep)**2)
    w_p4mom_nu  = MakeFourVector(Vars['w_p3mag_l'],  -1.*Vars['w_ctheta_l'],   Vars['w_phi_l']+Pi()    , 0.)
    cond        = delta_func(lb_p4mom_w+lb_p4mom_lc, Const(MLb), atol=atol)
    cond        = tf.logical_and(cond, delta_func(lc_p4mom_r+lc_p4mom_pi, Const(MLc), atol=atol))
    cond        = tf.logical_and(cond, delta_func(r_p4mom_p+r_p4mom_k,  Sqrt(Vars['m2pk']), atol=atol))
    cond        = tf.logical_and(cond, delta_func(w_p4mom_l+w_p4mom_nu, Sqrt(Vars['q2']),   atol=atol))
    dens        = tf.where(cond, dens, Zeros(dens))
    return dens

def dPhiTwoBody(mijsq, misq, mjsq):
    return 1./(2.**4 * Pi()**2) * pvecMag(mijsq, misq, mjsq)/Sqrt(mijsq)

def getPhasespaceTerm(Obs):
    #1/(8*pi^3) * dm2pk * dq2 * dm2rpi * dphi(Lb) * dPhi(W) * dPhi(Lc) * dPhi(r) -> Integrate[f(m2rpi)*BW(Lc)*BWbar(Lc)*dm2rpi] = pi/(mLc*GLc) * f(Lc)
    phsspace  = 1./(2. * Const(MLb) * 8. * Pi()**3) * dPhiTwoBody(Const(MLb)**2, Const(MLc)**2, Obs['q2'])   #Lb -> Lc W
    phsspace *= dPhiTwoBody(Obs['q2'], Const(Mlep)**2, 0.)   #W  ->  l nu
    phsspace *= Pi() * Tau_Lc/Const(MLc) * dPhiTwoBody(Const(MLc)**2, Obs['m2pk'], Const(Mpi)**2) #Lc ->  r pi
    phsspace *= dPhiTwoBody(Obs['m2pk'], Const(Mp)**2, Const(Mk)**2)   #r  ->  p k
    return phsspace

def getDynamicTerm(x, obsLb, flv, Wilcoef, Resns, FFact, pol, switches):
    fp_lpd_res_lr_wc_ff          = get_freeparams_dict(flv, Wilcoef, Resns, FFact) 
    print(len(fp_lpd_res_lr_wc_ff.keys()))             #4*nres*6*20 = 960
    LbDecay_lLb_lLc_lw_lwd_WC_FF = get_Lb_ampls(obsLb) 
    print(len(LbDecay_lLb_lLc_lw_lwd_WC_FF.keys()))    #192 
    WDecay_lw_lwd_WC_ll          = get_W_ampls(obsLb)  
    print(len(WDecay_lw_lwd_WC_ll.keys()))             #32
    LcDecay_lLc_lp_lpd_res_lR    = get_Lc_ampls(obsLb, Resns) 
    print(len(LcDecay_lLc_lp_lpd_res_lR.keys()))       #2*2*nres*4 = 32
    dens_d                        = get_dens(LbDecay_lLb_lLc_lw_lwd_WC_FF, WDecay_lw_lwd_WC_ll, LcDecay_lLc_lp_lpd_res_lR, fp_lpd_res_lr_wc_ff, pol, switches)
    return dens_d

def FixParam_Integral_val(model_intg, mcsize, niter, x, sess, phsp):
    return sum([sess.run(model_intg, feed_dict={x: sess.run(phsp.UnfilteredSampleGraph(size=mcsize))}) for i in range(niter)])

def Res_weights(wgts, x, sampl, v, switches):
    weights = {}
    for res in resns:
        fdict = {}
        fdict[x] = sampl
        for k in resns: fdict[switches[k]] = 0. #set all to zero
        fdict[switches[res]] = 1. #set one we want to 1
        v1 = sess.run(wgts, feed_dict=fdict)
        weights[res] = v1/v

    return weights

def genmodel(Sess, model, ph, smpl, php, Sws, Seed, Resns, fname, Size, Obsv):
    majorant    = EstimateMaximum(Sess, model, ph, smpl)*2.0
    fit_sample  = RunToyMC(Sess, model, ph, php, Size, majorant, chunk = 700000, switches = Sws, seed = Seed, interferences = False)
    ditc        = Sess.run(Obsv, feed_dict = {ph: fit_sample})
    df          = pd.DataFrame.from_dict(ditc)
    for j, Res in enumerate(Resns[::-1]): df['wght_'+Res.replace('-','_')] = fit_sample[:,-j-1]
    to_root(df, fname, key='tree', store_index=False)
############################
