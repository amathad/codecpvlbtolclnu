import math 
import sys 
import operator 
import itertools 
import numpy as np
import tensorflow as tf 
import pandas as pd 
import matplotlib
import matplotlib.pyplot as plt
import iminuit
from iminuit import Minuit 

#AmpliTF imports 
import amplitf.kinematics as atfk 
import amplitf.dynamics as atfd
from amplitf import interface as atfi
from amplitf.phasespace.rectangular_phasespace import RectangularPhaseSpace
import amplitf.likelihood as atfl

#TFA2 imports 
import tfa.toymc as tft
import tfa.plotting as tfp
import tfa.optimisation as tfo

def get_resonances(resns):
    resonances = {}
    for res in resns:
        if res == "Kstar-kpi":
                resonances["Kstar-kpi"] = {
                                        "lineshape" : "BW_lineshape",  
                                        "mass"      : atfi.const(0.892), 
                                        "width"     : atfi.const(0.051),  
                                        "spin"      : 2., 
                                        "parity"    : -1., 
                                        "coupl"     : [              
                                                        #tfo.FitParameter(res+"_Mag_1", 1.0    ,   0. , 20. , 0.01),     tfo.FitParameter(res+"_Phs_1", 0.     , -atfi.pi() , atfi.pi(), 0.01), 
                                                        #tfo.FitParameter(res+"_Mag_2", 0.     ,   0. , 20. , 0.01),     tfo.FitParameter(res+"_Phs_2", 0.     , -atfi.pi() , atfi.pi(), 0.01), 
                                                        #tfo.FitParameter(res+"_Mag_3", 0.     ,   0. , 20. , 0.01),     tfo.FitParameter(res+"_Phs_3", 0.     , -atfi.pi() , atfi.pi(), 0.01), 
                                                        #tfo.FitParameter(res+"_Mag_4", 0.     ,   0. , 20. , 0.01),     tfo.FitParameter(res+"_Phs_4", 0.     , -atfi.pi() , atfi.pi(), 0.01), 
                                                        atfi.const(1.0), atfi.const(0.),
                                                        atfi.const(0. ), atfi.const(0.), 
                                                        atfi.const(0. ), atfi.const(0.), 
                                                        atfi.const(0. ), atfi.const(0.), 
                                                      ]   
                }
        elif res == "Kstar-kpi2":
                resonances["Kstar-kpi2"] = {
                                        "lineshape" : "BW_lineshape",  
                                        "mass"      : atfi.const(0.850), 
                                        "width"     : atfi.const(0.051),  
                                        "spin"      : 2., 
                                        "parity"    : -1., 
                                        "coupl"     : [              
                                                        #tfo.FitParameter(res+"_Mag_1", 1.0    ,   0. , 20. , 0.01), tfo.FitParameter(res+"_Phs_1", atfi.pi()/2., -atfi.pi() , atfi.pi(), 0.01), 
                                                        #tfo.FitParameter(res+"_Mag_2", 0.     ,   0. , 20. , 0.01), tfo.FitParameter(res+"_Phs_2", 0., -atfi.pi() , atfi.pi(), 0.01), 
                                                        #tfo.FitParameter(res+"_Mag_3", 0.     ,   0., 20.  , 0.01), tfo.FitParameter(res+"_Phs_3", 0., -atfi.pi() , atfi.pi(), 0.01), 
                                                        #tfo.FitParameter(res+"_Mag_4", 0.     ,   0. , 20. , 0.01), tfo.FitParameter(res+"_Phs_4", 0., -atfi.pi() , atfi.pi(), 0.01), 
                                                        atfi.const(1.0), atfi.const(math.pi/2.), 
                                                        atfi.const(0. ), atfi.const(0.), 
                                                        atfi.const(0. ), atfi.const(0.), 
                                                        atfi.const(0. ), atfi.const(0.), 
                                                      ]   
                }
    return resonances 
