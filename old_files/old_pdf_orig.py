#import few packages
import math
from re import S
import sys, os
import tensorflow as tf
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # Do not use GPU for tensor flow
import numpy as np
import matplotlib.pyplot as plt
import pprint
import uproot

#amplitf
home = os.getenv('HOME')
sys.path.append(home+"/Packages/AmpliTF/")
import amplitf.interface as atfi
import amplitf.kinematics as atfk
import amplitf.dynamics as atfd
from amplitf.phasespace.rectangular_phasespace import RectangularPhaseSpace
import amplitf.likelihood as atfl

#tfa2
sys.path.append(home+"/Packages/TFA2/")
import tfa.optimisation as tfo
import tfa.toymc as tft
import tfa.plotting as tfp

#get current directory
fit_dir=os.path.dirname(os.path.abspath(__file__))

#define the complex
I = atfi.complex(atfi.const(0), atfi.const(1))

@atfi.function
def breit_wigner_lineshape_edited(
    m2,
    m0,
    gamma0,
    ma,
    mb,
    mc,
    md,
    dr,
    dd,
    lr,
    ld,
    barrier_factor=True,
    ma0=None,
    md0=None,
    ):
    """
    Breit-Wigner amplitude with Blatt-Weisskopf formfactors, mass-dependent width and orbital barriers
    """
    m = atfi.sqrt(m2)
    q = atfk.two_body_momentum(md, m, mc)
    q0 = atfk.two_body_momentum(md if md0 is None else md0, m0, mc)
    p = atfk.two_body_momentum(m, ma, mb)
    p0 = atfk.two_body_momentum(m0, ma if ma0 is None else ma0, mb)
    ffr = atfd.blatt_weisskopf_ff(p, p0, dr, lr)
    ffd = atfd.blatt_weisskopf_ff(q, q0, dd, ld)
    width = atfd.mass_dependent_width(m, m0, gamma0, p, p0, ffr, lr)
    bw = atfd.relativistic_breit_wigner(m2, m0, width)
    ff = ffr * ffd
    if barrier_factor:
        b1 = atfd.orbital_barrier_factor(p, p0, lr)
        b2 = atfd.orbital_barrier_factor(q, q0, ld)
        ff *= b1 * b2
    #return bw * atfi.complex(ff, atfi.const(0.0))
    return bw 

########### Make some common functions
def MakeFourVector(pmag, costheta, phi, msq): 
    """Make 4-vec given magnitude, cos(theta), phi and mass"""
    sintheta = atfi.sqrt(1. - costheta**2) #theta 0 to pi => atfi.sin always pos
    px = pmag * sintheta * atfi.cos(phi)
    py = pmag * sintheta * atfi.sin(phi)
    pz = pmag * costheta
    E  = atfi.sqrt(msq + pmag**2)
    return atfk.lorentz_vector(atfk.vector(px, py, pz), E)

def pvecMag(Msq, m1sq, m2sq): 
    """Momentum mag of (1 or 2) in M rest frame"""
    kallen = atfi.sqrt(Msq**2 + m1sq**2 + m2sq**2 - 2.*(Msq*m1sq + Msq*m2sq + m1sq*m2sq))
    const  = tf.cast(2. * atfi.sqrt(Msq), dtype = tf.float64)
    return  kallen / const 

def InvRotateAndBoostFromRest(m_p4mom_p1, m_p4mom_p2, gm_phi_m, gm_ctheta_m, gm_p4mom_m):
    """
    Function that gives 4momenta of the particles in the grand mother helicity frame

    m_p4mom_p1: particle p1 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
    m_p4mom_p2: particle p2 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
    gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
    gm_4mom_m: m 4mom in gm's helicity frame
    --------------
    Checks done such as 
        - (lc_p4mom_p+lc_p4mom_k == lc_p4mom_r), 
        - Going in reverse i.e. boost lc_p4mom_p into R rest frame (using lc_p4mom_r) and rotating into R helicity frame gives back r_p4mom_p 
            - i.e. lc_p4mom_p_opp = atfk.rotate_lorentz_vector(atfk.boost_to_rest(lc_p4mom_p, lc_p4mom_r), -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)) where lc_p4mom_p_opp == r_p4mom_p
        - Rotate and boost, instead of boost and rotate should give the same answer, checked (Does not agree with TFA implementation though)
            -i.e. lc_p4mom_prot  = atfk.rotate_lorentz_vector(lc_p4mom_p, -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)
            -i.e. lc_p4mom_krot  = atfk.rotate_lorentz_vector(lc_p4mom_k, -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)
            -i.e. lc_p4mom_p2    = atfk.boost_to_rest(lc_p4mom_prot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_p2 == r_p4mom_p
            -i.e. lc_p4mom_k2    = atfk.boost_to_rest(lc_p4mom_krot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_k2 == r_p4mom_k
    """
    #First: Rotate particle p 3mom defined in mother m helicity frame such that z now points along z in grand mother gm helicity frame 
    #(i.e interpreted as gm mom in it's grand-grand-mother ggm rest frame)
    m_p4mom_p1_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p1, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
    m_p4mom_p2_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p2, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
    #Second: Boost particle p 4mom from mother m's rest frame to gm helicity frame. This is done using boost vector from gm_p4mom_m  [checked: E(m_p4mom_p1_zmmom + m_p4mom_p2_zmmom) == Mass_m]
    gm_p4mom_p1            = atfk.boost_from_rest(m_p4mom_p1_zgmmom, gm_p4mom_m)
    gm_p4mom_p2            = atfk.boost_from_rest(m_p4mom_p2_zgmmom, gm_p4mom_m)
    return gm_p4mom_p1, gm_p4mom_p2

def HelAngles3Body(pa, pb, pc):
    """Get three-body helicity angles"""
    theta_r  = atfi.acos(-atfk.z_component(pc) / atfk.norm(atfk.spatial_components(pc)))
    phi_r    = atfi.atan2(-atfk.y_component(pc), -atfk.x_component(pc))
    pa_prime = atfk.rotate_lorentz_vector(pa, -phi_r, -theta_r, phi_r)
    pb_prime = atfk.rotate_lorentz_vector(pb, -phi_r, -theta_r, phi_r)
    pa_prime2= atfk.boost_to_rest(pa_prime, pa_prime+pb_prime)
    theta_a  = atfi.acos(atfk.z_component(pa_prime2) / atfk.norm(atfk.spatial_components(pa_prime2)))
    phi_a    = atfi.atan2(atfk.y_component(pa_prime2), atfk.x_component(pa_prime2))
    return (theta_r, phi_r, theta_a, phi_a)

def RotateAndBoostToRest(gm_p4mom_p1, gm_p4mom_p2, gm_phi_m, gm_ctheta_m):
    """
    gm_p4mom_p1: particle p1 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
    gm_p4mom_p2: particle p2 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
    gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
    """
    #First:  Rotate particle p 3mom defined in grand mother gm helicity frame such that z now points along z of mother m in gm rest frame.
    gm_p4mom_p1_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p1, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
    gm_p4mom_p2_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p2, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
    #Second: Boost particle p 4mom to mother m i.e. p1p2 rest frame
    gm_p4mom_m             = gm_p4mom_p1_zmmom+gm_p4mom_p2_zmmom
    m_p4mom_p1             = atfk.boost_to_rest(gm_p4mom_p1_zmmom, gm_p4mom_m)
    m_p4mom_p2             = atfk.boost_to_rest(gm_p4mom_p2_zmmom, gm_p4mom_m)
    return m_p4mom_p1, m_p4mom_p2
########

#define the class for Lb->Lclnu decay
class LcTopKpi_Model_OLD:
    """Class that defines the Lc->pKpi decay"""

    def __init__(self, MLc, Mp, Mk, Mpi, res_list, float_params ):
        """initialise some variables"""
        #Masses of particles involved 
        self.MLc     = MLc
        self.Mp      = Mp
        self.Mk      = Mk
        self.Mpi     = Mpi

        #Lists that will serve as index for incoherent sum of amplitudes (defined in units of 1/2)
        self.lLbs   = [1, -1]
        self.lLcs   = [1, -1]
        self.lls    = [1, -1]
        self.lps    = [1, -1]

        #Intrisic spins
        self.Jlam_c            = 1
        self.Jp                = 1
        
        #define limits and rectangular phase space i.e. (qsq, costhmu) space
        phsp_limits  = [((self.Mk + self.Mpi)**2 , (self.MLc - self.Mp)**2 ) ]
        phsp_limits += [(-math.pi, math.pi)]
        phsp_limits += [(0. , math.pi)]
        phsp_limits += [(-math.pi, math.pi)]
        phsp_limits += [(0. , math.pi)]
        self.phase_space = RectangularPhaseSpace(ranges=phsp_limits)

        #get all the fit params
        self.res_list      = res_list
        self.fit_params    = self.get_fit_params(res_list, float_params)

        #import resonance dictionary
        self.switches      = len(res_list) * [1]
        self.resn          = self.get_resonances(res_list)

    def prepare_data(self,x):
        """Function that calculates the variables for phase space variables and Lc decay amplitude variables"""

        #Store Lc phase_space Varsiables
        Vars = {}
        m2_kpi            = x[:,0]
        kstar_phi_lc      = x[:,1]
        kstar_theta_lc    = x[:,2]
        k_phi_kstar       = x[:,3]
        k_theta_kstar     = x[:,4]

        kstar_ctheta_lc               = tf.cos(kstar_theta_lc)
        k_ctheta_kstar                = tf.cos(k_theta_kstar)
        p_ctheta_lc                   = -kstar_ctheta_lc
        p_theta_lc                    = tf.acos(p_ctheta_lc)
        p_phi_lc                      = kstar_phi_lc + atfi.pi()
        pi_ctheta_kstar               = -k_ctheta_kstar
        pi_phi_kstar                  = k_phi_kstar + atfi.pi()

        #4-momenta K and pi in K* rest frame
        k_p4mom_kstar    = MakeFourVector(pvecMag(m2_kpi, self.Mk**2, self.Mpi**2), k_ctheta_kstar, k_phi_kstar, self.Mk**2)
        pi_p4mom_kstar   = MakeFourVector(pvecMag(m2_kpi, self.Mpi**2, self.Mk**2), pi_ctheta_kstar, pi_phi_kstar, self.Mpi**2)

        #4-momenta in Lc rest frame 
        kstar_p4mom_lc   = MakeFourVector(pvecMag(self.MLc**2, m2_kpi, self.Mp**2), kstar_ctheta_lc, kstar_phi_lc, m2_kpi)
        p_p4mom_lc       = MakeFourVector(pvecMag(self.MLc**2, self.Mp**2, m2_kpi), p_ctheta_lc, p_phi_lc, self.Mp**2)
        #boost and rotate 4-momenta K and pi in K* rest frame to Lc rest frame 
        k_p4mom_lc       = InvRotateAndBoostFromRest(k_p4mom_kstar, pi_p4mom_kstar, kstar_phi_lc, kstar_ctheta_lc, kstar_p4mom_lc)[0]
        pi_p4mom_lc      = InvRotateAndBoostFromRest(k_p4mom_kstar, pi_p4mom_kstar, kstar_phi_lc, kstar_ctheta_lc, kstar_p4mom_lc)[1]
        
        #k and pi angles in lc rest frame 
        pi_phi_lc        = atfk.spherical_angles(pi_p4mom_lc)[1]
        k_phi_lc         = atfk.spherical_angles(k_p4mom_lc)[1]     

        #Delta resonance angles 
        delta_theta_lc    =  HelAngles3Body(p_p4mom_lc, pi_p4mom_lc, k_p4mom_lc)[0]
        delta_phi_lc      =  HelAngles3Body(p_p4mom_lc, pi_p4mom_lc, k_p4mom_lc)[1]
        p_theta_delta     =  HelAngles3Body(p_p4mom_lc, pi_p4mom_lc, k_p4mom_lc)[2]
        p_phi_delta       =  HelAngles3Body(p_p4mom_lc, pi_p4mom_lc, k_p4mom_lc)[3]
        pi_theta_delta    =  HelAngles3Body(pi_p4mom_lc, p_p4mom_lc, k_p4mom_lc)[2]
        pi_phi_delta      =  HelAngles3Body(pi_p4mom_lc, p_p4mom_lc, k_p4mom_lc)[3]

        #Lambda* resonance angles 
        lambda_theta_lc  =  HelAngles3Body(p_p4mom_lc, k_p4mom_lc, pi_p4mom_lc)[0]
        lambda_phi_lc    =  HelAngles3Body(p_p4mom_lc, k_p4mom_lc, pi_p4mom_lc)[1]
        p_theta_lambda   =  HelAngles3Body(p_p4mom_lc, k_p4mom_lc, pi_p4mom_lc)[2]
        p_phi_lambda     =  HelAngles3Body(p_p4mom_lc, k_p4mom_lc, pi_p4mom_lc)[3]

        #invariant masses
        m2_ppi           = (p_p4mom_lc[:,3] + pi_p4mom_lc[:,3])**2 - ( (p_p4mom_lc[:,0]+pi_p4mom_lc[:,0])**2 + (p_p4mom_lc[:,1]+pi_p4mom_lc[:,1])**2 + (p_p4mom_lc[:,2]+pi_p4mom_lc[:,2])**2 )
        m2_pK            = (p_p4mom_lc[:,3] + k_p4mom_lc[:,3])**2 - ( (p_p4mom_lc[:,0]+k_p4mom_lc[:,0])**2 + (p_p4mom_lc[:,1]+k_p4mom_lc[:,1])**2 + (p_p4mom_lc[:,2]+k_p4mom_lc[:,2])**2 )

        #phase space variables 
        Vars['m2_kpi']                = m2_kpi
        Vars['kstar_phi_lc']          = kstar_phi_lc
        Vars['kstar_theta_lc']        = kstar_theta_lc 
        Vars['k_phi_kstar']           = k_phi_kstar 
        Vars['k_theta_kstar']         = k_theta_kstar
        
        #p, k, pi azimuthal angles in lc rest frame 
        Vars['p_phi_lc']              = p_phi_lc
        Vars['pi_phi_lc']             = pi_phi_lc  
        Vars['k_phi_lc']              = k_phi_lc

        #Delta resonance variables
        Vars['m2_ppi']                = m2_ppi
        Vars['delta_theta_lc']        = delta_theta_lc
        Vars['delta_phi_lc']          = delta_phi_lc
        Vars['p_theta_delta']         = p_theta_delta
        Vars['p_phi_delta']           = p_phi_delta 
        Vars['pi_theta_delta']        = pi_theta_delta
        Vars['pi_phi_delta']          = pi_phi_delta 

        #Lambda* resonance variables
        Vars['m2_pK']                 = m2_pK
        Vars['lambda_theta_lc']       = lambda_theta_lc
        Vars['lambda_phi_lc']         = lambda_phi_lc
        Vars['p_theta_lambda']        = p_theta_lambda
        Vars['p_phi_lambda']          = p_phi_lambda

        #Amplitude correction variables 
        Vars['alpha_D']                 = tf.math.abs(Vars['p_phi_lc'] - Vars['pi_phi_lc'])
        Vars['alpha_L']                 = tf.math.abs(Vars['p_phi_lc'] - Vars['k_phi_lc'])

        return Vars
    
    ''' For decay A to BC - Function to determine the angular momentum of L(BC) '''
    # use J(A) = J(B) +/- J(C) +/- L(BC)
    # where total angular momenta are definened in units of 1/2
    def _l_bc(self, ja , jb , jc):
        l_bc1 = ja/2 - jb/2 - jc/2
        l_bc2 = ja/2 - jb/2 + jc/2
        l_bc  = np.min( [l_bc1 , l_bc2] )
        return l_bc

    def _l_bc_k(self, ja , jb , jc):
        l_bc1 = ja/2 - jb/2 - jc/2
        l_bc2 = ja/2 - jb/2 + jc/2
        l_bc  = np.min( [l_bc1 , l_bc2] )
        return np.max([0, l_bc])

    def _l_bc_strong(self, ja , jb , jc, parity):
        l_bc1 = ja/2 - jb/2 - jc/2
        l_bc2 = ja/2 - jb/2 + jc/2
        l_bc  = [l_bc1 , l_bc2]
        for i in l_bc:
            eta = (-1)**(i+1)
            if parity == eta:
                return np.abs(i)

    def get_fit_params(self, res_list, float_params):
        
        f_params = {}
        if 'Kstar_kpi(892)' in res_list:
            f_params["Kstar_kpi(892)_mass"]   = tfo.FitParameter("Kstar_kpi(892)_mass"  , 0.89581  ,   0. , 10.  , 0.01)
            f_params["Kstar_kpi(892)_width"]  = tfo.FitParameter("Kstar_kpi(892)_width" , 0.0474   ,   0.001 , 10.  , 0.01)
            f_params["Kstar_kpi(892)_real_1"] = tfo.FitParameter("Kstar_kpi(892)_real_1", 1.192614 ,   -10. , 10.  , 0.01)
            f_params["Kstar_kpi(892)_imag_1"] = tfo.FitParameter("Kstar_kpi(892)_imag_1", -1.025814,   -10. , 10.  , 0.01)
            f_params["Kstar_kpi(892)_real_2"] = tfo.FitParameter("Kstar_kpi(892)_real_2", -3.141446,   -10. , 10.  , 0.01)
            f_params["Kstar_kpi(892)_imag_2"] = tfo.FitParameter("Kstar_kpi(892)_imag_2", -3.29341 ,   -10. , 10.  , 0.01)
            f_params["Kstar_kpi(892)_real_3"] = tfo.FitParameter("Kstar_kpi(892)_real_3", -0.727145,   -10. , 10.  , 0.01)
            f_params["Kstar_kpi(892)_imag_3"] = tfo.FitParameter("Kstar_kpi(892)_imag_3", -4.155027,   -10. , 10.  , 0.01)

        if 'D(1232)' in res_list:
            f_params["D(1232)_mass"]  = tfo.FitParameter("D(1232)_mass"   , 1.232  , 0.  , 10. , 0.01)
            f_params["D(1232)_width"] = tfo.FitParameter("D(1232)_width"  , 0.117  , 0.01  , 10. , 0.01)
            f_params["D(1232)_real_1"] = tfo.FitParameter("D(1232)_real_1", -6.778191  , -10.  , 10. , 0.01)
            f_params["D(1232)_imag_1"] = tfo.FitParameter("D(1232)_imag_1", 3.051805   , -10.  , 10. , 0.01)
            f_params["D(1232)_real_2"] = tfo.FitParameter("D(1232)_real_2", -12.987193 , -10.  , 10. , 0.01)
            f_params["D(1232)_imag_2"] = tfo.FitParameter("D(1232)_imag_2",  4.528336  , -10.  , 10. , 0.01)

        if 'L(1520)' in res_list:
            f_params["L(1520)_mass"]   = tfo.FitParameter("L(1520)_mass"  , 1.5195 ,   0. , 10. , 0.01)
            f_params["L(1520)_width"]  = tfo.FitParameter("L(1520)_width" , 0.0156 ,   0.001 , 10. , 0.01)
            f_params["L(1520)_real_1"] = tfo.FitParameter("L(1520)_real_1", 1.     ,   -10. , 10. , 0.01)
            f_params["L(1520)_imag_1"] = tfo.FitParameter("L(1520)_imag_1", 0.     ,   -10. , 10. , 0.01)
            f_params["L(1520)_real_2"] = tfo.FitParameter("L(1520)_real_2", 1.     ,   -10. , 10. , 0.01)
            f_params["L(1520)_imag_2"] = tfo.FitParameter("L(1520)_imag_2", 0.     ,   -10. , 10. , 0.01)
            #f_params["L(1520)_real_1"] = tfo.FitParameter("L(1520)_real_1", 0.293998 ,   -10. , 10. , 0.01)
            #f_params["L(1520)_imag_1"] = tfo.FitParameter("L(1520)_imag_1", 0.044324 ,   -10. , 10. , 0.01)
            #f_params["L(1520)_real_2"] = tfo.FitParameter("L(1520)_real_2", -0.160687,   -10. , 10. , 0.01)
            #f_params["L(1520)_imag_2"] = tfo.FitParameter("L(1520)_imag_2", 1.498833 ,   -10. , 10. , 0.01)

        for k in list(f_params.keys()):
            if k in list(f_params.keys()) and k in float_params:
                f_params[k].float()
            else:
                f_params[k].fix()
            
        return f_params

    def get_resonances(self, res_list):
        resonances = {}
        for res in res_list:
            if res == "Kstar_kpi(892)":
                    resonances["Kstar_kpi(892)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: self.fit_params["Kstar_kpi(892)_mass"](), 
                                            "width"     : lambda: self.fit_params["Kstar_kpi(892)_width"](),  
                                            "spin"      : 2, 
                                            "parity"    : -1, 
                                            "coupl"     : [     
                                                            lambda: atfi.complex( atfi.const(1.) , atfi.const(0.) ), 
                                                            lambda: atfi.complex( self.fit_params["Kstar_kpi(892)_real_1"](), self.fit_params["Kstar_kpi(892)_imag_1"]()),       
                                                            lambda: atfi.complex( self.fit_params["Kstar_kpi(892)_real_2"](), self.fit_params["Kstar_kpi(892)_imag_2"]()), 
                                                            lambda: atfi.complex( self.fit_params["Kstar_kpi(892)_real_3"](), self.fit_params["Kstar_kpi(892)_imag_3"]()),
                                                        ],
                                            "l_lamc"    : int( self._l_bc_k( 2 , self.Jp , self.Jlam_c ) ),
                                            "l_res"     : int( 2 / 2 ),
                    }

            elif res == "Kstar_kpi0(700)":
                    resonances["Kstar_kpi0(700)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(0.824), 
                                            "width"     : lambda: atfi.const(0.478),  
                                            "spin"      : 0, 
                                            "parity"    : 1, 
                                            "gamma"     : atfi.const(0.94106),
                                            "alpha"     : atfi.const(0.),
                                            "coupl"     : [  
                                                            lambda: atfi.complex(atfi.const(0.068908), atfi.const(2.521444)),
                                                            lambda: atfi.complex(atfi.const(-2.68563), atfi.const(0.03849)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc_k( 0 , self.Jp , self.Jlam_c ) ),
                                            "l_res"     : int( 0 / 2 ), 
                    }

            elif res == "Kstar_kpi0(1430)":
                    resonances["Kstar_kpi0(1430)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.375), 
                                            "width"     : lambda: atfi.const(0.190),  
                                            "spin"      : 0, 
                                            "parity"    : 1, 
                                            "gamma"     : atfi.const(0.020981),
                                            "alpha"     : atfi.const(0.),
                                            "coupl"     : [     
                                                            lambda: atfi.complex(atfi.const(-6.71516), atfi.const(10.479411)),
                                                            lambda: atfi.complex(atfi.const(0.219754 ), atfi.const(8.741196)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc_k( 0 , self.Jp , self.Jlam_c ) ),
                                            "l_res"     : int( 0 / 2 ), 
                    }
            
            elif res == "D(1232)":
                    resonances["D(1232)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: self.fit_params["D(1232)_mass"](), 
                                            "width"     : lambda: self.fit_params["D(1232)_width"](),  
                                            "spin"      : 3, 
                                            "parity"    : 1, 
                                            "coupl"     : [    
                                                            lambda: atfi.complex( self.fit_params["D(1232)_real_1"](), self.fit_params["D(1232)_imag_1"]()),       
                                                            lambda: atfi.complex( self.fit_params["D(1232)_real_2"](), self.fit_params["D(1232)_imag_2"]()),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 3 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0, 3 , 1) ), 
                    }

            elif res == "D(1600)":
                    resonances["D(1600)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.64), 
                                            "width"     : lambda: atfi.const(0.3),  
                                            "spin"      : 3, 
                                            "parity"    : 1, 
                                            "coupl"     : [        
                                                            lambda: atfi.complex(atfi.const(11.401585), atfi.const(-3.125511)),
                                                            lambda: atfi.complex(atfi.const(6.729211), atfi.const(-1.002383)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 3 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp, 0, 3 , 1) ),    
                    }
            
            elif res == "D(1700)":
                    resonances["D(1700)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.69), 
                                            "width"     : lambda: atfi.const(0.38),  
                                            "spin"      : 3, 
                                            "parity"    : -1, 
                                            "coupl"     : [     
                                                            lambda: atfi.complex(atfi.const(10.37828), atfi.const(1.434872)),
                                                            lambda: atfi.complex(atfi.const(12.874102), atfi.const(2.10557)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 3 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp, 0, 3 , -1) ),    
                    }
            
            elif res == "L(1405)":
                    resonances["L(1405)"] = {
                                            "lineshape" : "subthreshold_breit_wigner_lineshape",  
                                            "mass"      : lambda: atfi.const(1.4051), 
                                            "width1"    : lambda: atfi.const(0.0505),  
                                            "width2"    : lambda: atfi.const(0.0505),
                                            "ma2"       : atfi.const(1.18937), #sigma 
                                            "mb2"       : atfi.const(0.13957018), #pion
                                            "spin"      : 1, 
                                            "parity"    : -1, 
                                            "coupl"     : [     
                                                            lambda: atfi.complex(atfi.const(-4.572486), atfi.const(3.190144)),
                                                            lambda: atfi.complex(atfi.const(10.44608), atfi.const(2.787441)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 1 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0 , 1 , -1) ),    
                    }

            elif res == "L(1520)":
                    resonances["L(1520)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: self.fit_params["L(1520)_mass"](),  
                                            "width"     : lambda: self.fit_params["L(1520)_width"](),  
                                            "spin"      : 3, 
                                            "parity"    : -1, 
                                            "coupl"     : [     
                                                            lambda: atfi.complex( self.fit_params["L(1520)_real_1"](), self.fit_params["L(1520)_imag_1"]()),       
                                                            lambda: atfi.complex( self.fit_params["L(1520)_real_2"](), self.fit_params["L(1520)_imag_2"]()),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 3 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0 , 3 , -1) ), 
                    }

            elif res == "L(1600)":
                    resonances["L(1600)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.630), 
                                            "width"     : lambda: atfi.const(0.25),  
                                            "spin"      : 1, 
                                            "parity"    : 1, 
                                            "coupl"     : [     
                                                            lambda: atfi.complex(atfi.const(4.840649), atfi.const(3.082786)),
                                                            lambda: atfi.complex(atfi.const(-6.971233), atfi.const(0.842435)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 1 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0 , 1 , 1) ),  
                    } 
            
            elif res == "L(1670)":
                    resonances["L(1670)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.670), 
                                            "width"     : lambda: atfi.const(0.03),  
                                            "spin"      : 1., 
                                            "parity"    : -1., 
                                            "coupl"     : [     
                                                            lambda: atfi.complex(atfi.const(-0.339585), atfi.const(-0.144678)),
                                                            lambda: atfi.complex(atfi.const(-0.570978), atfi.const(1.011833)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 1 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0 , 1 , -1) ),  
                    }
            
            elif res == "L(1690)":
                    resonances["L(1690)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.690), 
                                            "width"     : lambda: atfi.const(0.07),  
                                            "spin"      : 3, 
                                            "parity"    : -1, 
                                            "coupl"     : [     
                                                            lambda: atfi.complex(atfi.const(-0.385772), atfi.const(-0.110235)),
                                                            lambda: atfi.complex(atfi.const(-2.730592), atfi.const(-0.353613)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 3 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0 , 3 , -1) ),  
                    }
            
            elif res == "L(2000)":
                    resonances["L(2000)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.98819), 
                                            "width"     : lambda: atfi.const(0.17926 ),  
                                            "spin"      : 1, 
                                            "parity"    : -1, 
                                            "coupl"     : [     
                                                            lambda: atfi.complex(atfi.const(-8.014857), atfi.const(-7.614006)),
                                                            lambda: atfi.complex(atfi.const(-4.336255), atfi.const(-3.796192)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 1 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0 , 1 , -1) ),  
                    }
        return resonances 
    
    
    def d_phi_twobody(self,mijsq, misq, mjsq):
        """Two body phase space element"""
        return 1./(2.**4 * atfi.pi()**2) * pvecMag(mijsq, misq, mjsq)/atfi.sqrt(mijsq)


    def get_phasespace_term(self, Obs):
        """Two body phase space factors along with the element"""
        phsspace  = 1./(2. * atfi.const(self.MLc))
        phsspace *= 1./(2. * atfi.pi()) * self.d_phi_twobody(atfi.const(self.MLc)**2, Obs['m2_kpi'], atfi.const(self.Mp)**2) * tf.sin(Obs['kstar_theta_lc'])    #Lc   ->   K* p 
        phsspace *= 1./(2. * atfi.pi()) * self.d_phi_twobody(Obs['m2_kpi'], atfi.const(self.Mk)**2, atfi.const(self.Mpi)**2) * tf.sin(Obs['k_theta_kstar'])   #K*   ->   K  pi
        return phsspace
    
    def get_zeta_angles_for_Lc(self, Obvs):
        """Zeta rotation angles to rotate Delta++ and L* into the K* reference frame"""
        def kallen(x, y, z):
            #kallen function: Lamda(x,y,z) = x^2 + y^2 + z^2 - 2xy - 2yz - 2zx
            kallen = x**2 + y**2 + z**2 - 2*( x*y + y*z + z*x )
            return kallen

        sig1                = Obvs['m2_kpi']
        sig2                = Obvs['m2_ppi']
        sig3                = Obvs['m2_pK']

        p_czeta_Kp          = ( 2 * self.Mp**2 * (sig3 - self.MLc**2 - self.Mpi**2) + (self.MLc**2 + self.Mp**2 - sig1)*(sig2 - self.Mp**2 - self.Mpi**2) ) / ( tf.math.sqrt(kallen(self.MLc**2, self.Mp**2 , sig1)) * tf.math.sqrt( kallen(sig2, self.Mp**2 , self.Mpi**2)) )
        p_czeta_ppi         = ( 2 * self.Mp**2 * (sig2 - self.MLc**2 - self.Mk**2) + (self.MLc**2 + self.Mp**2 - sig1)*(sig3 - self.Mp**2 - self.Mk**2) ) / ( tf.math.sqrt(kallen(self.MLc**2, self.Mp**2 , sig1)) * tf.math.sqrt( kallen(sig3, self.Mp**2 , self.Mk**2)) )

        #ensure all floating variables are in range [-1, 1]
        index_kp            = tf.where(tf.less(p_czeta_Kp, -1))
        index_ppi           = tf.where(tf.less(p_czeta_ppi, -1))
        updates_kp          = -1 * tf.ones_like(index_kp[:,-1], dtype = tf.float64)
        updates_ppi         = -1 * tf.ones_like(index_ppi[:,-1], dtype = tf.float64)
        updated_p_czeta_Kp  = tf.tensor_scatter_nd_update(p_czeta_Kp, index_kp, updates_kp)
        updated_p_czeta_ppi = tf.tensor_scatter_nd_update(p_czeta_ppi, index_ppi, updates_ppi)

        angle = {}
        angle['p_zeta_Kp']  = tf.acos(updated_p_czeta_Kp)
        angle['p_zeta_ppi'] = tf.acos(updated_p_czeta_ppi)
 
        return angle
    
    def BuggLineShape(self, m2, m0, gamma0, gamma, alpha, ma, mb, mc, md, dr, dd, lr, ld, barrierFactor = True, ma0=None, md0 = None) : 
        """
            Parametrisation of Kpi states from Bugg analysis Physics Letters B 632 (2006) 471 Eq.(2),(4), with Blatt-Weisskopf formfactors, mass-dependent width and orbital barriers
        """
        #  return Const(0.0)

        m   = atfi.sqrt(m2)
        q   = atfk.two_body_momentum(md, m, mc)
        q0  = atfk.two_body_momentum(md if md0 is None else md0, m0, mc)
        p   = atfk.two_body_momentum(m, ma, mb)
        p0  = atfk.two_body_momentum(m0, ma if ma0 is None else ma0, mb)
        ffr = atfd.blatt_weisskopf_ff(p, p0, dr, lr)
        ffd = atfd.blatt_weisskopf_ff(q, q0, dd, ld)

        sa = ma*ma-0.5*mb*mb
        width = (m2 - sa)/(m0*m0 - sa)*gamma0*atfi.exp(-gamma*m2)
        bw = atfd.relativistic_breit_wigner(m2, m0, width)

        ff = ffr*ffd
        if barrierFactor : 
            b1 = atfd.orbital_barrier_factor(p, p0, lr)
            b2 = atfd.orbital_barrier_factor(q, q0, ld)
            ff *= b1*b2
        return bw*atfi.complex(ff*atfi.exp(-alpha*q*q), atfi.const(0.))


    def FlatteLineShapeCorrected(self, m2, m0, gamma1, gamma2, ma, mb, ma2, mb2,mc, md, dr, dd, lr, ld, barrierFactor = True, ma0=None, ma02=None, md0 = None) : 
        """
        Flatte lineshape amplitude with Blatt-Weisskopf formfactors, mass-dependent width and orbital barriers
        See BreitWignerLineShape for parameter definition
            ma2, mb2: masses of the alternative decay channel
            gamma1, gamma2: widths associated to the two decay channels
        """
        m = atfi.sqrt(m2)
        #Mother particle decay
        q  = atfk.two_body_momentum(md, m, mc)
        q0 = atfk.two_body_momentum(md if md0 is None else md0, m0, mc)
        ffd = atfd.blatt_weisskopf_ff(q, q0, dd, ld)

        #Resonance First decay channel
        p  = atfk.two_body_momentum(m, ma, mb)
        #Use alternative channel since first is under threshold
        p0 = atfk.two_body_momentum(m0, ma2 if ma02 is None else ma02, mb2)
        ffr = atfd.blatt_weisskopf_ff(p, p0, dr, lr)
        width = atfd.mass_dependent_width(m, m0, gamma1, p, p0, ffr, lr)

        #Resonance Second decay channel
        p2  = atfk.two_body_momentum(m, ma2, mb2)
        ffr2 = atfd.blatt_weisskopf_ff(p2, p0, dr, lr)
        width2 = atfd.mass_dependent_width(m, m0, gamma2, p2, p0, ffr2, lr)

        totwidth = width+width2

        bw = atfd.relativistic_breit_wigner(m2, m0, totwidth)
        ff = ffr*ffd
        if barrierFactor : 
            b1 = atfd.orbital_barrier_factor(p, p0, lr)
            b2 = atfd.orbital_barrier_factor(q, q0, ld)
            ff *= b1*b2
        return bw*atfi.complex(ff, atfi.const(0.))
    
    def D_Ampl_correction(self, alpha_res, lp_D, lp, p_zeta_kp, phi_k_kstar):
        """Wigner D rotation for Delta ++ to K* frame"""
       
        index1 = tf.where(tf.greater(alpha_res, atfi.pi()))
        index2 = tf.where(tf.less_equal(alpha_res, atfi.pi()))

        wig_D1  = tf.math.conj(atfk.wigner_capital_d(  2*atfi.pi() , p_zeta_kp, phi_k_kstar,  1,  lp_D,  lp))
        wig_D2  = tf.math.conj(atfk.wigner_capital_d(  0 , p_zeta_kp, phi_k_kstar,  1,  lp_D,  lp))
        
        wig_D1_updates  = tf.gather(wig_D1, index1[:,-1])
        wig_D2_updates  = tf.gather(wig_D2, index2[:,-1])

        tensor1 = tf.zeros_like(alpha_res, dtype=tf.complex128)
        updated_wigD  = tf.tensor_scatter_nd_add(tensor1, index1, wig_D1_updates)
        updated_wigD2 = tf.tensor_scatter_nd_add(updated_wigD, index2, wig_D2_updates)

        return updated_wigD2


    def L_Ampl_correction(self, alpha_res, lp_L, lp, p_zeta_ppi, phi_k_kstar):
        """Wigner D rotation for Lambda* to K* frame"""

        index1 = tf.where(tf.greater(alpha_res, atfi.pi()))
        index2 = tf.where(tf.less_equal(alpha_res, atfi.pi()))

        #the factor is from the zeta definition
        wig_D1  = atfi.cast_complex( (-1)**(lp_L/2. - lp/2.) ) * tf.math.conj(atfk.wigner_capital_d(  2*atfi.pi() , p_zeta_ppi, phi_k_kstar,  1,  lp_L,  lp))
        wig_D2  = atfi.cast_complex( (-1)**(lp_L/2. - lp/2.) ) * tf.math.conj(atfk.wigner_capital_d(  0 , p_zeta_ppi, phi_k_kstar,  1,  lp_L,  lp))
        
        wig_D1_updates  = tf.gather(wig_D1, index1[:,-1])
        wig_D2_updates  = tf.gather(wig_D2, index2[:,-1])

        tensor1         = tf.zeros_like(alpha_res, dtype=tf.complex128)
        updated_wigD    = tf.tensor_scatter_nd_add(tensor1, index1, wig_D1_updates)
        updated_wigD2   = tf.tensor_scatter_nd_add(updated_wigD, index2, wig_D2_updates)

        return updated_wigD2

    
    def get_lc_ampls(self, Obvs):

        @atfi.function
        def _Ampl_K( res, J_lam_c_proj , lp , lres, coupling ):
            A  = tf.math.conj( atfk.wigner_capital_d( kstar_phi_lc , kstar_theta_lc  , 0 , self.Jlam_c  , J_lam_c_proj , lres - lp ) )
            A *= tf.math.conj( atfk.wigner_capital_d( k_phi_kstar  , k_theta_kstar   , 0 , res_val[res]['spin'] , lres , 0 ) )
            A *= atfi.cast_complex( ( -1 )**( self.Jp/2. - lp/2. ) )
            A *= coupling
            if 'kpi0' in res:
                A *= self.BuggLineShape( m2_kpi, res_val[res]['mass'](), res_val[res]['width'](), res_val[res]['gamma'], \
                                         res_val[res]['alpha'], self.Mk, self.Mpi , self.Mp , self.MLc , d_res , d_Lambda_c,  \
                                         res_val[res]['l_res'] , res_val[res]['l_lamc'], barrierFactor = True) 
            else:
                A *= atfd.breit_wigner_lineshape( m2_kpi , res_val[res]["mass"]() , res_val[res]["width"]() , self.Mk , \
                                                  self.Mpi , self.Mp , self.MLc , d_res , d_Lambda_c , \
                                                  res_val[res]['l_res'] , res_val[res]['l_lamc'])

            return A
        
        @atfi.function
        def _Ampl_L( res, J_lam_c_proj , lp1 , lres , coupling ,  lp ):
            eta  = atfi.cast_complex( 1.)
            if lp1 == -1: eta = atfi.cast_complex( ( -1 )**( 3/2. - res_val[res]['spin']/2.) * res_val[res]['parity'] )
            #define the amplitude
            #A  = tf.math.conj( atfk.wigner_capital_d( lambda_phi_lc , lambda_theta_lc , 0 , self.Jlam_c , J_lam_c_proj , lres ) )
            #A *= tf.math.conj( atfk.wigner_capital_d( p_phi_lambda , p_theta_lambda , 0 , res_val[res]['spin'] , lres , lp1 ) )
            A = coupling
            A *= eta
            A *= self.L_Ampl_correction(alpha_L, lp1, lp, p_zeta_ppi, k_phi_kstar)
            if res == 'L(1405)':
                A *= self.FlatteLineShapeCorrected( m2_pK, res_val[res]['mass'](), res_val[res]["width1"](), res_val[res]["width2"](), self.Mp, self.Mk, \
                                                   res_val[res]["ma2"], res_val[res]["mb2"], self.Mpi, self.MLc, d_res, d_Lambda_c, \
                                                   res_val[res]['l_res'] , res_val[res]['l_lamc'], barrierFactor = True)
            else:
                #A *= atfd.breit_wigner_lineshape( m2_pK , res_val[res]['mass'], res_val[res]['width'], self.Mp , self.Mk , self.Mpi , \
                                                 #self.MLc, d_res , d_Lambda_c , res_val[res]['l_res'] , res_val[res]['l_lamc'])
  
                A *= breit_wigner_lineshape_edited( m2_pK , res_val[res]['mass'](), res_val[res]['width'](), self.Mp , self.Mk , self.Mpi , \
                                                 self.MLc, d_res , d_Lambda_c , res_val[res]['l_res'] , res_val[res]['l_lamc'])
            return A
            
        @atfi.function
        def _Ampl_D( res, J_lam_c_proj , lp2 , lres , coupling ,lp):
            eta  = atfi.cast_complex( 1.)
            if lp2 == -1: eta     = atfi.cast_complex( ( -1 )**( 3/2 - res_val[res]['spin']/2.) * res_val[res]['parity'] )
            #define the amplitude
            #A  = tf.math.conj( atfk.wigner_capital_d( delta_phi_lc , delta_theta_lc , 0 , self.Jlam_c , J_lam_c_proj , lres ) )
            #A *= tf.math.conj( atfk.wigner_capital_d( p_phi_delta  , p_theta_delta , 0 , res_val[res]['spin'] , lres , -lp2 ) )
            A = coupling
            A *= eta
            A *= atfi.cast_complex( ( -1 )**( self.Jp/2. - lp2/2. ) )
            A *= self.D_Ampl_correction(alpha_D, lp2, lp, p_zeta_Kp, k_phi_kstar)
            #A *= atfd.breit_wigner_lineshape( m2_ppi , res_val[res]['mass'], res_val[res]['width'], self.Mp, self.Mpi , \
											 # self.Mk , self.MLc , d_res , d_Lambda_c ,res_val[res]['l_res'] , res_val[res]['l_lamc']  )
            A *= breit_wigner_lineshape_edited( m2_ppi , res_val[res]['mass'](), res_val[res]['width'](), self.Mp, self.Mpi , \
											  self.Mk , self.MLc , d_res , d_Lambda_c ,res_val[res]['l_res'] , res_val[res]['l_lamc']  )
        

            return A 

        @atfi.function
        def _A_res(lLc, lp):
            lp_ds = [1, -1]
            ampl_k  = atfi.complex(atfi.const(0.) , atfi.const(0.))
            ampl_l  = atfi.complex(atfi.const(0.) , atfi.const(0.))
            ampl_d  = atfi.complex(atfi.const(0.) , atfi.const(0.))
            #coherently sum over resonances and their helicities
            for indx, r in enumerate(list(res_val.keys())):
                if 'kpi(892)' in r:
                    coupling1 = res_val[r]['coupl'][0]()
                    coupling2 = res_val[r]['coupl'][1]()
                    coupling3 = res_val[r]['coupl'][2]()
                    coupling4 = res_val[r]['coupl'][3]()
                    if lp == 1:
                        ampl_k1  = _Ampl_K(r, lLc , lp, 0., coupling1 )
                        ampl_k1 += _Ampl_K(r, lLc , lp, 2., coupling2 )
                    else:
                        ampl_k1  = _Ampl_K(r,  lLc , lp, 0. , coupling3 )
                        ampl_k1 += _Ampl_K(r,  lLc , lp, -2., coupling4 )
                
                    ampl_k  += atfi.cast_complex(self.switches[indx]) * ampl_k1
                elif 'kpi0' in r:
                    coupling1       = res_val[r]['coupl'][1]()
                    coupling2       = res_val[r]['coupl'][0]()
                    if lp == 1:
                        ampl_k2  = _Ampl_K(r, lLc , lp, 0., coupling1 )
                    else:
                        ampl_k2  = _Ampl_K(r, lLc , lp, 0., coupling2 )
                
                    ampl_k  += atfi.cast_complex(self.switches[indx]) * ampl_k2
                elif 'L' in r: 
                    #coherently sum of lp_ds
                    ampl_lpd_L = atfi.complex(atfi.const(0.) , atfi.const(0.))
                    for lp_d in lp_ds:
                        coupling1   = res_val[r]['coupl'][0]()
                        coupling2   = res_val[r]['coupl'][1]()
                        ampl_lpd_L += _Ampl_L( r, lLc , lp_d ,  1 , coupling1 , lp ) 
                        ampl_lpd_L += _Ampl_L( r, lLc , lp_d , -1 , coupling2 , lp ) 

                    ampl_l  += atfi.cast_complex(self.switches[indx]) * ampl_lpd_L
                elif 'D' in r: 
                    #coherently sum of lp_ds
                    ampl_lpd_D = atfi.complex(atfi.const(0.) , atfi.const(0.))
                    for lp_d in lp_ds:
                        coupling1   = res_val[r]['coupl'][0]()
                        coupling2   = res_val[r]['coupl'][1]()
                        ampl_lpd_D += _Ampl_D( r, lLc , lp_d ,  1 , coupling1 , lp )
                        ampl_lpd_D += _Ampl_D( r, lLc , lp_d , -1 , coupling2 , lp )

                    ampl_d  += atfi.cast_complex(self.switches[indx]) * ampl_lpd_D
                else:
                    raise Exception('Channel not specified for resonance. Please check!')

            amplitude = ampl_k + ampl_l + ampl_d
            return amplitude

		#get the resonances
        res_val = self.resn

        #phase space variables - K* resonance variables
        m2_kpi            = Obvs['m2_kpi']
        kstar_phi_lc      = Obvs['kstar_phi_lc']
        kstar_theta_lc    = Obvs['kstar_theta_lc'] 
        k_phi_kstar       = Obvs['k_phi_kstar'] 
        k_theta_kstar     = Obvs['k_theta_kstar']

        #Delta resonance variables
        m2_ppi            = Obvs['m2_ppi']
        delta_theta_lc    = Obvs['delta_theta_lc']
        delta_phi_lc      = Obvs['delta_phi_lc']
        p_theta_delta     = Obvs['p_theta_delta']
        p_phi_delta       = Obvs['p_phi_delta']

        #Lambda* resonance variables
        m2_pK             = Obvs['m2_pK']
        lambda_theta_lc   = Obvs['lambda_theta_lc']
        lambda_phi_lc     = Obvs['lambda_phi_lc']
        p_theta_lambda    = Obvs['p_theta_lambda']
        p_phi_lambda      = Obvs['p_phi_lambda']
      
        #zeta angles
        zeta              = self.get_zeta_angles_for_Lc(Obvs)
        p_zeta_Kp         = zeta['p_zeta_Kp']
        p_zeta_ppi        = zeta['p_zeta_ppi']

        #alpha correction factor for Delta and Lambda resonances 
        alpha_D           = Obvs['alpha_D']
        alpha_L           = Obvs['alpha_L']

        #finite widths - can be varied to find optimal - (Blatt-Weisskopf radii)
        d_res             = atfi.const(1.5)  #GeV
        d_Lambda_c        = atfi.const(5.)  #GeV

        #define the amplitudes   
        LcAmp = {}
        LcAmp[( 1 ,  1)] = _A_res(  1 ,  1)
        LcAmp[( 1 , -1)] = _A_res(  1 , -1)
        LcAmp[(-1 ,  1)] = _A_res( -1 ,  1)
        LcAmp[(-1 , -1)] = _A_res( -1 , -1)
        return LcAmp

    def get_dynamic_term(self, obsv):
        """Get the dynamic density term in Lc->pKpi decay"""
    
        #get Lc amplitudes
        Lcampl = self.get_lc_ampls(obsv)  #LcDecay_J_Lc_proj, lp

        dens = atfi.const(0.)
        for lLc in self.lLcs:
            for lp in self.lps:
                lcindx = (lLc, lp)  
                dens += atfd.density(Lcampl[lcindx])
                
        return dens
    
    #@atfi.function 
    def get_unbinned_model(self, ph):
        """Define the unbinned model for Lc->pKpi"""
        obsv              = self.prepare_data(ph) 
        dGdO_dynm         = self.get_dynamic_term(obsv)
        dGdO_phsp         = self.get_phasespace_term(obsv)
        
        if self.res_list == ["phsp"]:
            model  = dGdO_phsp
            return model
        else:
            model  = dGdO_phsp * dGdO_dynm
            return model

    def MC(self, size, chunk = 5000, size_maxest = 100000): 
        atfi.set_seed(1)
        #set seed for reproducibility 
        maximum = tft.maximum_estimator(self.get_unbinned_model , self.phase_space, size_maxest) * 1.5
        print(maximum)
        toy_sample = tft.run_toymc( self.get_unbinned_model , self.phase_space , size , maximum, chunk, components = False)
        print(toy_sample.numpy())
        return toy_sample.numpy()

    def set_params_values(self, res, isfitresult = True):
        """Set the parameters values to the ones given in the dictionary"""
        for k in list(res.keys()):
            for p in list(self.fit_params.values()):
                if p.name == k:
                    if isfitresult:
                        if p.floating():
                            print('Setting', p.name, 'from ', p.numpy(), 'to ', res[k][0])
                            p.update(res[k][0])
                    else:
                        print('Setting', p.name, 'from ', p.numpy(), 'to ', res[k])
                        p.init_value = res[k]
                        p.update(res[k])

    
    def randomise_params(self, seed = None, verbose = True):
        if seed is not None: np.random.seed(seed)
        for p in list(self.fit_params.values()):
            if p.floating():
                valprev = p.numpy()
                newval = np.random.uniform(p.lower_limit, p.upper_limit, size=1)[0]
                p.update(newval)
                if verbose: print('Randomising', p.name, 'from', valprev, 'to', p.numpy())
    
    @atfi.function
    def integral(self, pdf):
        '''
        Return the graph for the integral of the PDF
        pdf : PDF
        '''
        vol = 1.
        for r in self.phase_space.ranges: vol *= (r[1] - r[0])
        return atfi.const(vol) * tf.reduce_mean(pdf)
    
    def unbinned_nll(self, data_unbinned):
        #normalisation sample
        norm_smpl  = self.phase_space.uniform_sample(2000000)
        #norm_smpl  = self.phase_space.rectangular_grid_sample(sizes=(20,20,20,20,20))

        @atfi.function
        def _nll(pars):
            #pars is not used because it is a class
            pdf_unnorm = self.get_unbinned_model(data_unbinned)
            pdf_norm   = self.get_unbinned_model(norm_smpl)
            intg       = self.integral(pdf_norm)
            return -tf.reduce_sum(atfi.log(pdf_unnorm / intg))

        return _nll
    
    def plot(self, data, i):
        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure
        label = [r"$m^2(K\pi)$", "phi", "theta", "phi", r"$\theta_{res}$"]
        
        tfp.plot_distr1d(
            data[:,i],
            bins=100,
            range=(np.min(data[:,i]), np.max(data[:,i])),
            ax=ax,
            label = label[i],
            units="",
            weights = None,
            color = 'k',
            ) 

        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
        return plt.show() 



def Minimize(nll, model, tot_params, nfits = 1, randomise_params = True):
    nllval = None 
    reslts = None 
    for nfit in range(nfits):
        if randomise_params:
            #Randomising the starting values of the parameters according a uniform distribution 
            model.randomise_params()
        
        #Conduct the fit
        results = tfo.run_minuit(nll, list(tot_params.values()), use_gradient=False, use_hesse = False, use_minos = False)
        print(nll)

        #out of nfits pick the result with the least negative log likelihood (NLL)
        if nfit == 0: 
            print('Fit number', nfit)
            nllval = results['loglh']
            reslts = results
        else:
            print('Fit number', nfit)
            if nllval > results['loglh']:
                nllval = results['loglh']
                reslts = results

        print(results)
    
    #set the parameters to the fit results of the least NLL
    model.set_params_values(reslts['params'], isfitresult = True)
    print(reslts['params'])
    return reslts

def norm(m):
    sum_weights = float( len(m) )
    numerators = np.ones_like( m )
    return( numerators / sum_weights )

def plot_fits(data_mc, data_fit, i):
    tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure
    label = [r"$m^2(K\pi)$", "phi", "theta", "phi", r"$\theta_{res}$"]
        
    tfp.plot_distr1d(
        data_mc[:,i],
        bins=100,
        range=(np.min(data_mc[:,i]), np.max(data_mc[:,i])),
        ax=ax,
        label = label[i],
        units="",
        weights = norm(data_mc[:,i]),
        color = 'k',
        ) 
    
    tfp.plot_distr1d(
        data_fit[:,i],
        bins=100,
        range=(np.min(data_fit[:,i]), np.max(data_fit[:,i])),
        ax=ax,
        label = label[i],
        units="",
        weights = norm(data_fit[:,i]),
        color = 'b',
        ) 

    plt.legend(['Before fitting', 'After fitting'], fontsize = 'x-small')
    plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
    return plt.show() 
