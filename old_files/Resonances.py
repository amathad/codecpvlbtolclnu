import math 
import sys 
import operator 
import itertools 
import numpy as np
import tensorflow as tf 
import pandas as pd 
import matplotlib
import matplotlib.pyplot as plt
import iminuit
from iminuit import Minuit 


#AmpliTF imports 
import amplitf.kinematics as atfk 
import amplitf.dynamics as atfd
from amplitf import interface as atfi
from amplitf.phasespace.rectangular_phasespace import RectangularPhaseSpace
import amplitf.likelihood as atfl

#TFA2 imports 
import tfa as tfa 
import tfa.toymc as tft
import tfa.plotting as tfp
import tfa.optimisation as tfo


#Resonances 
res = ["Kstar-kpi(892)","Kstar-kpi(700)", "Kstar-kpi(1430)", "D(1232)", "D(1600)", "D(1700)", "L(1405)", "L(1520)", "L(1600)", "L(1670)", "L(1690)", "L(2000)"]

def resonances(resns):
    resonances = {}
    for res in resns:

        if res == "Kstar-kpi(892)":
                resonances["Kstar-kpi(892)"] = {
                                        "lineshape" : "BW_lineshape",  
                                        "mass"      : atfi.const(0.8955), 
                                        "width"     : atfi.const(0.0473),  
                                        "spin"      : 2., 
                                        "parity"    : -1., 
                                        "coupl"     : [     
                                                        #real and imaginary coupling1         
                                                        1., 
                                                        0., 
                                                        #real and imaginary coupling2
                                                        1.192614, 
                                                        -1.025814, 
                                                        #real and imaginary coupling3
                                                        -3.141446, 
                                                        -3.29341, 
                                                        #real and imaginary coupling4
                                                        -0.727145, 
                                                        -4.155027,
                                                      ]   
                }

        elif res == "Kstar-kpi(700)":
                resonances["Kstar-kpi(700)"] = {
                                        "lineshape" : "BW_lineshape",  
                                        "mass"      : atfi.const(0.824), 
                                        "width"     : atfi.const(0.478),  
                                        "spin"      : 0., 
                                        "parity"    : +1., 
                                        "coupl"     : [  
                                                        #real and imaginary coupling1            
                                                        0.068908 , 
                                                        2.521444 , 
                                                        #real and imaginary coupling2
                                                        -2.68563, 
                                                        0.03849 ,
                                                      ]   
                }
        elif res == "Kstar-kpi(1430)":
                resonances["Kstar-kpi(1430)"] = {
                                        "lineshape" : "BW_lineshape",  
                                        "mass"      : atfi.const(1.375), 
                                        "width"     : atfi.const(0.190),  
                                        "spin"      : 0., 
                                        "parity"    : +1., 
                                        "coupl"     : [     
                                                        #real and imaginary coupling1          
                                                        -6.71516, 
                                                        10.479411, 
                                                        #real and imaginary coupling2
                                                        0.219754 , 
                                                        8.741196,
                                                      ]   
                }
        
        elif res == "D(1232)":
                resonances["D(1232)"] = {
                                        "lineshape" : "BW_lineshape",  
                                        "mass"      : atfi.const(1.232), 
                                        "width"     : atfi.const(0.117),  
                                        "spin"      : 3., 
                                        "parity"    : +1., 
                                        "coupl"     : [    
                                                        #real and imaginary coupling1           
                                                        -6.778191, 
                                                        3.051805, 
                                                        #real and imaginary coupling2
                                                        -12.987193, 
                                                        4.528336, 
                                                      ]   
                }

        elif res == "D(1600)":
                resonances["D(1600)"] = {
                                        "lineshape" : "BW_lineshape",  
                                        "mass"      : atfi.const(1.64), 
                                        "width"     : atfi.const(0.3),  
                                        "spin"      : 3., 
                                        "parity"    : +1., 
                                        "coupl"     : [        
                                                        #real and imaginary coupling1       
                                                        11.401585, 
                                                        -3.125511 , 
                                                        #real and imaginary coupling2
                                                        6.729211, 
                                                        -1.002383, 
                                                      ]   
                }
        
        elif res == "D(1700)":
                resonances["D(1700)"] = {
                                        "lineshape" : "BW_lineshape",  
                                        "mass"      : atfi.const(1.69), 
                                        "width"     : atfi.const(0.38),  
                                        "spin"      : 3., 
                                        "parity"    : -1., 
                                        "coupl"     : [     
                                                        #real and imaginary coupling1          
                                                        10.37828, 
                                                        1.434872 , 
                                                        #real and imaginary coupling2
                                                        12.874102, 
                                                        2.10557,  
                                                      ]   
                }
        
        elif res == "L(1405)":
                resonances["L(1405)"] = {
                                        "lineshape" : "subthreshold_breit_wigner_lineshape",  
                                        "mass"      : atfi.const(1.4051), 
                                        "width"     : atfi.const(0.0505),  
                                        "spin"      : 1., 
                                        "parity"    : -1., 
                                        "coupl"     : [     
                                                        #real and imaginary coupling1          
                                                        -4.572486, 
                                                        3.190144, 
                                                        #real and imaginary coupling2
                                                        10.44608, 
                                                        2.787441,  
                                                      ]   
                }

        elif res == "L(1520)":
                resonances["L(1520)"] = {
                                        "lineshape" : "BW_lineshape",  
                                        "mass"      : atfi.const(1.518467), 
                                        "width"     : atfi.const(0.015195 ),  
                                        "spin"      : 3., 
                                        "parity"    : -1., 
                                        "coupl"     : [     
                                                        #real and imaginary coupling1          
                                                        0.293998, 
                                                        0.044324, 
                                                        #real and imaginary coupling2
                                                        -0.160687, 
                                                        1.498833,  
                                                      ] 
                }

        elif res == "L(1600)":
                resonances["L(1600)"] = {
                                        "lineshape" : "BW_lineshape",  
                                        "mass"      : atfi.const(1.630), 
                                        "width"     : atfi.const(0.25),  
                                        "spin"      : 1., 
                                        "parity"    : 1., 
                                        "coupl"     : [     
                                                        #real and imaginary coupling1          
                                                        4.840649, 
                                                        3.082786, 
                                                        #real and imaginary coupling2
                                                        -6.971233, 
                                                        0.842435,  
                                                      ] 
                }
        
        elif res == "L(1670)":
                resonances["L(1670)"] = {
                                        "lineshape" : "BW_lineshape",  
                                        "mass"      : atfi.const(1.670), 
                                        "width"     : atfi.const(0.03),  
                                        "spin"      : 1., 
                                        "parity"    : -1., 
                                        "coupl"     : [     
                                                        #real and imaginary coupling1          
                                                        -0.339585, 
                                                        -0.144678, 
                                                        #real and imaginary coupling2
                                                        -0.570978, 
                                                        1.011833,  
                                                      ] 
                }
        
        elif res == "L(1690)":
                resonances["L(1690)"] = {
                                        "lineshape" : "BW_lineshape",  
                                        "mass"      : atfi.const(1.690), 
                                        "width"     : atfi.const(0.07),  
                                        "spin"      : 3., 
                                        "parity"    : -1., 
                                        "coupl"     : [     
                                                        #real and imaginary coupling1          
                                                        -0.385772 , 
                                                        -0.110235, 
                                                        #real and imaginary coupling2
                                                        -2.730592, 
                                                        -0.353613,  
                                                      ] 
                }
        
        elif res == "L(2000)":
                resonances["L(2000)"] = {
                                        "lineshape" : "BW_lineshape",  
                                        "mass"      : atfi.const(1.98819), 
                                        "width"     : atfi.const(0.17926 ),  
                                        "spin"      : 1., 
                                        "parity"    : -1., 
                                        "coupl"     : [     
                                                        #real and imaginary coupling1          
                                                        -8.014857, 
                                                        -7.614006 , 
                                                        #real and imaginary coupling2
                                                        -4.336255 , 
                                                        -3.796192,  
                                                      ] 
                }
        

        

        
    return resonances 
