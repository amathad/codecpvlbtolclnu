#import few packages
import pandas as pd
import sys, os
import tensorflow as tf
from tensorflow.python.ops.array_ops import ones, zeros
from tensorflow.python.ops.gen_array_ops import ones_like, zeros_like
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # Do not use GPU for tensor flow
import numpy as np
import matplotlib.pyplot as plt
import uproot

from Lc_decay_3res_2 import LcTopKpi_Model

#amplitf
home = os.getenv('HOME')
sys.path.append(home+"/Packages/AmpliTF/")
import amplitf.interface as atfi
import amplitf.kinematics as atfk
import amplitf.dynamics as atfd
from amplitf.phasespace.rectangular_phasespace import RectangularPhaseSpace

#tfa2
sys.path.append(home+"/Packages/TFA2/")
import tfa.optimisation as tfo
import tfa.toymc as tft
import tfa.plotting as tfp


########### import files
#file = uproot.open("LcMuNu_gen.root:MCDecayTreeTuple/MCDecayTree;1")

class Correction_to_Lc_decay:
    def __init__(self, MLb, MLc, Mp, Mk, Mpi, file, res):
        """initialise some variables"""
        #Masses of particles involved 
        self.MLb       = MLb
        self.MLc       = MLc
        self.Mp        = Mp
        self.Mk        = Mk
        self.Mpi       = Mpi

        self.file      = file
        self.res       = res
        #self.model_lc    = model_lc.LcTopKpi_Model(self.MLc, self.Mp , self.Mk, self.Mpi)

    def prepare_data_id(self):
        if self.res == "Kstar_kpi(892)":
            #Kstar -> Kpi 
            keys_res_p           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())), 313) ,  \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122)) )
            keys_res_pi          = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 313),  \
                                   tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122), \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())), 4122) )))
            keys_res_k           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())), 313),  \
                                   tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122), \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())), 4122) ) ) )

            keys_Lc_p            = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())), 4122), \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 313)))
            keys_Lc_k            = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())), 4122), \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122) ))
            keys_Lc_pi           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 4122), \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122) ))

        if self.res == "D(1232)":
            #D -> ppi
            keys_res_p           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())), 2224),  \
                                   tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122), \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())), 4122) )))
            keys_res_pi          = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 2224),  \
                                   tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122), \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())), 4122) )))
            keys_res_k           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())), 2224),  \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122) ))

            keys_Lc_p            = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())), 4122), \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122)))
            keys_Lc_pi           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 4122), \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122) ))
            keys_Lc_k            = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())), 4122), \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 2224)))
        if self.res == "L(1520)":
            #L -> pK
            keys_res_p           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())), 3124),  \
                                   tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122), \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 4122) )))
            keys_res_pi          = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 3124),  \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122)))

            keys_res_k           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())), 3124),  \
                                   tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122), \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 4122) )))

            keys_Lc_p            = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())), 4122), \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122)))

            keys_Lc_pi           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 4122), \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())), 3124)))

            keys_Lc_k            = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())), 4122), \
                                   tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122)))
            
        if self.res == 'phsp':
            keys_res_p           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())) , 4122) , tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122)) )
            keys_res_pi          = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 4122) , tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122)) )
            keys_res_k           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())), 4122) , tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122)) )
            
            keys_Lc_p            = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["pplus_MC_MOTHER_ID"].array())) , 4122) , tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122)))
            keys_Lc_k            = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["Kminus_MC_MOTHER_ID"].array())), 4122) , tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122) ))
            keys_Lc_pi           = tf.where( tf.logical_and(tf.equal(tf.abs(tf.convert_to_tensor(self.file["piplus_MC_MOTHER_ID"].array())), 4122) , tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122) ))
        
        keys_Lb_Lc           = tf.where( tf.equal(tf.abs(tf.convert_to_tensor(self.file["Lambda_cplus_MC_MOTHER_ID"].array())), 5122) )

        def concat(x, keys_1, keys_Lc1):
            x1            = tf.gather(x, keys_1[:,-1])
            nans          = np.nan * tf.ones_like( x )
            x_update_1    = tf.tensor_scatter_nd_update( nans, keys_1, x1 )
            empty = np.empty(0)
           
            if np.array_equal(x1.numpy() , empty) == False:
                return x_update_1

            else:
                x3  = tf.gather(x, keys_Lc1[:,-1])
                nans2          = np.nan * tf.ones_like( x )
                x_update_3    = tf.tensor_scatter_nd_update( nans2, keys_Lc1, x3 )
                return x_update_3

        print(keys_res_p)
        print(keys_res_pi)
        print(keys_res_k)
        print(keys_Lc_p)
        print(keys_Lc_pi)
        print(keys_Lc_k)
    
        particle_lab_info = {}
        
        #p+
        particle_lab_info['p_px_lab']    = concat(tf.convert_to_tensor(self.file["pplus_TRUEP_X"].array()), keys_res_p, keys_Lc_p) / 1000
        particle_lab_info['p_py_lab']    = concat(tf.convert_to_tensor(self.file["pplus_TRUEP_Y"].array()), keys_res_p, keys_Lc_p) / 1000
        particle_lab_info['p_pz_lab']    = concat(tf.convert_to_tensor(self.file["pplus_TRUEP_Z"].array()), keys_res_p, keys_Lc_p) / 1000
        particle_lab_info['p_E_lab']     = concat(tf.convert_to_tensor(self.file["pplus_TRUEP_E"].array()), keys_res_p, keys_Lc_p) / 1000
        #K-
        particle_lab_info['k_px_lab']    = concat(tf.convert_to_tensor(self.file["Kminus_TRUEP_X"].array()), keys_res_k, keys_Lc_k) / 1000
        particle_lab_info['k_py_lab']    = concat(tf.convert_to_tensor(self.file["Kminus_TRUEP_Y"].array()), keys_res_k, keys_Lc_k) / 1000
        particle_lab_info['k_pz_lab']    = concat(tf.convert_to_tensor(self.file["Kminus_TRUEP_Z"].array()), keys_res_k, keys_Lc_k) / 1000
        particle_lab_info['k_E_lab']     = concat(tf.convert_to_tensor(self.file["Kminus_TRUEP_E"].array()), keys_res_k, keys_Lc_k) / 1000
        #pi+
        particle_lab_info['pi_px_lab']   = concat(tf.convert_to_tensor(self.file["piplus_TRUEP_X"].array()), keys_res_pi, keys_Lc_pi) / 1000
        particle_lab_info['pi_py_lab']   = concat(tf.convert_to_tensor(self.file["piplus_TRUEP_Y"].array()), keys_res_pi, keys_Lc_pi) / 1000
        particle_lab_info['pi_pz_lab']   = concat(tf.convert_to_tensor(self.file["piplus_TRUEP_Z"].array()), keys_res_pi, keys_Lc_pi) / 1000
        particle_lab_info['pi_E_lab']    = concat(tf.convert_to_tensor(self.file["piplus_TRUEP_E"].array()), keys_res_pi, keys_Lc_pi) / 1000
        #Lb-0
        particle_lab_info['Lb_px_lab']   = tf.convert_to_tensor(self.file["Lambda_b0_TRUEP_X"].array()) / 1000
        particle_lab_info['Lb_py_lab']   = tf.convert_to_tensor(self.file["Lambda_b0_TRUEP_Y"].array()) / 1000
        particle_lab_info['Lb_pz_lab']   = tf.convert_to_tensor(self.file["Lambda_b0_TRUEP_Z"].array()) / 1000
        particle_lab_info['Lb_E_lab']    = tf.convert_to_tensor(self.file["Lambda_b0_TRUEP_E"].array()) / 1000
        #Lc+
        particle_lab_info['Lc_px_lab']   = concat(tf.convert_to_tensor(self.file["Lambda_cplus_TRUEP_X"].array()), keys_Lb_Lc, keys_Lb_Lc) / 1000
        particle_lab_info['Lc_py_lab']   = concat(tf.convert_to_tensor(self.file["Lambda_cplus_TRUEP_Y"].array()), keys_Lb_Lc, keys_Lb_Lc) / 1000
        particle_lab_info['Lc_pz_lab']   = concat(tf.convert_to_tensor(self.file["Lambda_cplus_TRUEP_Z"].array()), keys_Lb_Lc, keys_Lb_Lc) / 1000
        particle_lab_info['Lc_E_lab']    = concat(tf.convert_to_tensor(self.file["Lambda_cplus_TRUEP_E"].array()), keys_Lb_Lc, keys_Lb_Lc) / 1000
        
        return particle_lab_info

    ########### Make some common functions
    def MakeFourVector(self, px, py, pz, E): 
        """Make 4-vec given components of 3-momentum and energy"""
        return atfk.lorentz_vector(atfk.vector(px, py, pz), E)

    def InvRotateAndBoostFromRest(self, m_p4mom_p1, m_p4mom_p2, gm_phi_m, gm_ctheta_m, gm_p4mom_m):
        """
        Function that gives 4momenta of the particles in the grand mother helicity frame

        m_p4mom_p1: particle p1 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
        m_p4mom_p2: particle p2 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
        gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
        gm_4mom_m: m 4mom in gm's helicity frame
        --------------
        Checks done such as 
            - (lc_p4mom_p+lc_p4mom_k == lc_p4mom_r), 
            - Going in reverse i.e. boost lc_p4mom_p into R rest frame (using lc_p4mom_r) and rotating into R helicity frame gives back r_p4mom_p 
                - i.e. lc_p4mom_p_opp = atfk.rotate_lorentz_vector(atfk.boost_to_rest(lc_p4mom_p, lc_p4mom_r), -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)) where lc_p4mom_p_opp == r_p4mom_p
            - Rotate and boost, instead of boost and rotate should give the same answer, checked (Does not agree with TFA implementation though)
                -i.e. lc_p4mom_prot  = atfk.rotate_lorentz_vector(lc_p4mom_p, -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)
                -i.e. lc_p4mom_krot  = atfk.rotate_lorentz_vector(lc_p4mom_k, -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)
                -i.e. lc_p4mom_p2    = atfk.boost_to_rest(lc_p4mom_prot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_p2 == r_p4mom_p
                -i.e. lc_p4mom_k2    = atfk.boost_to_rest(lc_p4mom_krot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_k2 == r_p4mom_k
        """
        #First: Rotate particle p 3mom defined in mother m helicity frame such that z now points along z in grand mother gm helicity frame 
        #(i.e interpreted as gm mom in it's grand-grand-mother ggm rest frame)
        m_p4mom_p1_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p1, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
        m_p4mom_p2_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p2, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
        #Second: Boost particle p 4mom from mother m's rest frame to gm helicity frame. This is done using boost vector from gm_p4mom_m  [checked: E(m_p4mom_p1_zmmom + m_p4mom_p2_zmmom) == Mass_m]
        gm_p4mom_p1            = atfk.boost_from_rest(m_p4mom_p1_zgmmom, gm_p4mom_m)
        gm_p4mom_p2            = atfk.boost_from_rest(m_p4mom_p2_zgmmom, gm_p4mom_m)
        return gm_p4mom_p1, gm_p4mom_p2

    def RotateAndBoostToRest(self, gm_p4mom_p1, gm_p4mom_p2, gm_p4mom_p3, gm_phi_m, gm_ctheta_m):
        """
        gm_p4mom_p1: particle p1 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
        gm_p4mom_p2: particle p2 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
        gm_p4mom_p3: particle p3 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
        gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
        """
        
        #First:  Rotate particle p 3mom defined in grand mother gm helicity frame such that z now points along z of mother m in gm rest frame.
        gm_p4mom_p1_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p1, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
        gm_p4mom_p2_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p2, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
        gm_p4mom_p3_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p3, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)

        #Second: Boost particle p 4mom to mother m i.e. p1p2p3 rest frame
        gm_p4mom_m             = gm_p4mom_p1_zmmom + gm_p4mom_p2_zmmom + gm_p4mom_p3_zmmom 
        m_p4mom_p1             = atfk.boost_to_rest(gm_p4mom_p1_zmmom, gm_p4mom_m)
        m_p4mom_p2             = atfk.boost_to_rest(gm_p4mom_p2_zmmom, gm_p4mom_m)
        m_p4mom_p3             = atfk.boost_to_rest(gm_p4mom_p3_zmmom, gm_p4mom_m)
        return m_p4mom_p1, m_p4mom_p2, m_p4mom_p3 
    
    def RotateAndBoostToRest_Lb(self, p1_p4mom_lab, lamb_p4mom_lab, lamb_phi_lab, lamb_theta_lab):
        """
        p1_p4mom_lab: particle p1 4mom in lab helicity frame (i.e z points along Lb momentum in lab frame).
        lamb_phi_lab, lamb_theta_lab: Angles phi and ctheta of Lb in lab frame (i.e z points along Lb momentum in lab frame).
        """
        #First:  Rotate particle p 3mom defined in lab frame such that z now points along z of Lb in Lab frame.
        p1_p4mom_lab_zmmom      = atfk.rotate_lorentz_vector(p1_p4mom_lab, -lamb_phi_lab, -lamb_theta_lab, lamb_phi_lab)

        #Second: Boost particle p 4mom to mother Lb
        p1_p4mom_Lb             = atfk.boost_to_rest(p1_p4mom_lab_zmmom, lamb_p4mom_lab)
  
        return p1_p4mom_Lb

    def RotateAndBoostToRest_Lc(self, p1_p4mom_lb, lc_p4mom_lb, lc_phi_lb, lc_theta_lb):
        """
        p1_p4mom_lab: particle p1 4mom in lab helicity frame (i.e z points along Lb momentum in lab frame).
        lamb_phi_lab, lamb_theta_lab: Angles phi and ctheta of Lb in lab frame (i.e z points along Lb momentum in lab frame).
        """
        #First:  Rotate particle p 3mom defined in lab frame such that z now points along z of Lb in Lab frame.
        p1_p4mom_lab_zmmom      = atfk.rotate_lorentz_vector(p1_p4mom_lb, -lc_phi_lb, -lc_theta_lb, lc_phi_lb)

        #Second: Boost particle p 4mom to mother Lb
        p1_p4mom_Lb             = atfk.boost_to_rest(p1_p4mom_lab_zmmom, lc_p4mom_lb)
  
        return p1_p4mom_Lb

    ########### Make functions to determine the 5 phase space variables for the Lc -> pKpi decay 
    def p4mom(self):
        """p, K. pi 4-momenta in lab, lb and lc rest frames"""
        
        lab_info = self.prepare_data_id()
        
        p_px_lab    = lab_info['p_px_lab']
        p_py_lab    = lab_info['p_py_lab']
        p_pz_lab    = lab_info['p_pz_lab']
        p_E_lab     = lab_info['p_E_lab']

        k_px_lab    = lab_info['k_px_lab']
        k_py_lab    = lab_info['k_py_lab']
        k_pz_lab    = lab_info['k_pz_lab']
        k_E_lab     = lab_info['k_E_lab']

        pi_px_lab   = lab_info['pi_px_lab']
        pi_py_lab   = lab_info['pi_py_lab']
        pi_pz_lab   = lab_info['pi_pz_lab']
        pi_E_lab    = lab_info['pi_E_lab']

        Lc_px_lab   = lab_info['Lc_px_lab']
        Lc_py_lab   = lab_info['Lc_py_lab']
        Lc_pz_lab   = lab_info['Lc_pz_lab']
        Lc_E_lab    = lab_info['Lc_E_lab']

        Lb_px_lab   = lab_info['Lb_px_lab']
        Lb_py_lab   = lab_info['Lb_py_lab']
        Lb_pz_lab   = lab_info['Lb_pz_lab']    
        Lb_E_lab    = lab_info['Lb_E_lab']    

        p_p4mom_lab     = self.MakeFourVector(p_px_lab, p_py_lab, p_pz_lab, p_E_lab)
        k_p4mom_lab     = self.MakeFourVector(k_px_lab, k_py_lab, k_pz_lab, k_E_lab)
        pi_p4mom_lab    = self.MakeFourVector(pi_px_lab, pi_py_lab, pi_pz_lab, pi_E_lab)
        Lc_p4mom_lab    = self.MakeFourVector(Lc_px_lab, Lc_py_lab, Lc_pz_lab,Lc_E_lab)
        Lb_p4mom_lab    = self.MakeFourVector(Lb_px_lab, Lb_py_lab, Lb_pz_lab, Lb_E_lab)
        
        Lb_theta_lab    =  atfk.spherical_angles(Lb_p4mom_lab)[0]
        Lb_phi_lab      =  atfk.spherical_angles(Lb_p4mom_lab)[1]
        
        p_p4mom_Lb      = self.RotateAndBoostToRest_Lb(p_p4mom_lab, Lb_p4mom_lab, Lb_phi_lab, Lb_theta_lab)
        k_p4mom_Lb      = self.RotateAndBoostToRest_Lb(k_p4mom_lab, Lb_p4mom_lab, Lb_phi_lab, Lb_theta_lab)
        pi_p4mom_Lb     = self.RotateAndBoostToRest_Lb(pi_p4mom_lab, Lb_p4mom_lab, Lb_phi_lab, Lb_theta_lab)
        
        #checks 
        Lc_p4mom_Lb = self.RotateAndBoostToRest_Lb(Lc_p4mom_lab, Lb_p4mom_lab, Lb_phi_lab, Lb_theta_lab)
        #Lc_p4mom_Lb     =  p_p4mom_Lb + k_p4mom_Lb + pi_p4mom_Lb

        Lc_theta_Lb     =  atfk.spherical_angles(Lc_p4mom_Lb)[0]
        Lc_ctheta_Lb    =  atfi.cos(Lc_theta_Lb)
        Lc_phi_Lb       =  atfk.spherical_angles(Lc_p4mom_Lb)[1]

        #p_p4mom_Lc      = self.RotateAndBoostToRest( p_p4mom_Lb , k_p4mom_Lb, pi_p4mom_Lb, Lc_phi_Lb, Lc_ctheta_Lb )[0]
        #k_p4mom_Lc      = self.RotateAndBoostToRest( p_p4mom_Lb , k_p4mom_Lb, pi_p4mom_Lb, Lc_phi_Lb, Lc_ctheta_Lb )[1]
        #pi_p4mom_Lc     = self.RotateAndBoostToRest( p_p4mom_Lb , k_p4mom_Lb, pi_p4mom_Lb, Lc_phi_Lb, Lc_ctheta_Lb )[2]
        p_p4mom_Lc       = self.RotateAndBoostToRest_Lc(p_p4mom_Lb, Lc_p4mom_Lb, Lc_phi_Lb, Lc_theta_Lb)
        k_p4mom_Lc       = self.RotateAndBoostToRest_Lc(k_p4mom_Lb, Lc_p4mom_Lb, Lc_phi_Lb, Lc_theta_Lb)
        pi_p4mom_Lc      = self.RotateAndBoostToRest_Lc(pi_p4mom_Lb, Lc_p4mom_Lb, Lc_phi_Lb, Lc_theta_Lb)

        p4mom = {}
        p4mom['p_p4mom_lab']     = p_p4mom_lab
        p4mom['k_p4mom_lab']     = k_p4mom_lab
        p4mom['pi_p4mom_lab']    = pi_p4mom_lab
        p4mom['p_p4mom_Lb']      = p_p4mom_Lb
        p4mom['k_p4mom_Lb']      = k_p4mom_Lb
        p4mom['pi_p4mom_Lb']     = pi_p4mom_Lb

        p4mom['p_p4mom_Lc']      = p_p4mom_Lc
        p4mom['k_p4mom_Lc']      = k_p4mom_Lc
        p4mom['pi_p4mom_Lc']     = pi_p4mom_Lc

        return p4mom

    def phsp_vars(self):
        particle_4mom = self.p4mom()
        """5 phase space variables for Lc -> pKpi decay from p, pi, k 4-momenta"""
        p_p4mom_lab     = particle_4mom['p_p4mom_lab']
        k_p4mom_lab     = particle_4mom['k_p4mom_lab']
        pi_p4mom_lab    = particle_4mom['pi_p4mom_lab']
        
        p_p4mom_Lc      = particle_4mom['p_p4mom_Lc']
        k_p4mom_Lc      = particle_4mom['k_p4mom_Lc']
        pi_p4mom_Lc     = particle_4mom['pi_p4mom_Lc']  

        
        m2_kpi            = (k_p4mom_lab[:,3] + pi_p4mom_lab[:,3])**2 - ( (k_p4mom_lab[:,0]+pi_p4mom_lab[:,0])**2 + (k_p4mom_lab[:,1]+pi_p4mom_lab[:,1])**2 + (k_p4mom_lab[:,2]+pi_p4mom_lab[:,2])**2 )
        m2_ppi            = (p_p4mom_lab[:,3] + pi_p4mom_lab[:,3])**2 - ( (p_p4mom_lab[:,0]+pi_p4mom_lab[:,0])**2 + (p_p4mom_lab[:,1]+pi_p4mom_lab[:,1])**2 + (p_p4mom_lab[:,2]+pi_p4mom_lab[:,2])**2 )
        m2_pK             = (p_p4mom_lab[:,3] + k_p4mom_lab[:,3])**2 - ( (p_p4mom_lab[:,0]+k_p4mom_lab[:,0])**2 + (p_p4mom_lab[:,1]+k_p4mom_lab[:,1])**2 + (p_p4mom_lab[:,2]+k_p4mom_lab[:,2])**2 )
        
        #####Checks
        #m2_pK2        = (p_p4mom_Lc[:,3] + k_p4mom_Lc[:,3])**2 - ( (p_p4mom_Lc[:,0]+k_p4mom_Lc[:,0])**2 + (p_p4mom_Lc[:,1]+k_p4mom_Lc[:,1])**2 + (p_p4mom_Lc[:,2]+k_p4mom_Lc[:,2])**2 )
        #m2_kpi        =  self.Mk**2 + self.Mpi**2 + 2 * ( pi_p4mom_lab[:,3]*k_p4mom_lab[:,3] -  (pi_p4mom_lab[:,0]*k_p4mom_lab[:,0] +  pi_p4mom_lab[:,1]*k_p4mom_lab[:,1] + pi_p4mom_lab[:,2]*k_p4mom_lab[:,2] ) )
        #m2_ppi        =  self.Mp**2 + self.Mpi**2 + 2 * ( pi_p4mom_lab[:,3]*p_p4mom_lab[:,3] -  (pi_p4mom_lab[:,0]*p_p4mom_lab[:,0] +  pi_p4mom_lab[:,1]*p_p4mom_lab[:,1] + pi_p4mom_lab[:,2]*p_p4mom_lab[:,2] ) )
        #m2_pK         =  self.Mk**2 + self.Mp**2 + 2 * ( p_p4mom_lab[:,3]*k_p4mom_lab[:,3] -  (p_p4mom_lab[:,0]*k_p4mom_lab[:,0] +  p_p4mom_lab[:,1]*k_p4mom_lab[:,1] + p_p4mom_lab[:,2]*k_p4mom_lab[:,2] ) )

        kstar_theta_Lc    =  atfk.helicity_angles_3body(k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[0]
        kstar_phi_Lc      =  atfk.helicity_angles_3body(k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[1]
        k_theta_kstar     =  atfk.helicity_angles_3body(k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[2]
        k_phi_kstar       =  atfk.helicity_angles_3body(k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[3]

        Vars = {}
        #kstar resonance variables 
        Vars['m2_kpi']                = m2_kpi
        Vars['kstar_phi_lc']          = kstar_phi_Lc
        Vars['kstar_theta_lc']        = kstar_theta_Lc 
        Vars['k_phi_kstar']           = k_phi_kstar 
        Vars['k_theta_kstar']         = k_theta_kstar
        #delta and lambda * resonance variables 
        Vars['m2_ppi']                = m2_ppi
        Vars['m2_pk']                 = m2_pK
        #Vars['m2_pK_test']            = m2_pK2

        return Vars
    
    def concat_phsp_vars(self):
        
        """Make an array of the 5 phase space variables calculated from the 3-momenta"""
        Variables       = self.phsp_vars()
        m2_kpi          = tf.reshape(Variables['m2_kpi'], [len(Variables['m2_kpi']), 1])
        kstar_phi_lc    = tf.reshape(Variables['kstar_phi_lc'], [len(Variables['kstar_phi_lc']), 1])
        kstar_theta_lc  = tf.reshape(Variables['kstar_theta_lc'], [len(Variables['kstar_theta_lc']), 1])
        k_phi_kstar     = tf.reshape(Variables['k_phi_kstar'], [len(Variables['k_phi_kstar']), 1])
        k_theta_kstar   = tf.reshape(Variables['k_theta_kstar'], [len(Variables['k_theta_kstar']), 1])
        t1              = tf.concat([m2_kpi, kstar_phi_lc], axis = 1)
        t2              = tf.concat([t1, kstar_theta_lc], axis = 1)
        t3              = tf.concat([t2, k_phi_kstar], axis = 1)
        t4              = tf.concat([t3, k_theta_kstar], axis = 1)
        ######if need 2D
        #t1               = tf.concat([m2_kpi, k_theta_kstar], axis = 1)
        #t2              = tf.concat([t1, kstar_theta_lc], axis = 1)
        #t3              = tf.concat([t2, k_phi_kstar], axis = 1)
        #t4              = tf.concat([t3, kstar_phi_lc], axis = 1)
        print(t4)
        return t4

    def remove_nans(self, sample):
        c1 = tf.logical_and(tf.equal(tf.math.is_nan(sample[:,0]), False), tf.equal(tf.math.is_nan(sample[:,1]), False) )
        c2 = tf.logical_and(c1, tf.equal(tf.math.is_nan(sample[:,2]), False))
        c3 = tf.logical_and(c2, tf.equal(tf.math.is_nan(sample[:,3]), False))
        c4 = tf.logical_and(c3, tf.equal(tf.math.is_nan(sample[:,4]), False))
        keys = tf.where(tf.equal(c4, True))

        m2kpi          = tf.gather(sample[:,0], keys[:,-1])
        kstar_phi_Lc   = tf.gather(sample[:,1], keys[:,-1])
        kstar_theta_Lc = tf.gather(sample[:,2], keys[:,-1])
        k_phi_kstar    = tf.gather(sample[:,3], keys[:,-1])
        k_theta_kstar  = tf.gather(sample[:,4], keys[:,-1])

        m2kpi_1             = tf.reshape(m2kpi, [len(m2kpi), 1])
        kstar_phi_lc_1      = tf.reshape(kstar_phi_Lc, [len(kstar_phi_Lc), 1])
        kstar_theta_lc_1    = tf.reshape(kstar_theta_Lc, [len(kstar_theta_Lc), 1])
        k_phi_kstar_1       = tf.reshape(k_phi_kstar, [len(k_phi_kstar), 1])
        k_theta_kstar_1     = tf.reshape(k_theta_kstar, [len(k_theta_kstar), 1])

        t1              = tf.concat([m2kpi_1  , kstar_phi_lc_1 ], axis = 1)
        t2              = tf.concat([t1, kstar_theta_lc_1 ], axis = 1)
        t3              = tf.concat([t2, k_phi_kstar_1 ], axis = 1)
        t4              = tf.concat([t3, k_theta_kstar_1 ], axis = 1)
        print(t4)
        return t4
    '''
    def weights(self, fitted_couplings):
        x           = self.concat_phsp_vars()
        res_list    = ["Kstar_kpi(892)","Kstar_kpi0(700)", "Kstar_kpi0(1430)", "D(1232)", "D(1600)", "D(1700)", "L(1405)", "L(1520)", "L(1600)", "L(1670)", "L(1690)", "L(2000)"]
        resonances  = self.model_lc.fitted_resonances(res_list, fitted_couplings)
        old_pdf     = self.get_pdf_old(x, resonances)

        new_model   = new_model_mc.LcTopKpi_Model(self.MLc, self.Mp, self.Mk, self.Mpi)
        new_MC      = new_model.MC(x.shape[0], 50000, 1000000)
        new_pdf     = new_model.get_unbinned_model( new_MC, switches=np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]), method= '1' , normalisation_sample = False)
    
        weights     = new_pdf / old_pdf
        return weights 
    '''

    def norm(self, m):
        sum_weights = float( len(m) )
        numerators = np.ones_like( m )
        return( numerators / sum_weights )
    
    def plots(self, data, data2, i):
        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure
        label = [r"$m^2(K\pi)$", r"$\theta^{[K*]}_K$"]
       
        tfp.plot_distr1d(
            data2,
            bins=100,
            range=(np.min(data2), np.max(data2)),
            ax=ax,
            label = label[i],
            units="",
            weights = self.norm(data2),
            color = 'b',
            ) 
        
        tfp.plot_distr1d(
            data,
            bins=100,
            range=(np.min(data), np.max(data)),
            ax=ax,
            label = label[i],
            units="",
            weights = self.norm(data),
            color = 'r',
            ) 
        
        plt.legend(['MC - new', 'MC - old'], fontsize = 'x-small')
        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
        return plt.show()  

    def plot_MC(self, data, i):
        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure
        label = [r"$m^2(K\pi)$", r"$\phi^{[\Lambda_c]}_{K*}$", r"$\theta^{[\Lambda_c]}_{K*}$", r"$\phi^{[K*]}_K$", r"$\theta^{[K*]}_K$", r"$m^2(p\pi)$", r"$m^2(pK)$"]
        
        tfp.plot_distr1d(
            data,
            bins=100,
            range=(np.min(data), np.max(data)),
            ax=ax,
            label = label[i],
            units="",
            weights = None,
            color = 'k',
            ) 
       
        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
        return plt.show()  
    
    def plot_MC_ID(self, mass, p1):
        mass_term = self.phsp_vars()[mass]
        print(mass_term)
        cut_off = tf.zeros_like(mass_term)
        index = tf.where( tf.greater(mass_term, cut_off) )
        
        mother_id_p1   = tf.gather(tf.abs(tf.convert_to_tensor(self.file[p1+"_MC_MOTHER_ID"].array())), index[:,-1])

        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure

        tfp.plot_distr1d(
            mother_id_p1,
            bins=100,
            range=(0, 6000),
            ax=ax,
            label = "m2kpi "+p1+"_mother_id",
            units="Mother ID",
            weights = None,
            color = 'k',
            ) 
        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
        return plt.show()

def Minimize(nll, model, tot_params, nfits = 1, randomise_params = True):
    nllval = None 
    reslts = None 
    for nfit in range(nfits):
        if randomise_params:
            #Randomising the starting values of the parameters according a uniform distribution 
            model.randomise_params()
        
        #Conduct the fit
        results = tfo.run_minuit(nll, list(tot_params.values()), use_gradient=False, use_hesse = False, use_minos = False)
        print(nll)

        #out of nfits pick the result with the least negative log likelihood (NLL)
        if nfit == 0: 
            print('Fit number', nfit)
            nllval = results['loglh']
            reslts = results
        else:
            print('Fit number', nfit)
            if nllval > results['loglh']:
                nllval = results['loglh']
                reslts = results

        print(results)
    
    #set the parameters to the fit results of the least NLL
    model.set_params_values(reslts['params'], isfitresult = True)
    print(reslts['params'])
    return reslts
    
def main():
    MLb = 5619.49997776e-3
    MLc = 2.28646
    Mp  = 0.938272046
    Mk  = 0.493677
    Mpi = 0.13957018

    #res_list = ["Kstar_kpi(892)"]
    #res_list = ["D(1232)"]
    res_list = ["L(1520)"]

    file_new = uproot.open("LcMuNu_gen.root:MCDecayTreeTuple/MCDecayTree;1")
    cn = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file_new, res_list[0])
    sample          = cn.concat_phsp_vars()
    sample_fit      = cn.remove_nans(sample)
    sample_fit      = tf.stack([sample_fit[:,0], sample_fit[:, 4]], axis=1)

    float_params  = [res_list[0]+"_mass"]
    float_params += [res_list[0]+"_width"]
    float_params += [res_list[0]+"_real_1"]
    float_params += [res_list[0]+"_real_2"]
    float_params += [res_list[0]+"_imag_1"]
    float_params += [res_list[0]+"_imag_2"]
    if 'Kstar' in res_list[0]:
        float_params += [res_list[0]+"_real_3"]
        float_params += [res_list[0]+"_imag_3"]

    new_model  = LcTopKpi_Model(MLc, Mp, Mk, Mpi, res_list, float_params)
    tot_params = new_model.fit_params
    NLL= new_model.unbinned_nll(sample_fit)
    print(NLL(tot_params).numpy())

    #mc_new  = new_model.MC(100000)
    #mc_new  = new_model.prepare_data(mc_new)
    #sample_fit= new_model.prepare_data(sample_fit)
    #cn.plots(np.sqrt(sample_fit['m2_pK']), np.sqrt(mc_new['m2_pK']), 0)
    #cn.plots(sample_fit['p_theta_lambda'], mc_new['p_theta_lambda'], 1)
    #exit(1)

    results = Minimize(NLL, new_model, tot_params, nfits = 1, randomise_params = True)
    
    fitted_sample = new_model.MC(100000)
    cn.plots(sample_fit[:, 0], fitted_sample[:, 0], 0)
    cn.plots(sample_fit[:, 1], fitted_sample[:, 1], 1)

if __name__ == '__main__':
    main()

########### Define mass constants - GeV
#MLb = 5619.49997776e-3
#MLc = 2.28646
#Mp  = 0.938272046
#Mk  = 0.493677
#Mpi = 0.13957018

#file = uproot.open("LcMuNu_gen.root:MCDecayTreeTuple/MCDecayTree;1")
#cn = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file, "D(1232)")
#sample        = cn.concat_phsp_vars()
#s2            = cn.remove_nans(sample)
#print(s2)
#cn.prepare_data_id()
#cn.prepare_data_id(2224, 'D')
#cn.prepare_data_id(3124, 'L')

#cn.plot_MC_ID( "m2_kpi","Lambda_cplus" )
#cn.plot_MC_ID( "m2_kpi","Kminus" )
#cn.plot_MC_ID( "m2_kpi","piplus" )
#cn.plot_MC_ID( "m2_kpi","pplus" )


#####Perform Optimisation 
#cn.optimisation(s2 , "Kstar_kpi(892)")

#####Perform MC generation
#model = model_lc.LcTopKpi_Model(MLc, Mp, Mk, Mpi)
#toy_sample  = model.MC(10000, 5000, 100000)
#cn.optimisation(toy_sample , "full")



######Plotting
#sample = cn.concat_phsp_vars()
#sample2 = cn.phsp_vars()
#m2ppi = sample2['m2_ppi']
#m2pk  = sample2['m2_pk']
#m2pK_test = sample2['m2_pK_test']

#keys_m         = tf.where(tf.equal(tf.math.is_nan(sample[:,0]), False))
#keys_m2        = tf.where(tf.equal(tf.math.is_nan(m2ppi), False))
#keys_m3        = tf.where(tf.equal(tf.math.is_nan(m2pk), False))
#keys_phi1      = tf.where(tf.equal(tf.math.is_nan(sample[:,1]), False))
#keys_th1       = tf.where(tf.equal(tf.math.is_nan(sample[:,2]), False))
#keys_phi2      = tf.where(tf.equal(tf.math.is_nan(sample[:,3]), False))
#keys_th2       = tf.where(tf.equal(tf.math.is_nan(sample[:,4]), False))

#keys_test = tf.where(tf.equal(tf.math.is_nan(m2pK_test), False))

#m2kpi          = tf.gather(sample[:,0], keys_m[:,-1])
#kstar_phi_Lc   = tf.gather(sample[:,1], keys_phi1[:,-1])
#kstar_theta_Lc = tf.gather(sample[:,2], keys_th1[:,-1])
#k_phi_kstar    = tf.gather(sample[:,3], keys_phi2[:,-1])
#k_theta_kstar  = tf.gather(sample[:,4], keys_th2[:,-1])
#m2ppi_plot     = tf.gather(m2ppi, keys_m2[:,-1])
#m2pk_plot      = tf.gather(m2pk, keys_m3[:,-1])
#m2_test_pk = tf.gather(m2pK_test, keys_test[:,-1])

#print(m2kpi/ 1000000)
#print(kstar_theta_Lc)
#print(k_phi_kstar)
#cn.plot_MC_ID('m2_kpi', "pplus")
#cn.plot_MC_ID('m2_kpi', "piplus")
#cn.plot_MC_ID('m2_kpi', "Kminus")
#cn.plot_MC_ID('m2_kpi', "Lambda_cplus")

#cn.plot_MC(m2kpi, 0)
#cn.plot_MC(kstar_phi_Lc, 1)
#cn.plot_MC(kstar_theta_Lc, 2)
#cn.plot_MC(k_phi_kstar, 3)
#cn.plot_MC(k_theta_kstar, 4)
#cn.plot_MC(m2ppi_plot/1000000, 5)
#cn.plot_MC(m2pk_plot/1000000, 6)
#cn.plot_MC(m2_test_pk/1000000, 6)
#cn.plots(m2pk_plot/1000000,  m2_test_pk/1000000, 6)


#MLc2 = 2.28646
#Mp2  = 0.938272046
#Mk2  = 0.493677
#Mpi2 = 0.13957018
#my_model    = new_model_mc.LcTopKpi_Model(MLc2, Mp2, Mk2, Mpi2)
#toy_sample  = my_model.MC(100000, 50000, 1000000)
#switches    = np.array([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
#weights     = my_model.weights(toy_sample, switches)
#obsv        = my_model.prepare_data(toy_sample)

#cn.plots(m2kpi,  toy_sample[:,0], 0)
#cn.plots(kstar_phi_Lc,   toy_sample[:,1], 1)
#cn.plots(kstar_theta_Lc, toy_sample[:,2], 2)
#cn.plots(k_phi_kstar,    toy_sample[:,3], 3)
#cn.plots(k_theta_kstar,  toy_sample[:,4], 4)
#cn.plots(m2ppi_plot, obsv['m2_ppi'], 5)
#cn.plots(m2pk_plot, obsv['m2_pK'], 6)

