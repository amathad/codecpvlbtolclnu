""" Generate Monte Carlo data """
import math 
import sys 
import operator 
import itertools 
import numpy as np
import tensorflow as tf 
import pandas as pd 
import matplotlib
import matplotlib.pyplot as plt
import iminuit
from iminuit import Minuit 


#AmpliTF imports 
import amplitf.kinematics as atfk 
import amplitf.dynamics as atfd
from amplitf import interface as atfi
from amplitf.phasespace.rectangular_phasespace import RectangularPhaseSpace
import amplitf.likelihood as atfl

#TFA2 imports 
import TFA2.tfa as tfa 
import TFA2.tfa.toymc as tft
import TFA2.tfa.plotting as tfp
import TFA2.tfa.optimisation as tfo


""" Constants """
#Resonances 
res = ["Kstar-kpi", "Kstar-kpi2"]

# Masses
MLc     = 2.28646    
Mp      = 0.938      
Mk      = 0.497    
Mpi     = 0.140   

# angular momenta - units 1/2
Jlam_c = 1.
Jlam_c_proj = [ 1. , -1. ]
Jp = 1.
Jpi = 0.
Jk = 0.

J_k_star = 2.


# helicities - units of 1/2
h_p = [ 1. , -1.]
h_k_star = [ 2. , 0. , -2. ]

#finite widths - can be varied to find optimal - (Blatt-Weisskopf radii)
d_k_star = atfi.const(1.5)  #GeV
d_Lambda_c = atfi.const(5.)  #GeV

""" Define a complex number function in terms of phase and magnitude """
def complex_phase_mag(r, a):
    # r = magnitude, a = phase
    x = r * tf.math.cos(a)
    y = r * tf.math.sin(a)
    return atfi.complex(x, y)

""" For decay A to BC - Function to determine the angular momentum of L(BC) """
# use J(A) = J(B) +/- J(C) +/- L(BC)
# where total angular momenta are definened in units of 1/2
def l_bc( ja , jb , jc ):
    l_bc1 = - ( ja/2 - jb/2 - jc/2 )
    l_bc2 = ja/2 - jb/2 + jc/2
    l_bc = np.min( [l_bc1 , l_bc2] )
    return np.max( [l_bc, 0.] )

l_kstar = np.int( l_bc( J_k_star , Jk , Jpi ) )
l_lamc =  np.int( l_bc( Jlam_c , J_k_star , Jp ) )

# MC toy model constants
norm_grid = 1000
npoints = 1000000  # Number of points to generate


""" Propagator term for the Resonanace """
@atfi.function
def BW_lineshape(m_sq, m_0, T_0, ma, mb, mc, md, d_r, d_d, l_r, l_d):
    return atfd.breit_wigner_lineshape(m_sq, m_0, T_0, ma, mb, mc, md, d_r, d_d, l_r, l_d, barrier_factor=True, ma0=None, md0=None )

def resonances(resns):
    resonances = {}
    for res in resns:

        if res == "Kstar-kpi":
                resonances["Kstar-kpi"] = {
                                        "lineshape" : "BW_lineshape",  
                                        "mass"      : atfi.const(0.892), 
                                        "width"     : atfi.const(0.051),  
                                        "spin"      : 2., 
                                        "parity"    : -1., 
                                        "coupl"     : [              
                                                        tfo.FitParameter(res+"_Mag_1", 1.0    ,   0. , 20. , 0.01), 
                                                        tfo.FitParameter(res+"_Phs_1", 0.     , -atfi.pi() , atfi.pi() , 0.01), 
                                                        tfo.FitParameter(res+"_Mag_2", 0.     ,   0. , 20. , 0.01), 
                                                        tfo.FitParameter(res+"_Phs_2", 0.     , -atfi.pi() , atfi.pi() , 0.01), 
                                                        tfo.FitParameter(res+"_Mag_3", 0.     ,   0., 20.  , 0.01), 
                                                        tfo.FitParameter(res+"_Phs_3", 0.     ,-atfi.pi(), atfi.pi(), 0.01), 
                                                        tfo.FitParameter(res+"_Mag_4", 0.     ,   0. , 20.  , 0.01), 
                                                        tfo.FitParameter(res+"_Phs_4", 0.     , -atfi.pi() , atfi.pi(), 0.01), 
                                                      ]   
                }
        if res == "Kstar-kpi2":
                resonances["Kstar-kpi2"] = {
                                        "lineshape" : "BW_lineshape",  
                                        "mass"      : atfi.const(0.850), 
                                        "width"     : atfi.const(0.051),  
                                        "spin"      : 2., 
                                        "parity"    : -1., 
                                        "coupl"     : [              
                                                        tfo.FitParameter(res+"_Mag_1", 1.0    ,   0. , 20. , 0.01), 
                                                        tfo.FitParameter(res+"_Phs_1", 0.     , -atfi.pi() , atfi.pi() , 0.01), 
                                                        tfo.FitParameter(res+"_Mag_2", 0.     ,   0. , 20. , 0.01), 
                                                        tfo.FitParameter(res+"_Phs_2", 0.     , -atfi.pi() , atfi.pi() , 0.01), 
                                                        tfo.FitParameter(res+"_Mag_3", 0.     ,   0., 20.  , 0.01), 
                                                        tfo.FitParameter(res+"_Phs_3", 0.     ,-atfi.pi(), atfi.pi(), 0.01), 
                                                        tfo.FitParameter(res+"_Mag_4", 0.     ,   0. , 20.  , 0.01), 
                                                        tfo.FitParameter(res+"_Phs_4", 0.     , -atfi.pi() , atfi.pi(), 0.01), 
                                                      ]   
                }
    return resonances 

resn = resonances(res)

# Phase space for toy MC generation: 5D rectangular phase space 
phsp = RectangularPhaseSpace(( ((Mk + Mpi)**2 , (MLc - Mp)**2 ) , (-atfi.pi(), atfi.pi()) , (0. , atfi.pi()) , (-atfi.pi(), atfi.pi()), (0. , atfi.pi()) ))


""" Define Amplitude and density functions """

def Ampl( res, J_lam_c_proj , h_p1, h_res, coupling, m_sq_Kpi, phi_r_lamc, theta_r_lamc, phi_k_res, theta_k_res ):
    e_lp = atfi.cast_complex( ( -1 )**( Jp - h_p1 ) )
    A = BW_lineshape( m_sq_Kpi , resn[res]['mass'] , resn[res]['width'] , Mk , Mpi , Mp , MLc , d_k_star , d_Lambda_c , l_kstar , l_lamc )
    A *= tf.math.conj( atfk.wigner_capital_d( phi_r_lamc , theta_r_lamc , 0 , Jlam_c  , J_lam_c_proj , h_res - h_p1 ) )
    A *= tf.math.conj( atfk.wigner_capital_d( phi_k_res , theta_k_res , 0 , resn[res]['spin'] , h_res , 0 ) )
    A *= e_lp
    A *= coupling
    return A

def Ampl2_Kstar( J_lam_c_proj , h_p1, m_sq_Kpi, phi_r_lamc, theta_r_lamc, phi_k_res, theta_k_res, x, y, flavour ):
    amplf1 = atfi.complex( atfi.const(0.) , atfi.const(0.))
    amplf2 = atfi.complex( atfi.const(0.) , atfi.const(0.))

    #coherently sum of Kstar helicities
    for r in res:
        if r == "Kstar-kpi" :

            if h_p1 == 1.:
                #coupling1 = complex_phase_mag( resn["Kstar-kpi"]['coupl'][0]() , resn["Kstar-kpi"]['coupl'][1]() )
                #coupling2 = complex_phase_mag( resn["Kstar-kpi"]['coupl'][2]() , resn["Kstar-kpi"]['coupl'][3]() )
                coupling1 = complex_phase_mag( atfi.const(1.) , atfi.const(0.) )
                coupling2 = complex_phase_mag( atfi.const(0.) , atfi.const(0.) ) 
                ampl1  = Ampl(r, J_lam_c_proj , h_p1, 0., coupling1, m_sq_Kpi,  flavour * phi_r_lamc, theta_r_lamc,  flavour * phi_k_res, theta_k_res )
                ampl2  = Ampl(r, J_lam_c_proj , h_p1, 2., coupling2, m_sq_Kpi,  flavour * phi_r_lamc, theta_r_lamc,  flavour * phi_k_res, theta_k_res )
                amplf1  += ampl1 + ampl2

            else:
                #coupling1 = complex_phase_mag( resn["Kstar-kpi"]['coupl'][4]() , resn["Kstar-kpi"]['coupl'][5]() )
                #coupling2 = complex_phase_mag( resn["Kstar-kpi"]['coupl'][6]() , resn["Kstar-kpi"]['coupl'][7]() )
                coupling1 = complex_phase_mag( atfi.const(1.) , atfi.const(0.) ) 
                coupling2 = complex_phase_mag( atfi.const(0.) , atfi.const(0.) ) 
                ampl1  = Ampl(r,  J_lam_c_proj , h_p1, 0., coupling1, m_sq_Kpi, flavour * phi_r_lamc, theta_r_lamc, flavour * phi_k_res, theta_k_res )
                ampl2  = Ampl(r,  J_lam_c_proj , h_p1,-2., coupling2, m_sq_Kpi, flavour * phi_r_lamc, theta_r_lamc, flavour * phi_k_res, theta_k_res )
                amplf1  += ampl1 + ampl2

        if r == "Kstar-kpi2" :

            if h_p1 == 1.:
                #coupling1 = complex_phase_mag( resn["Kstar-kpi2"]['coupl'][0]() , resn["Kstar-kpi2"]['coupl'][1]() )
                #coupling2 = complex_phase_mag( resn["Kstar-kpi2"]['coupl'][2]() , resn["Kstar-kpi2"]['coupl'][3]() )
                delta = 0.6
                phi = 0.4
                coupling1 = complex_phase_mag( atfi.const(1.) , atfi.const(delta + flavour * phi) )
                coupling2 = complex_phase_mag( atfi.const(1.) , atfi.const(delta + flavour * phi) ) 
                ampl1  = Ampl(r, J_lam_c_proj , h_p1, 0., coupling1, m_sq_Kpi,  flavour * phi_r_lamc, theta_r_lamc,  flavour * phi_k_res, theta_k_res )
                ampl2  = Ampl(r, J_lam_c_proj , h_p1, 2., coupling2, m_sq_Kpi,  flavour * phi_r_lamc, theta_r_lamc,  flavour * phi_k_res, theta_k_res )
                amplf2  += ampl1 + ampl2

            else:
                #coupling1 = complex_phase_mag( resn["Kstar-kpi2"]['coupl'][4]() , resn["Kstar-kpi2"]['coupl'][5]() )
                #coupling2 = complex_phase_mag( resn["Kstar-kpi2"]['coupl'][6]() , resn["Kstar-kpi2"]['coupl'][7]() )
                delta = 0.6
                phi = 0.4
                coupling1 = complex_phase_mag( atfi.const(1.) , atfi.const(delta + flavour * phi) )
                coupling2 = complex_phase_mag( atfi.const(1.) , atfi.const(delta + flavour * phi) ) 
                ampl1  = Ampl(r,  J_lam_c_proj , h_p1, 0., coupling1, m_sq_Kpi, flavour * phi_r_lamc, theta_r_lamc, flavour * phi_k_res, theta_k_res )
                ampl2  = Ampl(r,  J_lam_c_proj , h_p1,-2., coupling2, m_sq_Kpi, flavour * phi_r_lamc, theta_r_lamc, flavour * phi_k_res, theta_k_res )
                amplf2  += ampl1 + ampl2
    
            
    ampl = x * amplf1 + y * amplf2
    re = 2. * tf.math.real( amplf1 * tf.math.conj(amplf2) ) 

    return atfd.density(ampl), re


""" Model for MC generation for particle """

def model_particle(x):

    m_sq_Kpi = phsp.coordinate(x, 0)
    phi_r_lamc = phsp.coordinate(x, 1)
    theta_r_lamc = phsp.coordinate(x, 2)
    phi_r_res = phsp.coordinate(x, 3)
    theta_r_res = phsp.coordinate(x, 4)

    density = atfi.const(0.)

    for J_lam_c_proj in Jlam_c_proj:
        for h_p1 in h_p:
            density += Ampl2_Kstar( J_lam_c_proj , h_p1, m_sq_Kpi, phi_r_lamc, theta_r_lamc, phi_r_res, theta_r_res, 1, 1, 1)[0]

    return density 


""" Model for MC generation for anti-paricle - CP congugate """

def model_antiparticle(x):

    m_sq_Kpi = phsp.coordinate(x, 0)
    phi_r_lamc = phsp.coordinate(x, 1)
    theta_r_lamc = phsp.coordinate(x, 2)
    phi_r_res = phsp.coordinate(x, 3)
    theta_r_res = phsp.coordinate(x, 4)

    density = atfi.const(0.)

    for J_lam_c_proj in Jlam_c_proj:
        for h_p1 in h_p:
            density += Ampl2_Kstar( J_lam_c_proj , h_p1, m_sq_Kpi, phi_r_lamc, theta_r_lamc, phi_r_res, theta_r_res, 1, 1, -1)[0]

    return density 


""" MC generation for 5 phase space variables """

#set seed for reproducibility 
atfi.set_seed(1)

# Calculate maximum of the PDF for accept-reject toy MC generation - particle 
maximum_p = tft.maximum_estimator(model_particle , phsp, 1000000) 
print("Maximum = ", maximum_p)

# Calculate maximum of the PDF for accept-reject toy MC generation - anti-particle
maximum_ap = tft.maximum_estimator(model_antiparticle , phsp, 1000000) 
print("Maximum = ", maximum_p)

# Run TFA2 toy MC generation
toy_sample_particle = tft.run_toymc( model_particle , phsp , 100000 , maximum_p, chunk=50000 )

toy_sample_antiparticle = tft.run_toymc( model_antiparticle , phsp , 100000 , maximum_ap , chunk=50000 )


""" Plotting functions """


""" Define density calculator for individual amplitudes of the resonances and their interference term """
def density_for_weight(k, flavour, xf, yf):
    density_w = atfi.const(0.)
    interf = atfi.const(0.)

    for J_lam_c_proj in Jlam_c_proj:
        for h_p1 in h_p:
            density_w += Ampl2_Kstar( J_lam_c_proj , h_p1, k[:, 0], k[:, 1], k[:, 2], k[:, 3], k[:, 4], xf, yf, flavour)[0]
            interf += Ampl2_Kstar( J_lam_c_proj , h_p1, k[:, 0], k[:, 1], k[:, 2], k[:, 3], k[:, 4], xf, yf, flavour)[1]
        
    return density_w, interf

def weights(sample, flavour, x, y):
    w1 = density_for_weight(sample, flavour, 1., 0.)[0] / ( density_for_weight(sample, flavour, 1., 1.)[0] )
    w2 = density_for_weight(sample, flavour, 0., 1.)[0] / ( density_for_weight(sample, flavour, 1., 1.)[0] )
    interference = density_for_weight(sample, flavour, 1., 1.)[1] / ( density_for_weight(sample, flavour, 1., 1.)[0] ) 

    if x == 1 and y == 0 :
        return w1 
    if y == 1 and x == 0 :
        return w2
    else :
        return interference


""" Plots for CP introduced """

#get the weights for each bin 
def norm_weight(m):
    sum_weights = float( len(m) )
    numerators = np.ones_like( m )
    return( numerators / sum_weights )

def bin(data): 
    bin = (np.max(data)-np.min(data))*50
    return np.int(bin)

print(np.sum(norm_weight(toy_sample_particle[:, 4])))

""" Mass Distribution """

tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure

hist1 = np.histogram( toy_sample_particle[:, 0], bins = 100, weights = norm_weight(toy_sample_particle[:, 0]) )
hist2 = np.histogram( toy_sample_antiparticle[:, 0], bins = 100, weights = norm_weight(toy_sample_antiparticle[:, 0]) )

distrib1 = (hist2[0] - hist1[0]) / (hist1[0] + hist2[0])
print(np.mean(distrib1))

#xarr = hist1[1][:-1]
x_arr = np.asarray(hist1[1] + 0.5 * (hist1[1][1] - hist1[1][0]))

plt.axhline(y = 0, color = 'k', linestyle = '-')
plt.scatter(x_arr[:-1] , distrib1, marker = 'x')
plt.title(r"$m(K\pi)$ - A_CP")
plt.xlabel(r"$m(K\pi)$ [GeV]")
plt.ylabel("A_CP")
plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
plt.show()

"""

#Plotting the difference between the normalised distributions for the m(K/pi) for CP conjugate and non CP conjugate densities for the decay 
tfp.set_lhcb_style(size=14, usetex=False)  # Adjust plotting style for LHCb papers
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(5, 4))  # Single subplot on the figure

tfp.plot_distr2d(
    hist1[1][:-1],
    distrib1,
    bins = (50, 50),
    ranges = ((0, 1.5), (-0.01, 0.0055)),
    fig = fig,
    ax = ax,
    labels = (r"$m(K\pi)$", r"particle - antiparticle distribution"),
    cmap="YlOrRd",
    log=False,
    ztitle=None,
    title= r"Differnece in $m(K\pi)$ distribution",
    units=("GeV", None),
    weights=None,
    colorbar=True,
)

plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
plt.show()

#Plot the mass distributions for particle and antiparticle
tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure

tfp.plot_distr1d(
    toy_sample_particle[:, 0],
    bins=100,
    range=(0.0, 1.5),
    ax=ax,
    label=r"$m(K\pi) (Jk = 1)$",
    units="GeV",
    weights = norm_weight(toy_sample_particle[:, 0]),
    color = 'r',
)

tfp.plot_distr1d(
    toy_sample_antiparticle[:, 0],
    bins=100,
    range=(0.0, 1.5),
    ax=ax,
    label=r"$m(K\pi) (Jk = 1)$",
    units="GeV",
    weights = norm_weight(toy_sample_antiparticle[:, 0]),
    color = 'b',
)

# Show the plot
plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
plt.show()

"""

# Angle Theta in rest frame of the Resonance
hist3 = np.histogram( toy_sample_particle[:, 4], bins = 100, weights = norm_weight(toy_sample_particle[:, 4]) )
hist4 = np.histogram( toy_sample_antiparticle[:, 4], bins = 100, weights = norm_weight(toy_sample_antiparticle[:, 4]) )

distrib2 = (hist4[0] - hist3[0]) / (hist3[0] + hist4[0]) 
print(np.mean(distrib2))
x_arr2 = np.asarray(hist3[1] + 0.5 * (hist3[1][1] - hist3[1][0]))

plt.axhline(y = 0, color = 'k', linestyle = '-')
plt.scatter(x_arr2[:-1] , distrib2, marker = 'x')
plt.title(r"$\theta^{[Res]}$ - A_CP")
plt.xlabel(r"\theta^{[Res]}$ [Rad]")
plt.ylabel("A_CP")
plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
plt.show()

exit(1)

#Plotting the difference between the normalised distributions for the m(K/pi) for CP conjugate and non CP conjugate densities for the decay 
tfp.set_lhcb_style(size=14, usetex=False)  # Adjust plotting style for LHCb papers
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(5, 4))  # Single subplot on the figure

tfp.plot_distr2d(
    hist3[1][:-1],
    distrib2,
    bins = (50, 50),
    ranges = ((0, 3.15), (-0.001, 0.00075)),
    fig = fig,
    ax = ax,
    labels = (r"\theta^{[Res]}$", r"particle - antiparticle distribution"),
    cmap="YlOrRd",
    log=False,
    ztitle=None,
    title= r"Differnece in \theta^{[Res]}$ distribution",
    units=("Rad", None),
    weights=None,
    colorbar=True,
)

# Show the plot
plt.tight_layout(pad=1.0, w_pad=1.0, h_pad=1.0)
plt.show()

#Plot the theta-res distributions for particle and antiparticle
tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure

# Plot 1D histogram from the toy MC sample - Theta2
tfp.plot_distr1d(
    toy_sample_particle[:, 4],
    bins=100,
    range=(0, 3.15),
    ax=ax,
    label=r"$theta-res (Jk = 1)$",
    units="Rad",
    color = 'r',
    weights = norm_weight(toy_sample_antiparticle[:, 4]),
)

tfp.plot_distr1d(
    toy_sample_antiparticle[:, 4],
    bins=100,
    range=(0, 3.15),
    ax=ax,
    label=r"$theta-res (Jk = 1)$",
    units="Rad",
    color='b', 
    weights = norm_weight(toy_sample_antiparticle[:, 4]),
)

# Show the plot
plt.tight_layout(pad=1.0, w_pad=1.0, h_pad=1.0)
plt.show()


exit(1)

# Plot 1D histogram from the toy MC sample - m(kpi)
tfp.plot_distr1d(
    toy_sample_particle[:, 0],
    bins=100,
    range=(0.0, 1.5),
    ax=ax,
    label=r"$m(K\pi) (Jk = 1)$",
    units="GeV",
    weights= None,
)

tfp.plot_distr1d(
    toy_sample_particle[:, 0],
    bins=100,
    range=(0.0, 1.5),
    ax=ax,
    label=r"$m(K\pi) (Jk = 1)$",
    units="GeV",
    weights= weights(toy_sample_particle, 1., 1., 0.),
    color='r',
)

tfp.plot_distr1d(
    toy_sample_particle[:, 0],
    bins=100,
    range=(0.0, 1.5),
    ax=ax,
    label=r"$m(K\pi) (Jk = 1)$",
    units="GeV",
    weights= weights(toy_sample_particle, 1., 0., 1.),
    color='b', 
)

tfp.plot_distr1d(
    toy_sample_particle[:, 0],
    bins=100,
    range=(0.0, 1.5),
    ax=ax,
    label=r"$m(K\pi) (Jk = 1)$",
    units="GeV",
    weights= weights(toy_sample_particle, 1., 1 ,1),
    color = 'g',
)


# Show the plot
plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
plt.show()


# Angle Phi in rest frame of the Lambda_c

tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure


# Plot 1D histogram from the toy MC sample - Phi1
tfp.plot_distr1d(
    toy_sample_particle[:, 1],
    bins=100,
    range=(-3.15, 3.15),
    ax=ax,
    label=r"$phi-lamc (Jk = 1)$",
    units="",
)
tfp.plot_distr1d(
    toy_sample_particle[:, 1],
    bins=100,
    range=(-3.15, 3.15),
    ax=ax,
    label=r"$phi-lamc (Jk = 1)$",
    units="",
    weights =  weights(toy_sample_particle, 1., 1., 0.),
    color='b', 
)
tfp.plot_distr1d(
    toy_sample_particle[:, 1],
    bins=100,
    range=(-3.15, 3.15),
    ax=ax,
    label=r"$phi-lamc (Jk = 1)$",
    units="",
    weights= weights(toy_sample_particle, 1, 0., 1.),
    color='r',
)

tfp.plot_distr1d(
    toy_sample_particle[:, 1],
    bins=100,
    range=(-3.15, 3.15),
    ax=ax,
    label=r"$phi-lamc (Jk = 1)$",
    units="",
    weights= weights(toy_sample_particle, 1, 1,1),
    color='g',
)

# Show the plot
plt.tight_layout(pad=1.0, w_pad=1.0, h_pad=1.0)
plt.show()


# Angle Theta in rest frame of the Lambda_c

tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure


# Plot 1D histogram from the toy MC sample - Theta1
tfp.plot_distr1d(
    toy_sample_particle[:, 2],
    bins=100,
    range=(0, 3.15),
    ax=ax,
    label=r"$theta-lamc (Jk = 1)$",
    units="",
)
tfp.plot_distr1d(
    toy_sample_particle[:, 2],
    bins=100,
    range=(0, 3.15),
    ax=ax,
    label=r"$theta-lamc (Jk = 1)$",
    units="",
    weights =  weights(toy_sample_particle, 1, 1., 0.),
    color='b', 
)
tfp.plot_distr1d(
    toy_sample_particle[:, 2],
    bins=100,
    range=(0, 3.15),
    ax=ax,
    label=r"$theta-lamc (Jk = 1)$",
    units="",
    weights= weights(toy_sample_particle, 1, 0., 1.),
    color='r',
)

tfp.plot_distr1d(
    toy_sample_particle[:, 2],
    bins=100,
    range=(0, 3.15),
    ax=ax,
    label=r"$theta-lamc (Jk = 1)$",
    units="",
    weights= weights(toy_sample_particle, 1, 1,1),
    color='g',
)

# Show the plot
plt.tight_layout(pad=1.0, w_pad=1.0, h_pad=1.0)
plt.show()


# Angle Phi in rest frame of the Resonance - K*

tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure


# Plot 1D histogram from the toy MC sample - phi2
tfp.plot_distr1d(
    toy_sample_particle[:, 3],
    bins=100,
    range=(-3.15, 3.15),
    ax=ax,
    label=r"$phi-res (Jk = 1)$",
    units="",
)
tfp.plot_distr1d(
    toy_sample_particle[:, 3],
    bins=100,
    range=(-3.15, 3.15),
    ax=ax,
    label=r"$phi-res (Jk = 1)$",
    units="",
    weights =  weights(toy_sample_particle, 1, 1., 0.),
    color='b', 
)

tfp.plot_distr1d(
    toy_sample_particle[:, 3],
    bins=100,
    range=(-3.15, 3.15),
    ax=ax,
    label=r"$phi-res (Jk = 1)$",
    units="",
    weights= weights(toy_sample_particle, 1, 0., 1.),
    color='r',
)


tfp.plot_distr1d(
    toy_sample_particle[:, 3],
    bins=100,
    range=(-3.15, 3.15),
    ax=ax,
    label=r"$phi-res (Jk = 1)$",
    units="",
    weights= weights(toy_sample_particle, 1, 1,1),
    color='g',
)


# Show the plot
plt.tight_layout(pad=1.0, w_pad=1.0, h_pad=1.0)
plt.show()




# Angle Theta in rest frame of the Resonance - K*

tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure

# Plot 1D histogram from the toy MC sample - Theta2
tfp.plot_distr1d(
    toy_sample_particle[:, 4],
    bins=100,
    range=(0, 3.15),
    ax=ax,
    label=r"$theta-res (Jk = 1)$",
    units="",
)


tfp.plot_distr1d(
    toy_sample_particle[:, 4],
    bins=100,
    range=(0, 3.15),
    ax=ax,
    label=r"$theta-res (Jk = 1)$",
    units="",
    weights = weights(toy_sample_particle, 1., 1. , 0. ),
    color='b', 
)

tfp.plot_distr1d(
    toy_sample_particle[:, 4],
    bins=100,
    range=(0, 3.15),
    ax=ax,
    label=r"$theta-res (Jk = 1)$",
    units="",
    weights = weights(toy_sample_particle, 1., 0. , 1. ),
    color='r', 
)


tfp.plot_distr1d(
    toy_sample_particle[:, 4],
    bins=100,
    range=(0, 3.15),
    ax=ax,
    label=r"$theta-res (Jk = 1)$",
    units="",
    weights = weights(toy_sample_particle, 1., 1 , 1 ),
    color='g', 
)


# Show the plot
plt.tight_layout(pad=1.0, w_pad=1.0, h_pad=1.0)
plt.show()


