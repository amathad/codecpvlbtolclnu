#import few packages
import math
import pandas as pd
import sys, os
import tensorflow as tf
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # Do not use GPU for tensor flow
import numpy as np
import matplotlib.pyplot as plt
import uproot
from root_pandas import read_root

#from corrected_Lc_decay_3res import LcTopKpi_Model
from Lc_helicity_oldpdf.py import LcTopKpi_Model_OLD
from Lc_helicity_newpdf.py import LcTopKpi_Model_NEW

#amplitf
home = os.getenv('HOME')
sys.path.append(home+"/Packages/AmpliTF/")
import amplitf.interface as atfi
import amplitf.kinematics as atfk
import amplitf.dynamics as atfd
from amplitf.phasespace.rectangular_phasespace import RectangularPhaseSpace

#tfa2
sys.path.append(home+"/Packages/TFA2/")
import tfa.optimisation as tfo
import tfa.toymc as tft
import tfa.plotting as tfp

########### import files

"""
file1 = uproot.open("LcMuNu_magup_Lc_corr.root:Decaytree;1")
print(file1.keys())


file1 = uproot.open("LcMuNu_gen.root:MCDecayTreeTuple/MCDecayTree;1")
file2 = uproot.open("LcMuNu_gen_new_SM_modeldependency.root:DecayTree;1")
file3 = uproot.open("Lb2Lcmunu_MagDown_2016_Combine_SM_modeldependency.root:DecayTree;1")
print(file3.keys())

files_den  = ['Lb2Lcmunu_MagDown_2016_Combine_SM_modeldependency.root']
tree_den   = 'DecayTree'
cut_den    = ''
colums_den = ['Lb_True_Q2_mu' , 'Lb_True_Costhetal_mu', 'eventNumber']
dfgeom_den = read_root(files_den, key=tree_den, where=cut_den, columns=colums_den) 
print(dfgeom_den)


df = read_root('LcMuNu_gen_new_SM_modeldependency.root', columns=['Lb_True_Q2_mu' , 'Lb_True_Costhetal_mu', 'Event_LbProdcorr', 'Event_FFcorr'])
print(df['Lb_True_Q2_mu'])
"""

class Correction_to_Lc_decay:
    def __init__(self, MLb, MLc, Mp, Mk, Mpi, file, res, df_type):
        """initialise some variables"""
        #Masses of particles involved 
        self.MLb       = MLb
        self.MLc       = MLc
        self.Mp        = Mp
        self.Mk        = Mk
        self.Mpi       = Mpi

        self.file      = file
        self.res       = res
        self.df_type   = df_type

        #for integral and checking limits 
        phsp_limits  = [((self.Mk + self.Mpi)**2 , (self.MLc - self.Mp)**2 ) ]
        phsp_limits += [(-math.pi, math.pi)]
        phsp_limits += [(0. , math.pi)]
        phsp_limits += [(-math.pi, math.pi)]
        phsp_limits += [(0. , math.pi)]
        self.phase_space = RectangularPhaseSpace(ranges=phsp_limits)
    
    def prepare_data_id(self):

        if self.df_type == "recon":

            p_name      = "p"      
            pi_name     = "pi" 
            k_name      = "K"
            Lc_name     = "Lc"
            Lb_name     = "Lb"
            files_den    = ['Lb2Lcmunu_MagUp_2016_Combine.root']
            tree_den     = 'DecayTree'
            colums_den   = ['Lb_True_Q2_mu' , 'Lb_True_Costhetal_mu', 'Event_LbProdcorr', 'Event_TrackCalibcorr', 'Event_PIDCalibEffWeight', 'Event_L0Muoncorr', 'Event_FFcorr', 'isFullsel']
            

            if self.res == "phsp":
                cut_den      = 'abs(Lc_MC_MOTHER_ID)==5122&&abs(pi_MC_MOTHER_ID)==4122&&abs(K_MC_MOTHER_ID)==4122&&abs(p_MC_MOTHER_ID)==4122'
                
            if self.res == "Kstar_kpi(892)":
                cut_den      = 'abs(Lc_MC_MOTHER_ID)==5122&&abs(pi_MC_MOTHER_ID)==313&&abs(K_MC_MOTHER_ID)==313&&abs(p_MC_MOTHER_ID)==4122'
            if self.res == "D(1232)":
                cut_den      = 'abs(Lc_MC_MOTHER_ID)==5122&&abs(pi_MC_MOTHER_ID)==2224&&abs(K_MC_MOTHER_ID)==4122&&abs(p_MC_MOTHER_ID)==2224'
            if self.res == "L(1520)":
                cut_den      = 'abs(Lc_MC_MOTHER_ID)==5122&&abs(pi_MC_MOTHER_ID)==4122&&abs(K_MC_MOTHER_ID)==3124&&abs(p_MC_MOTHER_ID)==3124'
        
        if self.df_type == "gen":
            p_name       = "pplus"      
            pi_name      = "piplus"  
            k_name       = "Kminus"
            Lc_name      = "Lambda_cplus"
            Lb_name      = "Lambda_b0"
            files_den    = ['LcMuNu_gen_new.root']
            tree_den     = 'DecayTree'
            colums_den   = ['Lb_True_Q2_mu' , 'Lb_True_Costhetal_mu', 'Event_LbProdcorr', 'Event_FFcorr']

            if self.res == "phsp":
                cut_den      = 'abs(nu_mu~_MC_MOTHER_ID)==5122&&abs(muminus_MC_MOTHER_ID)==5122&&abs(Lambda_cplus_MC_MOTHER_ID)==5122&&abs(piplus_MC_MOTHER_ID)==4122&&abs(Kminus_MC_MOTHER_ID)==4122&&abs(pplus_MC_MOTHER_ID)==4122'
            if self.res == "Kstar_kpi(892)":
                cut_den      = 'abs(nu_mu~_MC_MOTHER_ID)==5122&&abs(muminus_MC_MOTHER_ID)==5122&&abs(Lambda_cplus_MC_MOTHER_ID)==5122&&abs(piplus_MC_MOTHER_ID)==313&&abs(Kminus_MC_MOTHER_ID)==313&&abs(pplus_MC_MOTHER_ID)==4122'
            if self.res == "D(1232)":
                cut_den      = 'abs(nu_mu~_MC_MOTHER_ID)==5122&&abs(muminus_MC_MOTHER_ID)==5122&&abs(Lambda_cplus_MC_MOTHER_ID)==5122&&abs(piplus_MC_MOTHER_ID)==2224&&abs(Kminus_MC_MOTHER_ID)==4122&&abs(pplus_MC_MOTHER_ID)==2224'
            if self.res == "L(1520)":
                cut_den      = 'abs(nu_mu~_MC_MOTHER_ID)==5122&&abs(muminus_MC_MOTHER_ID)==5122&&abs(Lambda_cplus_MC_MOTHER_ID)==5122&&abs(piplus_MC_MOTHER_ID)==4122&&abs(Kminus_MC_MOTHER_ID)==3124&&abs(pplus_MC_MOTHER_ID)==3124'
        
        df_root_file     = read_root(files_den, key=tree_den, where=cut_den)   
        df_root_file_new = read_root(files_den, key=tree_den, where=cut_den, columns=colums_den) 
        
        particle_lab_info = {}
        
        #p+
        particle_lab_info['p_px_lab']    = tf.convert_to_tensor(df_root_file[p_name+"_TRUEP_X"].to_numpy())/ 1000
        particle_lab_info['p_py_lab']    = tf.convert_to_tensor(df_root_file[p_name+"_TRUEP_Y"].to_numpy())/ 1000
        particle_lab_info['p_pz_lab']    = tf.convert_to_tensor(df_root_file[p_name+"_TRUEP_Z"].to_numpy())/ 1000
        particle_lab_info['p_E_lab']     = tf.convert_to_tensor(df_root_file[p_name+"_TRUEP_E"].to_numpy())/ 1000
        #K-
        particle_lab_info['k_px_lab']    = tf.convert_to_tensor(df_root_file[k_name+"_TRUEP_X"].to_numpy())/ 1000
        particle_lab_info['k_py_lab']    = tf.convert_to_tensor(df_root_file[k_name+"_TRUEP_Y"].to_numpy())/ 1000
        particle_lab_info['k_pz_lab']    = tf.convert_to_tensor(df_root_file[k_name+"_TRUEP_Z"].to_numpy())/ 1000
        particle_lab_info['k_E_lab']     = tf.convert_to_tensor(df_root_file[k_name+"_TRUEP_E"].to_numpy())/ 1000
        #pi+
        particle_lab_info['pi_px_lab']   = tf.convert_to_tensor(df_root_file[pi_name+"_TRUEP_X"].to_numpy())/ 1000
        particle_lab_info['pi_py_lab']   = tf.convert_to_tensor(df_root_file[pi_name+"_TRUEP_Y"].to_numpy())/ 1000
        particle_lab_info['pi_pz_lab']   = tf.convert_to_tensor(df_root_file[pi_name+"_TRUEP_Z"].to_numpy())/ 1000
        particle_lab_info['pi_E_lab']    = tf.convert_to_tensor(df_root_file[pi_name+"_TRUEP_E"].to_numpy())/ 1000
        #Lb-0
        particle_lab_info['Lb_px_lab']   = tf.convert_to_tensor(df_root_file[Lb_name+"_TRUEP_X"].to_numpy())/ 1000
        particle_lab_info['Lb_py_lab']   = tf.convert_to_tensor(df_root_file[Lb_name+"_TRUEP_Y"].to_numpy())/ 1000
        particle_lab_info['Lb_pz_lab']   = tf.convert_to_tensor(df_root_file[Lb_name+"_TRUEP_Z"].to_numpy())/ 1000
        particle_lab_info['Lb_E_lab']    = tf.convert_to_tensor(df_root_file[Lb_name+"_TRUEP_E"].to_numpy())/ 1000
        #Lc+
        particle_lab_info['Lc_px_lab']   = tf.convert_to_tensor(df_root_file[Lc_name+"_TRUEP_X"].to_numpy())/ 1000
        particle_lab_info['Lc_py_lab']   = tf.convert_to_tensor(df_root_file[Lc_name+"_TRUEP_Y"].to_numpy())/ 1000
        particle_lab_info['Lc_pz_lab']   = tf.convert_to_tensor(df_root_file[Lc_name+"_TRUEP_Z"].to_numpy())/ 1000
        particle_lab_info['Lc_E_lab']    = tf.convert_to_tensor(df_root_file[Lc_name+"_TRUEP_E"].to_numpy())/ 1000
        
        return particle_lab_info, df_root_file_new

    ########### Make some common functions
    def MakeFourVector(self, pmag, costheta, phi, msq): 
        """Make 4-vec given magnitude, cos(theta), phi and mass"""
        sintheta = atfi.sqrt(1. - costheta**2) #theta 0 to pi => atfi.sin always pos
        px = pmag * sintheta * atfi.cos(phi)
        py = pmag * sintheta * atfi.sin(phi)
        pz = pmag * costheta
        E  = atfi.sqrt(msq + pmag**2)
        return atfk.lorentz_vector(atfk.vector(px, py, pz), E)

    def pvecMag(self, Msq, m1sq, m2sq): 
        """Momentum mag of (1 or 2) in M rest frame"""
        kallen = atfi.sqrt(Msq**2 + m1sq**2 + m2sq**2 - 2.*(Msq*m1sq + Msq*m2sq + m1sq*m2sq))
        const  = tf.cast(2. * atfi.sqrt(Msq), dtype = tf.float64)
        return  kallen / const 
    
    def HelAngles3Body(self, pa, pb, pc):
        """Get three-body helicity angles"""
        theta_r  = atfi.acos(-atfk.z_component(pc) / atfk.norm(atfk.spatial_components(pc)))
        phi_r    = atfi.atan2(-atfk.y_component(pc), -atfk.x_component(pc))
        pa_prime = atfk.rotate_lorentz_vector(pa, -phi_r, -theta_r, phi_r)
        pb_prime = atfk.rotate_lorentz_vector(pb, -phi_r, -theta_r, phi_r)
        pa_prime2= atfk.boost_to_rest(pa_prime, pa_prime+pb_prime)
        theta_a  = atfi.acos(atfk.z_component(pa_prime2) / atfk.norm(atfk.spatial_components(pa_prime2)))
        phi_a    = atfi.atan2(atfk.y_component(pa_prime2), atfk.x_component(pa_prime2))
        return (theta_r, phi_r, theta_a, phi_a)
    
    def RotateAndBoostToRest(self, gm_p4mom_p1, gm_p4mom_p2, gm_phi_m, gm_ctheta_m):
        """
        gm_p4mom_p1: particle p1 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
        gm_p4mom_p2: particle p2 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
        gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
        """
        #First:  Rotate particle p 3mom defined in grand mother gm helicity frame such that z now points along z of mother m in gm rest frame.
        gm_p4mom_p1_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p1, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
        gm_p4mom_p2_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p2, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
        #Second: Boost particle p 4mom to mother m i.e. p1p2 rest frame
        gm_p4mom_m             = gm_p4mom_p1_zmmom+gm_p4mom_p2_zmmom
        m_p4mom_p1             = atfk.boost_to_rest(gm_p4mom_p1_zmmom, gm_p4mom_m)
        m_p4mom_p2             = atfk.boost_to_rest(gm_p4mom_p2_zmmom, gm_p4mom_m)
        return m_p4mom_p1, m_p4mom_p2

    def InvRotateAndBoostFromRest(self, m_p4mom_p1, m_p4mom_p2, gm_phi_m, gm_ctheta_m, gm_p4mom_m):
        """
        Function that gives 4momenta of the particles in the grand mother helicity frame

        m_p4mom_p1: particle p1 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
        m_p4mom_p2: particle p2 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
        gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
        gm_4mom_m: m 4mom in gm's helicity frame
        --------------
        Checks done such as 
            - (lc_p4mom_p+lc_p4mom_k == lc_p4mom_r), 
            - Going in reverse i.e. boost lc_p4mom_p into R rest frame (using lc_p4mom_r) and rotating into R helicity frame gives back r_p4mom_p 
                - i.e. lc_p4mom_p_opp = atfk.rotate_lorentz_vector(atfk.boost_to_rest(lc_p4mom_p, lc_p4mom_r), -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)) where lc_p4mom_p_opp == r_p4mom_p
            - Rotate and boost, instead of boost and rotate should give the same answer, checked (Does not agree with TFA implementation though)
                -i.e. lc_p4mom_prot  = atfk.rotate_lorentz_vector(lc_p4mom_p, -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)
                -i.e. lc_p4mom_krot  = atfk.rotate_lorentz_vector(lc_p4mom_k, -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)
                -i.e. lc_p4mom_p2    = atfk.boost_to_rest(lc_p4mom_prot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_p2 == r_p4mom_p
                -i.e. lc_p4mom_k2    = atfk.boost_to_rest(lc_p4mom_krot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_k2 == r_p4mom_k
        """
        #First: Rotate particle p 3mom defined in mother m helicity frame such that z now points along z in grand mother gm helicity frame 
        #(i.e interpreted as gm mom in it's grand-grand-mother ggm rest frame)
        m_p4mom_p1_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p1, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
        m_p4mom_p2_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p2, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
        #Second: Boost particle p 4mom from mother m's rest frame to gm helicity frame. This is done using boost vector from gm_p4mom_m  [checked: E(m_p4mom_p1_zmmom + m_p4mom_p2_zmmom) == Mass_m]
        gm_p4mom_p1            = atfk.boost_from_rest(m_p4mom_p1_zgmmom, gm_p4mom_m)
        gm_p4mom_p2            = atfk.boost_from_rest(m_p4mom_p2_zgmmom, gm_p4mom_m)
        return gm_p4mom_p1, gm_p4mom_p2
    
    def InvRotateAndBoostFromRest_3particles(self, m_p4mom_p1, m_p4mom_p2, m_p4mom_p3, gm_phi_m, gm_ctheta_m, gm_p4mom_m):
        #First: Rotate particle p 3mom defined in mother m helicity frame such that z now points along z in grand mother gm helicity frame 
        #(i.e interpreted as gm mom in it's grand-grand-mother ggm rest frame)
        m_p4mom_p1_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p1, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
        m_p4mom_p2_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p2, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
        m_p4mom_p3_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p3, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
        #Second: Boost particle p 4mom from mother m's rest frame to gm helicity frame. This is done using boost vector from gm_p4mom_m  [checked: E(m_p4mom_p1_zmmom + m_p4mom_p2_zmmom) == Mass_m]
        gm_p4mom_p1            = atfk.boost_from_rest(m_p4mom_p1_zgmmom, gm_p4mom_m)
        gm_p4mom_p2            = atfk.boost_from_rest(m_p4mom_p2_zgmmom, gm_p4mom_m)
        gm_p4mom_p3            = atfk.boost_from_rest(m_p4mom_p3_zgmmom, gm_p4mom_m)
        return gm_p4mom_p1, gm_p4mom_p2, gm_p4mom_p3

    def RotateAndBoostToRest_3particles(self, gm_p4mom_p1, gm_p4mom_p2, gm_p4mom_p3, gm_phi_m, gm_ctheta_m):
        """
        gm_p4mom_p1: particle p1 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
        gm_p4mom_p2: particle p2 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
        gm_p4mom_p3: particle p3 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
        gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
        """
        #First:  Rotate particle p 3mom defined in grand mother gm helicity frame such that z now points along z of mother m in gm rest frame.
        gm_p4mom_p1_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p1, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
        gm_p4mom_p2_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p2, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
        gm_p4mom_p3_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p3, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)

        #Second: Boost particle p 4mom to mother m i.e. p1p2p3 rest frame
        gm_p4mom_m             = gm_p4mom_p1_zmmom + gm_p4mom_p2_zmmom + gm_p4mom_p3_zmmom 
        m_p4mom_p1             = atfk.boost_to_rest(gm_p4mom_p1_zmmom, gm_p4mom_m)
        m_p4mom_p2             = atfk.boost_to_rest(gm_p4mom_p2_zmmom, gm_p4mom_m)
        m_p4mom_p3             = atfk.boost_to_rest(gm_p4mom_p3_zmmom, gm_p4mom_m)
        return m_p4mom_p1, m_p4mom_p2, m_p4mom_p3 
    
    def RotateAndBoostToRest_Lb(self, p1_p4mom_lab, lamb_p4mom_lab, lamb_phi_lab, lamb_theta_lab):
        """
        p1_p4mom_lab: particle p1 4mom in lab helicity frame (i.e z points along Lb momentum in lab frame).
        lamb_phi_lab, lamb_theta_lab: Angles phi and ctheta of Lb in lab frame (i.e z points along Lb momentum in lab frame).
        """
        #First:  Rotate particle p 3mom defined in lab frame such that z now points along z of Lb in Lab frame.
        #p1_p4mom_lab_zmmom      = atfk.rotate_lorentz_vector(p1_p4mom_lab, -lamb_phi_lab, -lamb_theta_lab, lamb_phi_lab)

        #Second: Boost particle p 4mom to mother Lb
        p1_p4mom_Lb             = atfk.boost_to_rest(p1_p4mom_lab, lamb_p4mom_lab)
  
        return p1_p4mom_Lb

    ########### Make functions to determine the 5 phase space variables for the Lc -> pKpi decay 
    def p4mom(self):
        """p, K. pi 4-momenta in lab, lb and lc rest frames"""
        
        lab_info = self.prepare_data_id()[0]
        
        #proton 3-mom and energy in lab 
        p_px_lab    = lab_info['p_px_lab']
        p_py_lab    = lab_info['p_py_lab']
        p_pz_lab    = lab_info['p_pz_lab']
        p_E_lab     = lab_info['p_E_lab']
        #kaon 3-mom and energy in lab 
        k_px_lab    = lab_info['k_px_lab']
        k_py_lab    = lab_info['k_py_lab']
        k_pz_lab    = lab_info['k_pz_lab']
        k_E_lab     = lab_info['k_E_lab']
        #pion 3-mom and energy in lab 
        pi_px_lab   = lab_info['pi_px_lab']
        pi_py_lab   = lab_info['pi_py_lab']
        pi_pz_lab   = lab_info['pi_pz_lab']
        pi_E_lab    = lab_info['pi_E_lab']
        #Lambda_c 3-mom and energy in lab 
        Lc_px_lab   = lab_info['Lc_px_lab']
        Lc_py_lab   = lab_info['Lc_py_lab']
        Lc_pz_lab   = lab_info['Lc_pz_lab']
        Lc_E_lab    = lab_info['Lc_E_lab']
        #Lambda_b 3-mom and energy in lab 
        Lb_px_lab   = lab_info['Lb_px_lab']
        Lb_py_lab   = lab_info['Lb_py_lab']
        Lb_pz_lab   = lab_info['Lb_pz_lab']    
        Lb_E_lab    = lab_info['Lb_E_lab']    

        # 4 momenta - LAB FRAME
        p_p4mom_lab     = atfk.lorentz_vector(atfk.vector(p_px_lab, p_py_lab, p_pz_lab   ), p_E_lab)
        k_p4mom_lab     = atfk.lorentz_vector(atfk.vector(k_px_lab, k_py_lab, k_pz_lab   ), k_E_lab)
        pi_p4mom_lab    = atfk.lorentz_vector(atfk.vector(pi_px_lab, pi_py_lab, pi_pz_lab), pi_E_lab)
        Lc_p4mom_lab    = atfk.lorentz_vector(atfk.vector(Lc_px_lab, Lc_py_lab, Lc_pz_lab), Lc_E_lab)
        Lb_p4mom_lab    = atfk.lorentz_vector(atfk.vector(Lb_px_lab, Lb_py_lab, Lb_pz_lab), Lb_E_lab)
        #Lb momenta spherical angles in lab frame 
        Lb_theta_lab    =  atfk.spherical_angles(Lb_p4mom_lab)[0]
        Lb_phi_lab      =  atfk.spherical_angles(Lb_p4mom_lab)[1]
        Lb_ctheta_lab   =  atfi.cos(Lb_theta_lab)
        
        # 4 momenta - Lb FRAME
        p_p4mom_Lb      = self.RotateAndBoostToRest_Lb(p_p4mom_lab , Lb_p4mom_lab, Lb_phi_lab, Lb_theta_lab)
        k_p4mom_Lb      = self.RotateAndBoostToRest_Lb(k_p4mom_lab , Lb_p4mom_lab, Lb_phi_lab, Lb_theta_lab)
        pi_p4mom_Lb     = self.RotateAndBoostToRest_Lb(pi_p4mom_lab, Lb_p4mom_lab, Lb_phi_lab, Lb_theta_lab)
        Lc_p4mom_Lb     = self.RotateAndBoostToRest_Lb(Lc_p4mom_lab, Lb_p4mom_lab, Lb_phi_lab, Lb_theta_lab)
        #Lc momenta spherical angles in Lb frame 
        Lc_theta_Lb     =  atfk.spherical_angles(Lc_p4mom_Lb)[0]
        Lc_phi_Lb       =  atfk.spherical_angles(Lc_p4mom_Lb)[1]
        Lc_ctheta_Lb    =  atfi.cos(Lc_theta_Lb)

        # 4 momenta - Lc FRAME
        p_p4mom_Lc      = self.RotateAndBoostToRest_3particles( p_p4mom_Lb , k_p4mom_Lb, pi_p4mom_Lb, Lc_phi_Lb, Lc_ctheta_Lb )[0]
        k_p4mom_Lc      = self.RotateAndBoostToRest_3particles( p_p4mom_Lb , k_p4mom_Lb, pi_p4mom_Lb, Lc_phi_Lb, Lc_ctheta_Lb )[1]
        pi_p4mom_Lc     = self.RotateAndBoostToRest_3particles( p_p4mom_Lb , k_p4mom_Lb, pi_p4mom_Lb, Lc_phi_Lb, Lc_ctheta_Lb )[2]

        #####Checks
        #p_p4mom_Lc_check       = self.RotateAndBoostToRest_Lc(p_p4mom_Lb , Lc_p4mom_Lb, Lc_phi_Lb, Lc_theta_Lb)
        #k_p4mom_Lc       = self.RotateAndBoostToRest_Lc(k_p4mom_Lb , Lc_p4mom_Lb, Lc_phi_Lb, Lc_theta_Lb)
        #pi_p4mom_Lc      = self.RotateAndBoostToRest_Lc(pi_p4mom_Lb, Lc_p4mom_Lb, Lc_phi_Lb, Lc_theta_Lb) 

        #print(p_p4mom_Lc)
        #print(p_p4mom_Lc_check)

        ''' Checks done:
        1. Lb_p4mom_Lb   approx = 0.
        2. Lc_p4mom_Lb     =  p_p4mom_Lb + k_p4mom_Lb + pi_p4mom_Lb 
        -small differeneces, approximately same 
        3. invrotboost = self.InvRotateAndBoostFromRest_3particles(p_p4mom_Lb, pi_p4mom_Lb, k_p4mom_Lb, Lc_phi_Lb, Lc_ctheta_Lb, Lc_p4mom_Lb)
        print(invrotboost_lb[0])
        print(p_p4mom_Lb)
        print(invrotboost_lb[1])
        print(pi_p4mom_Lb)
        print(invrotboost_lb[2])
        print(k_p4mom_Lb)
        returns p_p4mom_Lb, pi_p4mom_Lb, pi_p4mom_Lb
        -not exact but roughly equal 
        '''
        p4mom = {}
        p4mom['p_p4mom_lab']     = p_p4mom_lab
        p4mom['k_p4mom_lab']     = k_p4mom_lab
        p4mom['pi_p4mom_lab']    = pi_p4mom_lab
        p4mom['p_p4mom_Lb']      = p_p4mom_Lb
        p4mom['k_p4mom_Lb']      = k_p4mom_Lb
        p4mom['pi_p4mom_Lb']     = pi_p4mom_Lb

        p4mom['p_p4mom_Lc']      = p_p4mom_Lc
        p4mom['k_p4mom_Lc']      = k_p4mom_Lc
        p4mom['pi_p4mom_Lc']     = pi_p4mom_Lc

        return p4mom
    
    def phsp_vars(self):
        particle_4mom = self.p4mom()
        """5 phase space variables for Lc -> pKpi decay from p, pi, k 4-momenta"""
        p_p4mom_lab       = particle_4mom['p_p4mom_lab']
        k_p4mom_lab       = particle_4mom['k_p4mom_lab']
        pi_p4mom_lab      = particle_4mom['pi_p4mom_lab']
        
        p_p4mom_Lc        = particle_4mom['p_p4mom_Lc']
        k_p4mom_Lc        = particle_4mom['k_p4mom_Lc']
        pi_p4mom_Lc       = particle_4mom['pi_p4mom_Lc']  
 
        m2_kpi            = atfk.mass_squared(pi_p4mom_Lc+k_p4mom_Lc)
        m2_ppi            = atfk.mass_squared(p_p4mom_Lc+pi_p4mom_Lc)
        m2_pK             = atfk.mass_squared(p_p4mom_Lc+k_p4mom_Lc)

        kstar_theta_Lc    =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[0]
        kstar_phi_Lc      =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[1]
        k_theta_kstar     =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[2]
        k_phi_kstar       =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[3]

        # Angle for Delta and Lambda * 
        #Delta resonance angles 
        delta_theta_lc    =  self.HelAngles3Body(p_p4mom_Lc, pi_p4mom_Lc, k_p4mom_Lc)[0]
        delta_phi_lc      =  self.HelAngles3Body(p_p4mom_Lc, pi_p4mom_Lc, k_p4mom_Lc)[1]
        p_theta_delta     =  self.HelAngles3Body(p_p4mom_Lc, pi_p4mom_Lc, k_p4mom_Lc)[2]
        p_phi_delta       =  self.HelAngles3Body(p_p4mom_Lc, pi_p4mom_Lc, k_p4mom_Lc)[3]
        #pi_theta_delta    =  self.HelAngles3Body(pi_p4mom_Lc, p_p4mom_Lc, k_p4mom_Lc)[2]
        #pi_phi_delta      =  self.HelAngles3Body(pi_p4mom_Lc, p_p4mom_Lc, k_p4mom_Lc)[3]

        #Lambda* resonance angles 
        lambda_theta_lc  =  self.HelAngles3Body(p_p4mom_Lc, k_p4mom_Lc, pi_p4mom_Lc)[0]
        lambda_phi_lc    =  self.HelAngles3Body(p_p4mom_Lc, k_p4mom_Lc, pi_p4mom_Lc)[1]
        p_theta_lambda   =  self.HelAngles3Body(p_p4mom_Lc, k_p4mom_Lc, pi_p4mom_Lc)[2]
        p_phi_lambda     =  self.HelAngles3Body(p_p4mom_Lc, k_p4mom_Lc, pi_p4mom_Lc)[3]

        '''Checks:
        1.  Mass:
            m2_pK2        = (p_p4mom_Lc[:,3] + k_p4mom_Lc[:,3])**2 - ( (p_p4mom_Lc[:,0]+k_p4mom_Lc[:,0])**2 + (p_p4mom_Lc[:,1]+k_p4mom_Lc[:,1])**2 + (p_p4mom_Lc[:,2]+k_p4mom_Lc[:,2])**2 )
            m2_kpi        =  self.Mk**2 + self.Mpi**2 + 2 * ( pi_p4mom_lab[:,3]*k_p4mom_lab[:,3] -  (pi_p4mom_lab[:,0]*k_p4mom_lab[:,0] +  pi_p4mom_lab[:,1]*k_p4mom_lab[:,1] + pi_p4mom_lab[:,2]*k_p4mom_lab[:,2] ) )
            m2_ppi        =  self.Mp**2 + self.Mpi**2 + 2 * ( pi_p4mom_lab[:,3]*p_p4mom_lab[:,3] -  (pi_p4mom_lab[:,0]*p_p4mom_lab[:,0] +  pi_p4mom_lab[:,1]*p_p4mom_lab[:,1] + pi_p4mom_lab[:,2]*p_p4mom_lab[:,2] ) )
            m2_pK         =  self.Mk**2 + self.Mp**2 + 2 * ( p_p4mom_lab[:,3]*k_p4mom_lab[:,3] -  (p_p4mom_lab[:,0]*k_p4mom_lab[:,0] +  p_p4mom_lab[:,1]*k_p4mom_lab[:,1] + p_p4mom_lab[:,2]*k_p4mom_lab[:,2] ) )
        2. Angles: 
            k_star_p4mom_Lc   =  k_p4mom_Lc + pi_p4mom_Lc
            kstar_theta_Lc    =  atfk.spherical_angles(k_star_p4mom_Lc)[0]
            kstar_phi_Lc      =  atfk.spherical_angles(k_star_p4mom_Lc)[1]
            kstar_ctheta_Lc   =  atfi.cos(kstar_theta_Lc)

            k_p4mom_kstar     =  self.RotateAndBoostToRest(k_p4mom_Lc, pi_p4mom_Lc, kstar_phi_Lc, kstar_ctheta_Lc)[0]
            #k_p4mom_kstar    =  self.RotateAndBoostToRest_Lc(k_p4mom_Lc, k_star_p4mom_Lc, kstar_phi_Lc, kstar_theta_Lc)
            k_theta_kstar     =  atfk.spherical_angles(k_p4mom_kstar)[0]
            k_phi_kstar       =  atfk.spherical_angles(k_p4mom_kstar)[1]

            kstar_theta_Lc2    =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[0]
            kstar_phi_Lc2      =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[1]
            k_theta_kstar2     =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[2]
            k_phi_kstar2       =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[3]

            print(kstar_theta_Lc)
            print(kstar_theta_Lc2)
            print(kstar_phi_Lc)
            print(kstar_phi_Lc2)
            print(k_theta_kstar)
            print(k_theta_kstar2)
            print(k_phi_kstar)
            print(k_phi_kstar2)

            kstar_theta_Lc2    =  atfk.helicity_angles_3body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[0]
            kstar_phi_Lc2      =  atfk.helicity_angles_3body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[1]
            k_theta_kstar2     =  atfk.helicity_angles_3body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[2]
            k_phi_kstar2       =  atfk.helicity_angles_3body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[3]
        '''

        Vars = {}
        #kstar resonance variables 
        Vars['m2_kpi']                = m2_kpi
        Vars['kstar_phi_lc']          = kstar_phi_Lc
        Vars['kstar_theta_lc']        = kstar_theta_Lc 
        Vars['k_phi_kstar']           = k_phi_kstar 
        Vars['k_theta_kstar']         = k_theta_kstar
        #delta and lambda * resonance variables 
        Vars['m2_ppi']                = m2_ppi
        Vars['m2_pk']                 = m2_pK
        #Delta resonance variables
        Vars['delta_theta_lc']        = delta_theta_lc
        Vars['delta_phi_lc']          = delta_phi_lc
        Vars['p_theta_delta']         = p_theta_delta
        Vars['p_phi_delta']           = p_phi_delta 
        #Vars['pi_theta_delta']        = pi_theta_delta
        #Vars['pi_phi_delta']          = pi_phi_delta 

        #Lambda* resonance variables
        Vars['lambda_theta_lc']       = lambda_theta_lc
        Vars['lambda_phi_lc']         = lambda_phi_lc
        Vars['p_theta_lambda']        = p_theta_lambda
        Vars['p_phi_lambda']          = p_phi_lambda

        return Vars
    
    def concat_phsp_vars(self):
        """Make an array of the 5 phase space variables calculated from the 3-momenta"""
        Variables       = self.phsp_vars()
        m2_kpi          = tf.reshape(Variables['m2_kpi'], [len(Variables['m2_kpi']), 1])
        kstar_phi_lc    = tf.reshape(Variables['kstar_phi_lc'], [len(Variables['kstar_phi_lc']), 1])
        kstar_theta_lc  = tf.reshape(Variables['kstar_theta_lc'], [len(Variables['kstar_theta_lc']), 1])
        k_phi_kstar     = tf.reshape(Variables['k_phi_kstar'], [len(Variables['k_phi_kstar']), 1])
        k_theta_kstar   = tf.reshape(Variables['k_theta_kstar'], [len(Variables['k_theta_kstar']), 1])
        t1              = tf.concat([m2_kpi, kstar_phi_lc], axis = 1)
        t2              = tf.concat([t1, kstar_theta_lc], axis = 1)
        t3              = tf.concat([t2, k_phi_kstar], axis = 1)
        t4              = tf.concat([t3, k_theta_kstar], axis = 1)
        return t4
    
    def remove_nans(self, sample):
        c1   = tf.logical_and(tf.equal(tf.math.is_nan(sample[:,0]), False), tf.equal(tf.math.is_nan(sample[:,1]), False) )
        c2   = tf.logical_and(c1, tf.equal(tf.math.is_nan(sample[:,2]), False))
        c3   = tf.logical_and(c2, tf.equal(tf.math.is_nan(sample[:,3]), False))
        c4   = tf.logical_and(c3, tf.equal(tf.math.is_nan(sample[:,4]), False))
        keys = tf.where(tf.equal(c4, True))
        print(keys)

        m2kpi               = tf.gather(sample[:,0], keys[:,-1])
        kstar_phi_Lc        = tf.gather(sample[:,1], keys[:,-1])
        kstar_theta_Lc      = tf.gather(sample[:,2], keys[:,-1])
        k_phi_kstar         = tf.gather(sample[:,3], keys[:,-1])
        k_theta_kstar       = tf.gather(sample[:,4], keys[:,-1])

        m2kpi_1             = tf.reshape(m2kpi, [len(m2kpi), 1])
        kstar_phi_lc_1      = tf.reshape(kstar_phi_Lc, [len(kstar_phi_Lc), 1])
        kstar_theta_lc_1    = tf.reshape(kstar_theta_Lc, [len(kstar_theta_Lc), 1])
        k_phi_kstar_1       = tf.reshape(k_phi_kstar, [len(k_phi_kstar), 1])
        k_theta_kstar_1     = tf.reshape(k_theta_kstar, [len(k_theta_kstar), 1])

        t1                  = tf.concat([m2kpi_1  , kstar_phi_lc_1 ], axis = 1)
        t2                  = tf.concat([t1, kstar_theta_lc_1 ], axis = 1)
        t3                  = tf.concat([t2, k_phi_kstar_1 ], axis = 1)
        t4                  = tf.concat([t3, k_theta_kstar_1 ], axis = 1)
        #print(t4)
        rows                = tf.reshape(keys, shape = (1, len(t4))).numpy()
        print(rows)
        print(keys[:, -1].numpy())
        return t4, keys

    def reversed_method(self, x):
        """Function that calculates the variables for phase space variables and Lc decay amplitude variables"""

        #Store Lc phase_space Varsiables
        Vars = {}
        m2_kpi            = x[:,0]
        kstar_phi_lc      = x[:,1]
        kstar_theta_lc    = x[:,2]
        k_phi_kstar       = x[:,3]
        k_theta_kstar     = x[:,4]

        kstar_ctheta_lc               = tf.cos(kstar_theta_lc)
        k_ctheta_kstar                = tf.cos(k_theta_kstar)
        p_ctheta_lc                   = -kstar_ctheta_lc
        p_theta_lc                    = tf.acos(p_ctheta_lc)
        p_phi_lc                      = kstar_phi_lc + atfi.pi()
        pi_ctheta_kstar               = -k_ctheta_kstar
        pi_phi_kstar                  = k_phi_kstar + atfi.pi()

        #4-momenta K and pi in K* rest frame
        k_p4mom_kstar    = self.MakeFourVector(self.pvecMag(m2_kpi, self.Mk**2, self.Mpi**2), k_ctheta_kstar, k_phi_kstar, self.Mk**2)
        pi_p4mom_kstar   = self.MakeFourVector(self.pvecMag(m2_kpi, self.Mpi**2, self.Mk**2), pi_ctheta_kstar, pi_phi_kstar, self.Mpi**2)

        #4-momenta in Lc rest frame 
        kstar_p4mom_lc   = self.MakeFourVector(self.pvecMag(self.MLc**2, m2_kpi, self.Mp**2), kstar_ctheta_lc, kstar_phi_lc, m2_kpi)
        p_p4mom_lc       = self.MakeFourVector(self.pvecMag(self.MLc**2, self.Mp**2, m2_kpi), p_ctheta_lc, p_phi_lc, self.Mp**2)
        #boost and rotate 4-momenta K and pi in K* rest frame to Lc rest frame 
        k_p4mom_lc       = self.InvRotateAndBoostFromRest(k_p4mom_kstar, pi_p4mom_kstar, kstar_phi_lc, kstar_ctheta_lc, kstar_p4mom_lc)[0]
        pi_p4mom_lc      = self.InvRotateAndBoostFromRest(k_p4mom_kstar, pi_p4mom_kstar, kstar_phi_lc, kstar_ctheta_lc, kstar_p4mom_lc)[1]

        Vars['k_p4mom_Lc']  = k_p4mom_lc
        Vars['pi_p4mom_Lc'] = pi_p4mom_lc
        Vars['p_p4mom_Lc']  = p_p4mom_lc
        return Vars

    @atfi.function 
    def integral(self, pdf):
            '''
            Return the graph for the integral of the PDF
            pdf : PDF
            '''
            vol = 1.
            for r in self.phase_space.ranges: vol *= (r[1] - r[0])
            return atfi.const(vol) * tf.reduce_mean(pdf) 
    
    def weights(self, new_model,  old_model, data_mc):
        old_pdf     = old_model.get_unbinned_model(data_mc)
        new_pdf     = new_model.model_MC(data_mc)
        norm_old    = old_pdf / self.integral(old_pdf)
        norm_new    = new_pdf / self.integral(new_pdf)
        weights     = norm_new / norm_old
        print(weights)
        return weights.numpy()
    
    def make_root_file(self, weights_Lc_corr):
        df_root_file    = self.prepare_data_id()[1]
        df_w            = pd.DataFrame(data = weights_Lc_corr, columns = ['Event_Lc_corr'])
        df_new          = pd.concat([df_root_file, df_w], axis = 1)
        return print(df_new)
        #return df_new.to_root('LcMuNu_gen_Lc_corr.root', key='Decaytree')
    
    def make_root_file_recon(self, keys, weights_Lc_corr):
        #rows                = tf.reshape(keys, shape = (1, len(weights_Lc_corr))).numpy()
        rows = keys[:, -1].numpy()
        print(rows)
        df_root_file        = self.prepare_data_id()[1]
        df_root_file_nonans = df_root_file.iloc[rows, :]
        df_w                = pd.DataFrame(data = weights_Lc_corr, index = rows, columns = ['Event_Lc_corr'])
        df_new              = pd.concat([df_root_file_nonans, df_w], axis = 1)
        return print(df_new)
        #return df_new.to_root('LcMuNu_magup_Lc_corr.root', key='Decaytree')

    def norm(self, m):
        sum_weights = float( len(m) )
        numerators  = np.ones_like( m )
        return( numerators / sum_weights )
    
    def plots(self, data, data2, i):
        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure
        label = [r"$m^2(K\pi)$", r"$\phi^{[\Lambda_c]}_{K*}$", r"$\theta^{[\Lambda_c]}_{K*}$", r"$\phi^{[K*]}_K$", r"$\theta^{[K*]}_K$", r"$m^2(p\pi)$", r"$m^2(pK)$", r"$\theta^{[\Lambda_*]}_{p}$", r"$\theta^{[\Delta]}_{p}$"]
        #label = [r"$m^2(K\pi)$", r"$\theta^{[K*]}_K$"]
       
        tfp.plot_distr1d(
            data2,
            bins= 70,
            range=(np.min(data2), np.max(data2)),
            ax=ax,
            label = label[i],
            units="",
            weights = self.norm(data2),
            color = 'b',
            ) 
        
        tfp.plot_distr1d(
            data,
            bins=70,
            range=(np.min(data), np.max(data)),
            ax=ax,
            label = label[i],
            units="",
            weights = self.norm(data),
            color = 'r',
            ) 
        
        plt.legend(['MC - new', 'MC - old'], fontsize = 'x-small')
        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
        return plt.show()  

    def plot_MC(self, data, i):
        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure
        label = [r"$m^2(K\pi)$", r"$\phi^{[\Lambda_c]}_{K*}$", r"$\theta^{[\Lambda_c]}_{K*}$", r"$\phi^{[K*]}_K$", r"$\theta^{[K*]}_K$", r"$m^2(p\pi)$", r"$m^2(pK)$", r"$\theta^{[\Lambda_*]}_{p}$"]
        
        tfp.plot_distr1d(
            data,
            bins=100,
            range=(np.min(data), np.max(data)),
            ax=ax,
            label = label[i],
            units="",
            weights = None,
            color = 'k',
            ) 
       
        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
        return plt.show()  
    
    def plot_MC_ID(self, mass, p1):
        mass_term = self.phsp_vars()[mass]
        print(mass_term)
        cut_off = tf.zeros_like(mass_term)
        index = tf.where( tf.greater(mass_term, cut_off) )
        
        mother_id_p1   = tf.gather(tf.abs(tf.convert_to_tensor(self.file[p1+"_MC_MOTHER_ID"].array())), index[:,-1])

        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure

        tfp.plot_distr1d(
            mother_id_p1,
            bins=100,
            range=(0, 6000),
            ax=ax,
            label = "m2kpi "+p1+"_mother_id",
            units ="Mother ID",
            weights = None,
            color = 'k',
            ) 
        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
        return plt.show()
    
    def plot_weights(self, data, weights, data_new, i):
        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure
        label = [r"$m^2(K\pi)$", r"$\phi^{[\Lambda_c]}_{K*}$", r"$\theta^{[\Lambda_c]}_{K*}$", r"$\phi^{[K*]}_K$", r"$\theta^{[K*]}_K$", r"$m^2(p\pi)$", r"$m^2(pK)$", r"$\theta^{[\Lambda_*]}_{p}$", r"$\theta^{[\Delta]}_{p}$"]
        
        tfp.plot_distr1d(
            data,
            bins=100,
            range=(np.min(data), np.max(data)),
            ax=ax,
            label = label[i],
            units="",
            weights = weights * self.norm(weights) ,
            color = 'r',
            ) 
       
        tfp.plot_distr1d(
            data_new,
            bins=100,
            range=(np.min(data_new), np.max(data_new)),
            ax=ax,
            label = label[i],
            units="",
            weights = self.norm(data_new),
            color = 'k',
            ) 

        plt.legend(['MC OLD with weights', 'Generated sample - NEW model'], fontsize = 'x-small')
        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
        return plt.show()  
    
    def check_phsp(self, x):
        x = x.numpy()
        if np.min(x[:,0]) < (self.Mk + self.Mpi)**2 or np.max(x[:,0]) >  (self.MLc - self.Mp)**2:
            min = tf.ones_like(x[:, 0]) * (self.Mk + self.Mpi)**2
            max = tf.ones_like(x[:, 0]) * (self.MLc - self.Mp)**2
            index = tf.where(tf.logical_or(tf.less(x[:,0], min), tf.greater(x[:,0], max)))
            print(index)
            return index
            #raise Exception('Mass outside Phsp limits. Please check!')
        elif np.min(x[:,1]) < -np.pi or np.max(x[:,1]) > np.pi:
            raise Exception('Phi_K* outside Phsp limits. Please check!')
        elif np.min(x[:,2]) < 0. or np.max(x[:,2]) >  np.pi:
            raise Exception('Theta_k* outside Phsp limits. Please check!')
        elif np.min(x[:,3]) < -np.pi or np.max(x[:,3]) > np.pi:
            raise Exception('Phi_K outside Phsp limits. Please check!')
        elif np.min(x[:,4]) < 0. or np.max(x[:,4]) > np.pi:
            raise Exception('Theta_K outside Phsp limits. Please check!')


def Minimize(nll, model, tot_params, nfits = 1, randomise_params = True):
    nllval = None 
    reslts = None 
    for nfit in range(nfits):
        if randomise_params:
            #Randomising the starting values of the parameters according a uniform distribution 
            model.randomise_params()
        
        #Conduct the fit
        results = tfo.run_minuit(nll, list(tot_params.values()), use_gradient=False, use_hesse = False, use_minos = False)
        print(nll)

        #out of nfits pick the result with the least negative log likelihood (NLL)
        if nfit == 0: 
            print('Fit number', nfit)
            nllval = results['loglh']
            reslts = results
        else:
            print('Fit number', nfit)
            if nllval > results['loglh']:
                nllval = results['loglh']
                reslts = results

        print(results)
    
    #set the parameters to the fit results of the least NLL
    model.set_params_values(reslts['params'], isfitresult = True)
    print(reslts['params'])
    return reslts




def main():
    MLb = 5619.49997776e-3
    MLc = 2.28646
    Mp  = 0.938272046
    Mk  = 0.493677
    Mpi = 0.13957018

    #res_list = ["Kstar_kpi(892)"]
    #res_list = ["D(1232)"]
    #res_list = ["L(1520)"]
    res_list = ["phsp"]


    #models for weights
    old_model  = LcTopKpi_Model_OLD(MLc, Mp, Mk, Mpi, res_list, float_params = [])
    new_model  = LcTopKpi_Model_NEW(MLc, Mp, Mk, Mpi, False)

    #gen new root file for the reconstructed sample
    file_recon = uproot.open("Lb2Lcmunu_MagUp_2016_Combine.root:DecayTree;1")    
    cn_recon = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file_recon, res_list[0], "recon")
    #cn_recon.prepare_data_id()
    sample_wnans_r        = cn_recon.concat_phsp_vars()
    rem_nans              = cn_recon.remove_nans(sample_wnans_r)
    sample_r              = rem_nans[0]
    cn_recon.check_phsp(sample_r)

    keys_r                = rem_nans[1]
    weights_recon         = cn_recon.weights( new_model, old_model, sample_r )
    #cn_recon.make_root_file_recon(keys_r, weights_recon)
    
    #gen new root file for the generated sample
    #file_gen = uproot.open("LcMuNu_gen_new.root:DecayTree;1")    
    #cn_gen = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file_gen, res_list[0], "gen")
    #sample_wnans_g        = cn_gen.concat_phsp_vars()
    #sample_g              = cn_gen.remove_nans(sample_wnans_g)
    #weights_gen           = cn_gen.weights( new_model, old_model, sample_g )
    #cn_gen.make_root_file(weights_gen)

    
    #sample_wnans        = cn.concat_phsp_vars()
    #sample              = cn.remove_nans(sample_wnans)

    #phsp plot w and w/o weights
    ### Retrieve Weights
    #old_model  = LcTopKpi_Model_OLD(MLc, Mp, Mk, Mpi, res_list, float_params = [])
    #new_model  = LcTopKpi_Model_NEW(MLc, Mp, Mk, Mpi, False)
    #weights    = cn.weights( new_model, old_model, sample )
    new_data = new_model.MC(10000)

    cn_recon.plot_weights(sample_r[:, 0], weights_recon,  new_data[:, 0], 0)
    cn_recon.plot_weights(sample_r[:, 1], weights_recon,  new_data[:, 1], 1)
    cn_recon.plot_weights(sample_r[:, 2], weights_recon,  new_data[:, 2], 2)
    cn_recon.plot_weights(sample_r[:, 3], weights_recon,  new_data[:, 3], 3)
    cn_recon.plot_weights(sample_r[:, 4], weights_recon,  new_data[:, 4], 4)

    exit(1)
    float_params        = [res_list[0]+"_mass"]
    float_params       += [res_list[0]+"_width"]
    float_params       += [res_list[0]+"_real_1"]
    float_params       += [res_list[0]+"_real_2"]
    float_params       += [res_list[0]+"_imag_1"]
    float_params       += [res_list[0]+"_imag_2"]
    if 'Kstar' in res_list[0]:
        float_params += [res_list[0]+"_real_3"]
        float_params += [res_list[0]+"_imag_3"]
    
    old_model  = LcTopKpi_Model_OLD(MLc, Mp, Mk, Mpi, res_list, float_params)
    mc_old     = old_model.MC(10000)
    mc_old2    = old_model.prepare_data(mc_old)
    
    tot_params = old_model.fit_params
    NLL= old_model.unbinned_nll(sample)
    print(NLL(tot_params).numpy())

    results = Minimize(NLL, old_model, tot_params, nfits = 1, randomise_params = True)
    
    fitted_sample = old_model.MC(100000)
    cn.plots(sample[:, 0], fitted_sample[:, 0], 0)
    cn.plots(sample[:, 1], fitted_sample[:, 1], 1)
    cn.plots(sample[:, 2], fitted_sample[:, 2], 2)
    cn.plots(sample[:, 3], fitted_sample[:, 3], 3)
    cn.plots(sample[:, 4], fitted_sample[:, 4], 4)
    
    ''' Plot m2(pK), theta_p_Lam, m2(ppi), theta_p_D '''
    phspvar             = cn.phsp_vars()
    m2pk                = phspvar['m2_pk']
    m2ppi               = phspvar['m2_ppi']
    theta_p_Lam         = phspvar['p_theta_lambda']
    theta_p_D           = phspvar['p_theta_delta']

    keys_m2             = tf.where(tf.equal(tf.math.is_nan(m2ppi), False))
    keys_m3             = tf.where(tf.equal(tf.math.is_nan(m2pk), False))
    keys_theta1         = tf.where(tf.equal(tf.math.is_nan(theta_p_Lam), False))
    keys_theta2         = tf.where(tf.equal(tf.math.is_nan(theta_p_D), False))
    m2ppi_plot          = tf.gather(m2ppi, keys_m2[:,-1])
    m2pk_plot           = tf.gather(m2pk, keys_m3[:,-1])
    theta_p_Lam_plot    = tf.gather(theta_p_Lam, keys_theta1[:,-1])
    theta_p_D_plot      = tf.gather(theta_p_D, keys_theta2[:,-1])

    #Plot m2(pK), theta_p_Lam
    cn.plots( m2pk_plot, mc_old2['m2_pK'], 6)
    cn.plots( theta_p_Lam_plot, mc_old2['p_theta_lambda'], 7)

    #Plot m2(ppi), theta_p_D 
    #cn.plots( m2ppi_plot, mc_old2['m2_ppi'], 5)
    #cn.plots( theta_p_D_plot, mc_old2['p_theta_delta'], 8)
    
if __name__ == '__main__':
    main()

########### Define mass constants - GeV
#MLb = 5619.49997776e-3
#MLc = 2.28646
#Mp  = 0.938272046
#Mk  = 0.493677
#Mpi = 0.13957018

#file = uproot.open("LcMuNu_gen.root:MCDecayTreeTuple/MCDecayTree;1")
#cn = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file, "D(1232)")
#sample        = cn.concat_phsp_vars()
#s2            = cn.remove_nans(sample)
#print(s2)
#cn.prepare_data_id()
#cn.prepare_data_id(2224, 'D')
#cn.prepare_data_id(3124, 'L')

#cn.plot_MC_ID( "m2_kpi","Lambda_cplus" )
#cn.plot_MC_ID( "m2_kpi","Kminus" )
#cn.plot_MC_ID( "m2_kpi","piplus" )
#cn.plot_MC_ID( "m2_kpi","pplus" )


#####Perform Optimisation 
#cn.optimisation(s2 , "Kstar_kpi(892)")

#####Perform MC generation
#model = model_lc.LcTopKpi_Model(MLc, Mp, Mk, Mpi)
#toy_sample  = model.MC(10000, 5000, 100000)
#cn.optimisation(toy_sample , "full")



######Plotting
#sample = cn.concat_phsp_vars()
#sample2 = cn.phsp_vars()
#m2ppi = sample2['m2_ppi']
#m2pk  = sample2['m2_pk']
#m2pK_test = sample2['m2_pK_test']

#keys_m         = tf.where(tf.equal(tf.math.is_nan(sample[:,0]), False))
#keys_m2        = tf.where(tf.equal(tf.math.is_nan(m2ppi), False))
#keys_m3        = tf.where(tf.equal(tf.math.is_nan(m2pk), False))
#keys_phi1      = tf.where(tf.equal(tf.math.is_nan(sample[:,1]), False))
#keys_th1       = tf.where(tf.equal(tf.math.is_nan(sample[:,2]), False))
#keys_phi2      = tf.where(tf.equal(tf.math.is_nan(sample[:,3]), False))
#keys_th2       = tf.where(tf.equal(tf.math.is_nan(sample[:,4]), False))

#keys_test = tf.where(tf.equal(tf.math.is_nan(m2pK_test), False))

#m2kpi          = tf.gather(sample[:,0], keys_m[:,-1])
#kstar_phi_Lc   = tf.gather(sample[:,1], keys_phi1[:,-1])
#kstar_theta_Lc = tf.gather(sample[:,2], keys_th1[:,-1])
#k_phi_kstar    = tf.gather(sample[:,3], keys_phi2[:,-1])
#k_theta_kstar  = tf.gather(sample[:,4], keys_th2[:,-1])
#m2ppi_plot     = tf.gather(m2ppi, keys_m2[:,-1])
#m2pk_plot      = tf.gather(m2pk, keys_m3[:,-1])
#m2_test_pk = tf.gather(m2pK_test, keys_test[:,-1])

#print(m2kpi/ 1000000)
#print(kstar_theta_Lc)
#print(k_phi_kstar)
#cn.plot_MC_ID('m2_kpi', "pplus")
#cn.plot_MC_ID('m2_kpi', "piplus")
#cn.plot_MC_ID('m2_kpi', "Kminus")
#cn.plot_MC_ID('m2_kpi', "Lambda_cplus")

#cn.plot_MC(m2kpi, 0)
#cn.plot_MC(kstar_phi_Lc, 1)
#cn.plot_MC(kstar_theta_Lc, 2)
#cn.plot_MC(k_phi_kstar, 3)
#cn.plot_MC(k_theta_kstar, 4)
#cn.plot_MC(m2ppi_plot/1000000, 5)
#cn.plot_MC(m2pk_plot/1000000, 6)
#cn.plot_MC(m2_test_pk/1000000, 6)
#cn.plots(m2pk_plot/1000000,  m2_test_pk/1000000, 6)


#MLc2 = 2.28646
#Mp2  = 0.938272046
#Mk2  = 0.493677
#Mpi2 = 0.13957018
#my_model    = new_model_mc.LcTopKpi_Model(MLc2, Mp2, Mk2, Mpi2)
#toy_sample  = my_model.MC(100000, 50000, 1000000)
#switches    = np.array([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
#weights     = my_model.weights(toy_sample, switches)
#obsv        = my_model.prepare_data(toy_sample)

#cn.plots(m2kpi,  toy_sample[:,0], 0)
#cn.plots(kstar_phi_Lc,   toy_sample[:,1], 1)
#cn.plots(kstar_theta_Lc, toy_sample[:,2], 2)
#cn.plots(k_phi_kstar,    toy_sample[:,3], 3)
#cn.plots(k_theta_kstar,  toy_sample[:,4], 4)
#cn.plots(m2ppi_plot, obsv['m2_ppi'], 5)
#cn.plots(m2pk_plot, obsv['m2_pK'], 6)

