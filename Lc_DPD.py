#import few packages
import math
import sys, os
import tensorflow as tf
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # Do not use GPU for tensor flow
import numpy as np
import matplotlib.pyplot as plt
import pprint
import pickle

#amplitf
home = os.getenv('HOME')
sys.path.append(home+"/Packages/AmpliTF/")
import amplitf.interface as atfi
import amplitf.kinematics as atfk
import amplitf.dynamics as atfd
from amplitf.phasespace import Baryonic3BodyPhaseSpace
import amplitf.likelihood as atfl
import amplitf.dalitz_decomposition as atfdd

#tfa2
sys.path.append(home+"/Packages/TFA2/")
import tfa.optimisation as tfo
import tfa.toymc as tft
import tfa.plotting as tfp

#get current directory
fit_dir=os.path.dirname(os.path.abspath(__file__))

#define the complex
I = atfi.complex(atfi.const(0), atfi.const(1))

def MakeFourVector(pmag, costheta, phi, msq): 
    """Make 4-vec given magnitude, cos(theta), phi and mass"""
    sintheta = atfi.sqrt(1. - costheta**2) #theta 0 to pi => atfi.sin always pos
    px = pmag * sintheta * atfi.cos(phi)
    py = pmag * sintheta * atfi.sin(phi)
    pz = pmag * costheta
    E  = atfi.sqrt(msq + pmag**2)
    return atfk.lorentz_vector(atfk.vector(px, py, pz), E)

def pvecMag(Msq, m1sq, m2sq): 
    """Momentum mag of (1 or 2) in M rest frame"""
    kallen = atfi.sqrt(Msq**2 + m1sq**2 + m2sq**2 - 2.*(Msq*m1sq + Msq*m2sq + m1sq*m2sq))
    const  = tf.cast(2. * atfi.sqrt(Msq), dtype = tf.float64)
    return  kallen / const 

class LcTopKpi_Model:
    """Class that defines the Lc->pKpi decay"""

    def __init__(self, MLc, Mp, Mk, Mpi, res_list, float_params , model_type):
        """initialise some variables"""
        #Masses of particles involved 
        self.MLc     = MLc
        self.Mp      = Mp
        self.Mk      = Mk
        self.Mpi     = Mpi

        #Lists that will serve as index for incoherent sum of amplitudes (defined in units of 1/2)
        self.lLcs   = [1, -1]
        self.lps    = [1, -1]

        #Intrisic spins
        self.Jlam_c            = 1
        self.Jp                = 1
        
        #define limits, note mpK, mkpi do not have a rectangular phase space
        mpksq_range  = ((self.Mp + self.Mk )**2 , (self.MLc - self.Mpi)**2)
        mkpisq_range = ((self.Mk + self.Mpi)**2 , (self.MLc - self.Mp)**2 )
        #mppisq_range = ((self.Mp + self.Mpi)**2 , (self.MLc - self.Mk)**2 )
        self.phsp_limits  = [mpksq_range]
        self.phsp_limits += [mkpisq_range]
        self.phsp_limits += [(-1., 1.)]
        self.phsp_limits += [(-math.pi, math.pi)]
        self.phsp_limits += [(-math.pi, math.pi)]

        #set model type
        self.model_type    = model_type

        #define phase space
        self.phase_space   = Baryonic3BodyPhaseSpace(self.Mp, self.Mk, self.Mpi, self.MLc)

        #get all the fit params
        self.fit_params    = self.get_fit_params(res_list, float_params)

        #import resonance dictionary
        self.res_list      = res_list
        self.switches      = len(res_list) * [1]
        self.resn          = self.get_resonances(res_list)

    @atfi.function
    def Final_State_Momenta(self, m2_pk, m2_kpi, ctheta_p, phi_p, phi_kpi):
        # Magnitude of the momenta
        m2_ppi = self.MLc**2 + self.Mk**2 + self.Mp**2 + self.Mpi**2 - m2_pk - m2_kpi
        p_p    = atfk.two_body_momentum(self.MLc, self.Mp , atfi.sqrt(m2_kpi))
        p_k    = atfk.two_body_momentum(self.MLc, self.Mk , atfi.sqrt(m2_ppi))
        p_pi   = atfk.two_body_momentum(self.MLc, self.Mpi, atfi.sqrt(m2_pk ))
    
        cos_theta_pk  = (p_p * p_p + p_k * p_k - p_pi * p_pi) / (2.0 * p_p * p_k)
        cos_theta_kpi = (p_p * p_p + p_pi * p_pi - p_k * p_k) / (2.0 * p_p * p_pi)
        sin_theta_pk  = atfi.sqrt(1.0 - cos_theta_pk  ** 2) 
        sin_theta_kpi = atfi.sqrt(1.0 - cos_theta_kpi ** 2) 
    
        # Fix momenta with p3_p oriented in -z direction (+z is the quantisation axis)
        #Aligned CM frame: p1 = (0,0,-z), p2=(x,0,z), p3=(-x,0,z)
        p3_p_aligned  = atfk.vector(atfi.zeros(p_p)     , atfi.zeros(p_p) , -p_p)
        p3_k_aligned  = atfk.vector( p_k * sin_theta_pk , atfi.zeros(p_k) ,  p_k  * cos_theta_pk )
        p3_pi_aligned = atfk.vector(-p_pi* sin_theta_kpi, atfi.zeros(p_pi),  p_pi * cos_theta_kpi)
    
        #Space fixed CM frame: 
        p3_p_spacefixed  = atfk.rotate_euler(p3_p_aligned , phi_kpi, atfi.acos(ctheta_p), phi_p)
        p3_k_spacefixed  = atfk.rotate_euler(p3_k_aligned , phi_kpi, atfi.acos(ctheta_p), phi_p)
        p3_pi_spacefixed = atfk.rotate_euler(p3_pi_aligned, phi_kpi, atfi.acos(ctheta_p), phi_p)

        # Define 4-vectors
        p4_p  = atfk.lorentz_vector(p3_p_spacefixed  , atfi.sqrt(p_p  ** 2 + self.Mp**2 ))
        p4_k  = atfk.lorentz_vector(p3_k_spacefixed  , atfi.sqrt(p_k  ** 2 + self.Mk**2 ))
        p4_pi = atfk.lorentz_vector(p3_pi_spacefixed , atfi.sqrt(p_pi ** 2 + self.Mpi**2))

        return (p4_p, p4_k, p4_pi)

    @atfi.function
    def get_phase_variables(self, p4_p_spacefixed, p4_k_spacefixed, p4_pi_spacefixed):
        #get square dalitz plot variables
        m2_pk  = atfk.mass_squared(p4_p_spacefixed + p4_k_spacefixed )
        m2_kpi = atfk.mass_squared(p4_k_spacefixed + p4_pi_spacefixed)

        #get 3 vectors
        p3_p_spacefixed  = atfk.spatial_components(p4_p_spacefixed )
        p3_k_spacefixed  = atfk.spatial_components(p4_k_spacefixed )
        p3_pi_spacefixed = atfk.spatial_components(p4_pi_spacefixed)

        #get polar and azimuthal angles opposite to proton direction i.e. z direction
        th_p, ph_p = atfk.spherical_angles(-p3_p_spacefixed)
        cth_p      = atfi.cos(th_p)

        #get the angle between the kpi plane and the plane formed by the quantisation axis and proton momenta
        #unit normal vector to kpi plane
        norm_kpi      = atfk.unit_vector(atfk.cross_product(p3_k_spacefixed, p3_pi_spacefixed))
        #unit normal vector to Lc quantisation axis (+z axis) and proton momenta in Lc rest frame (-z axis)
        lc_quant_axis = atfk.vector(atfi.zeros(cth_p), atfi.zeros(cth_p) , atfi.ones(cth_p))
        norm_quantp   = atfk.unit_vector(atfk.cross_product(p3_p_spacefixed, lc_quant_axis))

        #cosine of angle b/w the two normal unit vectors
        x_comp  = atfk.scalar_product(norm_kpi, norm_quantp)
        #sine of angle b/w the two normal unit vectors
        y_comp  = atfk.scalar_product(atfk.cross_product(norm_kpi, norm_quantp), atfk.unit_vector(p3_p_spacefixed))
        #get the angle b/w the decay plane
        phi_kpi = atfi.atan2(-y_comp, -x_comp)

        return (m2_pk, m2_kpi, cth_p, ph_p, phi_kpi)
    
    def prepare_data(self,x):
        #Store Lc phase_space Varsiables
        Vars     = {}
        m2_pk    = self.phase_space.m2ab(x)
        m2_kpi   = self.phase_space.m2bc(x)
        ctheta_p = self.phase_space.cos_theta_a(x)
        phi_p    = self.phase_space.phi_a(x)
        phi_kpi  = self.phase_space.phi_bc(x)

        #calculate 4-mom in rest frame of lc
        m2_ppi    = self.MLc**2 + self.Mk**2 + self.Mp**2 + self.Mpi**2 - m2_pk - m2_kpi
        func_args = (self.MLc, self.Mp, self.Mk, self.Mpi, m2_kpi, m2_ppi, m2_pk)
        #p_p4mom_lc, k_p4mom_lc, pi_p4mom_lc = self.Final_State_Momenta(m2_pk, m2_kpi, ctheta_p, phi_p, phi_kpi)
        #m2_pk_o, m2_kpi_o, cth_p_o, ph_p_o, phi_kpi_o = self.get_phase_variables(p_p4mom_lc, k_p4mom_lc, pi_p4mom_lc)

        #calculate angles
        theta_23     = tf.acos(atfdd.cos_theta_23(*func_args))
        theta_hat_12 = tf.acos(atfdd.cos_theta_hat_1_canonical_2(*func_args)) #this has to be converted to theta_hat_21
        theta_31     = tf.acos(atfdd.cos_theta_31(*func_args))
        zeta_121     = tf.acos(atfdd.cos_zeta_1_aligned_1_in_frame_2(*func_args))
        theta_hat_31 = tf.acos(atfdd.cos_theta_hat_3_canonical_1(*func_args) )
        theta_12     = tf.acos(atfdd.cos_theta_12(*func_args))
        zeta_113     = tf.acos(atfdd.cos_zeta_1_aligned_3_in_frame_1(*func_args)) #this has to be converted to zeta_131

        #store the variables
        Vars['m2_pk']        = m2_pk    
        Vars['m2_kpi']       = m2_kpi    
        Vars['ctheta_p']     = ctheta_p    
        Vars['phi_p']        = phi_p    
        Vars['phi_kpi']      = phi_kpi    
        Vars['m2_ppi']       = m2_ppi    
        Vars['theta_23']     = theta_23    
        Vars['theta_hat_12'] = theta_hat_12
        Vars['theta_31']     = theta_31    
        Vars['zeta_121']     = zeta_121    
        Vars['theta_hat_31'] = theta_hat_31
        Vars['theta_12']     = theta_12    
        Vars['zeta_113']     = zeta_113    
        return Vars
    
    def _l_bc(self, ja , jb , jc):
        l_bc1 = ja/2 - jb/2 - jc/2
        l_bc2 = ja/2 - jb/2 + jc/2
        l_bc  = np.min( [l_bc1 , l_bc2] )
        return l_bc

    def _l_bc_k(self, ja , jb , jc):
        l_bc1 = ja/2 - jb/2 - jc/2
        l_bc2 = ja/2 - jb/2 + jc/2
        l_bc  = np.min( [l_bc1 , l_bc2] )
        return np.max([0, l_bc])

    def _l_bc_strong(self, ja , jb , jc, parity):
        l_bc1 = ja/2 - jb/2 - jc/2
        l_bc2 = ja/2 - jb/2 + jc/2
        l_bc  = [l_bc1 , l_bc2]
        for i in l_bc:
            eta = (-1)**(i+1)
            if parity == eta:
                return np.abs(i)

    def get_fit_params(self, res_list, float_params):
        
        f_params = {}
        if self.model_type == 'OLD':
            f_params["Px"]   = tfo.FitParameter("Px"  , 0.,   -1.   , 1.   , 0.01)
            f_params["Py"]   = tfo.FitParameter("Py"  , 0.,   -1.   , 1.   , 0.01)
            f_params["Pz"]   = tfo.FitParameter("Pz"  , 0.,   -1.   , 1.   , 0.01)
        else:
            f_params["Px"]   = tfo.FitParameter("Px"  , 0.208155,   -1.   , 1.   , 0.01)
            f_params["Py"]   = tfo.FitParameter("Py"  , 0.011094,   -1.   , 1.   , 0.01)
            f_params["Pz"]   = tfo.FitParameter("Pz"  , -0.66334,   -1.   , 1.   , 0.01)

        if 'Kstar_kpi(892)' in res_list:
            if self.model_type == 'OLD':
                f_params["Kstar_kpi(892)_mass"]   = tfo.FitParameter("Kstar_kpi(892)_mass"  ,  0.8933162508700607 ,   0.   , 2.   , 0.01)
                f_params["Kstar_kpi(892)_width"]  = tfo.FitParameter("Kstar_kpi(892)_width" ,  0.04379753591098739,   0.03 , 0.05 , 0.0001)
                f_params["Kstar_kpi(892)_real_1"] = tfo.FitParameter("Kstar_kpi(892)_real_1",  5.043546642212341  ,   -10. , 10.  , 0.01)
                f_params["Kstar_kpi(892)_imag_1"] = tfo.FitParameter("Kstar_kpi(892)_imag_1", -1.862253283747373  ,   -10. , 10.  , 0.01)
                f_params["Kstar_kpi(892)_real_2"] = tfo.FitParameter("Kstar_kpi(892)_real_2", -3.1597560963214457 ,   -10. , 10.  , 0.01)
                f_params["Kstar_kpi(892)_imag_2"] = tfo.FitParameter("Kstar_kpi(892)_imag_2", -3.3117031588059955 ,   -10. , 10.  , 0.01)
                f_params["Kstar_kpi(892)_real_3"] = tfo.FitParameter("Kstar_kpi(892)_real_3",  0.12845976085492694,   -10. , 10.  , 0.01)
                f_params["Kstar_kpi(892)_imag_3"] = tfo.FitParameter("Kstar_kpi(892)_imag_3", -4.160977960398091  ,   -10. , 10.  , 0.01)
            else:
                f_params["Kstar_kpi(892)_mass"]   = tfo.FitParameter("Kstar_kpi(892)_mass"  , 0.89581  ,     0. , 10.  , 0.01)
                f_params["Kstar_kpi(892)_width"]  = tfo.FitParameter("Kstar_kpi(892)_width" , 0.0474   ,  0.001 , 10.  , 0.01)
                #f_params["Kstar_kpi(892)_real_1"] = tfo.FitParameter("Kstar_kpi(892)_real_1", 1.192614 ,   -10. , 10.  , 0.01)
                #f_params["Kstar_kpi(892)_imag_1"] = tfo.FitParameter("Kstar_kpi(892)_imag_1", -1.025814,   -10. , 10.  , 0.01)
                #f_params["Kstar_kpi(892)_real_2"] = tfo.FitParameter("Kstar_kpi(892)_real_2", -3.141446,   -10. , 10.  , 0.01)
                #f_params["Kstar_kpi(892)_imag_2"] = tfo.FitParameter("Kstar_kpi(892)_imag_2", -3.29341 ,   -10. , 10.  , 0.01)
                #f_params["Kstar_kpi(892)_real_3"] = tfo.FitParameter("Kstar_kpi(892)_real_3", -0.727145,   -10. , 10.  , 0.01)
                #f_params["Kstar_kpi(892)_imag_3"] = tfo.FitParameter("Kstar_kpi(892)_imag_3", -4.155027,   -10. , 10.  , 0.01)
                f_params["Kstar_kpi(892)_real_1"] = tfo.FitParameter("Kstar_kpi(892)_real_1", 2.392614 ,   -10. , 10.  , 0.01)
                f_params["Kstar_kpi(892)_imag_1"] = tfo.FitParameter("Kstar_kpi(892)_imag_1", -2.025814,   -10. , 10.  , 0.01)
                f_params["Kstar_kpi(892)_real_2"] = tfo.FitParameter("Kstar_kpi(892)_real_2", -4.041446,   -10. , 10.  , 0.01)
                f_params["Kstar_kpi(892)_imag_2"] = tfo.FitParameter("Kstar_kpi(892)_imag_2", -4.09341 ,   -10. , 10.  , 0.01)
                f_params["Kstar_kpi(892)_real_3"] = tfo.FitParameter("Kstar_kpi(892)_real_3", -1.527145,   -10. , 10.  , 0.01)
                f_params["Kstar_kpi(892)_imag_3"] = tfo.FitParameter("Kstar_kpi(892)_imag_3", -5.055027,   -10. , 10.  , 0.01)

        if 'D(1232)' in res_list:
            if self.model_type == 'OLD':
                f_params["D(1232)_mass"]   = tfo.FitParameter("D(1232)_mass"  , 1.2418535600679992, 0.    , 10. , 0.01)
                f_params["D(1232)_width"]  = tfo.FitParameter("D(1232)_width" , 0.1541041332022065,  0.01 , 5. , 0.0001)
                f_params["D(1232)_real_1"] = tfo.FitParameter("D(1232)_real_1", 1. , -10.         , 10.   , 0.01)
                f_params["D(1232)_imag_1"] = tfo.FitParameter("D(1232)_imag_1", 0. , -10.         , 10.   , 0.01)
                f_params["D(1232)_real_2"] = tfo.FitParameter("D(1232)_real_2", 1. , -10.         , 10.   , 0.01)
                f_params["D(1232)_imag_2"] = tfo.FitParameter("D(1232)_imag_2", 0. , -10.         , 10.   , 0.01)
            else:
                f_params["D(1232)_mass"]  = tfo.FitParameter("D(1232)_mass"   , 1.232      , 0.    , 10. , 0.01)
                f_params["D(1232)_width"] = tfo.FitParameter("D(1232)_width"  , 0.117      , 0.01  , 10. , 0.01)
                #f_params["D(1232)_real_1"] = tfo.FitParameter("D(1232)_real_1", -6.778191  , -10.  , 10. , 0.01)
                #f_params["D(1232)_imag_1"] = tfo.FitParameter("D(1232)_imag_1", 3.051805   , -10.  , 10. , 0.01)
                #f_params["D(1232)_real_2"] = tfo.FitParameter("D(1232)_real_2", -12.987193 , -10.  , 10. , 0.01)
                #f_params["D(1232)_imag_2"] = tfo.FitParameter("D(1232)_imag_2",  4.528336  , -10.  , 10. , 0.01)
                f_params["D(1232)_real_1"] = tfo.FitParameter("D(1232)_real_1", -8.078191  , -10.  , 10. , 0.01)
                f_params["D(1232)_imag_1"] = tfo.FitParameter("D(1232)_imag_1", 5.551805   , -10.  , 10. , 0.01)
                f_params["D(1232)_real_2"] = tfo.FitParameter("D(1232)_real_2", -14.087193 , -10.  , 10. , 0.01)
                f_params["D(1232)_imag_2"] = tfo.FitParameter("D(1232)_imag_2",  5.928336  , -10.  , 10. , 0.01)

        if 'L(1520)' in res_list:
            if self.model_type == 'OLD':
                f_params["L(1520)_mass"]   = tfo.FitParameter("L(1520)_mass"  , 1.5205748791097067  ,   0.   , 10. , 0.01)
                f_params["L(1520)_width"]  = tfo.FitParameter("L(1520)_width" , 0.01590519483058561 ,  0.0001 ,  2. , 0.0001) 
                f_params["L(1520)_real_1"] = tfo.FitParameter("L(1520)_real_1", 1.0,   -10.         , 10. , 0.01)
                f_params["L(1520)_imag_1"] = tfo.FitParameter("L(1520)_imag_1", 0.0,   -10.         , 10. , 0.01)
                f_params["L(1520)_real_2"] = tfo.FitParameter("L(1520)_real_2", 1.0,   -10.         , 10. , 0.01)
                f_params["L(1520)_imag_2"] = tfo.FitParameter("L(1520)_imag_2", 0.0,   -10.         , 10. , 0.01)
            else:
                f_params["L(1520)_mass"]   = tfo.FitParameter("L(1520)_mass"  , 1.5195   ,   0. , 10. , 0.01)
                f_params["L(1520)_width"]  = tfo.FitParameter("L(1520)_width" , 0.0156   ,   0.001 , 10. , 0.01)
                f_params["L(1520)_real_1"] = tfo.FitParameter("L(1520)_real_1", 0.293998 ,   -10. , 10. , 0.01)
                f_params["L(1520)_imag_1"] = tfo.FitParameter("L(1520)_imag_1", 0.044324 ,   -10. , 10. , 0.01)
                f_params["L(1520)_real_2"] = tfo.FitParameter("L(1520)_real_2", -0.160687,   -10. , 10. , 0.01)
                f_params["L(1520)_imag_2"] = tfo.FitParameter("L(1520)_imag_2", 1.498833 ,   -10. , 10. , 0.01)

        for k in list(f_params.keys()):
            if k in list(f_params.keys()) and k in float_params:
                f_params[k].float()
            else:
                f_params[k].fix()
            
        return f_params

    def get_resonances(self, res_list):
        resonances = {}
        for res in res_list:
            if res == "Kstar_kpi(892)":
                    l_lamc_val = int( self._l_bc_k( 2 , self.Jp , self.Jlam_c ) )
                    resonances["Kstar_kpi(892)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: self.fit_params["Kstar_kpi(892)_mass"](), 
                                            "width"     : lambda: self.fit_params["Kstar_kpi(892)_width"](),  
                                            "spin"      : 2, 
                                            "parity"    : -1, 
                                            "coupl"     : [     
                                                            lambda: atfi.complex( atfi.const(1.) , atfi.const(0.) ), 
                                                            lambda: atfi.complex( self.fit_params["Kstar_kpi(892)_real_1"](), self.fit_params["Kstar_kpi(892)_imag_1"]()),       
                                                            lambda: atfi.complex( self.fit_params["Kstar_kpi(892)_real_2"](), self.fit_params["Kstar_kpi(892)_imag_2"]()), 
                                                            lambda: atfi.complex( self.fit_params["Kstar_kpi(892)_real_3"](), self.fit_params["Kstar_kpi(892)_imag_3"]()),
                                                        ],
                                            "l_lamc"    : l_lamc_val,
                                            "l_res"     : int( 2 / 2 ),
                    }
                    #print(res_val[r]['l_res'] , res_val[r]['l_lamc']) (1,0)

            elif res == "Kstar_kpi0(700)":
                    resonances["Kstar_kpi0(700)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(0.824), 
                                            "width"     : lambda: atfi.const(0.478),  
                                            "spin"      : 0, 
                                            "parity"    : 1, 
                                            "gamma"     : atfi.const(0.94106),
                                            "alpha"     : atfi.const(0.),
                                            "coupl"     : [  
                                                            #lambda: atfi.complex(atfi.const(0.068908), atfi.const(2.521444)),
                                                            #lambda: atfi.complex(atfi.const(-2.68563), atfi.const(0.03849)),
                                                            lambda: atfi.complex(atfi.const(1.588908), atfi.const(4.921444)),
                                                            lambda: atfi.complex(atfi.const(-5.98563), atfi.const(2.53849)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc_k( 0 , self.Jp , self.Jlam_c ) ),
                                            "l_res"     : int( 0 / 2 ), 
                    }

            elif res == "Kstar_kpi0(1430)":
                    resonances["Kstar_kpi0(1430)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.375), 
                                            "width"     : lambda: atfi.const(0.190),  
                                            "spin"      : 0, 
                                            "parity"    : 1, 
                                            "gamma"     : atfi.const(0.020981),
                                            "alpha"     : atfi.const(0.),
                                            "coupl"     : [     
                                                            #lambda: atfi.complex(atfi.const(-6.71516), atfi.const(10.479411)),
                                                            #lambda: atfi.complex(atfi.const(0.219754 ), atfi.const(8.741196)),
                                                            lambda: atfi.complex(atfi.const(-15.81516), atfi.const(19.579411)),
                                                            lambda: atfi.complex(atfi.const(9.319754 ), atfi.const(17.841196)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc_k( 0 , self.Jp , self.Jlam_c ) ),
                                            "l_res"     : int( 0 / 2 ), 
                    }
            
            elif res == "D(1232)":
                    l_lamc_val = int( self._l_bc( 3 , 0 , self.Jlam_c ) )
                    if self.model_type == 'OLD': l_lamc_val = 3
                    resonances["D(1232)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: self.fit_params["D(1232)_mass"](), 
                                            "width"     : lambda: self.fit_params["D(1232)_width"](),  
                                            "spin"      : 3, 
                                            "parity"    : 1, 
                                            "coupl"     : [    
                                                            lambda: atfi.complex( self.fit_params["D(1232)_real_1"](), self.fit_params["D(1232)_imag_1"]()),       
                                                            lambda: atfi.complex( self.fit_params["D(1232)_real_2"](), self.fit_params["D(1232)_imag_2"]()),
                                                        ],
                                            "l_lamc"    : l_lamc_val,
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0, 3 , 1) ), 
                    }
                    #print(res_val[r]['l_res'] , res_val[r]['l_lamc']) (1,1)

            elif res == "D(1600)":
                    resonances["D(1600)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.64), 
                                            "width"     : lambda: atfi.const(0.3),  
                                            "spin"      : 3, 
                                            "parity"    : 1, 
                                            "coupl"     : [        
                                                            lambda: atfi.complex(atfi.const(11.401585), atfi.const(-3.125511)),
                                                            lambda: atfi.complex(atfi.const(6.729211), atfi.const(-1.002383)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 3 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp, 0, 3 , 1) ),    
                    }
            
            elif res == "D(1700)":
                    resonances["D(1700)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.69), 
                                            "width"     : lambda: atfi.const(0.38),  
                                            "spin"      : 3, 
                                            "parity"    : -1, 
                                            "coupl"     : [     
                                                            lambda: atfi.complex(atfi.const(10.37828), atfi.const(1.434872)),
                                                            lambda: atfi.complex(atfi.const(12.874102), atfi.const(2.10557)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 3 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp, 0, 3 , -1) ),    
                    }
            
            elif res == "L(1405)":
                    resonances["L(1405)"] = {
                                            "lineshape" : "subthreshold_breit_wigner_lineshape",  
                                            "mass"      : lambda: atfi.const(1.4051), 
                                            "width1"    : lambda: atfi.const(0.0505),  
                                            "width2"    : lambda: atfi.const(0.0505),
                                            "ma2"       : atfi.const(1.18937), #sigma 
                                            "mb2"       : atfi.const(0.13957018), #pion
                                            "spin"      : 1, 
                                            "parity"    : -1, 
                                            "coupl"     : [     
                                                            #lambda: atfi.complex(atfi.const(-4.572486), atfi.const(3.190144)),
                                                            #lambda: atfi.complex(atfi.const(10.44608), atfi.const(2.787441)),
                                                            lambda: atfi.complex(atfi.const(-7.772486), atfi.const(6.490144)),
                                                            lambda: atfi.complex(atfi.const(13.74608), atfi.const(5.987441)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 1 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0 , 1 , -1) ),    
                    }

            elif res == "L(1520)":
                    l_lamc_val = int( self._l_bc( 3 , 0 , self.Jlam_c ) )
                    if self.model_type == 'OLD': l_lamc_val = 4
                    resonances["L(1520)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: self.fit_params["L(1520)_mass"](),  
                                            "width"     : lambda: self.fit_params["L(1520)_width"](),  
                                            "spin"      :  3, 
                                            "parity"    : -1, 
                                            "coupl"     : [     
                                                            lambda: atfi.complex( self.fit_params["L(1520)_real_1"](), self.fit_params["L(1520)_imag_1"]()),       
                                                            lambda: atfi.complex( self.fit_params["L(1520)_real_2"](), self.fit_params["L(1520)_imag_2"]()),
                                                          ],
                                            "l_lamc"    : l_lamc_val,
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0 , 3 , -1) ), 
                    }
                    #print(res_val[r]['l_res'] , res_val[r]['l_lamc']) #(2,1)

            elif res == "L(1600)":
                    resonances["L(1600)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.630), 
                                            "width"     : lambda: atfi.const(0.25),  
                                            "spin"      : 1, 
                                            "parity"    : 1, 
                                            "coupl"     : [     
                                                            #lambda: atfi.complex(atfi.const(4.840649), atfi.const(3.082786)),
                                                            #lambda: atfi.complex(atfi.const(-6.971233), atfi.const(0.842435)),
                                                            lambda: atfi.complex(atfi.const(7.840649), atfi.const(6.082786)),
                                                            lambda: atfi.complex(atfi.const(-9.971233), atfi.const(2.842435)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 1 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0 , 1 , 1) ),  
                    } 
            
            elif res == "L(1670)":
                    resonances["L(1670)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.670), 
                                            "width"     : lambda: atfi.const(0.03),  
                                            "spin"      : 1., 
                                            "parity"    : -1., 
                                            "coupl"     : [     
                                                            #lambda: atfi.complex(atfi.const(-0.339585), atfi.const(-0.144678)),
                                                            #lambda: atfi.complex(atfi.const(-0.570978), atfi.const(1.011833)),
                                                            lambda: atfi.complex(atfi.const(-0.669585), atfi.const(-0.244678)),
                                                            lambda: atfi.complex(atfi.const(-1.070978), atfi.const(2.011833)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 1 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0 , 1 , -1) ),  
                    }
            
            elif res == "L(1690)":
                    resonances["L(1690)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.690), 
                                            "width"     : lambda: atfi.const(0.07),  
                                            "spin"      : 3, 
                                            "parity"    : -1, 
                                            "coupl"     : [     
                                                            lambda: atfi.complex(atfi.const(-0.385772), atfi.const(-0.110235)),
                                                            lambda: atfi.complex(atfi.const(-2.730592), atfi.const(-0.353613)),
                                                        
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 3 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0 , 3 , -1) ),  
                    }
            
            elif res == "L(2000)":
                    resonances["L(2000)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.98819), 
                                            "width"     : lambda: atfi.const(0.17926 ),  
                                            "spin"      : 1, 
                                            "parity"    : -1, 
                                            "coupl"     : [     
                                                            #lambda: atfi.complex(atfi.const(-8.014857), atfi.const(-7.614006)),
                                                            #lambda: atfi.complex(atfi.const(-4.336255), atfi.const(-3.796192)),
                                                            lambda: atfi.complex(atfi.const(-11.214857), atfi.const(-10.814006)),
                                                            lambda: atfi.complex(atfi.const(-6.536255), atfi.const(-5.996192)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 1 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0 , 1 , -1) ),  
                    }
        return resonances 
    
    def BuggLineShape(self, m2, m0, gamma0, gamma, alpha, ma, mb, mc, md, dr, dd, lr, ld, barrierFactor = True, ma0=None, md0 = None) : 
        m   = atfi.sqrt(m2)
        q   = atfk.two_body_momentum(md, m, mc)
        q0  = atfk.two_body_momentum(md if md0 is None else md0, m0, mc)
        p   = atfk.two_body_momentum(m, ma, mb)
        p0  = atfk.two_body_momentum(m0, ma if ma0 is None else ma0, mb)
        ffr = atfd.blatt_weisskopf_ff(p, p0, dr, lr)
        ffd = atfd.blatt_weisskopf_ff(q, q0, dd, ld)

        sa = ma*ma-0.5*mb*mb
        width = (m2 - sa)/(m0*m0 - sa)*gamma0*atfi.exp(-gamma*m2)
        bw = atfd.relativistic_breit_wigner(m2, m0, width)

        ff = ffr*ffd
        if barrierFactor : 
            b1 = atfd.orbital_barrier_factor(p, p0, lr)
            b2 = atfd.orbital_barrier_factor(q, q0, ld)
            ff *= b1*b2
        return bw*atfi.complex(ff*atfi.exp(-alpha*q*q), atfi.const(0.))


    def FlatteLineShapeCorrected(self, m2, m0, gamma1, gamma2, ma, mb, ma2, mb2,mc, md, dr, dd, lr, ld, barrierFactor = True, ma0=None, ma02=None, md0 = None) : 
        m = atfi.sqrt(m2)
        #Mother particle decay
        q  = atfk.two_body_momentum(md, m, mc)
        q0 = atfk.two_body_momentum(md if md0 is None else md0, m0, mc)
        ffd = atfd.blatt_weisskopf_ff(q, q0, dd, ld)

        #Resonance First decay channel
        p  = atfk.two_body_momentum(m, ma, mb)
        #Use alternative channel since first is under threshold
        p0 = atfk.two_body_momentum(m0, ma2 if ma02 is None else ma02, mb2)
        ffr = atfd.blatt_weisskopf_ff(p, p0, dr, lr)
        width = atfd.mass_dependent_width(m, m0, gamma1, p, p0, ffr, lr)

        #Resonance Second decay channel
        p2  = atfk.two_body_momentum(m, ma2, mb2)
        ffr2 = atfd.blatt_weisskopf_ff(p2, p0, dr, lr)
        width2 = atfd.mass_dependent_width(m, m0, gamma2, p2, p0, ffr2, lr)

        totwidth = width+width2

        bw = atfd.relativistic_breit_wigner(m2, m0, totwidth)
        ff = ffr*ffd
        if barrierFactor : 
            b1 = atfd.orbital_barrier_factor(p, p0, lr)
            b2 = atfd.orbital_barrier_factor(q, q0, ld)
            ff *= b1*b2

        return bw*atfi.complex(ff, atfi.const(0.))
    
    def get_Lc_amps(self, Obvs):

        @atfi.function
        def _Ampl_K( res, Nu , lp , lres, coupling ):

            #delta function
            delta = atfi.cast_complex(atfi.const(1.))
            if Nu/2. != (lres/2. - lp/2.): delta = atfi.cast_complex(atfi.const(0.))

            #lineshape
            if 'kpi0' in res:
                Xs = self.BuggLineShape( m2_kpi, res_val[res]['mass'](), res_val[res]['width'](), res_val[res]['gamma'], \
                                         res_val[res]['alpha'], self.Mk, self.Mpi , self.Mp , self.MLc , d_res, d_Lambda_c, \
                                         res_val[res]['l_res'] , res_val[res]['l_lamc'], barrierFactor = True) 
            else:
                Xs = atfd.breit_wigner_lineshape( m2_kpi , res_val[res]["mass"]() , res_val[res]["width"]() , self.Mk , \
                                                      self.Mpi , self.Mp , self.MLc , d_res, d_Lambda_c, \
                                                      res_val[res]['l_res'] , res_val[res]['l_lamc'])

            A  = atfi.cast_complex(atfi.sqrt(2.) * atfi.sqrt(2. * res_val[res]['spin']/2. + 1))
            A *= delta
            A *= Xs
            A *= atfi.cast_complex(atfk.wigner_small_d( theta_23, res_val[res]['spin'] , lres , 0 ) )
            A *= atfi.cast_complex( ( -1 )**( self.Jp/2. - lp/2. ) ) #factor due JW particle-2 phase convention of helicity coupling
            A *= coupling
            return A
        
        @atfi.function
        def _Ampl_D( res, Nu , lp_d , lres , coupling ,lp):
            #lineshape
            Xs  = atfd.breit_wigner_lineshape( m2_ppi , res_val[res]['mass'](), res_val[res]['width'](), self.Mp, self.Mpi , \
											  self.Mk , self.MLc , d_res , d_Lambda_c ,res_val[res]['l_res'] , res_val[res]['l_lamc'])
        
            #parity conservation factor in JW convention
            eta = atfi.cast_complex( 1.)
            if lp_d == -1: eta = atfi.cast_complex( ( -1 )**( 3/2. - res_val[res]['spin']/2.) * res_val[res]['parity'] )

            A  = atfi.cast_complex(atfi.sqrt(2.) * atfi.sqrt(2. * res_val[res]['spin']/2. + 1))
            A *= atfi.cast_complex( ( -1 )**(Nu/2. - lres/2.) * atfk.wigner_small_d( theta_hat_12 , self.Jlam_c , Nu , lres ) )
            A *= Xs
            if self.model_type != 'OLD': A *= atfi.cast_complex( atfk.wigner_small_d( theta_31  , res_val[res]['spin'] , lres , -lp_d) )
            A *= atfi.cast_complex( atfk.wigner_small_d( zeta_121 , self.Jlam_c , lp_d , lp ) )
            A *= atfi.cast_complex( ( -1 )**( self.Jp/2. - lp_d/2. ) ) #factor due JW particle-2 phase convention of helicity coupling
            A *= coupling
            A *= eta
            return A 

        @atfi.function
        def _Ampl_L( res, Nu , lp_d , lres , coupling ,  lp ):
            #lineshape
            if res == 'L(1405)':
                Xs = self.FlatteLineShapeCorrected( m2_pk, res_val[res]['mass'](), res_val[res]["width1"](), res_val[res]["width2"](), self.Mp, self.Mk, \
                                                   res_val[res]["ma2"], res_val[res]["mb2"], self.Mpi, self.MLc, d_res, d_Lambda_c, \
                                                   res_val[res]['l_res'] , res_val[res]['l_lamc'], barrierFactor = True)
            else:
                barrier_factor = True
                if self.model_type == 'OLD': barrier_factor = False
                Xs = atfd.breit_wigner_lineshape( m2_pk , res_val[res]['mass'](), res_val[res]['width'](), self.Mp , self.Mk , self.Mpi , \
                                                 self.MLc, d_res , d_Lambda_c , res_val[res]['l_res'] , res_val[res]['l_lamc'], barrier_factor = barrier_factor)

            #parity conservation factor in JW convention
            eta  = atfi.cast_complex( 1.)
            if lp_d == -1: eta = atfi.cast_complex( ( -1 )**( 3/2. - res_val[res]['spin']/2.) * res_val[res]['parity'] )

            A  = atfi.cast_complex(atfi.sqrt(2.) * atfi.sqrt(2. * res_val[res]['spin']/2. + 1))
            A *= atfi.cast_complex(atfk.wigner_small_d( theta_hat_31, self.Jlam_c , Nu , lres ) )
            A *= Xs
            if self.model_type != 'OLD': A *= atfi.cast_complex( atfk.wigner_small_d( theta_12, res_val[res]['spin'] , lres , lp_d ) )
            A *= atfi.cast_complex( ( -1 )**(lp_d/2. - lp/2.))  * atfi.cast_complex( atfk.wigner_small_d( zeta_113 , self.Jlam_c , lp_d , lp ) )
            A *= coupling
            A *= eta
            return A
            

        @atfi.function
        def _A_res(Nu, lp):
            lp_ds = [1, -1]
            ampl_k  = atfi.complex(atfi.const(0.) , atfi.const(0.))
            ampl_l  = atfi.complex(atfi.const(0.) , atfi.const(0.))
            ampl_d  = atfi.complex(atfi.const(0.) , atfi.const(0.))
            #coherently sum over resonances and their helicities
            for indx, r in enumerate(list(res_val.keys())):
                if 'kpi(892)' in r:
                    coupling1 = res_val[r]['coupl'][0]()
                    coupling2 = res_val[r]['coupl'][1]()
                    coupling3 = res_val[r]['coupl'][2]()
                    coupling4 = res_val[r]['coupl'][3]()
                    if lp == 1:
                        ampl_k1  = _Ampl_K(r, Nu , lp, 0., coupling1 )
                        ampl_k1 += _Ampl_K(r, Nu , lp, 2., coupling2 )
                    else:
                        ampl_k1  = _Ampl_K(r,  Nu , lp, 0. , coupling3 )
                        ampl_k1 += _Ampl_K(r,  Nu , lp, -2., coupling4 )
                
                    ampl_k  += atfi.cast_complex(self.switches[indx]) * ampl_k1
                    
                elif 'kpi0' in r:
                    coupling1       = res_val[r]['coupl'][1]()
                    coupling2       = res_val[r]['coupl'][0]()
                    if lp == 1:
                        ampl_k1  = _Ampl_K(r, Nu , lp, 0., coupling1 )
                    else:
                        ampl_k1  = _Ampl_K(r, Nu , lp, 0., coupling2 )
                
                    ampl_k  += atfi.cast_complex(self.switches[indx]) * ampl_k1
                elif 'L' in r: 
                    #coherently sum of lp_ds
                    ampl_lpd_L = atfi.complex(atfi.const(0.) , atfi.const(0.))
                    for lp_d in lp_ds:
                        coupling1   = res_val[r]['coupl'][0]()
                        coupling2   = res_val[r]['coupl'][1]()
                        ampl_lpd_L += _Ampl_L( r, Nu , lp_d ,  1 , coupling1 , lp ) 
                        ampl_lpd_L += _Ampl_L( r, Nu , lp_d , -1 , coupling2 , lp ) 

                    ampl_l  += atfi.cast_complex(self.switches[indx]) * ampl_lpd_L
                elif 'D' in r: 
                    #coherently sum of lp_ds
                    ampl_lpd_D = atfi.complex(atfi.const(0.) , atfi.const(0.))
                    for lp_d in lp_ds:
                        coupling1   = res_val[r]['coupl'][0]()
                        coupling2   = res_val[r]['coupl'][1]()
                        ampl_lpd_D += _Ampl_D( r, Nu , lp_d ,  1 , coupling1 , lp )
                        ampl_lpd_D += _Ampl_D( r, Nu , lp_d , -1 , coupling2 , lp )

                    ampl_d  += atfi.cast_complex(self.switches[indx]) * ampl_lpd_D
                else:
                    raise Exception('Channel not specified for resonance. Please check!')

            amplitude = ampl_k + ampl_l + ampl_d
            return amplitude

		#get the resonances
        res_val = self.resn

        #get variables
        m2_pk        = Obvs['m2_pk']        
        m2_kpi       = Obvs['m2_kpi']       
        ctheta_p     = Obvs['ctheta_p']
        phi_p        = Obvs['phi_p']  
        phi_kpi      = Obvs['phi_kpi']
        m2_ppi       = Obvs['m2_ppi']       
        theta_23     = Obvs['theta_23']     
        theta_hat_12 = Obvs['theta_hat_12'] 
        theta_31     = Obvs['theta_31']     
        zeta_121     = Obvs['zeta_121']     
        theta_hat_31 = Obvs['theta_hat_31'] 
        theta_12     = Obvs['theta_12']     
        zeta_113     = Obvs['zeta_113']   

        #finite widths - can be varied to find optimal - (Blatt-Weisskopf radii)
        d_res             = atfi.const(1.5)  #GeV
        d_Lambda_c        = atfi.const(5.)  #GeV

        #invariant amplitude
        O_nu_lp = {}
        O_nu_lp[( 1 ,  1)] = _A_res( 1,  1)
        O_nu_lp[( 1 , -1)] = _A_res( 1, -1)
        O_nu_lp[(-1 ,  1)] = _A_res(-1,  1)
        O_nu_lp[(-1 , -1)] = _A_res(-1, -1)

        #wignerd rotate
        D_lLc_nu = {}
        D_lLc_nu[( 1,  1)] = tf.math.conj( atfk.wigner_capital_d( phi_p , tf.acos(ctheta_p), phi_kpi, self.Jlam_c,  1,  1 ) )
        D_lLc_nu[( 1, -1)] = tf.math.conj( atfk.wigner_capital_d( phi_p , tf.acos(ctheta_p), phi_kpi, self.Jlam_c,  1, -1 ) )
        D_lLc_nu[(-1,  1)] = tf.math.conj( atfk.wigner_capital_d( phi_p , tf.acos(ctheta_p), phi_kpi, self.Jlam_c, -1,  1 ) )
        D_lLc_nu[(-1, -1)] = tf.math.conj( atfk.wigner_capital_d( phi_p , tf.acos(ctheta_p), phi_kpi, self.Jlam_c, -1, -1 ) )

        #M_lLc_lp
        M_lLc_lp = {}
        M_lLc_lp[( 1,  1)] = atfi.complex(atfi.const(0), atfi.const(0))
        M_lLc_lp[( 1, -1)] = atfi.complex(atfi.const(0), atfi.const(0))
        M_lLc_lp[(-1,  1)] = atfi.complex(atfi.const(0), atfi.const(0))
        M_lLc_lp[(-1, -1)] = atfi.complex(atfi.const(0), atfi.const(0))
        for lLc in [-1, 1]:
            for lp in [-1, 1]:
                for nu in [-1, 1]:
                    M_lLc_lp[(lLc, lp)] += D_lLc_nu[(lLc, nu)] * O_nu_lp[( nu, lp)]

        return M_lLc_lp

    @atfi.function
    def get_dynamic_term(self, Obvs):
        #get Lc amplitudes
        M_lLc_lp = self.get_Lc_amps(Obvs)

        #make polarisation matrix with index lLc, lLc_d
        rho_lLc_lLcd = {}
        rho_lLc_lLcd[( 1 ,  1)] = atfi.cast_complex(0.5) * atfi.cast_complex(1. + self.fit_params['Pz']())
        rho_lLc_lLcd[( 1 , -1)] = atfi.cast_complex(0.5) * (atfi.cast_complex(self.fit_params['Px']()) - I * atfi.cast_complex(self.fit_params['Py']()))
        rho_lLc_lLcd[(-1 ,  1)] = atfi.cast_complex(0.5) * (atfi.cast_complex(self.fit_params['Px']()) + I * atfi.cast_complex(self.fit_params['Py']()))
        rho_lLc_lLcd[(-1 , -1)] = atfi.cast_complex(0.5) * atfi.cast_complex(1. - self.fit_params['Pz']())
        
        #get density
        dens_O = atfi.complex(atfi.const(0), atfi.const(0))
        for lLc in [-1, 1]:
            for lLc_d in [-1, 1]:
                for lp in [-1, 1]:
                    dens_O += rho_lLc_lLcd[(lLc, lLc_d)] * M_lLc_lp[(lLc, lp)] * tf.math.conj(M_lLc_lp[(lLc_d, lp)])

        return tf.math.real(dens_O)

    def get_unbinned_model(self, ph):
        """Define the unbinned model for Lc->pKpi"""
        obsv              = self.prepare_data(ph) 
        if self.res_list == ["phsp"]:
            model = atfi.ones(obsv['m2_pk']) #phase space term is just uniform in this case
        else:
            dGdO_dynm         = self.get_dynamic_term(obsv)
            model = dGdO_dynm

        return model

    def get_normalised_model(self, ph):
        norm_smpl  = self.phase_space.uniform_sample(1000000)
        pdf_norm   = self.get_unbinned_model(norm_smpl)
        intg       = self.integral(pdf_norm)
        pdf_unnorm = self.get_unbinned_model(ph)
        return pdf_unnorm / intg

    def MC(self, size, chunk = 5000, size_maxest = 100000): 
        atfi.set_seed(1)
        #set seed for reproducibility 
        maximum = tft.maximum_estimator(self.get_unbinned_model , self.phase_space, size_maxest) * 1.5
        print(maximum)
        toy_sample = tft.run_toymc( self.get_unbinned_model , self.phase_space , size , maximum, chunk, components = False)
        print(toy_sample.numpy())
        return toy_sample.numpy()

    def set_params_values(self, res, isfitresult = True):
        """Set the parameters values to the ones given in the dictionary"""
        for k in list(res.keys()):
            for p in list(self.fit_params.values()):
                if p.name == k:
                    if isfitresult:
                        if p.floating():
                            print('Setting', p.name, 'from ', p.numpy(), 'to ', res[k][0])
                            p.update(res[k][0])
                    else:
                        print('Setting', p.name, 'from ', p.numpy(), 'to ', res[k])
                        p.init_value = res[k]
                        p.update(res[k])

    def randomise_params(self, seed = None, verbose = True):
        if seed is not None: np.random.seed(seed)
        for p in list(self.fit_params.values()):
            if p.floating():
                valprev = p.numpy()
                newval = np.random.uniform(p.lower_limit, p.upper_limit, size=1)[0]
                p.update(newval)
                if verbose: print('Randomising', p.name, 'from', valprev, 'to', p.numpy())
    
    @atfi.function
    def integral(self, pdf):
        '''
        Return the graph for the integral of the PDF
        pdf : PDF
        '''
        vol = 1.
        for r in self.phsp_limits: vol *= (r[1] - r[0]) #shouldn't matter as this forms a constant
        return atfi.const(vol) * tf.reduce_mean(pdf)
    
    def unbinned_nll(self, data_unbinned):
        #normalisation sample
        norm_smpl  = self.phase_space.uniform_sample(1000000)
        #norm_smpl  = self.phase_space.rectangular_grid_sample(sizes=(20,20,20,20,20))

        @atfi.function
        def _nll(pars):
            #pars is not used because it is a class
            pdf_unnorm = self.get_unbinned_model(data_unbinned)
            pdf_norm   = self.get_unbinned_model(norm_smpl)
            intg       = self.integral(pdf_norm)
            return -tf.reduce_sum(atfi.log(pdf_unnorm / intg))

        return _nll
    
    def plot(self, data, i):
        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure
        label  = ["m2_pK", "m2_kpi", "cth_p", "ph_p", "phi_kpi", "m2_ppi"]
        
        tfp.plot_distr1d(
            data[:,i],
            bins=100,
            range=(np.min(data[:,i]), np.max(data[:,i])),
            ax=ax,
            label = label[i],
            units="",
            weights = None,
            color = 'k',
            ) 

        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
        return plt.show() 
    
    def plot2(self, data, i):
        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure
        label  = ["m2_pK", "m2_kpi", "cth_p", "ph_p", "phi_kpi", "m2_ppi"]
        
        tfp.plot_distr1d(
            data,
            bins=100,
            range=(np.min(data), np.max(data)),
            ax=ax,
            label = label[i],
            units="",
            weights = None,
            color = 'k',
            ) 

        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
        return plt.show()

@atfi.function
def determine_fit_fractions(model_full, model_res): 
    @atfi.function
    def f(x, model):
        return model.get_unbinned_model(x) 

    @atfi.function
    def integral(pdf, model):
        vol = 1.
        for r in model.phsp_limits: vol *= (r[1] - r[0]) #shouldn't matter as this forms a constant
        return atfi.const(vol) * tf.reduce_mean(pdf)

    tf.random.set_seed(1)
    x = model_full.phase_space.uniform_sample(100000)
    x = model_full.phase_space.filter(x)
        
    int_res  = integral(f(x, model_res), model_res)
    int_full = integral(f(x, model_full), model_full)
    fit_fraction = int_res / int_full
    return fit_fraction 



def Minimize(nll, model, tot_params, nfits = 1, randomise_params = True):
    nllval = None 
    reslts = None 
    for nfit in range(nfits):
        if randomise_params:
            #Randomising the starting values of the parameters according a uniform distribution 
            model.randomise_params()
        
        #Conduct the fit
        results = tfo.run_minuit(nll, list(tot_params.values()), use_gradient=False, use_hesse = False, use_minos = False)
        print(nll)

        #out of nfits pick the result with the least negative log likelihood (NLL)
        if nfit == 0: 
            print('Fit number', nfit)
            nllval = results['loglh']
            reslts = results
        else:
            print('Fit number', nfit)
            if nllval > results['loglh']:
                nllval = results['loglh']
                reslts = results

        print(results)
    
    #set the parameters to the fit results of the least NLL
    model.set_params_values(reslts['params'], isfitresult = True)
    print(reslts['params'])
    return reslts

def plot_fits(data_mc, data_fit, i):
    tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure
    label = [r"$m^2(K\pi)$", "phi", "theta", "phi", r"$\theta_{res}$"]
        
    tfp.plot_distr1d(
        data_mc[:,i],
        bins=100,
        range=(np.min(data_mc[:,i]), np.max(data_mc[:,i])),
        ax=ax,
        label = label[i],
        units="",
        weights = None,
        color = 'k',
        ) 
    
    tfp.plot_distr1d(
        data_fit[:,i],
        bins=100,
        range=(np.min(data_fit[:,i]), np.max(data_fit[:,i])),
        ax=ax,
        label = label[i],
        units="",
        weights = None,
        color = 'b',
        ) 

#def main():
#    MLc = 2.28646
#    Mp  = 0.938272046
#    Mk  = 0.493677
#    Mpi = 0.13957018

    '''
    res_list          = ["Kstar_kpi(892)","Kstar_kpi0(700)", "Kstar_kpi0(1430)", "D(1232)", "D(1600)", "D(1700)", "L(1405)", "L(1520)", "L(1600)", "L(1670)", "L(1690)", "L(2000)"]
    model_full        = LcTopKpi_Model(MLc, Mp, Mk, Mpi, res_list, float_params=[],  model_type="NEW")

    print(res_list[0])
    res_list1         = ["Kstar_kpi(892)"]
    model_res1        = LcTopKpi_Model(MLc, Mp, Mk, Mpi, res_list1, float_params=[],  model_type="NEW")
    print(determine_fit_fractions(model_full, model_res1))
    
    print(res_list[1])
    res_list2         = ["Kstar_kpi0(700)"]
    model_res2        = LcTopKpi_Model(MLc, Mp, Mk, Mpi, res_list2, float_params=[],  model_type="NEW")
    print(determine_fit_fractions(model_full, model_res2))
    
    print(res_list[2])
    res_list3         = ["Kstar_kpi0(1430)"]
    model_res3        = LcTopKpi_Model(MLc, Mp, Mk, Mpi, res_list3, float_params=[],  model_type="NEW")
    print(determine_fit_fractions(model_full, model_res3))
   
    
    print(res_list[3])
    res_list4         = ["D(1232)"]
    model_res4        = LcTopKpi_Model(MLc, Mp, Mk, Mpi, res_list4, float_params=[],  model_type="NEW")
    print(determine_fit_fractions(model_full, model_res4))

    
    print(res_list[4])
    res_list5         = ["D(1600)"]
    model_res5        = LcTopKpi_Model(MLc, Mp, Mk, Mpi, res_list5, float_params=[],  model_type="NEW")
    print(determine_fit_fractions(model_full, model_res5))

    print(res_list[5])
    res_list6         = ["D(1700)"]
    model_res6        = LcTopKpi_Model(MLc, Mp, Mk, Mpi, res_list6, float_params=[],  model_type="NEW")
    print(determine_fit_fractions(model_full, model_res6))

    print(res_list[6])
    res_list7         = ["L(1405)"]
    model_res7        = LcTopKpi_Model(MLc, Mp, Mk, Mpi, res_list7, float_params=[],  model_type="NEW")
    print(determine_fit_fractions(model_full, model_res7))

    print(res_list[7])
    res_list8         = ["L(1520)"]
    model_res8        = LcTopKpi_Model(MLc, Mp, Mk, Mpi, res_list8, float_params=[],  model_type="NEW")
    print(determine_fit_fractions(model_full, model_res8))

    print(res_list[8])
    res_list9         = ["L(1600)"]
    model_res9        = LcTopKpi_Model(MLc, Mp, Mk, Mpi, res_list9, float_params=[],  model_type="NEW")
    print(determine_fit_fractions(model_full, model_res9))

    print(res_list[9])
    res_list10         = ["L(1670)"]
    model_res10        = LcTopKpi_Model(MLc, Mp, Mk, Mpi, res_list10, float_params=[],  model_type="NEW")
    print(determine_fit_fractions(model_full, model_res10))

    print(res_list[10])
    res_list11         = ["L(1690)"]
    model_res11        = LcTopKpi_Model(MLc, Mp, Mk, Mpi, res_list11, float_params=[],  model_type="NEW")
    print(determine_fit_fractions(model_full, model_res11))

    print(res_list[11])
    res_list12         = ["L(2000)"]
    model_res12        = LcTopKpi_Model(MLc, Mp, Mk, Mpi, res_list12, float_params=[],  model_type="NEW")
    print(determine_fit_fractions(model_full, model_res12))
    '''


#
#    #res_list     = ["phsp"]
#    #float_params = []
#
#    res_list     = ["Kstar_kpi(892)"]
#    float_params = ["Kstar_kpi(892)_mass", "Kstar_kpi(892)_width", "Kstar_kpi(892)_real_1", "Kstar_kpi(892)_imag_1", "Kstar_kpi(892)_real_2", "Kstar_kpi(892)_imag_2", "Kstar_kpi(892)_real_3", "Kstar_kpi(892)_imag_3"]
#
#    #res_list     = ["D(1232)"]
#    #float_params = ["D(1232)_mass", "D(1232)_width", "D(1232)_real_2", "D(1232)_imag_2"]
#
#    #res_list     = ["L(1520)"]
#    #float_params = ["L(1520)_mass", "L(1520)_width", "L(1520)_real_2", "L(1520)_imag_2"]
#
#    #res_list      = ["Kstar_kpi(892)", "L(1520)"]
#    #float_params  = ["Kstar_kpi(892)_real_1"]
#    #float_params += ["Kstar_kpi(892)_imag_1"]
#    #float_params += ["Kstar_kpi(892)_real_2"]
#    #float_params += ["Kstar_kpi(892)_imag_2"]
#    #float_params += ["Kstar_kpi(892)_real_3"]
#    #float_params += ["Kstar_kpi(892)_imag_3"]
#    #float_params += ["L(1520)_real_1"]
#    #float_params += ["L(1520)_imag_1"]
#    #float_params += ["L(1520)_real_2"]
#    #float_params += ["L(1520)_imag_2"]
#       
#    res_list      = ["Kstar_kpi(892)","Kstar_kpi0(700)", "Kstar_kpi0(1430)", "D(1232)", "D(1600)", "D(1700)", "L(1405)", "L(1520)", "L(1600)", "L(1670)", "L(1690)", "L(2000)"]
#    model1        = LcTopKpi_Model(MLc, Mp, Mk, Mpi, res_list, float_params=[],  model_type="NEW")
#    sample =  model1.MC(100000)
#    s_m2_ppi   = MLc**2 + Mk**2 + Mp**2 + Mpi**2 - sample[:, 0] - sample[:, 1]
#    model1.plot(sample, 0)
#    model1.plot(sample, 1)
#    model1.plot(sample, 2)
#    model1.plot(sample, 3)
#    model1.plot(sample, 4)
#    model1.plot2(s_m2_ppi, 5)
#
#    tot_params = model1.fit_params
#    pprint.pprint(tot_params)
#
#    sample1 = model1.MC(20000)
#    print(sample1)
#    #exit(1)
#    #pickle.dump( sample1, open( "sample1.p", "wb" ) )
#
#    #print('phsp_limits', model1.phsp_limits)
#    #ph   = tf.constant([[3.51772542, 1.32501599, 0.92707289, -1.65124888, 0.49943723], 
#    #                    [2.29862288, 1.13920606, -0.01365547, -2.066542, 1.37470547]], dtype = tf.float64)
#    #dat  = tf.reshape(ph, shape = (2, 5))
#    #print('data', dat)
#    #print(model1.phase_space.inside(dat))
#    #print(model1.get_unbinned_model(dat).numpy())
#    ##tot_params['Kstar_kpi(892)_mass'].update(3.2)
#    ##print(model1.get_unbinned_model(sample1).numpy())
#    #exit(1)
#
#    #sample1 = pickle.load( open( "sample1.p", "rb" ) )
#    #print(sample1)
#    #print(model1.get_unbinned_model(sample1).numpy())
#    #exit(1)
#    
#    NLL = model1.unbinned_nll(sample1)
#    print(NLL(tot_params).numpy())
#    #tot_params['Kstar_kpi(892)_real_2'].update(3.2) ;print(NLL(tot_params).numpy())
#    #tot_params['Kstar_kpi(892)_imag_2'].update(-4.0);print(NLL(tot_params).numpy())
#    #exit(1)
#
#    results = Minimize(NLL, model1, tot_params, nfits = 1, randomise_params = True)

#if __name__ == '__main__':
#    main()

