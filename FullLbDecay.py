#import few packages
import math
import sys, os
import tensorflow as tf
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # Do not use GPU for tensor flow
import numpy as np
import matplotlib.pyplot as plt
import pprint
import pickle

#amplitf
home = os.getenv('HOME')
sys.path.append(home+"/Packages/AmpliTF/")
import amplitf.interface as atfi
import amplitf.kinematics as atfk
import amplitf.dynamics as atfd
from amplitf.phasespace import Baryonic3BodyPhaseSpace, CombinedPhaseSpace
from amplitf.phasespace.rectangular_phasespace import RectangularPhaseSpace
import amplitf.likelihood as atfl
import amplitf.dalitz_decomposition as atfdd

#tfa2
sys.path.append(home+"/Packages/TFA2/")
import tfa.optimisation as tfo
import tfa.toymc as tft
import tfa.plotting as tfp

#get current directory
fit_dir=os.path.dirname(os.path.abspath(__file__))

#define the complex
I = atfi.complex(atfi.const(0), atfi.const(1))

########### Make some common functions
def MakeFourVector(pmag, costheta, phi, msq): 
    """Make 4-vec given magnitude, cos(theta), phi and mass"""
    sintheta = atfi.sqrt(1. - costheta**2) #theta 0 to pi => atfi.sin always pos
    px = pmag * sintheta * atfi.cos(phi)
    py = pmag * sintheta * atfi.sin(phi)
    pz = pmag * costheta
    E  = atfi.sqrt(msq + pmag**2)
    return atfk.lorentz_vector(atfk.vector(px, py, pz), E)

def pvecMag(Msq, m1sq, m2sq): 
    """Momentum mag of (1 or 2) in M rest frame"""
    kallen = atfi.sqrt(Msq**2 + m1sq**2 + m2sq**2 - 2.*(Msq*m1sq + Msq*m2sq + m1sq*m2sq))
    return kallen/(2.*atfi.sqrt(Msq))

def InvRotateAndBoostFromRest(m_p4mom_p1, m_p4mom_p2, gm_phi_m, gm_ctheta_m, gm_p4mom_m):
    """
    Function that gives 4momenta of the particles in the grand mother helicity frame

    m_p4mom_p1: particle p1 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
    m_p4mom_p2: particle p2 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
    gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
    gm_4mom_m: m 4mom in gm's helicity frame
    --------------
    Checks done such as 
        - (lc_p4mom_p+lc_p4mom_k == lc_p4mom_r), 
        - Going in reverse i.e. boost lc_p4mom_p into R rest frame (using lc_p4mom_r) and rotating into R helicity frame gives back r_p4mom_p 
            - i.e. lc_p4mom_p_opp = atfk.rotate_lorentz_vector(atfk.boost_to_rest(lc_p4mom_p, lc_p4mom_r), -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)) where lc_p4mom_p_opp == r_p4mom_p
        - Rotate and boost, instead of boost and rotate should give the same answer, checked (Does not agree with TFA implementation though)
            -i.e. lc_p4mom_prot  = atfk.rotate_lorentz_vector(lc_p4mom_p, -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)
            -i.e. lc_p4mom_krot  = atfk.rotate_lorentz_vector(lc_p4mom_k, -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)
            -i.e. lc_p4mom_p2    = atfk.boost_to_rest(lc_p4mom_prot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_p2 == r_p4mom_p
            -i.e. lc_p4mom_k2    = atfk.boost_to_rest(lc_p4mom_krot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_k2 == r_p4mom_k
    """
    #First: Rotate particle p 3mom defined in mother m helicity frame such that z now points along z in grand mother gm helicity frame 
    #(i.e interpreted as gm mom in it's grand-grand-mother ggm rest frame)
    m_p4mom_p1_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p1, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
    m_p4mom_p2_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p2, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
    #Second: Boost particle p 4mom from mother m's rest frame to gm helicity frame. This is done using boost vector from gm_p4mom_m  [checked: E(m_p4mom_p1_zmmom + m_p4mom_p2_zmmom) == Mass_m]
    gm_p4mom_p1            = atfk.boost_from_rest(m_p4mom_p1_zgmmom, gm_p4mom_m)
    gm_p4mom_p2            = atfk.boost_from_rest(m_p4mom_p2_zgmmom, gm_p4mom_m)
    return gm_p4mom_p1, gm_p4mom_p2

def make_q2_cthl(dt_np, Lb4moml, Lc4moml, Mu4moml, Nu4moml):
    """Get q^2 and cos(theta) given 4 momenta of all particles"""
    Lb4mom = atfk.lorentz_vector(atfk.vector(dt_np[Lb4moml[1]],dt_np[Lb4moml[2]],dt_np[Lb4moml[3]]), dt_np[Lb4moml[0]])
    Lc4mom = atfk.lorentz_vector(atfk.vector(dt_np[Lc4moml[1]],dt_np[Lc4moml[2]],dt_np[Lc4moml[3]]), dt_np[Lc4moml[0]])
    Mu4mom = atfk.lorentz_vector(atfk.vector(dt_np[Mu4moml[1]],dt_np[Mu4moml[2]],dt_np[Mu4moml[3]]), dt_np[Mu4moml[0]])
    Nu4mom = atfk.lorentz_vector(atfk.vector(dt_np[Nu4moml[1]],dt_np[Nu4moml[2]],dt_np[Nu4moml[3]]), dt_np[Nu4moml[0]])
    #Nu4mom = Nu4mom + (Lb4mom-(Lc4mom+Mu4mom+Nu4mom))
    #Boost Lb frame
    Lb4mom_Lb  = atfk.boost_to_rest(Lb4mom, Lb4mom)
    Lc4mom_Lb  = atfk.boost_to_rest(Lc4mom, Lb4mom)
    Mu4mom_Lb  = atfk.boost_to_rest(Mu4mom, Lb4mom)
    Nu4mom_Lb  = atfk.boost_to_rest(Nu4mom, Lb4mom)
    W4mom_Lb   = atfk.boost_to_rest(Mu4mom+Nu4mom, Lb4mom)
    #Q2
    Q2_var     = tf.square(atfk.mass(Mu4mom+Nu4mom))
    #Cthl
    Lb4mom_W  = atfk.boost_to_rest(Lb4mom_Lb, W4mom_Lb)
    Mu4mom_W  = atfk.boost_to_rest(Mu4mom_Lb, W4mom_Lb)
    Cthl_var  =-atfk.scalar_product(atfk.spatial_components(Mu4mom_W), atfk.spatial_components(Lb4mom_W))/(P(Mu4mom_W) * P(Lb4mom_W))
    #clthl    = atfk.scalar_product(atfk.spatial_components(Mu4mom_W), atfk.spatial_components(W4mom_Lb))/(P(Mu4mom_W) * P(W4mom_Lb))
    return Q2_var, Cthl_var

def HelAngles3Body(pa, pb, pc):
    """Get three-body helicity angles"""
    theta_r  = atfi.acos(-atfk.z_component(pc) / atfk.norm(atfk.spatial_components(pc)))
    phi_r    = atfi.atan2(-atfk.y_component(pc), -atfk.x_component(pc))
    pa_prime = atfk.rotate_lorentz_vector(pa, -phi_r, -theta_r, phi_r)
    pb_prime = atfk.rotate_lorentz_vector(pb, -phi_r, -theta_r, phi_r)
    pa_prime2= atfk.boost_to_rest(pa_prime, pa_prime+pb_prime)
    theta_a  = atfi.acos(atfk.z_component(pa_prime2) / atfk.norm(atfk.spatial_components(pa_prime2)))
    phi_a    = atfi.atan2(atfk.y_component(pa_prime2), atfk.x_component(pa_prime2))
    return (theta_r, phi_r, theta_a, phi_a)

def RotateAndBoostToRest(gm_p4mom_p1, gm_p4mom_p2, gm_phi_m, gm_ctheta_m):
    """
    gm_p4mom_p1: particle p1 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
    gm_p4mom_p2: particle p2 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
    gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
    """
    #First:  Rotate particle p 3mom defined in grand mother gm helicity frame such that z now points along z of mother m in gm rest frame.
    gm_p4mom_p1_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p1, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
    gm_p4mom_p2_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p2, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
    #Second: Boost particle p 4mom to mother m i.e. p1p2 rest frame
    gm_p4mom_m             = gm_p4mom_p1_zmmom+gm_p4mom_p2_zmmom
    m_p4mom_p1             = atfk.boost_to_rest(gm_p4mom_p1_zmmom, gm_p4mom_m)
    m_p4mom_p2             = atfk.boost_to_rest(gm_p4mom_p2_zmmom, gm_p4mom_m)
    return m_p4mom_p1, m_p4mom_p2
########

#define the class for Lb->Lclnu decay
class LbToLclNu_Model:
    """Class that defines the Lb->Lclnu decay"""

    def __init__(self, MLb, MLc, Mlep, Mp, Mk, Mpi, flavour = 1, wc_floated_names = ['CVR'], ff_floated_names = ['None'], res_list = ["phsp"]):
        """initialise some variables"""
        #Masses of particles involved 
        self.MLb     = MLb
        self.MLc     = MLc
        self.Mlep    = Mlep
        self.Mp      = Mp
        self.Mk      = Mk
        self.Mpi     = Mpi
        #print(self.MLb, self.MLc, self.Mlep)

        #define the flavour of the particle
        self.flavour = flavour
    
        #Constants expressed in GeV
        self.GF      = 1.166378e-5 #GeV^-2
        self.Vcb     = 4.22e-2     #avg of incl and excl
        #print(self.GF, self.Vcb)
        
        #Define function defined in lattice QCD here related to form factors
        self.Tf_plus = {} #mf_pole = M_BC + delta_f and Tf_plus = mf_pole**2
        self.Tf_plus['fplus']      = np.power(6.276 + 56e-3         , 2)#GeV
        self.Tf_plus['fperp']      = np.power(6.276 + 56e-3         , 2)#GeV
        self.Tf_plus['f0']         = np.power(6.276 + 449e-3        , 2)#GeV
        self.Tf_plus['gplus']      = np.power(6.276 + 492e-3        , 2)#GeV
        self.Tf_plus['gperp']      = np.power(6.276 + 492e-3        , 2)#GeV
        self.Tf_plus['g0']         = np.power(6.276 + 0.            , 2)#GeV
        self.Tf_plus['hplus']      = np.power(6.276 + 6.332 - 6.276 , 2)#GeV
        self.Tf_plus['hperp']      = np.power(6.276 + 6.332 - 6.276 , 2)#GeV
        self.Tf_plus['htildeplus'] = np.power(6.276 + 6.768 - 6.276 , 2)#GeV
        self.Tf_plus['htildeperp'] = np.power(6.276 + 6.768 - 6.276 , 2)#GeV
        #print(self.Tf_plus)
        
        #Map to relate W* resonance helicity to the sign in the amplitude (used in building the amplitde)
        self.eta    = {'t': 1.,  0 : -1.,  -1: -1., 1 : -1.}
        #print(self.eta)
        
        #Map to relate WC to the sign in the amplitude (used in building the amplitde)
        self.signWC = {'V': 1., 'A': -1., 'S': 1., 'PS': -1., 'T': 1., 'PT': -1.}
        #print(self.signWC)

        #make SM FF mean and covariance values 
        #ff mean
        self.ff_mean  = {}
        f     = open(fit_dir+"/FF_cov/LambdabLambdac_results.dat", "r")
        for l in f.readlines(): self.ff_mean[l.split()[0]] = float(l.split()[1])
        f.close()
        #print(self.ff_mean)

        #ff covariant
        self.ff_cov = {}
        for l in list(self.ff_mean.keys()): self.ff_cov[l] = {}
        f  = open(fit_dir+"/FF_cov/LambdabLambdac_covariance.dat", "r")
        for l in f.readlines(): self.ff_cov[l.split()[0]][l.split()[1]] = float(l.split()[2])
        f.close()
        #print(self.ff_cov)
        
        #Lists that will serve as index for incoherent sum of amplitudes (defined in units of 1/2 except for lw used in leptonic case that does not need this)
        self.lLbs   = [1, -1]
        self.lls    = [1, -1]
        self.lps    = [1, -1]
        #print(self.lLbs, self.lls, self.lps)
        
        #Lists that will serve as index for coherent sum of amplitudes (defined in units of 1/2 except for lw used in leptonic case that does not need this)
        self.lLcs = [1, -1]
        self.lws  = ['t', 0, 1, -1]
        self.WCs  = ['V', 'A', 'S', 'PS', 'T', 'PT']
        self.FFs  = ['a0f0', 'a0fplus', 'a0fperp', 'a0g0', 'a0gplus', 'a0hplus', 'a0hperp', 'a0htildeplus']
        self.FFs += ['a1f0', 'a1fplus', 'a1fperp', 'a1g0', 'a1gplus', 'a1gperp', 'a1hplus', 'a1hperp', 'a1htildeplus', 'a1htildeperp']
        self.FFs += ['a0gperp', 'a0htildeperp']
        #print(self.lws, self.WCs, self.FFs)
        
        #get wilson coefficients (wc)
        self.wc_params     = self.get_wc_params()
        #get form factor (ff) parameters
        self.ff_params     = self.get_ff_params()
        #define limits and rectangular phase space i.e. (qsq, costhmu) space
        phsp_limits  = [(self.Mlep**2, (self.MLb - self.MLc)**2)] 
        phsp_limits += [(0. , math.pi)]
        phase_space1 = RectangularPhaseSpace(ranges=phsp_limits)
        phase_space2 = Baryonic3BodyPhaseSpace(self.Mp, self.Mk, self.Mpi, self.MLc)
        self.phase_space  = CombinedPhaseSpace(phase_space1, phase_space2)

        #get resonance fit parameters
        self.Jlam_c            = 1
        self.Jp                = 1
        self.fit_params    = self.get_fit_params(res_list)
        self.res_list      = res_list
        self.switches      = len(res_list) * [1]
        self.resn          = self.get_resonances(res_list)
        self.tot_params    = {**self.wc_params, **self.ff_params, **self.fit_params}

    def get_wc_params(self):
        """Make WC parameters"""
        Wlcoef = {}
        Wlcoef['CVL'] = tfo.FitParameter("CVL" , 0.0, -2., 2., 0.08)
        Wlcoef['CVR'] = tfo.FitParameter("CVR" , 0.0, -2., 2., 0.08)
        Wlcoef['CSR'] = tfo.FitParameter("CSR" , 0.0, -2., 2., 0.08)
        Wlcoef['CSL'] = tfo.FitParameter("CSL" , 0.0, -2., 2., 0.08)
        Wlcoef['CT']  = tfo.FitParameter("CT"  , 0.0, -2., 2., 0.08)
        for k in list(Wlcoef.keys()): Wlcoef[k].fix() #Fix all the wcs
        return Wlcoef

    def get_ff_params(self):
        """Make FF parameters"""
        #make dictionary of ff params
        ffact = {}
        print('Setting FF to LQCD central value and allowed to vary b/w [lqcd_val - 100 * lqcd_sigma, lqcd_val + 100 * lqcd_sigma]. So this corresponds to...')
        for FF in self.FFs[:-2]: 
            ff_mn  = self.ff_mean[FF]
            ff_sg  = np.sqrt(self.ff_cov[FF][FF])
            nsigma_ff = 100.
            ff_l   = ff_mn - nsigma_ff*ff_sg
            ff_h   = ff_mn + nsigma_ff*ff_sg
            print('Setting', FF, 'to SM value:', ff_mn, 'with sigma', ff_sg, ', allowed to vary in fit b/w [', ff_l, ff_h, ']')
            ffact[FF] = tfo.FitParameter(FF, ff_mn, ff_l, ff_h, 0.08)
            ffact[FF].fix() #fix all the ff params
            
        return ffact

    def get_freeparams(self):
        """Make dictionary of free parameters i.e. WC * FF"""

        #define function for change of basis
        def _wc_basis(basis_name):
            """Change the WC basis e.g. 1+CVL+CVR -> V"""
            wcp = None
            if basis_name == 'V':
                wcp =  atfi.const(1.) + self.wc_params['CVL']() + self.wc_params['CVR']()
            elif basis_name == 'A':
                wcp =  atfi.const(1.) + self.wc_params['CVL']() - self.wc_params['CVR']()
            elif basis_name == 'S':
                wcp =  self.wc_params['CSL']() + self.wc_params['CSR']()
            elif basis_name == 'PS':
                wcp =  self.wc_params['CSL']() - self.wc_params['CSR']()
            elif basis_name == 'T' or basis_name == 'PT':
                wcp =  self.wc_params['CT']()
            else:
                raise Exception('The passed basis_name', basis_name, 'not recognised')
    
            return wcp

        def _ff_common(ff_name):
            """Set a0gplus = a0gperp and a0htildeplus = a0htildeperp"""
            ffp = None
            if ff_name == 'a0gperp':
                ffp = self.ff_params['a0gplus']()
            elif ff_name == 'a0htildeperp':
                ffp = self.ff_params['a0htildeplus']()
            else:
                ffp = self.ff_params[ff_name]()

            return ffp

        #Define free parameters dictionary
        free_params = {}
        for WC in self.WCs: 
            for FF in self.FFs:
                free_params[( WC, FF)] = atfi.cast_complex(_wc_basis(WC)) * atfi.cast_complex(_ff_common(FF))
    
        #print(len(free_params.keys()))                        #6*20 = 120
        return free_params

    def get_fit_params(self, res_list):
        
        f_params = {}
        if 'Kstar_kpi(892)' in res_list:
            f_params["Kstar_kpi(892)_mass"]   = tfo.FitParameter("Kstar_kpi(892)_mass"  , 0.8981936228550071 ,   0.   , 2.   , 0.01)
            f_params["Kstar_kpi(892)_width"]  = tfo.FitParameter("Kstar_kpi(892)_width" , 0.04495108867684322,   0.03 , 0.05 , 0.0001)
            f_params["Kstar_kpi(892)_real_1"] = tfo.FitParameter("Kstar_kpi(892)_real_1",  5.043546642212341  ,   -10. , 10.  , 0.01)
            f_params["Kstar_kpi(892)_imag_1"] = tfo.FitParameter("Kstar_kpi(892)_imag_1", -1.862253283747373  ,   -10. , 10.  , 0.01)
            f_params["Kstar_kpi(892)_real_2"] = tfo.FitParameter("Kstar_kpi(892)_real_2", -3.1597560963214457 ,   -10. , 10.  , 0.01)
            f_params["Kstar_kpi(892)_imag_2"] = tfo.FitParameter("Kstar_kpi(892)_imag_2", -3.3117031588059955 ,   -10. , 10.  , 0.01)
            f_params["Kstar_kpi(892)_real_3"] = tfo.FitParameter("Kstar_kpi(892)_real_3",  0.12845976085492694,   -10. , 10.  , 0.01)
            f_params["Kstar_kpi(892)_imag_3"] = tfo.FitParameter("Kstar_kpi(892)_imag_3", -4.160977960398091  ,   -10. , 10.  , 0.01)

        if 'D(1232)' in res_list:
            f_params["D(1232)_mass"]   = tfo.FitParameter("D(1232)_mass"  , 1.2336295875102392,  0.   , 10. , 0.01)
            f_params["D(1232)_width"]  = tfo.FitParameter("D(1232)_width" , 0.1246267987861399,  0.01 , 5. , 0.0001)
            f_params["D(1232)_real_1"] = tfo.FitParameter("D(1232)_real_1",  5.043546642212341   , -10.  , 10. , 0.01)
            f_params["D(1232)_imag_1"] = tfo.FitParameter("D(1232)_imag_1", -1.862253283747373   , -10.  , 10. , 0.01)
            f_params["D(1232)_real_2"] = tfo.FitParameter("D(1232)_real_2", -3.1597560963214457  , -10.  , 10. , 0.01)
            f_params["D(1232)_imag_2"] = tfo.FitParameter("D(1232)_imag_2", -3.3117031588059955  , -10.  , 10. , 0.01)


        if 'L(1520)' in res_list:
            f_params["L(1520)_mass"]   = tfo.FitParameter("L(1520)_mass"  , 1.518467 ,   0.   , 10. , 0.01)
            f_params["L(1520)_width"]  = tfo.FitParameter("L(1520)_width" , 0.015195 ,  0.0001 ,  2. , 0.0001) 
            f_params["L(1520)_real_1"] = tfo.FitParameter("L(1520)_real_1", -1.8622,   -10. , 10. , 0.01)
            f_params["L(1520)_imag_1"] = tfo.FitParameter("L(1520)_imag_1", -3.1597,   -10. , 10. , 0.01)
            f_params["L(1520)_real_2"] = tfo.FitParameter("L(1520)_real_2", -3.3117,   -10. , 10. , 0.01)
            f_params["L(1520)_imag_2"] = tfo.FitParameter("L(1520)_imag_2",  0.1284,   -10. , 10. , 0.01)

        return f_params

    def _l_bc(self, ja , jb , jc):
        l_bc1 = ja/2 - jb/2 - jc/2
        l_bc2 = ja/2 - jb/2 + jc/2
        l_bc  = np.min( [l_bc1 , l_bc2] )
        return l_bc

    def _l_bc_k(self, ja , jb , jc):
        l_bc1 = ja/2 - jb/2 - jc/2
        l_bc2 = ja/2 - jb/2 + jc/2
        l_bc  = np.min( [l_bc1 , l_bc2] )
        return np.max([0, l_bc])

    def _l_bc_strong(self, ja , jb , jc, parity):
        l_bc1 = ja/2 - jb/2 - jc/2
        l_bc2 = ja/2 - jb/2 + jc/2
        l_bc  = [l_bc1 , l_bc2]
        for i in l_bc:
            eta = (-1)**(i+1)
            if parity == eta:
                return np.abs(i)

    def get_resonances(self, res_list):
        resonances = {}
        for res in res_list:
            if res == "Kstar_kpi(892)":
                    l_lamc_val = int( self._l_bc_k( 2 , self.Jp , self.Jlam_c ) )
                    resonances["Kstar_kpi(892)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: self.fit_params["Kstar_kpi(892)_mass"](), 
                                            "width"     : lambda: self.fit_params["Kstar_kpi(892)_width"](),  
                                            "spin"      : 2, 
                                            "parity"    : -1, 
                                            "coupl"     : [     
                                                            lambda: atfi.complex( atfi.const(1.) , atfi.const(0.) ), 
                                                            lambda: atfi.complex( self.fit_params["Kstar_kpi(892)_real_1"](), self.fit_params["Kstar_kpi(892)_imag_1"]()),       
                                                            lambda: atfi.complex( self.fit_params["Kstar_kpi(892)_real_2"](), self.fit_params["Kstar_kpi(892)_imag_2"]()), 
                                                            lambda: atfi.complex( self.fit_params["Kstar_kpi(892)_real_3"](), self.fit_params["Kstar_kpi(892)_imag_3"]()),
                                                        ],
                                            "l_lamc"    : l_lamc_val,
                                            "l_res"     : int( 2 / 2 ),
                    }
                    #print(res_val[r]['l_res'] , res_val[r]['l_lamc']) (1,0)

            elif res == "Kstar_kpi0(700)":
                    resonances["Kstar_kpi0(700)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(0.824), 
                                            "width"     : lambda: atfi.const(0.478),  
                                            "spin"      : 0, 
                                            "parity"    : 1, 
                                            "gamma"     : atfi.const(0.94106),
                                            "alpha"     : atfi.const(0.),
                                            "coupl"     : [  
                                                            lambda: atfi.complex(atfi.const(0.068908), atfi.const(2.521444)),
                                                            lambda: atfi.complex(atfi.const(-2.68563), atfi.const(0.03849)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc_k( 0 , self.Jp , self.Jlam_c ) ),
                                            "l_res"     : int( 0 / 2 ), 
                    }

            elif res == "Kstar_kpi0(1430)":
                    resonances["Kstar_kpi0(1430)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.375), 
                                            "width"     : lambda: atfi.const(0.190),  
                                            "spin"      : 0, 
                                            "parity"    : 1, 
                                            "gamma"     : atfi.const(0.020981),
                                            "alpha"     : atfi.const(0.),
                                            "coupl"     : [     
                                                            lambda: atfi.complex(atfi.const(-6.71516), atfi.const(10.479411)),
                                                            lambda: atfi.complex(atfi.const(0.219754 ), atfi.const(8.741196)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc_k( 0 , self.Jp , self.Jlam_c ) ),
                                            "l_res"     : int( 0 / 2 ), 
                    }
            
            elif res == "D(1232)":
                    l_lamc_val = int( self._l_bc( 3 , 0 , self.Jlam_c ) )
                    resonances["D(1232)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: self.fit_params["D(1232)_mass"](), 
                                            "width"     : lambda: self.fit_params["D(1232)_width"](),  
                                            "spin"      : 3, 
                                            "parity"    : 1, 
                                            "coupl"     : [    
                                                            lambda: atfi.complex( self.fit_params["D(1232)_real_1"](), self.fit_params["D(1232)_imag_1"]()),       
                                                            lambda: atfi.complex( self.fit_params["D(1232)_real_2"](), self.fit_params["D(1232)_imag_2"]()),
                                                        ],
                                            "l_lamc"    : l_lamc_val,
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0, 3 , 1) ), 
                    }
                    #print(res_val[r]['l_res'] , res_val[r]['l_lamc']) (1,1)

            elif res == "D(1600)":
                    resonances["D(1600)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.64), 
                                            "width"     : lambda: atfi.const(0.3),  
                                            "spin"      : 3, 
                                            "parity"    : 1, 
                                            "coupl"     : [        
                                                            lambda: atfi.complex(atfi.const(11.401585), atfi.const(-3.125511)),
                                                            lambda: atfi.complex(atfi.const(6.729211), atfi.const(-1.002383)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 3 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp, 0, 3 , 1) ),    
                    }
            
            elif res == "D(1700)":
                    resonances["D(1700)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.69), 
                                            "width"     : lambda: atfi.const(0.38),  
                                            "spin"      : 3, 
                                            "parity"    : -1, 
                                            "coupl"     : [     
                                                            lambda: atfi.complex(atfi.const(10.37828), atfi.const(1.434872)),
                                                            lambda: atfi.complex(atfi.const(12.874102), atfi.const(2.10557)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 3 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp, 0, 3 , -1) ),    
                    }
            
            elif res == "L(1405)":
                    resonances["L(1405)"] = {
                                            "lineshape" : "subthreshold_breit_wigner_lineshape",  
                                            "mass"      : lambda: atfi.const(1.4051), 
                                            "width1"    : lambda: atfi.const(0.0505),  
                                            "width2"    : lambda: atfi.const(0.0505),
                                            "ma2"       : atfi.const(1.18937), #sigma 
                                            "mb2"       : atfi.const(0.13957018), #pion
                                            "spin"      : 1, 
                                            "parity"    : -1, 
                                            "coupl"     : [     
                                                            lambda: atfi.complex(atfi.const(-4.572486), atfi.const(3.190144)),
                                                            lambda: atfi.complex(atfi.const(10.44608), atfi.const(2.787441)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 1 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0 , 1 , -1) ),    
                    }

            elif res == "L(1520)":
                    l_lamc_val = int( self._l_bc( 3 , 0 , self.Jlam_c ) )
                    resonances["L(1520)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: self.fit_params["L(1520)_mass"](),  
                                            "width"     : lambda: self.fit_params["L(1520)_width"](),  
                                            "spin"      :  3, 
                                            "parity"    : -1, 
                                            "coupl"     : [     
                                                            lambda: atfi.complex( self.fit_params["L(1520)_real_1"](), self.fit_params["L(1520)_imag_1"]()),       
                                                            lambda: atfi.complex( self.fit_params["L(1520)_real_2"](), self.fit_params["L(1520)_imag_2"]()),
                                                          ],
                                            "l_lamc"    : l_lamc_val,
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0 , 3 , -1) ), 
                    }
                    #print(res_val[r]['l_res'] , res_val[r]['l_lamc']) #(2,1)

            elif res == "L(1600)":
                    resonances["L(1600)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.630), 
                                            "width"     : lambda: atfi.const(0.25),  
                                            "spin"      : 1, 
                                            "parity"    : 1, 
                                            "coupl"     : [     
                                                            lambda: atfi.complex(atfi.const(4.840649), atfi.const(3.082786)),
                                                            lambda: atfi.complex(atfi.const(-6.971233), atfi.const(0.842435)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 1 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0 , 1 , 1) ),  
                    } 
            
            elif res == "L(1670)":
                    resonances["L(1670)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.670), 
                                            "width"     : lambda: atfi.const(0.03),  
                                            "spin"      : 1., 
                                            "parity"    : -1., 
                                            "coupl"     : [     
                                                            lambda: atfi.complex(atfi.const(-0.339585), atfi.const(-0.144678)),
                                                            lambda: atfi.complex(atfi.const(-0.570978), atfi.const(1.011833)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 1 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0 , 1 , -1) ),  
                    }
            
            elif res == "L(1690)":
                    resonances["L(1690)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.690), 
                                            "width"     : lambda: atfi.const(0.07),  
                                            "spin"      : 3, 
                                            "parity"    : -1, 
                                            "coupl"     : [     
                                                            lambda: atfi.complex(atfi.const(-0.385772), atfi.const(-0.110235)),
                                                            lambda: atfi.complex(atfi.const(-2.730592), atfi.const(-0.353613)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 3 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0 , 3 , -1) ),  
                    }
            
            elif res == "L(2000)":
                    resonances["L(2000)"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : lambda: atfi.const(1.98819), 
                                            "width"     : lambda: atfi.const(0.17926 ),  
                                            "spin"      : 1, 
                                            "parity"    : -1, 
                                            "coupl"     : [     
                                                            lambda: atfi.complex(atfi.const(-8.014857), atfi.const(-7.614006)),
                                                            lambda: atfi.complex(atfi.const(-4.336255), atfi.const(-3.796192)),
                                                        ],
                                            "l_lamc"    : int( self._l_bc( 1 , 0 , self.Jlam_c ) ),
                                            "l_res"     : int( self._l_bc_strong( self.Jp , 0 , 1 , -1) ),  
                    }
        return resonances 
    
    def prepare_data(self,x):
        """Function that calculates most of the variables for phase space variables"""
        Vars = {}
        #Store Lc phase_space Varsiables and make sure volume element stays as dq2*dcthlc*dcthl*dphl*dm2pk
        q2        = x[:,0]
        w_theta_l = x[:,1]
        m2_pk     = x[:,2]
        m2_kpi    = x[:,3]
        ctheta_p  = x[:,4]
        phi_p     = atfi.const(self.flavour) * x[:,5]
        phi_kpi   = atfi.const(self.flavour) * x[:,6]

        #some extra info related in Lb decay
        w_ctheta_l    = tf.cos(w_theta_l)
        lb_ctheta_lc  = atfi.ones(x[:,0])  #Lb polarisation is zero (shape same as integrating or fixing)
        lb_phi_lc     = atfi.zeros(x[:,0]) #always zeros
        w_phi_l       = atfi.zeros(x[:,0]) #Lb polarisation is zero (shape same as integrating or fixing)
        lb_ctheta_w   = -lb_ctheta_lc
        lb_phi_w      =  lb_phi_lc+atfi.pi()
        w_ctheta_nu   = -w_ctheta_l
        w_phi_nu      =  w_phi_l+atfi.pi()

        #some extra info related in Lc decay
        m2_ppi       = self.MLc**2 + self.Mk**2 + self.Mp**2 + self.Mpi**2 - m2_pk - m2_kpi
        func_args    = (self.MLc, self.Mp, self.Mk, self.Mpi, m2_kpi, m2_ppi, m2_pk)
        theta_23     = tf.acos(atfdd.cos_theta_23(*func_args))
        theta_hat_12 = tf.acos(atfdd.cos_theta_hat_1_canonical_2(*func_args)) #this has to be converted to theta_hat_21
        theta_31     = tf.acos(atfdd.cos_theta_31(*func_args))
        zeta_121     = tf.acos(atfdd.cos_zeta_1_aligned_1_in_frame_2(*func_args))
        theta_hat_31 = tf.acos(atfdd.cos_theta_hat_3_canonical_1(*func_args) )
        theta_12     = tf.acos(atfdd.cos_theta_12(*func_args))
        zeta_113     = tf.acos(atfdd.cos_zeta_1_aligned_3_in_frame_1(*func_args)) #this has to be converted to zeta_131

        #Store Varss
        Vars['q2']            = q2
        Vars['lb_ctheta_w']   = lb_ctheta_w
        Vars['lb_phi_w']      = lb_phi_w
        Vars['w_theta_l']     = w_theta_l
        Vars['w_ctheta_l']    = w_ctheta_l
        Vars['w_phi_l']       = w_phi_l
        #add in Lambda_c decay 
        Vars['m2_pk']        = m2_pk    
        Vars['m2_kpi']       = m2_kpi    
        Vars['ctheta_p']     = ctheta_p    
        Vars['phi_p']        = phi_p    
        Vars['phi_kpi']      = phi_kpi    
        Vars['m2_ppi']       = m2_ppi    
        Vars['theta_23']     = theta_23    
        Vars['theta_hat_12'] = theta_hat_12
        Vars['theta_31']     = theta_31    
        Vars['zeta_121']     = zeta_121    
        Vars['theta_hat_31'] = theta_hat_31
        Vars['theta_12']     = theta_12    
        Vars['zeta_113']     = zeta_113    
        return Vars

    def d_phi_twobody(self,mijsq, misq, mjsq):
        """Two body phase space element"""
        return 1./(2.**4 * atfi.pi()**2) * pvecMag(mijsq, misq, mjsq)/atfi.sqrt(mijsq)

    def get_phasespace_term(self, Obs):
        """Two body phase space factors along with the element"""
        phsspace  = 1./(2. * atfi.const(self.MLb) * 2. * atfi.pi()) * self.d_phi_twobody(atfi.const(self.MLb)**2, atfi.const(self.MLc)**2, Obs['q2'])  #Lb -> Lc W
        phsspace *= self.d_phi_twobody(Obs['q2'], atfi.const(self.Mlep)**2, 0.) * tf.sin(Obs['w_theta_l'])   #W  ->  l nu
        #Note phsp in dpd is for Lc decay is 1
        return phsspace

    def get_lb_ampls(self, Obs):
        """
        Some important comments about the hadronic currents:
        V currents:
            - aV, bV and cV are form factor parameter independent (unlike in our paper)
            - When summing over lwd only one term is needed. This term is taken as 't' (b'cos eta[t] = 1). Make sure Leptonic V and A current lwd='t'.
            - The ffterms func return corrent q2 dependent terms pertaining to FF parameter a0 and a1.
            - Hadr(lb, lc, lw, 't', 'V', 'a0') have 16 comb out of which 12 survive and 4 are zero
        S currents:
            - Fake lw and lwd index (see above for reason). Fixed to zero, make sure Leptonic S, PS currents also have lw = lwd = 't'.
            - Hadr(lb, lc, 't', 't', 'S', 'a0') have 4 terms all which are nonzero.
        A currents: 
            - Like in Leptonic current these are NOT equal to V currents.
            - Same comments as V currents.
        PS currents: 
            - Like in Leptonic current these are NOT equal to S currents.
            - Same comments as S currents.
        T currents:
            - Hadr(lb, lc, lw, lwd, 'T', 'a0') have 64 terms out of which 32 are nonzero.
        PT currents:
            - Like in Leptonic current these are NOT equal to T currents.
            - same comments as T currents.
        Func returns:
            Dict with index as tuples Hadr[(lb, lc, lw, lwd, wc, ff)]
        """
        #q2 and angular terms: Used sin(x/2) = sqrt((1-cthlc)/2) and cos(x/2)=sqrt((1+cthlc)/2).Since x/2 ranges from 0 to pi/2 both sine and cosine are pos.
        q2          =  Obs['q2']
        Mpos        = atfi.const(self.MLb) + atfi.const(self.MLc)
        Mneg        = atfi.const(self.MLb) - atfi.const(self.MLc)
        sqrtQp      = atfi.sqrt(atfi.pow(Mpos,2) - q2)
        sqrtQn      = atfi.sqrt(atfi.pow(Mneg,2) - q2)
        sqrtQpMn    = sqrtQp * Mneg
        sqrtQnMp    = sqrtQn * Mpos
        aV          = sqrtQpMn/atfi.sqrt(q2)
        bV          = sqrtQnMp/atfi.sqrt(q2)
        cV          = atfi.sqrt(np.array(2.)) * sqrtQn
        aA          = bV
        bA          = aV
        cA          = atfi.sqrt(np.array(2.)) * sqrtQp
        mb          = atfi.const(4.18       ) #GeV
        mc          = atfi.const(1.275      ) #GeV
        aS          = sqrtQpMn/(mb - mc)
        aP          = sqrtQnMp/(mb + mc)
        aT          = sqrtQn
        bT          = atfi.sqrt(np.array(2.)) * bV
        cT          = atfi.sqrt(np.array(2.)) * aV
        dT          = sqrtQp
        cthlc       = -Obs['lb_ctheta_w'] #ONE Lb polarisation ignored
        costhlchalf = atfi.sqrt((1+cthlc)/2.)  #One Lb polarisation ignored
        sinthlchalf = atfi.sqrt((1-cthlc)/2.)  #ZERO Lb polarisation ignored, so commented out the terms
        t0          = atfi.pow(Mneg,2)
    
        #get only q2 dependent terms in FF expansion i.e. ones pertaining to a0 and a1.
        ffterms = {}
        for ff in ['fplus', 'fperp', 'f0', 'gplus', 'gperp', 'g0', 'hplus', 'hperp', 'htildeperp', 'htildeplus']:
            cf = 1./(1. - q2/atfi.const(self.Tf_plus[ff]))
            zf = (atfi.sqrt(atfi.const(self.Tf_plus[ff]) - q2) - atfi.sqrt(atfi.const(self.Tf_plus[ff]) - t0))/(atfi.sqrt(atfi.const(self.Tf_plus[ff]) - q2) + atfi.sqrt(atfi.const(self.Tf_plus[ff]) - t0))
            ffterms['a0'+ff] =  cf
            ffterms['a1'+ff] =  cf * zf
    
        #define hadronic amplitudes
        Hadr = {}
        #f0 terms: contains V and S currents
        for a in ['a0f0', 'a1f0']:
            Hadr[( 1, 1,'t', 't', 'V', a)] =  costhlchalf * aV * ffterms[a]
            Hadr[(-1,-1,'t', 't', 'V', a)] =  costhlchalf * aV * ffterms[a]
            Hadr[( 1, 1,'t', 't', 'S', a)] =  costhlchalf * aS * ffterms[a]
            Hadr[(-1,-1,'t', 't', 'S', a)] =  costhlchalf * aS * ffterms[a]
            #Hadr[( 1,-1,'t', 't', 'V', a)] = -sinthlchalf * aV * ffterms[a]
            #Hadr[(-1, 1,'t', 't', 'V', a)] =  sinthlchalf * aV * ffterms[a]
            #Hadr[( 1,-1,'t', 't', 'S', a)] = -sinthlchalf * aS * ffterms[a]
            #Hadr[(-1, 1,'t', 't', 'S', a)] =  sinthlchalf * aS * ffterms[a]
        
        #fplus terms
        for a in ['a0fplus', 'a1fplus']:
            Hadr[( 1, 1,  0, 't', 'V', a)] =  costhlchalf  * bV * ffterms[a]
            Hadr[(-1,-1,  0, 't', 'V', a)] =  costhlchalf  * bV * ffterms[a]
            #Hadr[( 1,-1,  0, 't', 'V', a)] = -sinthlchalf  * bV * ffterms[a]
            #Hadr[(-1, 1,  0, 't', 'V', a)] =  sinthlchalf  * bV * ffterms[a]
        
        #fperp terms
        for a in ['a0fperp', 'a1fperp']:
            Hadr[( 1,-1, -1, 't', 'V', a)] = -costhlchalf  * cV * ffterms[a]
            Hadr[(-1, 1,  1, 't', 'V', a)] = -costhlchalf  * cV * ffterms[a]
            #Hadr[(-1,-1, -1, 't', 'V', a)] = -sinthlchalf  * cV * ffterms[a]
            #Hadr[( 1, 1,  1, 't', 'V', a)] =  sinthlchalf  * cV * ffterms[a]
        
        #g0 terms: contains A and PS currents
        for a in ['a0g0','a1g0']:
            Hadr[( 1, 1,'t', 't', 'A', a)] =  costhlchalf * aA * ffterms[a]
            Hadr[(-1,-1,'t', 't', 'A', a)] = -costhlchalf * aA * ffterms[a]
            Hadr[( 1, 1,'t', 't','PS', a)] = -costhlchalf * aP * ffterms[a]
            Hadr[(-1,-1,'t', 't','PS', a)] =  costhlchalf * aP * ffterms[a]
            #Hadr[( 1,-1,'t', 't', 'A', a)] =  sinthlchalf * aA * ffterms[a]
            #Hadr[(-1, 1,'t', 't', 'A', a)] =  sinthlchalf * aA * ffterms[a]
            #Hadr[( 1,-1,'t', 't','PS', a)] = -sinthlchalf * aP * ffterms[a]
            #Hadr[(-1, 1,'t', 't','PS', a)] = -sinthlchalf * aP * ffterms[a]
        
        #gplus terms
        for a in ['a0gplus','a1gplus']:
            Hadr[( 1, 1,  0, 't', 'A', a)] =  costhlchalf * bA * ffterms[a]
            Hadr[(-1,-1,  0, 't', 'A', a)] = -costhlchalf * bA * ffterms[a]
            #Hadr[( 1,-1,  0, 't', 'A', a)] =  sinthlchalf * bA * ffterms[a]
            #Hadr[(-1, 1,  0, 't', 'A', a)] =  sinthlchalf * bA * ffterms[a]
        
        #gperp terms
        for a in ['a0gperp','a1gperp']:
            Hadr[( 1,-1, -1, 't', 'A', a)] =  costhlchalf * cA * ffterms[a]
            Hadr[(-1, 1,  1, 't', 'A', a)] = -costhlchalf * cA * ffterms[a]
            #Hadr[(-1,-1, -1, 't', 'A', a)] =  sinthlchalf * cA * ffterms[a]
            #Hadr[( 1, 1,  1, 't', 'A', a)] =  sinthlchalf * cA * ffterms[a]
        
        #hplus terms: T and PT
        for a in ['a0hplus','a1hplus']:
            Hadr[( 1, 1,'t',  0, 'T', a)] =  costhlchalf * aT * ffterms[a]
            Hadr[(-1,-1,'t',  0, 'T', a)] =  costhlchalf * aT * ffterms[a]
            Hadr[( 1, 1,  0,'t', 'T', a)] = -costhlchalf * aT * ffterms[a]
            Hadr[(-1,-1,  0,'t', 'T', a)] = -costhlchalf * aT * ffterms[a]
            Hadr[( 1, 1,  1, -1, 'PT',a)] =  costhlchalf * aT * ffterms[a]
            Hadr[(-1,-1,  1, -1, 'PT',a)] =  costhlchalf * aT * ffterms[a]
            Hadr[( 1, 1, -1,  1, 'PT',a)] = -costhlchalf * aT * ffterms[a]
            Hadr[(-1,-1, -1,  1, 'PT',a)] = -costhlchalf * aT * ffterms[a]
            #Hadr[( 1,-1,'t',  0, 'T', a)] = -sinthlchalf * aT * ffterms[a]
            #Hadr[(-1, 1,'t',  0, 'T', a)] =  sinthlchalf * aT * ffterms[a]
            #Hadr[( 1,-1,  0,'t', 'T', a)] =  sinthlchalf * aT * ffterms[a]
            #Hadr[(-1, 1,  0,'t', 'T', a)] = -sinthlchalf * aT * ffterms[a]
            #Hadr[( 1,-1,  1, -1, 'PT',a)] = -sinthlchalf * aT * ffterms[a]
            #Hadr[(-1, 1,  1, -1, 'PT',a)] =  sinthlchalf * aT * ffterms[a]
            #Hadr[( 1,-1, -1,  1, 'PT',a)] =  sinthlchalf * aT * ffterms[a]
            #Hadr[(-1, 1, -1,  1, 'PT',a)] = -sinthlchalf * aT * ffterms[a]
        
        #hperp terms: T and PT
        for a in ['a0hperp','a1hperp']:
            Hadr[(-1, 1,'t',  1, 'T', a)] = -costhlchalf * bT *  ffterms[a]
            Hadr[( 1,-1,'t', -1, 'T', a)] = -costhlchalf * bT *  ffterms[a]
            Hadr[(-1, 1,  1,'t', 'T', a)] =  costhlchalf * bT *  ffterms[a]
            Hadr[( 1,-1, -1,'t', 'T', a)] =  costhlchalf * bT *  ffterms[a]
            Hadr[( 1,-1,  0, -1, 'PT',a)] = -costhlchalf * bT *  ffterms[a]
            Hadr[( 1,-1, -1,  0, 'PT',a)] =  costhlchalf * bT *  ffterms[a]
            Hadr[(-1, 1,  0,  1, 'PT',a)] =  costhlchalf * bT *  ffterms[a]
            Hadr[(-1, 1,  1,  0, 'PT',a)] = -costhlchalf * bT *  ffterms[a]
            #Hadr[(-1,-1,'t', -1, 'T', a)] = -sinthlchalf * bT *  ffterms[a]
            #Hadr[( 1, 1,'t',  1, 'T', a)] =  sinthlchalf * bT *  ffterms[a]
            #Hadr[(-1,-1, -1,'t', 'T', a)] =  sinthlchalf * bT *  ffterms[a]
            #Hadr[( 1, 1,  1,'t', 'T', a)] = -sinthlchalf * bT *  ffterms[a]
            #Hadr[(-1,-1,  0, -1, 'PT',a)] = -sinthlchalf * bT *  ffterms[a]
            #Hadr[( 1, 1,  1,  0, 'PT',a)] =  sinthlchalf * bT *  ffterms[a]
            #Hadr[(-1,-1, -1,  0, 'PT',a)] =  sinthlchalf * bT *  ffterms[a]
            #Hadr[( 1, 1,  0,  1, 'PT',a)] = -sinthlchalf * bT *  ffterms[a]
        
        #htildeperp terms: T and PT
        for a in ['a0htildeperp','a1htildeperp']:
            Hadr[(-1, 1,  0,  1, 'T', a)] = -costhlchalf * cT * ffterms[a]
            Hadr[( 1,-1,  0, -1, 'T', a)] = -costhlchalf * cT * ffterms[a]
            Hadr[(-1, 1,  1,  0, 'T', a)] =  costhlchalf * cT * ffterms[a]
            Hadr[( 1,-1, -1,  0, 'T', a)] =  costhlchalf * cT * ffterms[a]
            Hadr[( 1,-1,'t', -1, 'PT',a)] = -costhlchalf * cT * ffterms[a]
            Hadr[(-1, 1,'t',  1, 'PT',a)] =  costhlchalf * cT * ffterms[a]
            Hadr[( 1,-1, -1,'t', 'PT',a)] =  costhlchalf * cT * ffterms[a]
            Hadr[(-1, 1,  1,'t', 'PT',a)] = -costhlchalf * cT * ffterms[a]
            #Hadr[(-1,-1,  0, -1, 'T', a)] = -sinthlchalf * cT * ffterms[a]
            #Hadr[( 1, 1,  0,  1, 'T', a)] =  sinthlchalf * cT * ffterms[a]
            #Hadr[(-1,-1, -1,  0, 'T', a)] =  sinthlchalf * cT * ffterms[a]
            #Hadr[( 1, 1,  1,  0, 'T', a)] = -sinthlchalf * cT * ffterms[a]
            #Hadr[( 1, 1,'t',  1, 'PT',a)] = -sinthlchalf * cT * ffterms[a]
            #Hadr[(-1,-1,'t', -1, 'PT',a)] = -sinthlchalf * cT * ffterms[a]
            #Hadr[( 1, 1,  1,'t', 'PT',a)] =  sinthlchalf * cT * ffterms[a]
            #Hadr[(-1,-1, -1,'t', 'PT',a)] =  sinthlchalf * cT * ffterms[a]
        
        #htildeplus terms: T and PT
        for a in ['a0htildeplus','a1htildeplus']:
            Hadr[( 1, 1,  1, -1, 'T', a)] = -costhlchalf * dT * ffterms[a]
            Hadr[(-1,-1,  1, -1, 'T', a)] =  costhlchalf * dT * ffterms[a]
            Hadr[( 1, 1, -1,  1, 'T', a)] =  costhlchalf * dT * ffterms[a]
            Hadr[(-1,-1, -1,  1, 'T', a)] = -costhlchalf * dT * ffterms[a]
            Hadr[( 1, 1,'t',  0, 'PT',a)] = -costhlchalf * dT * ffterms[a]
            Hadr[(-1,-1,'t',  0, 'PT',a)] =  costhlchalf * dT * ffterms[a]
            Hadr[( 1, 1,  0,'t', 'PT',a)] =  costhlchalf * dT * ffterms[a]
            Hadr[(-1,-1,  0,'t', 'PT',a)] = -costhlchalf * dT * ffterms[a]
            #Hadr[( 1,-1,  1, -1, 'T', a)] = -sinthlchalf * dT * ffterms[a]
            #Hadr[(-1, 1,  1, -1, 'T', a)] = -sinthlchalf * dT * ffterms[a]
            #Hadr[( 1,-1, -1,  1, 'T', a)] =  sinthlchalf * dT * ffterms[a]
            #Hadr[(-1, 1, -1,  1, 'T', a)] =  sinthlchalf * dT * ffterms[a]
            #Hadr[( 1,-1,'t',  0, 'PT',a)] = -sinthlchalf * dT * ffterms[a]
            #Hadr[(-1, 1,'t',  0, 'PT',a)] = -sinthlchalf * dT * ffterms[a]
            #Hadr[( 1,-1,  0,'t', 'PT',a)] =  sinthlchalf * dT * ffterms[a]
            #Hadr[(-1, 1,  0,'t', 'PT',a)] =  sinthlchalf * dT * ffterms[a]
    
        return Hadr

    def get_w_ampls(self, Obs):
        """
        Some important comments on all the leptonic currents
        V or A current:
            - Terms with WC index V and A are equal.
            - Since only one term appears in the ampl for lwd. We fix lwd to 't' as done in hadronic V, A currents. Index 't' choosen because eta['t'] = 1.
            - Therefore Lept(lw, 't', 'V', ll) has total 8 components out of which 1 is zero.
        S or PS current:
            - Terms with WC index S and PS are equal.
            - In this case both lw and lwd are fake indeces (see above for reason), choosen to be 't' (see above) as done in hadronic S, PS currents.
            - Lept('t', 't', 'S', ll) has total 2 components out of which 1 is zero.
        T or PT current:
            - Terms with WC index T and PT are equal.
            - Lept(lw,lwd,'T',l) has total 32 components out of which 8 are zero.
        Func returns:
            Dict with index as tuples Lept[(lw, lwd, wc, ll)]
        """
        
        #q2 and angular terms
        q2              = Obs['q2'] #real
        cthl            =-Obs['w_ctheta_l'] #real (our pdf is expressed a function of w_ctheta_nu not w_ctheta_l
        phl             = Obs['w_phi_l'] #ZERO Lb polarisation ignored as a result amplitude real
        v               = atfi.sqrt(1. - atfi.pow(atfi.const(self.Mlep),2)/q2) #real
        sinthl          = atfi.sqrt(1. - atfi.pow(cthl,2.)) #real
        expPlusOneIphl  = atfi.cos(phl)    #real and One since Lb polarisation ignored
        expMinusOneIphl = atfi.cos(-phl)   #real and One since Lb polarisation ignored
        expMinusTwoIphl = atfi.cos(-2.*phl)#real and One since Lb polarisation ignored
        OneMinuscthl    = (1. - cthl)
        OnePluscthl     = (1. + cthl)
        al              = 2. * atfi.const(self.Mlep) * v
        bl              = 2. * atfi.sqrt(q2)* v
        complexsqrttwo  = atfi.sqrt(np.array(2.))
        
        Lept = {}
        
        Lept[('t', 't', 'V',  1)] =  expMinusOneIphl * al
        Lept[(  0, 't', 'V',  1)] = -expMinusOneIphl * cthl * al
        Lept[(  1, 't', 'V',  1)] =  expMinusTwoIphl * sinthl * al/complexsqrttwo
        Lept[( -1, 't', 'V',  1)] = -sinthl * al/complexsqrttwo
        Lept[(  0, 't', 'V', -1)] =  sinthl * bl
        Lept[(  1, 't', 'V', -1)] =  expMinusOneIphl * OnePluscthl  * bl/complexsqrttwo
        Lept[( -1, 't', 'V', -1)] =  expPlusOneIphl  * OneMinuscthl * bl/complexsqrttwo
        
        Lept[('t', 't', 'S', 1)]  =  expMinusOneIphl * bl
        
        Lept[('t',  0, 'T', -1)]  =  sinthl * al
        Lept[(  1, -1, 'T', -1)]  =  sinthl * al
        Lept[(  0, -1, 'T', -1)]  =  expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
        Lept[('t', -1, 'T', -1)]  =  expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
        Lept[('t',  1, 'T', -1)]  =  expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
        Lept[(  0,  1, 'T', -1)]  = -expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
        Lept[('t',  0, 'T',  1)]  = -expMinusOneIphl * cthl * bl
        Lept[(  1, -1, 'T',  1)]  = -expMinusOneIphl * cthl * bl
        Lept[('t',  1, 'T',  1)]  =  expMinusTwoIphl * sinthl * bl/complexsqrttwo
        Lept[(  0,  1, 'T',  1)]  = -expMinusTwoIphl * sinthl * bl/complexsqrttwo
        Lept[('t', -1, 'T',  1)]  = -sinthl * bl/complexsqrttwo
        Lept[(  0, -1, 'T',  1)]  = -sinthl * bl/complexsqrttwo
        Lept[(  0,'t', 'T', -1)]  = -sinthl * al
        Lept[( -1,  1, 'T', -1)]  = -sinthl * al
        Lept[( -1,  0, 'T', -1)]  = -expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
        Lept[( -1,'t', 'T', -1)]  = -expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
        Lept[(  1,'t', 'T', -1)]  = -expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
        Lept[(  1,  0, 'T', -1)]  =  expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
        Lept[(  0,'t', 'T',  1)]  =  expMinusOneIphl * cthl * bl
        Lept[( -1,  1, 'T',  1)]  =  expMinusOneIphl * cthl * bl
        Lept[(  1,'t', 'T',  1)]  = -expMinusTwoIphl * sinthl * bl/complexsqrttwo
        Lept[(  1,  0, 'T',  1)]  =  expMinusTwoIphl * sinthl * bl/complexsqrttwo
        Lept[( -1,'t', 'T',  1)]  =  sinthl * bl/complexsqrttwo
        Lept[( -1,  0, 'T',  1)]  =  sinthl * bl/complexsqrttwo
        return Lept

    def BuggLineShape(self, m2, m0, gamma0, gamma, alpha, ma, mb, mc, md, dr, dd, lr, ld, barrierFactor = True, ma0=None, md0 = None) : 
        m   = atfi.sqrt(m2)
        q   = atfk.two_body_momentum(md, m, mc)
        q0  = atfk.two_body_momentum(md if md0 is None else md0, m0, mc)
        p   = atfk.two_body_momentum(m, ma, mb)
        p0  = atfk.two_body_momentum(m0, ma if ma0 is None else ma0, mb)
        ffr = atfd.blatt_weisskopf_ff(p, p0, dr, lr)
        ffd = atfd.blatt_weisskopf_ff(q, q0, dd, ld)

        sa = ma*ma-0.5*mb*mb
        width = (m2 - sa)/(m0*m0 - sa)*gamma0*atfi.exp(-gamma*m2)
        bw = atfd.relativistic_breit_wigner(m2, m0, width)

        ff = ffr*ffd
        if barrierFactor : 
            b1 = atfd.orbital_barrier_factor(p, p0, lr)
            b2 = atfd.orbital_barrier_factor(q, q0, ld)
            ff *= b1*b2
        return bw*atfi.complex(ff*atfi.exp(-alpha*q*q), atfi.const(0.))

    def FlatteLineShapeCorrected(self, m2, m0, gamma1, gamma2, ma, mb, ma2, mb2,mc, md, dr, dd, lr, ld, barrierFactor = True, ma0=None, ma02=None, md0 = None) : 
        m = atfi.sqrt(m2)
        #Mother particle decay
        q  = atfk.two_body_momentum(md, m, mc)
        q0 = atfk.two_body_momentum(md if md0 is None else md0, m0, mc)
        ffd = atfd.blatt_weisskopf_ff(q, q0, dd, ld)

        #Resonance First decay channel
        p  = atfk.two_body_momentum(m, ma, mb)
        #Use alternative channel since first is under threshold
        p0 = atfk.two_body_momentum(m0, ma2 if ma02 is None else ma02, mb2)
        ffr = atfd.blatt_weisskopf_ff(p, p0, dr, lr)
        width = atfd.mass_dependent_width(m, m0, gamma1, p, p0, ffr, lr)

        #Resonance Second decay channel
        p2  = atfk.two_body_momentum(m, ma2, mb2)
        ffr2 = atfd.blatt_weisskopf_ff(p2, p0, dr, lr)
        width2 = atfd.mass_dependent_width(m, m0, gamma2, p2, p0, ffr2, lr)

        totwidth = width+width2

        bw = atfd.relativistic_breit_wigner(m2, m0, totwidth)
        ff = ffr*ffd
        if barrierFactor : 
            b1 = atfd.orbital_barrier_factor(p, p0, lr)
            b2 = atfd.orbital_barrier_factor(q, q0, ld)
            ff *= b1*b2

        return bw*atfi.complex(ff, atfi.const(0.))
    
    def get_Lc_amps(self, Obvs):

        @atfi.function
        def _Ampl_K( res, Nu , lp , lres, coupling ):

            #delta function
            delta = atfi.cast_complex(atfi.const(1.))
            if Nu/2. != (lres/2. - lp/2.): delta = atfi.cast_complex(atfi.const(0.))

            #lineshape
            if 'kpi0' in res:
                Xs = self.BuggLineShape( m2_kpi, res_val[res]['mass'](), res_val[res]['width'](), res_val[res]['gamma'], \
                                         res_val[res]['alpha'], self.Mk, self.Mpi , self.Mp , self.MLc , d_res, d_Lambda_c, \
                                         res_val[res]['l_res'] , res_val[res]['l_lamc'], barrierFactor = True) 
            else:
                Xs = atfd.breit_wigner_lineshape( m2_kpi , res_val[res]["mass"]() , res_val[res]["width"]() , self.Mk , \
                                                      self.Mpi , self.Mp , self.MLc , d_res, d_Lambda_c, \
                                                      res_val[res]['l_res'] , res_val[res]['l_lamc'])

            A  = atfi.cast_complex(atfi.sqrt(2.) * atfi.sqrt(2. * res_val[res]['spin']/2. + 1))
            A *= delta
            A *= Xs
            A *= atfi.cast_complex(atfk.wigner_small_d( theta_23, res_val[res]['spin'] , lres , 0 ) )
            A *= atfi.cast_complex( ( -1 )**( self.Jp/2. - lp/2. ) ) #factor due JW particle-2 phase convention of helicity coupling
            A *= coupling
            return A
        
        @atfi.function
        def _Ampl_D( res, Nu , lp_d , lres , coupling ,lp):
            #lineshape
            Xs  = atfd.breit_wigner_lineshape( m2_ppi , res_val[res]['mass'](), res_val[res]['width'](), self.Mp, self.Mpi , \
											  self.Mk , self.MLc , d_res , d_Lambda_c ,res_val[res]['l_res'] , res_val[res]['l_lamc'])
        
            #parity conservation factor in JW convention
            eta = atfi.cast_complex( 1.)
            if lp_d == -1: eta = atfi.cast_complex( ( -1 )**( 3/2. - res_val[res]['spin']/2.) * res_val[res]['parity'] )

            A  = atfi.cast_complex(atfi.sqrt(2.) * atfi.sqrt(2. * res_val[res]['spin']/2. + 1))
            A *= atfi.cast_complex( ( -1 )**(Nu/2. - lres/2.) * atfk.wigner_small_d( theta_hat_12 , self.Jlam_c , Nu , lres ) )
            A *= Xs
            A *= atfi.cast_complex( atfk.wigner_small_d( zeta_121 , self.Jlam_c , lp_d , lp ) )
            A *= atfi.cast_complex( ( -1 )**( self.Jp/2. - lp_d/2. ) ) #factor due JW particle-2 phase convention of helicity coupling
            A *= coupling
            A *= eta
            return A 

        @atfi.function
        def _Ampl_L( res, Nu , lp_d , lres , coupling ,  lp ):
            #lineshape
            if res == 'L(1405)':
                Xs = self.FlatteLineShapeCorrected( m2_pk, res_val[res]['mass'](), res_val[res]["width1"](), res_val[res]["width2"](), self.Mp, self.Mk, \
                                                   res_val[res]["ma2"], res_val[res]["mb2"], self.Mpi, self.MLc, d_res, d_Lambda_c, \
                                                   res_val[res]['l_res'] , res_val[res]['l_lamc'], barrierFactor = True)
            else:
                barrier_factor = True
                Xs = atfd.breit_wigner_lineshape( m2_pk , res_val[res]['mass'](), res_val[res]['width'](), self.Mp , self.Mk , self.Mpi , \
                                                 self.MLc, d_res , d_Lambda_c , res_val[res]['l_res'] , res_val[res]['l_lamc'], barrier_factor = barrier_factor)

            #parity conservation factor in JW convention
            eta  = atfi.cast_complex( 1.)
            if lp_d == -1: eta = atfi.cast_complex( ( -1 )**( 3/2. - res_val[res]['spin']/2.) * res_val[res]['parity'] )

            A  = atfi.cast_complex(atfi.sqrt(2.) * atfi.sqrt(2. * res_val[res]['spin']/2. + 1))
            A *= atfi.cast_complex(atfk.wigner_small_d( theta_hat_31, self.Jlam_c , Nu , lres ) )
            A *= Xs
            A *= atfi.cast_complex( ( -1 )**(lp_d/2. - lp/2.))  * atfi.cast_complex( atfk.wigner_small_d( zeta_113 , self.Jlam_c , lp_d , lp ) )
            A *= coupling
            A *= eta
            return A
            

        @atfi.function
        def _A_res(Nu, lp):
            lp_ds = [1, -1]
            ampl_k  = atfi.complex(atfi.const(0.) , atfi.const(0.))
            ampl_l  = atfi.complex(atfi.const(0.) , atfi.const(0.))
            ampl_d  = atfi.complex(atfi.const(0.) , atfi.const(0.))
            #coherently sum over resonances and their helicities
            for indx, r in enumerate(list(res_val.keys())):
                if 'kpi(892)' in r:
                    coupling1 = res_val[r]['coupl'][0]()
                    coupling2 = res_val[r]['coupl'][1]()
                    coupling3 = res_val[r]['coupl'][2]()
                    coupling4 = res_val[r]['coupl'][3]()
                    if lp == 1:
                        ampl_k1  = _Ampl_K(r, Nu , lp, 0., coupling1 )
                        ampl_k1 += _Ampl_K(r, Nu , lp, 2., coupling2 )
                    else:
                        ampl_k1  = _Ampl_K(r,  Nu , lp, 0. , coupling3 )
                        ampl_k1 += _Ampl_K(r,  Nu , lp, -2., coupling4 )
                
                    ampl_k  += atfi.cast_complex(self.switches[indx]) * ampl_k1
                elif 'kpi0' in r:
                    coupling1       = res_val[r]['coupl'][1]()
                    coupling2       = res_val[r]['coupl'][0]()
                    if lp == 1:
                        ampl_k1  = _Ampl_K(r, Nu , lp, 0., coupling1 )
                    else:
                        ampl_k1  = _Ampl_K(r, Nu , lp, 0., coupling2 )
                
                    ampl_k  += atfi.cast_complex(self.switches[indx]) * ampl_k1
                elif 'L' in r: 
                    #coherently sum of lp_ds
                    ampl_lpd_L = atfi.complex(atfi.const(0.) , atfi.const(0.))
                    for lp_d in lp_ds:
                        coupling1   = res_val[r]['coupl'][0]()
                        coupling2   = res_val[r]['coupl'][1]()
                        ampl_lpd_L += _Ampl_L( r, Nu , lp_d ,  1 , coupling1 , lp ) 
                        ampl_lpd_L += _Ampl_L( r, Nu , lp_d , -1 , coupling2 , lp ) 

                    ampl_l  += atfi.cast_complex(self.switches[indx]) * ampl_lpd_L
                elif 'D' in r: 
                    #coherently sum of lp_ds
                    ampl_lpd_D = atfi.complex(atfi.const(0.) , atfi.const(0.))
                    for lp_d in lp_ds:
                        coupling1   = res_val[r]['coupl'][0]()
                        coupling2   = res_val[r]['coupl'][1]()
                        ampl_lpd_D += _Ampl_D( r, Nu , lp_d ,  1 , coupling1 , lp )
                        ampl_lpd_D += _Ampl_D( r, Nu , lp_d , -1 , coupling2 , lp )

                    ampl_d  += atfi.cast_complex(self.switches[indx]) * ampl_lpd_D
                else:
                    raise Exception('Channel not specified for resonance. Please check!')

            amplitude = ampl_k + ampl_l + ampl_d
            return amplitude

		#get the resonances
        res_val = self.resn

        #get variables
        m2_pk        = Obvs['m2_pk']        
        m2_kpi       = Obvs['m2_kpi']       
        ctheta_p     = Obvs['ctheta_p']
        phi_p        = Obvs['phi_p']  
        phi_kpi      = Obvs['phi_kpi']
        m2_ppi       = Obvs['m2_ppi']       
        theta_23     = Obvs['theta_23']     
        theta_hat_12 = Obvs['theta_hat_12'] 
        theta_31     = Obvs['theta_31']     
        zeta_121     = Obvs['zeta_121']     
        theta_hat_31 = Obvs['theta_hat_31'] 
        theta_12     = Obvs['theta_12']     
        zeta_113     = Obvs['zeta_113']   

        #finite widths - can be varied to find optimal - (Blatt-Weisskopf radii)
        d_res             = atfi.const(1.5)  #GeV
        d_Lambda_c        = atfi.const(5.)  #GeV

        #invariant amplitude
        O_nu_lp = {}
        O_nu_lp[( 1 ,  1)] = _A_res( 1,  1)
        O_nu_lp[( 1 , -1)] = _A_res( 1, -1)
        O_nu_lp[(-1 ,  1)] = _A_res(-1,  1)
        O_nu_lp[(-1 , -1)] = _A_res(-1, -1)

        #wignerd rotate
        D_lLc_nu = {}
        D_lLc_nu[( 1,  1)] = tf.math.conj( atfk.wigner_capital_d( phi_p , tf.acos(ctheta_p), phi_kpi, self.Jlam_c,  1,  1 ) )
        D_lLc_nu[( 1, -1)] = tf.math.conj( atfk.wigner_capital_d( phi_p , tf.acos(ctheta_p), phi_kpi, self.Jlam_c,  1, -1 ) )
        D_lLc_nu[(-1,  1)] = tf.math.conj( atfk.wigner_capital_d( phi_p , tf.acos(ctheta_p), phi_kpi, self.Jlam_c, -1,  1 ) )
        D_lLc_nu[(-1, -1)] = tf.math.conj( atfk.wigner_capital_d( phi_p , tf.acos(ctheta_p), phi_kpi, self.Jlam_c, -1, -1 ) )

        #M_lLc_lp
        M_lLc_lp = {}
        M_lLc_lp[( 1,  1)] = atfi.complex(atfi.const(0), atfi.const(0))
        M_lLc_lp[( 1, -1)] = atfi.complex(atfi.const(0), atfi.const(0))
        M_lLc_lp[(-1,  1)] = atfi.complex(atfi.const(0), atfi.const(0))
        M_lLc_lp[(-1, -1)] = atfi.complex(atfi.const(0), atfi.const(0))
        for lLc in [-1, 1]:
            for lp in [-1, 1]:
                for nu in [-1, 1]:
                    M_lLc_lp[(lLc, lp)] += D_lLc_nu[(lLc, nu)] * O_nu_lp[( nu, lp)]

        return M_lLc_lp

    def get_dynamic_term(self, obsv):
        """Get the dynamic density term in Lb->Lclnu decay"""
    
        #define a helpful function
        def _replace_wc(strg):
            """Helpful function when for leptonic amplitudes in definition of density function"""
            strg_wc = strg.replace('A' , 'V')
            strg_wc = strg_wc.replace('PS', 'S')
            strg_wc = strg_wc.replace('PT', 'T')
            return strg_wc
    
        #get Lb and w amplitudes
        free_params     = self.get_freeparams()
        Lbampl          = self.get_lb_ampls(obsv)  #LbDecay_lLb_lLc_lw_lwd_WC_FF
        Wampl           = self.get_w_ampls(obsv)   #WDecay_lw_lwd_WC_ll
        Lcampl          = self.get_Lc_amps(obsv)  #LcDecay_J_Lc_proj, lp
        #print(len(LbDecay_lLb_lLc_lw_lwd_WC_FF.keys()))    #192 reduces to 92 if Lb is unpolarised
        #print(len(WDecay_lw_lwd_WC_ll.keys()))             #32
        #print(len(fp_wc_ff.keys()))                        #6*20 = 120

        dens = atfi.const(0.)
        for lLb in self.lLbs: 
            for ll in self.lls:
                for lp in self.lps:
                    ampls_coherent = atfi.complex( atfi.const(0.), atfi.const(0.) ) 
                    for lLc in self.lLcs:
                        for lw in self.lws:
                            for lwd in self.lws:
                                for WC in self.WCs:
                                    for FF in self.FFs:
                                        lbindx = (lLb,lLc,lw,lwd,WC,FF)
                                        windx  = (lw,lwd,_replace_wc(WC),ll)
                                        fpindx = (WC, FF)
                                        lcindx = (lLc, lp)
                                        cond   = lbindx not in Lbampl or windx not in Wampl or lcindx not in Lcampl
                                        if cond: continue
                                        amp  = atfi.cast_complex(atfi.sqrt(np.array(2.))*atfi.const(self.GF)*atfi.const(self.Vcb)*atfi.const(self.signWC[WC])*atfi.const(self.eta[lw])*atfi.const(self.eta[lwd]) )
                                        amp *= atfi.cast_complex(Lbampl[lbindx])*atfi.cast_complex(Wampl[windx])*free_params[fpindx] 
                                        amp *= Lcampl[lcindx]
                                        #print(amp)
                                        ampls_coherent += amp

                    #print(ampls_coherent)
                    dens += atfd.density(ampls_coherent)

        return dens

    def get_unbinned_model(self, ph):
        """Define the unbinned model for Lb->Lclnu"""
        obsv      = self.prepare_data(ph) 
        if self.res_list == ["phsp"]:
            model     = self.get_phasespace_term(obsv)
        else:
            dGdO_phsp = self.get_phasespace_term(obsv)
            dGdO_dynm = self.get_dynamic_term(obsv)
            model     = dGdO_phsp * dGdO_dynm

        return model

    def MC(self, size, chunk = 5000, size_maxest = 100000): 
        atfi.set_seed(1)
        #set seed for reproducibility 
        maximum = tft.maximum_estimator(self.get_unbinned_model , self.phase_space, size_maxest) * 1.5
        print(maximum)
        toy_sample = tft.run_toymc( self.get_unbinned_model , self.phase_space , size , maximum, chunk, components = False)
        print(toy_sample.numpy())
        return toy_sample.numpy()

    def set_params_values(self, res, isfitresult = True):
        """Set the parameters values to the ones given in the dictionary"""
        for k in list(res.keys()):
            for p in list(self.tot_params.values()):
                if p.name == k:
                    if isfitresult:
                        if p.floating():
                            print('Setting', p.name, 'from ', p.numpy(), 'to ', res[k][0])
                            p.update(res[k][0])
                    else:
                        print('Setting', p.name, 'from ', p.numpy(), 'to ', res[k])
                        p.init_value = res[k]
                        p.update(res[k])

    def plots(self, data, data2, i):
        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure
        label  = ["q2", "cth_l", "m2_pK", "m2_kpi", "cth_p", "ph_p", "phi_kpi", "m2_ppi"]
        if i == 1:
            data = np.cos(data)
            data2 = np.cos(data2)

        tfp.plot_distr1d_comparison(
            data, 
            data2,
            bins= 80,
            range=(np.min(data2), np.max(data2)),
            ax=ax,
            label = label[i])
       
        #plt.legend(['SM', 'NP'], fontsize = 'x-small')
        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
        return plt.show()  
    
########### tests lam_c added
MLb     = 5619.49997776e-3    #GeV
MLc     = 2286.45992749e-3    #GeV
Mmu     = 105.6583712e-3      #GeV Mu
Mtau    = 1.777      #GeV Mu
Mp      = 0.938      
Mk      = 0.497    
Mpi     = 0.140 

#res_list= ["phsp"]
#res_list= ["Kstar_kpi(892)"]
res_list= ["Kstar_kpi(892)", "D(1232)", "L(1520)"]

md      = LbToLclNu_Model(MLb, MLc, Mmu, Mp, Mk, Mpi, res_list = res_list)

#phsp_x = np.array([[ 7.6327249   , 2.39983158  , 2.75241324  , 0.84506253 , -0.16253154 , -1.31819642 , -1.73111351], 
#                  [ 8.8174615   , 2.24045093  , 3.39649043  , 0.60204142 , -0.58377375 ,  2.6469123  , -0.62482103]])
#pdf = md.get_unbinned_model(phsp_x)
#print(pdf.numpy())
#md.set_params_values({'CVR': -0.8}, isfitresult = False)
#pdf = md.get_unbinned_model(phsp_x)
#print(pdf.numpy())

toy_sample = md.MC(50000)
print(toy_sample)
md.set_params_values({'CVR':  0.1}, isfitresult = False)
toy_sample2 = md.MC(50000)

mdtau.plots(toy_sample[:, 0], toy_sample2[:, 0], 0)
mdtau.plots(toy_sample[:, 1], toy_sample2[:, 1], 1)
mdtau.plots(toy_sample[:, 2], toy_sample2[:, 2], 2)
mdtau.plots(toy_sample[:, 3], toy_sample2[:, 3], 3)
mdtau.plots(toy_sample[:, 4], toy_sample2[:, 4], 4)
mdtau.plots(toy_sample[:, 5], toy_sample2[:, 5], 5)
mdtau.plots(toy_sample[:, 6], toy_sample2[:, 6], 6)

#mdtau      = LbToLclNu_Model(MLb, MLc, Mtau, Mp, Mk, Mpi, res_list = res_list)
#toy_sample = mdtau.MC(50000)
#mdtau.set_params_values({'CSR': -0.5}, isfitresult = False)
#toy_sample2 = mdtau.MC(50000)
#
#
#mdtau.plots(toy_sample[:, 0], toy_sample2[:, 0], 0)
#mdtau.plots(toy_sample[:, 1], toy_sample2[:, 1], 1)
#mdtau.plots(toy_sample[:, 2], toy_sample2[:, 2], 2)
#mdtau.plots(toy_sample[:, 3], toy_sample2[:, 3], 3)
#mdtau.plots(toy_sample[:, 4], toy_sample2[:, 4], 4)
#mdtau.plots(toy_sample[:, 5], toy_sample2[:, 5], 5)
#mdtau.plots(toy_sample[:, 6], toy_sample2[:, 6], 6)
