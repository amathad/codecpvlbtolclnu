#import few packages
import math
import pandas as pd
import sys, os
import tensorflow as tf
from tensorflow.python.ops.array_ops import ones, zeros
from tensorflow.python.ops.gen_array_ops import ones_like, zeros_like
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # Do not use GPU for tensor flow
import numpy as np
import matplotlib.pyplot as plt
import uproot
import pprint
from root_pandas import read_root

#from corrected_Lc_decay_3res import LcTopKpi_Model
from Lc_DPD_Ab import LcTopKpi_Model as LcTopKpi_Model_OLD
from Lc_DPD_Ab import Minimize

#amplitf
home = os.getenv('HOME')
sys.path.append(home+"/Packages/AmpliTF/")
import amplitf.interface as atfi
import amplitf.kinematics as atfk
import amplitf.dynamics as atfd
from amplitf.phasespace.rectangular_phasespace import RectangularPhaseSpace
import amplitf.dalitz_decomposition as atfdd

#tfa2
sys.path.append(home+"/Packages/TFA2/")
import tfa.optimisation as tfo
import tfa.toymc as tft
import tfa.plotting as tfp

class Correction_to_Lc_decay:
    def __init__(self, MLb, MLc, Mp, Mk, Mpi, file, res, df_type):
        """initialise some variables"""
        #Masses of particles involved 
        self.MLb       = MLb
        self.MLc       = MLc
        self.Mp        = Mp
        self.Mk        = Mk
        self.Mpi       = Mpi

        self.file      = file
        self.res       = res
        self.df_type   = df_type

        #for integral and checking limits 
        phsp_limits  = [((self.Mk + self.Mpi)**2 , (self.MLc - self.Mp)**2 ) ]
        phsp_limits += [(-math.pi, math.pi)]
        phsp_limits += [(0. , math.pi)]
        phsp_limits += [(-math.pi, math.pi)]
        phsp_limits += [(0. , math.pi)]
        self.phase_space = RectangularPhaseSpace(ranges=phsp_limits)
    
    def prepare_data_full_file(self):
        if self.df_type == "recon":
        
            files_den    = ['Lb2Lcmunu_MagUp_2016_Combine.root']
            tree_den     = 'DecayTree'
            df     = read_root(files_den, key=tree_den)  

            #phsp CONDITION
            condph = (abs(df['isTruth']) == 1) & (abs(df['Lc_MC_MOTHER_ID']) == 5122) & \
                     (abs(df['pi_MC_MOTHER_ID']) == 4122) & (abs(df['K_MC_MOTHER_ID']) == 4122) & (abs(df['p_MC_MOTHER_ID']) == 4122)
            #k* CONDITION
            condk = (abs(df['isTruth']) == 1) & (abs(df['Lc_MC_MOTHER_ID']) == 5122) & \
                    (abs(df['pi_MC_MOTHER_ID']) == 313) & (abs(df['K_MC_MOTHER_ID']) == 313) & (abs(df['p_MC_MOTHER_ID']) == 4122)
            #Delta CONDITION
            condd = (abs(df['isTruth']) == 1) & (abs(df['Lc_MC_MOTHER_ID']) == 5122) & \
                    (abs(df['pi_MC_MOTHER_ID']) == 2224) & (abs(df['K_MC_MOTHER_ID']) == 4122) & (abs(df['p_MC_MOTHER_ID']) == 2224)
            #Lambda CONDITION
            condl = (abs(df['isTruth']) == 1) & (abs(df['Lc_MC_MOTHER_ID']) == 5122) & \
                    (abs(df['pi_MC_MOTHER_ID']) == 4122) & (abs(df['K_MC_MOTHER_ID']) == 3124) & (abs(df['p_MC_MOTHER_ID']) == 3124)
            condpass = (condph|condk|condd|condl)
            weights_zeros   = np.zeros_like(df['pi_TRUEP_X'])

        if self.df_type == "gen":
        
            files_den    = ['LcMuNu_gen_new.root']
            tree_den     = 'DecayTree'
            df     = read_root(files_den, key=tree_den)  

            #phsp CONDITION
            condph = (abs(df['nu_mu~_MC_MOTHER_ID']) == 5122) & (abs(df['muminus_MC_MOTHER_ID']) == 5122) & (abs(df['Lambda_cplus_MC_MOTHER_ID']) == 5122) & \
                     (abs(df['piplus_MC_MOTHER_ID']) == 4122) & (abs(df['Kminus_MC_MOTHER_ID']) == 4122) & (abs(df['pplus_MC_MOTHER_ID'] )== 4122)
            #k* CONDITION
            condk = (abs(df['nu_mu~_MC_MOTHER_ID']) == 5122) & (abs(df['muminus_MC_MOTHER_ID']) == 5122) & (abs(df['Lambda_cplus_MC_MOTHER_ID']) == 5122) & \
                    (abs(df['piplus_MC_MOTHER_ID']) == 313) & (abs(df['Kminus_MC_MOTHER_ID']) == 313) & (abs(df['pplus_MC_MOTHER_ID'] )== 4122)
            #Delta CONDITION
            condd = (abs(df['nu_mu~_MC_MOTHER_ID']) == 5122) & (abs(df['muminus_MC_MOTHER_ID']) == 5122) & (abs(df['Lambda_cplus_MC_MOTHER_ID']) == 5122) & \
                    (abs(df['piplus_MC_MOTHER_ID']) == 2224) & (abs(df['Kminus_MC_MOTHER_ID']) == 4122) & (abs(df['pplus_MC_MOTHER_ID']) == 2224) 
            #Lambda CONDITION
            condl = (abs(df['nu_mu~_MC_MOTHER_ID']) == 5122) & (abs(df['muminus_MC_MOTHER_ID']) == 5122) & (abs(df['Lambda_cplus_MC_MOTHER_ID']) == 5122) & \
                    (abs(df['piplus_MC_MOTHER_ID']) == 4122) & (abs(df['Kminus_MC_MOTHER_ID']) == 3124) & (abs(df['pplus_MC_MOTHER_ID']) == 3124) 
            condpass = (condph|condk|condd|condl)
            weights_zeros   = np.zeros_like( df['nu_mu~_MC_MOTHER_ID'])
            
        indices = df[condpass == True].index.tolist()
        indices_fails = df[condpass == False].index.tolist()
  
        return df, indices, weights_zeros, indices_fails

    def prepare_data_id(self):

        if self.df_type == "recon":

            p_name       = "p"      
            pi_name      = "pi" 
            k_name       = "K"
            Lc_name      = "Lc"
            Lb_name      = "Lb"
            files_den    = ['Lb2Lcmunu_MagUp_2016_Combine.root']
            tree_den     = 'DecayTree'
            colums_den   = ['Lb_True_Q2_mu' , 'Lb_True_Costhetal_mu', 'Event_LbProdcorr', 'Event_TrackCalibcorr', 'Event_PIDCalibEffWeight', 'Event_L0Muoncorr', 'Event_FFcorr', 'isFullsel']
            

            if self.res == "phsp":
                cut_den      = 'abs(isTruth)==1&&abs(Lc_MC_MOTHER_ID)==5122&&abs(pi_MC_MOTHER_ID)==4122&&abs(K_MC_MOTHER_ID)==4122&&abs(p_MC_MOTHER_ID)==4122'
            elif self.res == "Kstar_kpi(892)":
                cut_den      = 'abs(isTruth)==1&&abs(Lc_MC_MOTHER_ID)==5122&&abs(pi_MC_MOTHER_ID)==313&&abs(K_MC_MOTHER_ID)==313&&abs(p_MC_MOTHER_ID)==4122'
            elif self.res == "D(1232)":
                cut_den      = 'abs(isTruth)==1&&abs(Lc_MC_MOTHER_ID)==5122&&abs(pi_MC_MOTHER_ID)==2224&&abs(K_MC_MOTHER_ID)==4122&&abs(p_MC_MOTHER_ID)==2224'
            elif self.res == "L(1520)":
                cut_den      = 'abs(isTruth)==1&&abs(Lc_MC_MOTHER_ID)==5122&&abs(pi_MC_MOTHER_ID)==4122&&abs(K_MC_MOTHER_ID)==3124&&abs(p_MC_MOTHER_ID)==3124'
            else:
                cut_den = ''
        
        if self.df_type == "gen":
            p_name       = "pplus"      
            pi_name      = "piplus"  
            k_name       = "Kminus"
            Lc_name      = "Lambda_cplus"
            Lb_name      = "Lambda_b0"
            files_den    = ['LcMuNu_gen_new.root']
            tree_den     = 'DecayTree'
            colums_den   = ['Lb_True_Q2_mu' , 'Lb_True_Costhetal_mu', 'Event_LbProdcorr', 'Event_FFcorr']

            if self.res == "phsp":
                cut_den      = 'abs(nu_mu~_MC_MOTHER_ID)==5122&&abs(muminus_MC_MOTHER_ID)==5122&&abs(Lambda_cplus_MC_MOTHER_ID)==5122&&abs(piplus_MC_MOTHER_ID)==4122&&abs(Kminus_MC_MOTHER_ID)==4122&&abs(pplus_MC_MOTHER_ID)==4122'
            elif self.res == "Kstar_kpi(892)":
                cut_den      = 'abs(nu_mu~_MC_MOTHER_ID)==5122&&abs(muminus_MC_MOTHER_ID)==5122&&abs(Lambda_cplus_MC_MOTHER_ID)==5122&&abs(piplus_MC_MOTHER_ID)==313&&abs(Kminus_MC_MOTHER_ID)==313&&abs(pplus_MC_MOTHER_ID)==4122'
            elif self.res == "D(1232)":
                cut_den      = 'abs(nu_mu~_MC_MOTHER_ID)==5122&&abs(muminus_MC_MOTHER_ID)==5122&&abs(Lambda_cplus_MC_MOTHER_ID)==5122&&abs(piplus_MC_MOTHER_ID)==2224&&abs(Kminus_MC_MOTHER_ID)==4122&&abs(pplus_MC_MOTHER_ID)==2224'
            elif self.res == "L(1520)":
                cut_den      = 'abs(nu_mu~_MC_MOTHER_ID)==5122&&abs(muminus_MC_MOTHER_ID)==5122&&abs(Lambda_cplus_MC_MOTHER_ID)==5122&&abs(piplus_MC_MOTHER_ID)==4122&&abs(Kminus_MC_MOTHER_ID)==3124&&abs(pplus_MC_MOTHER_ID)==3124'
            else:
                cut_den = ''

        df_root_file     = read_root(files_den, key=tree_den, where=cut_den)   
        df_root_file_new = read_root(files_den, key=tree_den, where=cut_den, columns=colums_den) 
        
        particle_lab_info = {}
        
        #p+
        particle_lab_info['p_px_lab']    = tf.convert_to_tensor(df_root_file[p_name+"_TRUEP_X"].to_numpy())/ 1000
        particle_lab_info['p_py_lab']    = tf.convert_to_tensor(df_root_file[p_name+"_TRUEP_Y"].to_numpy())/ 1000
        particle_lab_info['p_pz_lab']    = tf.convert_to_tensor(df_root_file[p_name+"_TRUEP_Z"].to_numpy())/ 1000
        particle_lab_info['p_E_lab']     = tf.convert_to_tensor(df_root_file[p_name+"_TRUEP_E"].to_numpy())/ 1000
        #K-
        particle_lab_info['k_px_lab']    = tf.convert_to_tensor(df_root_file[k_name+"_TRUEP_X"].to_numpy())/ 1000
        particle_lab_info['k_py_lab']    = tf.convert_to_tensor(df_root_file[k_name+"_TRUEP_Y"].to_numpy())/ 1000
        particle_lab_info['k_pz_lab']    = tf.convert_to_tensor(df_root_file[k_name+"_TRUEP_Z"].to_numpy())/ 1000
        particle_lab_info['k_E_lab']     = tf.convert_to_tensor(df_root_file[k_name+"_TRUEP_E"].to_numpy())/ 1000
        #pi+
        particle_lab_info['pi_px_lab']   = tf.convert_to_tensor(df_root_file[pi_name+"_TRUEP_X"].to_numpy())/ 1000
        particle_lab_info['pi_py_lab']   = tf.convert_to_tensor(df_root_file[pi_name+"_TRUEP_Y"].to_numpy())/ 1000
        particle_lab_info['pi_pz_lab']   = tf.convert_to_tensor(df_root_file[pi_name+"_TRUEP_Z"].to_numpy())/ 1000
        particle_lab_info['pi_E_lab']    = tf.convert_to_tensor(df_root_file[pi_name+"_TRUEP_E"].to_numpy())/ 1000
        #Lb-0
        particle_lab_info['Lb_px_lab']   = tf.convert_to_tensor(df_root_file[Lb_name+"_TRUEP_X"].to_numpy())/ 1000
        particle_lab_info['Lb_py_lab']   = tf.convert_to_tensor(df_root_file[Lb_name+"_TRUEP_Y"].to_numpy())/ 1000
        particle_lab_info['Lb_pz_lab']   = tf.convert_to_tensor(df_root_file[Lb_name+"_TRUEP_Z"].to_numpy())/ 1000
        particle_lab_info['Lb_E_lab']    = tf.convert_to_tensor(df_root_file[Lb_name+"_TRUEP_E"].to_numpy())/ 1000
        #Lc+
        particle_lab_info['Lc_px_lab']   = tf.convert_to_tensor(df_root_file[Lc_name+"_TRUEP_X"].to_numpy())/ 1000
        particle_lab_info['Lc_py_lab']   = tf.convert_to_tensor(df_root_file[Lc_name+"_TRUEP_Y"].to_numpy())/ 1000
        particle_lab_info['Lc_pz_lab']   = tf.convert_to_tensor(df_root_file[Lc_name+"_TRUEP_Z"].to_numpy())/ 1000
        particle_lab_info['Lc_E_lab']    = tf.convert_to_tensor(df_root_file[Lc_name+"_TRUEP_E"].to_numpy())/ 1000
        
        return particle_lab_info, df_root_file_new

    ########### Make some common functions
    def MakeFourVector(self, pmag, costheta, phi, msq): 
        """Make 4-vec given magnitude, cos(theta), phi and mass"""
        sintheta = atfi.sqrt(1. - costheta**2) #theta 0 to pi => atfi.sin always pos
        px = pmag * sintheta * atfi.cos(phi)
        py = pmag * sintheta * atfi.sin(phi)
        pz = pmag * costheta
        E  = atfi.sqrt(msq + pmag**2)
        return atfk.lorentz_vector(atfk.vector(px, py, pz), E)

    def pvecMag(self, Msq, m1sq, m2sq): 
        """Momentum mag of (1 or 2) in M rest frame"""
        kallen = atfi.sqrt(Msq**2 + m1sq**2 + m2sq**2 - 2.*(Msq*m1sq + Msq*m2sq + m1sq*m2sq))
        const  = tf.cast(2. * atfi.sqrt(Msq), dtype = tf.float64)
        return  kallen / const 
    
    def HelAngles3Body(self, pa, pb, pc):
        """Get three-body helicity angles"""
        theta_r  = atfi.acos(-atfk.z_component(pc) / atfk.norm(atfk.spatial_components(pc)))
        phi_r    = atfi.atan2(-atfk.y_component(pc), -atfk.x_component(pc))
        pa_prime = atfk.rotate_lorentz_vector(pa, -phi_r, -theta_r, phi_r)
        pb_prime = atfk.rotate_lorentz_vector(pb, -phi_r, -theta_r, phi_r)
        pa_prime2= atfk.boost_to_rest(pa_prime, pa_prime+pb_prime)
        theta_a  = atfi.acos(atfk.z_component(pa_prime2) / atfk.norm(atfk.spatial_components(pa_prime2)))
        phi_a    = atfi.atan2(atfk.y_component(pa_prime2), atfk.x_component(pa_prime2))
        return (theta_r, phi_r, theta_a, phi_a)
    
    def RotateAndBoostToRest(self, gm_p4mom_p1, gm_p4mom_p2, gm_phi_m, gm_ctheta_m):
        """
        gm_p4mom_p1: particle p1 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
        gm_p4mom_p2: particle p2 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
        gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
        """
        #First:  Rotate particle p 3mom defined in grand mother gm helicity frame such that z now points along z of mother m in gm rest frame.
        gm_p4mom_p1_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p1, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
        gm_p4mom_p2_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p2, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
        #Second: Boost particle p 4mom to mother m i.e. p1p2 rest frame
        gm_p4mom_m             = gm_p4mom_p1_zmmom+gm_p4mom_p2_zmmom
        m_p4mom_p1             = atfk.boost_to_rest(gm_p4mom_p1_zmmom, gm_p4mom_m)
        m_p4mom_p2             = atfk.boost_to_rest(gm_p4mom_p2_zmmom, gm_p4mom_m)
        return m_p4mom_p1, m_p4mom_p2

    def InvRotateAndBoostFromRest(self, m_p4mom_p1, m_p4mom_p2, gm_phi_m, gm_ctheta_m, gm_p4mom_m):
        """
        Function that gives 4momenta of the particles in the grand mother helicity frame

        m_p4mom_p1: particle p1 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
        m_p4mom_p2: particle p2 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
        gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
        gm_4mom_m: m 4mom in gm's helicity frame
        --------------
        Checks done such as 
            - (lc_p4mom_p+lc_p4mom_k == lc_p4mom_r), 
            - Going in reverse i.e. boost lc_p4mom_p into R rest frame (using lc_p4mom_r) and rotating into R helicity frame gives back r_p4mom_p 
                - i.e. lc_p4mom_p_opp = atfk.rotate_lorentz_vector(atfk.boost_to_rest(lc_p4mom_p, lc_p4mom_r), -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)) where lc_p4mom_p_opp == r_p4mom_p
            - Rotate and boost, instead of boost and rotate should give the same answer, checked (Does not agree with TFA implementation though)
                -i.e. lc_p4mom_prot  = atfk.rotate_lorentz_vector(lc_p4mom_p, -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)
                -i.e. lc_p4mom_krot  = atfk.rotate_lorentz_vector(lc_p4mom_k, -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)
                -i.e. lc_p4mom_p2    = atfk.boost_to_rest(lc_p4mom_prot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_p2 == r_p4mom_p
                -i.e. lc_p4mom_k2    = atfk.boost_to_rest(lc_p4mom_krot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_k2 == r_p4mom_k
        """
        #First: Rotate particle p 3mom defined in mother m helicity frame such that z now points along z in grand mother gm helicity frame 
        #(i.e interpreted as gm mom in it's grand-grand-mother ggm rest frame)
        m_p4mom_p1_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p1, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
        m_p4mom_p2_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p2, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
        #Second: Boost particle p 4mom from mother m's rest frame to gm helicity frame. This is done using boost vector from gm_p4mom_m  [checked: E(m_p4mom_p1_zmmom + m_p4mom_p2_zmmom) == Mass_m]
        gm_p4mom_p1            = atfk.boost_from_rest(m_p4mom_p1_zgmmom, gm_p4mom_m)
        gm_p4mom_p2            = atfk.boost_from_rest(m_p4mom_p2_zgmmom, gm_p4mom_m)
        return gm_p4mom_p1, gm_p4mom_p2
    
    def InvRotateAndBoostFromRest_3particles(self, m_p4mom_p1, m_p4mom_p2, m_p4mom_p3, gm_phi_m, gm_ctheta_m, gm_p4mom_m):
        #First: Rotate particle p 3mom defined in mother m helicity frame such that z now points along z in grand mother gm helicity frame 
        #(i.e interpreted as gm mom in it's grand-grand-mother ggm rest frame)
        m_p4mom_p1_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p1, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
        m_p4mom_p2_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p2, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
        m_p4mom_p3_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p3, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
        #Second: Boost particle p 4mom from mother m's rest frame to gm helicity frame. This is done using boost vector from gm_p4mom_m  [checked: E(m_p4mom_p1_zmmom + m_p4mom_p2_zmmom) == Mass_m]
        gm_p4mom_p1            = atfk.boost_from_rest(m_p4mom_p1_zgmmom, gm_p4mom_m)
        gm_p4mom_p2            = atfk.boost_from_rest(m_p4mom_p2_zgmmom, gm_p4mom_m)
        gm_p4mom_p3            = atfk.boost_from_rest(m_p4mom_p3_zgmmom, gm_p4mom_m)
        return gm_p4mom_p1, gm_p4mom_p2, gm_p4mom_p3

    def RotateAndBoostToRest_3particles(self, gm_p4mom_p1, gm_p4mom_p2, gm_p4mom_p3, gm_phi_m, gm_ctheta_m):
        """
        gm_p4mom_p1: particle p1 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
        gm_p4mom_p2: particle p2 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
        gm_p4mom_p3: particle p3 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
        gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
        """
        #First:  Rotate particle p 3mom defined in grand mother gm helicity frame such that z now points along z of mother m in gm rest frame.
        gm_p4mom_p1_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p1, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
        gm_p4mom_p2_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p2, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
        gm_p4mom_p3_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p3, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)

        #Second: Boost particle p 4mom to mother m i.e. p1p2p3 rest frame
        gm_p4mom_m             = gm_p4mom_p1_zmmom + gm_p4mom_p2_zmmom + gm_p4mom_p3_zmmom 
        m_p4mom_p1             = atfk.boost_to_rest(gm_p4mom_p1_zmmom, gm_p4mom_m)
        m_p4mom_p2             = atfk.boost_to_rest(gm_p4mom_p2_zmmom, gm_p4mom_m)
        m_p4mom_p3             = atfk.boost_to_rest(gm_p4mom_p3_zmmom, gm_p4mom_m)
        return m_p4mom_p1, m_p4mom_p2, m_p4mom_p3 
    
    def RotateAndBoostToRest_Lb(self, p1_p4mom_lab, lamb_p4mom_lab, lamb_phi_lab, lamb_theta_lab):
        """
        p1_p4mom_lab: particle p1 4mom in lab helicity frame (i.e z points along Lb momentum in lab frame).
        lamb_phi_lab, lamb_theta_lab: Angles phi and ctheta of Lb in lab frame (i.e z points along Lb momentum in lab frame).
        """
        #First:  Rotate particle p 3mom defined in lab frame such that z now points along z of Lb in Lab frame.
        #p1_p4mom_lab_zmmom      = atfk.rotate_lorentz_vector(p1_p4mom_lab, -lamb_phi_lab, -lamb_theta_lab, lamb_phi_lab)

        #Second: Boost particle p 4mom to mother Lb
        p1_p4mom_Lb             = atfk.boost_to_rest(p1_p4mom_lab, lamb_p4mom_lab)
  
        return p1_p4mom_Lb

    def get_phase_variables(self, p4_p_spacefixed, p4_k_spacefixed, p4_pi_spacefixed):
        #get square dalitz plot variables
        m2_pk  = atfk.mass_squared(p4_p_spacefixed + p4_k_spacefixed )
        m2_kpi = atfk.mass_squared(p4_k_spacefixed + p4_pi_spacefixed)

        #get 3 vectors
        p3_p_spacefixed  = atfk.spatial_components(p4_p_spacefixed )
        p3_k_spacefixed  = atfk.spatial_components(p4_k_spacefixed )
        p3_pi_spacefixed = atfk.spatial_components(p4_pi_spacefixed)

        #get polar and azimuthal angles opposite to proton direction i.e. z direction
        th_p, ph_p = atfk.spherical_angles(-p3_p_spacefixed)
        cth_p = atfi.cos(th_p)

        #get the angle between the kpi plane and the plane formed by the quantisation axis and proton momenta
        #unit normal vector to kpi plane
        norm_kpi      = atfk.unit_vector(atfk.cross_product(p3_k_spacefixed, p3_pi_spacefixed))
        #unit normal vector to Lc quantisation axis (+z axis) and proton momenta in Lc rest frame (-z axis)
        lc_quant_axis = atfk.vector(atfi.zeros(cth_p), atfi.zeros(cth_p) , atfi.ones(cth_p))
        norm_quantp   = atfk.unit_vector(atfk.cross_product(p3_p_spacefixed, lc_quant_axis))

        #cosine of angle b/w the two normal unit vectors
        x_comp  = atfk.scalar_product(norm_kpi, norm_quantp)
        #sine of angle b/w the two normal unit vectors
        y_comp  = atfk.scalar_product(atfk.cross_product(norm_kpi, norm_quantp), atfk.unit_vector(p3_p_spacefixed))
        #get the angle b/w the decay plane
        ph_kpi = atfi.atan2(-y_comp, -x_comp)

        return (m2_pk, m2_kpi, cth_p, ph_p, ph_kpi)
    
    ########### Make functions to determine the 5 phase space variables for the Lc -> pKpi decay 
    def p4mom(self):
        """p, K. pi 4-momenta in lab, lb and lc rest frames"""
        
        lab_info = self.prepare_data_id()[0]

        #proton 3-mom and energy in lab 
        p_px_lab    = lab_info['p_px_lab']
        p_py_lab    = lab_info['p_py_lab']
        p_pz_lab    = lab_info['p_pz_lab']
        p_E_lab     = lab_info['p_E_lab']
        #kaon 3-mom and energy in lab 
        k_px_lab    = lab_info['k_px_lab']
        k_py_lab    = lab_info['k_py_lab']
        k_pz_lab    = lab_info['k_pz_lab']
        k_E_lab     = lab_info['k_E_lab']
        #pion 3-mom and energy in lab 
        pi_px_lab   = lab_info['pi_px_lab']
        pi_py_lab   = lab_info['pi_py_lab']
        pi_pz_lab   = lab_info['pi_pz_lab']
        pi_E_lab    = lab_info['pi_E_lab']
        #Lambda_c 3-mom and energy in lab 
        Lc_px_lab   = lab_info['Lc_px_lab']
        Lc_py_lab   = lab_info['Lc_py_lab']
        Lc_pz_lab   = lab_info['Lc_pz_lab']
        Lc_E_lab    = lab_info['Lc_E_lab']
        #Lambda_b 3-mom and energy in lab 
        Lb_px_lab   = lab_info['Lb_px_lab']
        Lb_py_lab   = lab_info['Lb_py_lab']
        Lb_pz_lab   = lab_info['Lb_pz_lab']    
        Lb_E_lab    = lab_info['Lb_E_lab']    

        # 4 momenta - LAB FRAME
        p_p4mom_lab     = atfk.lorentz_vector(atfk.vector(p_px_lab, p_py_lab, p_pz_lab   ), p_E_lab)
        k_p4mom_lab     = atfk.lorentz_vector(atfk.vector(k_px_lab, k_py_lab, k_pz_lab   ), k_E_lab)
        pi_p4mom_lab    = atfk.lorentz_vector(atfk.vector(pi_px_lab, pi_py_lab, pi_pz_lab), pi_E_lab)
        Lc_p4mom_lab    = atfk.lorentz_vector(atfk.vector(Lc_px_lab, Lc_py_lab, Lc_pz_lab), Lc_E_lab)
        Lb_p4mom_lab    = atfk.lorentz_vector(atfk.vector(Lb_px_lab, Lb_py_lab, Lb_pz_lab), Lb_E_lab)
        #Lb momenta spherical angles in lab frame 
        Lb_theta_lab    =  atfk.spherical_angles(Lb_p4mom_lab)[0]
        Lb_phi_lab      =  atfk.spherical_angles(Lb_p4mom_lab)[1]
        Lb_ctheta_lab   =  atfi.cos(Lb_theta_lab)
        
        # 4 momenta - Lb FRAME
        p_p4mom_Lb      = self.RotateAndBoostToRest_Lb(p_p4mom_lab , Lb_p4mom_lab, Lb_phi_lab, Lb_theta_lab)
        k_p4mom_Lb      = self.RotateAndBoostToRest_Lb(k_p4mom_lab , Lb_p4mom_lab, Lb_phi_lab, Lb_theta_lab)
        pi_p4mom_Lb     = self.RotateAndBoostToRest_Lb(pi_p4mom_lab, Lb_p4mom_lab, Lb_phi_lab, Lb_theta_lab)
        Lc_p4mom_Lb     = self.RotateAndBoostToRest_Lb(Lc_p4mom_lab, Lb_p4mom_lab, Lb_phi_lab, Lb_theta_lab)

        #Lc momenta spherical angles in Lb frame 
        Lc_theta_Lb     =  atfk.spherical_angles(Lc_p4mom_Lb)[0]
        Lc_phi_Lb       =  atfk.spherical_angles(Lc_p4mom_Lb)[1]
        Lc_ctheta_Lb    =  atfi.cos(Lc_theta_Lb)

        # 4 momenta - Lc FRAME
        p_p4mom_Lc      = self.RotateAndBoostToRest_3particles( p_p4mom_Lb , k_p4mom_Lb, pi_p4mom_Lb, Lc_phi_Lb, Lc_ctheta_Lb )[0]
        k_p4mom_Lc      = self.RotateAndBoostToRest_3particles( p_p4mom_Lb , k_p4mom_Lb, pi_p4mom_Lb, Lc_phi_Lb, Lc_ctheta_Lb )[1]
        pi_p4mom_Lc     = self.RotateAndBoostToRest_3particles( p_p4mom_Lb , k_p4mom_Lb, pi_p4mom_Lb, Lc_phi_Lb, Lc_ctheta_Lb )[2]

        #####Checks
        #p_p4mom_Lc_check = self.RotateAndBoostToRest_Lc(p_p4mom_Lb , Lc_p4mom_Lb, Lc_phi_Lb, Lc_theta_Lb)
        #k_p4mom_Lc       = self.RotateAndBoostToRest_Lc(k_p4mom_Lb , Lc_p4mom_Lb, Lc_phi_Lb, Lc_theta_Lb)
        #pi_p4mom_Lc      = self.RotateAndBoostToRest_Lc(pi_p4mom_Lb, Lc_p4mom_Lb, Lc_phi_Lb, Lc_theta_Lb) 

        #print(p_p4mom_Lc)
        #print(p_p4mom_Lc_check)

        ''' Checks done:
        1. Lb_p4mom_Lb   approx = 0.
        2. Lc_p4mom_Lb     =  p_p4mom_Lb + k_p4mom_Lb + pi_p4mom_Lb 
        -small differeneces, approximately same 
        3. invrotboost = self.InvRotateAndBoostFromRest_3particles(p_p4mom_Lb, pi_p4mom_Lb, k_p4mom_Lb, Lc_phi_Lb, Lc_ctheta_Lb, Lc_p4mom_Lb)
        print(invrotboost_lb[0])
        print(p_p4mom_Lb)
        print(invrotboost_lb[1])
        print(pi_p4mom_Lb)
        print(invrotboost_lb[2])
        print(k_p4mom_Lb)
        returns p_p4mom_Lb, pi_p4mom_Lb, pi_p4mom_Lb
        -not exact but roughly equal 
        '''
        p4mom = {}
        p4mom['p_p4mom_lab']     = p_p4mom_lab
        p4mom['k_p4mom_lab']     = k_p4mom_lab
        p4mom['pi_p4mom_lab']    = pi_p4mom_lab
        p4mom['p_p4mom_Lb']      = p_p4mom_Lb
        p4mom['k_p4mom_Lb']      = k_p4mom_Lb
        p4mom['pi_p4mom_Lb']     = pi_p4mom_Lb

        p4mom['p_p4mom_Lc']      = p_p4mom_Lc
        p4mom['k_p4mom_Lc']      = k_p4mom_Lc
        p4mom['pi_p4mom_Lc']     = pi_p4mom_Lc

        p4mom['p_p4mom_Lcrf']    = atfk.boost_to_rest(p_p4mom_Lb , Lc_p4mom_Lb) 
        p4mom['k_p4mom_Lcrf']    = atfk.boost_to_rest(k_p4mom_Lb , Lc_p4mom_Lb) 
        p4mom['pi_p4mom_Lcrf']   = atfk.boost_to_rest(pi_p4mom_Lb, Lc_p4mom_Lb)

        return p4mom
    
    def phsp_vars(self):
        particle_4mom = self.p4mom()
        """5 phase space variables for Lc -> pKpi decay from p, pi, k 4-momenta"""        
        p_p4mom_Lc        = particle_4mom['p_p4mom_Lc']
        k_p4mom_Lc        = particle_4mom['k_p4mom_Lc']
        pi_p4mom_Lc       = particle_4mom['pi_p4mom_Lc']  
 
        m2_kpi            = atfk.mass_squared(pi_p4mom_Lc+k_p4mom_Lc)
        m2_ppi            = atfk.mass_squared(p_p4mom_Lc+pi_p4mom_Lc)
        m2_pK             = atfk.mass_squared(p_p4mom_Lc+k_p4mom_Lc)

        kstar_theta_Lc    =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[0]
        kstar_phi_Lc      =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[1]
        k_theta_kstar     =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[2]
        k_phi_kstar       =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[3]

        # Angle for Delta and Lambda * 
        #Delta resonance angles 
        delta_theta_lc    =  self.HelAngles3Body(p_p4mom_Lc, pi_p4mom_Lc, k_p4mom_Lc)[0]
        delta_phi_lc      =  self.HelAngles3Body(p_p4mom_Lc, pi_p4mom_Lc, k_p4mom_Lc)[1]
        p_theta_delta     =  self.HelAngles3Body(p_p4mom_Lc, pi_p4mom_Lc, k_p4mom_Lc)[2]
        p_phi_delta       =  self.HelAngles3Body(p_p4mom_Lc, pi_p4mom_Lc, k_p4mom_Lc)[3]
        #pi_theta_delta    =  self.HelAngles3Body(pi_p4mom_Lc, p_p4mom_Lc, k_p4mom_Lc)[2]
        #pi_phi_delta      =  self.HelAngles3Body(pi_p4mom_Lc, p_p4mom_Lc, k_p4mom_Lc)[3]

        #Lambda* resonance angles 
        lambda_theta_lc  =  self.HelAngles3Body(p_p4mom_Lc, k_p4mom_Lc, pi_p4mom_Lc)[0]
        lambda_phi_lc    =  self.HelAngles3Body(p_p4mom_Lc, k_p4mom_Lc, pi_p4mom_Lc)[1]
        p_theta_lambda   =  self.HelAngles3Body(p_p4mom_Lc, k_p4mom_Lc, pi_p4mom_Lc)[2]
        p_phi_lambda     =  self.HelAngles3Body(p_p4mom_Lc, k_p4mom_Lc, pi_p4mom_Lc)[3]

        '''Checks:
        1.  Mass:
            m2_pK2        = (p_p4mom_Lc[:,3] + k_p4mom_Lc[:,3])**2 - ( (p_p4mom_Lc[:,0]+k_p4mom_Lc[:,0])**2 + (p_p4mom_Lc[:,1]+k_p4mom_Lc[:,1])**2 + (p_p4mom_Lc[:,2]+k_p4mom_Lc[:,2])**2 )
            m2_kpi        =  self.Mk**2 + self.Mpi**2 + 2 * ( pi_p4mom_lab[:,3]*k_p4mom_lab[:,3] -  (pi_p4mom_lab[:,0]*k_p4mom_lab[:,0] +  pi_p4mom_lab[:,1]*k_p4mom_lab[:,1] + pi_p4mom_lab[:,2]*k_p4mom_lab[:,2] ) )
            m2_ppi        =  self.Mp**2 + self.Mpi**2 + 2 * ( pi_p4mom_lab[:,3]*p_p4mom_lab[:,3] -  (pi_p4mom_lab[:,0]*p_p4mom_lab[:,0] +  pi_p4mom_lab[:,1]*p_p4mom_lab[:,1] + pi_p4mom_lab[:,2]*p_p4mom_lab[:,2] ) )
            m2_pK         =  self.Mk**2 + self.Mp**2 + 2 * ( p_p4mom_lab[:,3]*k_p4mom_lab[:,3] -  (p_p4mom_lab[:,0]*k_p4mom_lab[:,0] +  p_p4mom_lab[:,1]*k_p4mom_lab[:,1] + p_p4mom_lab[:,2]*k_p4mom_lab[:,2] ) )
        2. Angles: 
            k_star_p4mom_Lc   =  k_p4mom_Lc + pi_p4mom_Lc
            kstar_theta_Lc    =  atfk.spherical_angles(k_star_p4mom_Lc)[0]
            kstar_phi_Lc      =  atfk.spherical_angles(k_star_p4mom_Lc)[1]
            kstar_ctheta_Lc   =  atfi.cos(kstar_theta_Lc)

            k_p4mom_kstar     =  self.RotateAndBoostToRest(k_p4mom_Lc, pi_p4mom_Lc, kstar_phi_Lc, kstar_ctheta_Lc)[0]
            #k_p4mom_kstar    =  self.RotateAndBoostToRest_Lc(k_p4mom_Lc, k_star_p4mom_Lc, kstar_phi_Lc, kstar_theta_Lc)
            k_theta_kstar     =  atfk.spherical_angles(k_p4mom_kstar)[0]
            k_phi_kstar       =  atfk.spherical_angles(k_p4mom_kstar)[1]

            kstar_theta_Lc2    =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[0]
            kstar_phi_Lc2      =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[1]
            k_theta_kstar2     =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[2]
            k_phi_kstar2       =  self.HelAngles3Body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[3]

            print(kstar_theta_Lc)
            print(kstar_theta_Lc2)
            print(kstar_phi_Lc)
            print(kstar_phi_Lc2)
            print(k_theta_kstar)
            print(k_theta_kstar2)
            print(k_phi_kstar)
            print(k_phi_kstar2)

            kstar_theta_Lc2    =  atfk.helicity_angles_3body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[0]
            kstar_phi_Lc2      =  atfk.helicity_angles_3body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[1]
            k_theta_kstar2     =  atfk.helicity_angles_3body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[2]
            k_phi_kstar2       =  atfk.helicity_angles_3body( k_p4mom_Lc, pi_p4mom_Lc, p_p4mom_Lc)[3]
        '''

        Vars = {}
        #kstar resonance variables 
        Vars['m2_kpi']                = m2_kpi
        Vars['kstar_phi_lc']          = kstar_phi_Lc
        Vars['kstar_theta_lc']        = kstar_theta_Lc 
        Vars['k_phi_kstar']           = k_phi_kstar 
        Vars['k_theta_kstar']         = k_theta_kstar
        #delta and lambda * resonance variables 
        Vars['m2_ppi']                = m2_ppi
        Vars['m2_pk']                 = m2_pK
        #Delta resonance variables
        Vars['delta_theta_lc']        = delta_theta_lc
        Vars['delta_phi_lc']          = delta_phi_lc
        Vars['p_theta_delta']         = p_theta_delta
        Vars['p_phi_delta']           = p_phi_delta 

        #Lambda* resonance variables
        Vars['lambda_theta_lc']       = lambda_theta_lc
        Vars['lambda_phi_lc']         = lambda_phi_lc
        Vars['p_theta_lambda']        = p_theta_lambda
        Vars['p_phi_lambda']          = p_phi_lambda

        m2_pknew, m2_kpinew, cth_p, ph_p, ph_kpi = self.get_phase_variables(particle_4mom['p_p4mom_Lcrf'], particle_4mom['k_p4mom_Lcrf'], particle_4mom['pi_p4mom_Lcrf'])
        Vars['m2_pknew']  = m2_pknew
        Vars['m2_kpinew'] = m2_kpinew
        Vars['cth_p']     = cth_p
        Vars['ph_p']      = ph_p
        Vars['ph_kpi']    = ph_kpi

        return Vars
    
    def concat_phsp_vars(self):
        """Make an array of the 5 phase space variables calculated from the 3-momenta"""
        Variables = self.phsp_vars()
        m2_pk     = tf.reshape(Variables['m2_pknew']  , [len(Variables['m2_pknew']  ), 1])
        m2_kpi    = tf.reshape(Variables['m2_kpinew'] , [len(Variables['m2_kpinew'] ), 1])
        cth_p     = tf.reshape(Variables['cth_p']     , [len(Variables['cth_p']     ), 1])
        ph_p      = tf.reshape(Variables['ph_p']      , [len(Variables['ph_p']      ), 1])
        ph_kpi    = tf.reshape(Variables['ph_kpi']    , [len(Variables['ph_kpi']    ), 1])
        t1              = tf.concat([m2_pk, m2_kpi], axis = 1)
        t2              = tf.concat([t1, cth_p], axis = 1)
        t3              = tf.concat([t2, ph_p ], axis = 1)
        t4              = tf.concat([t3, ph_kpi], axis = 1)
        return t4
    
    def remove_nans(self, sample):
        c1   = tf.logical_and(tf.equal(tf.math.is_nan(sample[:,0]), False), tf.equal(tf.math.is_nan(sample[:,1]), False) )
        c2   = tf.logical_and(c1, tf.equal(tf.math.is_nan(sample[:,2]), False))
        c3   = tf.logical_and(c2, tf.equal(tf.math.is_nan(sample[:,3]), False))
        c4   = tf.logical_and(c3, tf.equal(tf.math.is_nan(sample[:,4]), False))
        keys = tf.where(tf.equal(c4, True))

        m2kpi               = tf.gather(sample[:,0], keys[:,-1])
        kstar_phi_Lc        = tf.gather(sample[:,1], keys[:,-1])
        kstar_theta_Lc      = tf.gather(sample[:,2], keys[:,-1])
        k_phi_kstar         = tf.gather(sample[:,3], keys[:,-1])
        k_theta_kstar       = tf.gather(sample[:,4], keys[:,-1])

        m2kpi_1             = tf.reshape(m2kpi, [len(m2kpi), 1])
        kstar_phi_lc_1      = tf.reshape(kstar_phi_Lc, [len(kstar_phi_Lc), 1])
        kstar_theta_lc_1    = tf.reshape(kstar_theta_Lc, [len(kstar_theta_Lc), 1])
        k_phi_kstar_1       = tf.reshape(k_phi_kstar, [len(k_phi_kstar), 1])
        k_theta_kstar_1     = tf.reshape(k_theta_kstar, [len(k_theta_kstar), 1])

        t1                  = tf.concat([m2kpi_1  , kstar_phi_lc_1 ], axis = 1)
        t2                  = tf.concat([t1, kstar_theta_lc_1 ], axis = 1)
        t3                  = tf.concat([t2, k_phi_kstar_1 ], axis = 1)
        t4                  = tf.concat([t3, k_theta_kstar_1 ], axis = 1)
        return t4
    
    
    def make_root_file(self, weights_Lc_corr):
        df_root_file    = self.prepare_data_id()[1]
        df_w            = pd.DataFrame(data = weights_Lc_corr, columns = ['Event_Lc_corr'])
        df_new          = pd.concat([df_root_file, df_w], axis = 1)
        return print(df_new)
        #return df_new.to_root('LcMuNu_gen_Lc_corr_LAMBDA.root', key='Decaytree')
    
    def make_root_file_recon(self, keys, weights_Lc_corr):
        #rows                = tf.reshape(keys, shape = (1, len(weights_Lc_corr))).numpy()
        rows = keys[:, -1].numpy()
        print(rows)
        df_root_file        = self.prepare_data_id()[1]
        df_root_file_nonans = df_root_file.iloc[rows, :]
        df_w                = pd.DataFrame(data = weights_Lc_corr, index = rows, columns = ['Event_Lc_corr'])
        df_new              = pd.concat([df_root_file_nonans, df_w], axis = 1)
        return print(df_new)
        #return df_new.to_root('LcMuNu_magup_Lc_corr.root', key='Decaytree')

    def norm(self, m):
        sum_weights = float( len(m) )
        numerators  = np.ones_like( m )
        return( numerators / sum_weights )
    
    def plot_weights(self, data, weights, data_new, i):
        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure
        label  = [r"$m^2(pK)$" , r"$m^2(K\pi)$" , r"$\cos(\theta_p)$" , r"$\phi_{p}$" ,r"$\phi_{K\pi}$", r"$m^2(p\pi)$" , r"$q^2$", r"$\cos(\theta_\mu)$", "Corr Mass"]      
        units  = [r"$GeV^2$", r"$GeV^2$", "", "rad", "rad", r"$GeV^2$", r"$GeV^2$", "", r"MeV"]   
        
        tfp.plot_distr1d(
            data,
            bins=100,
            range=(np.min(data_new), np.max(data_new)),
            ax=ax,
            label = label[i],
            units = units[i],
            weights = weights / np.sum(weights) ,
            color = 'r',
            ) 
        
        tfp.plot_distr1d(
            data,
            bins=100,
            range=(np.min(data_new), np.max(data_new)),
            ax=ax,
            label = label[i],
            units = units[i],
            weights = self.norm(data),
            color = 'b',
            ) 
        
        tfp.plot_distr1d(
            data_new,
            bins=100,
            range=(np.min(data_new), np.max(data_new)),
            ax=ax,
            label = label[i],
            units = units[i],
            weights = self.norm(data_new),
            color = 'k',
            ) 

        plt.legend(['MC OLD with weights', 'MC OLD w/o weights', 'NEW model', ], fontsize = 'x-small')
        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
        return plt.show()  
    
    def plot_weights_nonew(self, data, weights_old, weights_new, i):
        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure
        label  = [r"$m^2(pK)$" , r"$m^2(K\pi)$" , r"$\cos(\theta_p)$" , r"$\phi_{p}$" ,r"$\phi_{K\pi}$", r"$m^2(p\pi)$" , r"$q^2$", r"$\cos(\theta_\mu)$", r"$m_{corr}$"]      
        units  = [r"$GeV^2$", r"$GeV^2$", "rad", "", "rad", r"$GeV^2$", r"$GeV^2$", "", r"MeV"]  
        
        tfp.plot_distr1d(
            data,
            bins=100,
            range=(np.min(data), np.max(data)),
            ax=ax,
            label = label[i],
            units = units[i],
            weights = weights_new / np.sum(weights_new) ,
            color = 'r',
            ) 
        
        tfp.plot_distr1d(
            data,
            bins=100,
            range=(np.min(data), np.max(data)),
            ax=ax,
            label = label[i],
            units = units[i],
            weights = weights_old / np.sum(weights_old) ,
            color = 'b',
            ) 

        plt.legend(['MC OLD with weights', 'MC OLD w/o weights' ], fontsize = 'x-small')
        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
        return plt.show()  
    
    def plot_fits(self, data_old_mc, data_gen, i):
        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure
        label  = [r"$m^2(pK)$" , r"$m^2(K\pi)$" , r"$\cos(\theta_p)$" , r"$\phi_{p}$" ,r"$\phi_{K\pi}$", r"$m^2(p\pi)$" , r"$q^2$", r"$\cos(\theta_\mu)$", r"$m_{corr}$"]      
        units  = [r"$GeV^2$", r"$GeV^2$", "rad", "", "rad", r"$GeV^2$", r"$GeV^2$", "", r"MeV"]  
        
        tfp.plot_distr1d(
            data_old_mc,
            bins=100,
            range=(np.min(data_old_mc), np.max(data_old_mc)),
            ax=ax,
            label = label[i],
            units = units[i],
            weights = self.norm(data_old_mc) ,
            color = 'r',
            ) 
        
        tfp.plot_distr1d(
            data_gen,
            bins=100,
            range=(np.min(data_old_mc), np.max(data_old_mc)),
            ax=ax,
            label = label[i],
            units = units[i],
            weights = self.norm(data_gen) ,
            color = 'b',
            ) 

        plt.legend(['Fit to MC OLD', 'MC OLD' ], fontsize = 'x-small')
        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
        return plt.show()  
    
    def check_phsp(self, x):
        x = x.numpy()
        if np.min(x[:,0]) < (self.Mk + self.Mpi)**2 or np.max(x[:,0]) >  (self.MLc - self.Mp)**2:
            min = tf.ones_like(x[:, 0]) * (self.Mk + self.Mpi)**2
            max = tf.ones_like(x[:, 0]) * (self.MLc - self.Mp)**2
            index = tf.where(tf.logical_or(tf.less(x[:,0], min), tf.greater(x[:,0], max)))
            print(index)
            return index
            #raise Exception('Mass outside Phsp limits. Please check!')
        elif np.min(x[:,1]) < -np.pi or np.max(x[:,1]) > np.pi:
            raise Exception('Phi_K* outside Phsp limits. Please check!')
        elif np.min(x[:,2]) < 0. or np.max(x[:,2]) >  np.pi:
            raise Exception('Theta_k* outside Phsp limits. Please check!')
        elif np.min(x[:,3]) < -np.pi or np.max(x[:,3]) > np.pi:
            raise Exception('Phi_K outside Phsp limits. Please check!')
        elif np.min(x[:,4]) < 0. or np.max(x[:,4]) > np.pi:
            raise Exception('Theta_K outside Phsp limits. Please check!')

    def make_root_file_full(self, weights_Lc_corr, name):
        df_root_file    = self.prepare_data_full_file()[0]
        df_w            = pd.DataFrame(data = weights_Lc_corr, columns = ['Event_Lc_corr'])
        df_new          = pd.concat([df_root_file, df_w], axis = 1)
        print(df_new)
        return df_new.to_root(name, key='Decaytree')    
    
    def weights_full(self, new_model, old_model_phsp, old_model_k, old_model_D, old_model_L, x, k_frac, D_frac, L_frac, phsp_frac):
        df_root_file , condpass, weights_zeros, condfail    = self.prepare_data_full_file()
        old_pdf      = D_frac * old_model_D.get_normalised_model(x) + k_frac * old_model_k.get_normalised_model(x) + L_frac * old_model_L.get_normalised_model(x) + phsp_frac * old_model_phsp.get_normalised_model(x)
        new_pdf      = new_model.get_normalised_model(x) 
        print(condpass)

        out_k_old     = tf.where(tf.equal(old_model_k.phase_space.inside(x), False))
        out_D_old     = tf.where(tf.equal(old_model_D.phase_space.inside(x), False))
        out_L_old     = tf.where(tf.equal(old_model_L.phase_space.inside(x), False))
        out_phsp_old  = tf.where(tf.equal(old_model_phsp.phase_space.inside(x), False))
        out_new       = tf.where(tf.equal(new_model.phase_space.inside(x), False)) 

        update_K      = tf.zeros_like(out_k_old[:, -1],dtype = tf.float64 )
        update_D      = tf.zeros_like(out_D_old[:, -1],dtype = tf.float64 )
        update_L      = tf.zeros_like(out_L_old[:, -1],dtype = tf.float64 )
        update_phsp   = tf.zeros_like(out_phsp_old[:, -1],dtype = tf.float64 )

        weights_all      = new_pdf / old_pdf
        print(weights_all)
        weights1         = tf.tensor_scatter_nd_update(weights_all, out_k_old, update_K)
        weights2         = tf.tensor_scatter_nd_update(weights1, out_D_old, update_D)
        weights3         = tf.tensor_scatter_nd_update(weights2, out_L_old, update_L)
        weights_nonzero  = tf.tensor_scatter_nd_update(weights3, out_phsp_old, update_phsp)
        #weights_nonzero  = list(weights_nonzero)
        #print(weights_nonzero)
        #weights = np.insert(weights_zeros, condpass, weights_nonzero)
        #weights_zeros[condpass] = weights_nonzero
        #print(len(weights_zeros))
        #print(weights_zeros)
        zero = tf.zeros_like(weights_nonzero, dtype = tf.float64)
        weights = weights_nonzero.numpy()
        weights[condfail] = 0.
        print("WHERE WEIGHTS ARE ZERO")
        print(tf.where(tf.equal(tf.convert_to_tensor(weights_zeros), zero)))

        #return weights_zeros
        return weights
    

def main():
    
    MLb = 5.6195e-3
    MLc = 2.28646
    Mp  = 0.938272
    Mk  = 0.493677
    Mpi = 0.13957
    
    #gen new root file for the generated sample
    res_list_k       = ["Kstar_kpi(892)"]
    res_list_phsp    = ["phsp"]
    res_list_D       = ["D(1232)"]
    res_list_L       = ["L(1520)"]
    
    #OLD model - DPD
    old_model_k     = LcTopKpi_Model_OLD(MLc, Mp, Mk, Mpi, res_list = ["Kstar_kpi(892)"], float_params = [], model_type = 'OLD')
    old_model_phsp  = LcTopKpi_Model_OLD(MLc, Mp, Mk, Mpi, res_list = ["phsp"]          , float_params = [], model_type = 'OLD')
    old_model_D     = LcTopKpi_Model_OLD(MLc, Mp, Mk, Mpi, res_list = ["D(1232)"]       , float_params = [], model_type = 'OLD')
    old_model_L     = LcTopKpi_Model_OLD(MLc, Mp, Mk, Mpi, res_list = ["L(1520)"]       , float_params = [], model_type = 'OLD')
    '''
    # NEW model - DPD
    res_list_new      = ["Kstar_kpi(892)","Kstar_kpi0(700)", "Kstar_kpi0(1430)", "D(1232)", "D(1600)", "D(1700)", "L(1405)", "L(1520)", "L(1600)", "L(1670)", "L(1690)", "L(2000)"]
    float_params_new  = []
    new_model         = LcTopKpi_Model_OLD(MLc, Mp, Mk, Mpi, res_list_new, float_params_new, model_type = 'NEW')
    new_data          = new_model.MC(100000)
    new_data          = new_model.phase_space.filter(new_data)
    
    #recon+strip file 
    file_recon            = uproot.open("Lb2Lcmunu_MagUp_2016_Combine.root:DecayTree;1") 
    
    #K* MC data:
    cn_recon_k            = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file_recon, res_list_k[0], "recon")
    sample_r_wnans_k      = cn_recon_k.concat_phsp_vars()
    sample_r_k            = cn_recon_k.remove_nans(sample_r_wnans_k)
    print(sample_r_k)
    
    #Delta MC data:
    cn_recon_D            = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file_recon, res_list_D[0], "recon")
    sample_r_wnans_D      = cn_recon_D.concat_phsp_vars()
    sample_r_D            = cn_recon_D.remove_nans(sample_r_wnans_D)
    print(sample_r_D)
    
    #Lambda* MC data:
    cn_recon_L            = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file_recon, res_list_L[0], "recon")
    sample_r_wnans_L      = cn_recon_L.concat_phsp_vars()
    sample_r_L            = cn_recon_L.remove_nans(sample_r_wnans_L)
    print(sample_r_L)
    
    cn_recon_phsp         = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file_recon, res_list_phsp[0], "recon")
    sample_r_wnans_phsp   = cn_recon_phsp.concat_phsp_vars()
    sample_r_phsp         = cn_recon_phsp.remove_nans(sample_r_wnans_phsp)
    print(sample_r_phsp)
    
    f_phsp_r = len(sample_r_phsp) / ( len(sample_r_k) + len(sample_r_D) + len(sample_r_L)  + len(sample_r_phsp))
    f_k_r    = len(sample_r_k)    / ( len(sample_r_k) + len(sample_r_D) + len(sample_r_L)  + len(sample_r_phsp))
    f_D_r    = len(sample_r_D)    / ( len(sample_r_k) + len(sample_r_D) + len(sample_r_L)  + len(sample_r_phsp))
    f_L_r    = len(sample_r_L)    / ( len(sample_r_k) + len(sample_r_D) + len(sample_r_L)  + len(sample_r_phsp))

    print(f_k_r + f_D_r + f_L_r + f_phsp_r)
   
    cn_recon     = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file_recon, "full", "recon")
    x1_r         = cn_recon.concat_phsp_vars()

    weights_r    = cn_recon.weights_full(new_model, old_model_phsp, old_model_k, old_model_D, old_model_L, x1_r, f_k_r, f_D_r, f_L_r, f_phsp_r)
    #cn_recon.make_root_file_full(weights_r, 'LcMuNu_recon3_Lc_corr_FULL.root')

    cn_recon.plot_weights(x1_r[:, 0], weights_r,  new_data[:, 0], 0)
    cn_recon.plot_weights(x1_r[:, 1], weights_r,  new_data[:, 1], 1)
    cn_recon.plot_weights(x1_r[:, 2], weights_r,  new_data[:, 2], 2)
    cn_recon.plot_weights(x1_r[:, 3], weights_r,  new_data[:, 3], 3)
    cn_recon.plot_weights(x1_r[:, 4], weights_r,  new_data[:, 4], 4)
    
    '''
    #generated file 
    file_gen              = uproot.open("LcMuNu_gen_new.root:DecayTree;1")    
      
    #K* MC data:
    cn_gen_k              = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file_gen, res_list_k[0], "gen")
    sample_g_wnans_k      = cn_gen_k.concat_phsp_vars()
    sample_g_k            = cn_gen_k.remove_nans(sample_g_wnans_k)
    print(sample_g_k)

    #Delta MC data:
    cn_gen_D              = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file_gen, res_list_D[0], "gen")
    sample_g_wnans_D      = cn_gen_D.concat_phsp_vars()
    sample_g_D            = cn_gen_D.remove_nans(sample_g_wnans_D)
    print(sample_g_D)

    #Lambda* MC data:
    cn_gen_L              = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file_gen, res_list_L[0], "gen")
    sample_g_wnans_L      = cn_gen_L.concat_phsp_vars()
    sample_g_L            = cn_gen_L.remove_nans(sample_g_wnans_L)
    print(sample_g_L)

    cn_gen_phsp           = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file_gen, res_list_phsp[0], "gen")
    sample_g_wnans_phsp   = cn_gen_phsp.concat_phsp_vars()
    sample_g_phsp         = cn_gen_phsp.remove_nans(sample_g_wnans_phsp)
    print(sample_g_phsp)
    '''
    f_phsp_g = len(sample_g_phsp) / ( len(sample_g_k) + len(sample_g_D) + len(sample_g_L)  + len(sample_g_phsp))
    f_k_g    = len(sample_g_k)    / ( len(sample_g_k) + len(sample_g_D) + len(sample_g_L)  + len(sample_g_phsp))
    f_D_g    = len(sample_g_D)    / ( len(sample_g_k) + len(sample_g_D) + len(sample_g_L)  + len(sample_g_phsp))
    f_L_g    = len(sample_g_L)    / ( len(sample_g_k) + len(sample_g_D) + len(sample_g_L)  + len(sample_g_phsp))

    print(f_k_g + f_D_g + f_L_g + f_phsp_g)
    

    cn_gen       = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file_gen, "full", "gen")
    x1_g         = cn_gen.concat_phsp_vars()
   
    weights_g    = cn_gen.weights_full(new_model, old_model_phsp, old_model_k, old_model_D, old_model_L, x1_g, f_k_g, f_D_g, f_L_g, f_phsp_g)
    #cn_gen.make_root_file_full(weights_g, 'LcMuNu_gen3_Lc_corr_FULL.root')

    cn_gen.plot_weights(x1_g[:, 0], weights_g,  new_data[:, 0], 0)
    cn_gen.plot_weights(x1_g[:, 1], weights_g,  new_data[:, 1], 1)
    cn_gen.plot_weights(x1_g[:, 2], weights_g,  new_data[:, 2], 2)
    cn_gen.plot_weights(x1_g[:, 3], weights_g,  new_data[:, 3], 3)
    cn_gen.plot_weights(x1_g[:, 4], weights_g,  new_data[:, 4], 4)
    

    ####CHECKS - RECON+STRIP
    file_weight_r       = uproot.open("LcMuNu_recon3_Lc_corr_FULL.root:Decaytree;1")
    weights_r_check     = file_weight_r['Event_Lc_corr']
    weights_r_check     = tf.convert_to_tensor(weights_r_check.array())
    
    cn_recon.plot_weights(x1_r[:, 0], weights_r_check,  new_data[:, 0], 0)
    cn_recon.plot_weights(x1_r[:, 1], weights_r_check,  new_data[:, 1], 1)
    cn_recon.plot_weights(x1_r[:, 2], weights_r_check,  new_data[:, 2], 2)
    cn_recon.plot_weights(x1_r[:, 3], weights_r_check,  new_data[:, 3], 3)
    cn_recon.plot_weights(x1_r[:, 4], weights_r_check,  new_data[:, 4], 4)
    
    
    ####CHECKS - GEN
    file_weight_g       = uproot.open("LcMuNu_gen3_Lc_corr_FULL.root:Decaytree;1")
    weights_g_check     = file_weight_g['Event_Lc_corr']
    weights_g_check     = tf.convert_to_tensor(weights_g_check.array())

    cn_gen.plot_weights(x1_g[:, 0], weights_g_check,  new_data[:, 0], 0)
    cn_gen.plot_weights(x1_g[:, 1], weights_g_check,  new_data[:, 1], 1)
    cn_gen.plot_weights(x1_g[:, 2], weights_g_check,  new_data[:, 2], 2)
    cn_gen.plot_weights(x1_g[:, 3], weights_g_check,  new_data[:, 3], 3)
    cn_gen.plot_weights(x1_g[:, 4], weights_g_check,  new_data[:, 4], 4)

    ####Check q2, costhetal, Lb corrected mass
    file_recon            = uproot.open("Lb2Lcmunu_MagUp_2016_Combine.root:DecayTree;1") 
    cn_recon              = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file_recon, "full", "recon")

    file_weight_r       = ["LcMuNu_recon3_Lc_corr_FULL.root"]
    tree_weight_r       = 'Decaytree'
    files_num           = ['Lb2Lcmunu_MagUp_2016_Combine.root']
    tree_num            = 'DecayTree'
    cut_num             = 'isFullsel==1'
    colums_num          = ['q2_Pred' , 'costhl_Pred', 'Lb_Corrected_Mass', 'Event_LbProdcorr', 'Event_TrackCalibcorr', 'Event_PIDCalibEffWeight', 'Event_L0Muoncorr', 'Event_FFcorr']
    dfgeom_num          = read_root(files_num, key=tree_num, where=cut_num, columns=colums_num) 
    d_weight            = read_root(file_weight_r, key=tree_weight_r, where=cut_num, columns=['Event_Lc_corr', 'Lb_Corrected_Mass', 'Event_LbProdcorr', 'Event_TrackCalibcorr', 'Event_PIDCalibEffWeight', 'Event_L0Muoncorr', 'Event_FFcorr']) 
    q2_num              = dfgeom_num['q2_Pred'].to_numpy()/1e6
    cthl_num            = dfgeom_num['costhl_Pred'].to_numpy()
    corrm_num           = dfgeom_num['Lb_Corrected_Mass'].to_numpy()
    weights_num_ww      = (d_weight['Event_FFcorr']*d_weight['Event_LbProdcorr']*d_weight['Event_TrackCalibcorr']\
                            *d_weight['Event_PIDCalibEffWeight']*d_weight['Event_L0Muoncorr']*d_weight['Event_Lc_corr']).to_numpy()
    weights_num_wow     = (dfgeom_num['Event_FFcorr']*dfgeom_num['Event_LbProdcorr']*dfgeom_num['Event_TrackCalibcorr']\
                            *dfgeom_num['Event_PIDCalibEffWeight']*dfgeom_num['Event_L0Muoncorr']).to_numpy()

    cn_recon.plot_weights_nonew(q2_num, weights_num_wow, weights_num_ww, 6)
    cn_recon.plot_weights_nonew(cthl_num, weights_num_wow, weights_num_ww, 7)
    cn_recon.plot_weights_nonew(corrm_num, weights_num_wow, weights_num_ww, 8)
    '''
    #####Make K*, Delta, Lambda, phsp plots to see goodness of fit
    #cn_recon  = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file_recon, "full", "recon")
    
    #K* Fit 
    #k_mc          = old_model_k.MC(10000)
    #k_m2_ppi_mc   = MLc**2 + Mk**2 + Mp**2 + Mpi**2 - k_mc[:, 0] - k_mc[:, 1]
    #k_m2_ppi      = MLc**2 + Mk**2 + Mp**2 + Mpi**2 - sample_r_k[:, 0] - sample_r_k[:, 1]
    #cn_recon.plot_fits(k_mc[:, 0], sample_r_k[:, 0], 0)
    #cn_recon.plot_fits(k_mc[:, 1], sample_r_k[:, 1], 1)
    #cn_recon.plot_fits(k_mc[:, 2], sample_r_k[:, 2], 2)
    #cn_recon.plot_fits(k_mc[:, 3], sample_r_k[:, 3], 3)
    #cn_recon.plot_fits(k_mc[:, 4], sample_r_k[:, 4], 4)
    #cn_recon.plot_fits(k_m2_ppi_mc, k_m2_ppi, 5)

    #Delta Fit 
    #D_mc          = old_model_D.MC(100000)
    #D_m2_ppi_mc   = MLc**2 + Mk**2 + Mp**2 + Mpi**2 - D_mc[:, 0] - D_mc[:, 1]
    #D_m2_ppi      = MLc**2 + Mk**2 + Mp**2 + Mpi**2 - sample_r_D[:, 0] - sample_r_D[:, 1]
    #cn_recon.plot_fits(D_mc[:, 0], sample_r_D[:, 0], 0)
    #cn_recon.plot_fits(D_mc[:, 1], sample_r_D[:, 1], 1)
    #cn_recon.plot_fits(D_mc[:, 2], sample_r_D[:, 2], 2)
    #cn_recon.plot_fits(D_mc[:, 3], sample_r_D[:, 3], 3)
    #cn_recon.plot_fits(D_mc[:, 4], sample_r_D[:, 4], 4)
    #cn_recon.plot_fits(D_m2_ppi_mc, D_m2_ppi, 5)
    
    #Lambda* Fit 
    #L_mc          = old_model_L.MC(100000)
    #L_m2_ppi_mc   = MLc**2 + Mk**2 + Mp**2 + Mpi**2 - L_mc[:, 0] - L_mc[:, 1]
    #L_m2_ppi      = MLc**2 + Mk**2 + Mp**2 + Mpi**2 - sample_r_L[:, 0] - sample_r_L[:, 1]
    #cn_recon.plot_fits(L_mc[:, 0], sample_r_L[:, 0], 0)
    #cn_recon.plot_fits(L_mc[:, 1], sample_r_L[:, 1], 1)
    #cn_recon.plot_fits(L_mc[:, 2], sample_r_L[:, 2], 2)
    #cn_recon.plot_fits(L_mc[:, 3], sample_r_L[:, 3], 3)
    #cn_recon.plot_fits(L_mc[:, 4], sample_r_L[:, 4], 4)
    #cn_recon.plot_fits(L_m2_ppi_mc, L_m2_ppi, 5)
   
    #PHSP Fit 
    #phsp_mc          = old_model_phsp.MC(100000)
    #phsp_m2_ppi_mc   = MLc**2 + Mk**2 + Mp**2 + Mpi**2 - phsp_mc[:, 0] - phsp_mc[:, 1]
    #phsp_m2_ppi      = MLc**2 + Mk**2 + Mp**2 + Mpi**2 - sample_r_phsp[:, 0] - sample_r_phsp[:, 1]
    #cn_recon.plot_fits(phsp_mc[:, 0], sample_r_phsp[:, 0], 0)
    #cn_recon.plot_fits(phsp_mc[:, 1], sample_r_phsp[:, 1], 1)
    #cn_recon.plot_fits(phsp_mc[:, 2], sample_r_phsp[:, 2], 2)
    #cn_recon.plot_fits(phsp_mc[:, 3], sample_r_phsp[:, 3], 3)
    #cn_recon.plot_fits(phsp_mc[:, 4], sample_r_phsp[:, 4], 4)
    #cn_recon.plot_fits(phsp_m2_ppi_mc, phsp_m2_ppi, 5)

    cn_gen  = Correction_to_Lc_decay(MLb, MLc, Mp, Mk, Mpi, file_gen, "full", "gen")
    
    #K* Fit 
    k_mc          = old_model_k.MC(10000)
    k_m2_ppi_mc   = MLc**2 + Mk**2 + Mp**2 + Mpi**2 - k_mc[:, 0] - k_mc[:, 1]
    k_m2_ppi      = MLc**2 + Mk**2 + Mp**2 + Mpi**2 - sample_g_k[:, 0] - sample_g_k[:, 1]
    cn_gen.plot_fits(k_mc[:, 0], sample_g_k[:, 0], 0)
    cn_gen.plot_fits(k_mc[:, 1], sample_g_k[:, 1], 1)
    cn_gen.plot_fits(k_mc[:, 2], sample_g_k[:, 2], 2)
    cn_gen.plot_fits(k_mc[:, 3], sample_g_k[:, 3], 3)
    cn_gen.plot_fits(k_mc[:, 4], sample_g_k[:, 4], 4)
    cn_gen.plot_fits(k_m2_ppi_mc, k_m2_ppi, 5)

    #Delta Fit 
    D_mc          = old_model_D.MC(100000)
    D_m2_ppi_mc   = MLc**2 + Mk**2 + Mp**2 + Mpi**2 - D_mc[:, 0] - D_mc[:, 1]
    D_m2_ppi      = MLc**2 + Mk**2 + Mp**2 + Mpi**2 - sample_g_D[:, 0] - sample_g_D[:, 1]
    cn_gen.plot_fits(D_mc[:, 0], sample_g_D[:, 0], 0)
    cn_gen.plot_fits(D_mc[:, 1], sample_g_D[:, 1], 1)
    cn_gen.plot_fits(D_mc[:, 2], sample_g_D[:, 2], 2)
    cn_gen.plot_fits(D_mc[:, 3], sample_g_D[:, 3], 3)
    cn_gen.plot_fits(D_mc[:, 4], sample_g_D[:, 4], 4)
    cn_gen.plot_fits(D_m2_ppi_mc, D_m2_ppi, 5)
    
    #Lambda* Fit 
    L_mc          = old_model_L.MC(100000)
    L_m2_ppi_mc   = MLc**2 + Mk**2 + Mp**2 + Mpi**2 - L_mc[:, 0] - L_mc[:, 1]
    L_m2_ppi      = MLc**2 + Mk**2 + Mp**2 + Mpi**2 - sample_g_L[:, 0] - sample_g_L[:, 1]
    cn_gen.plot_fits(L_mc[:, 0], sample_g_L[:, 0], 0)
    cn_gen.plot_fits(L_mc[:, 1], sample_g_L[:, 1], 1)
    cn_gen.plot_fits(L_mc[:, 2], sample_g_L[:, 2], 2)
    cn_gen.plot_fits(L_mc[:, 3], sample_g_L[:, 3], 3)
    cn_gen.plot_fits(L_mc[:, 4], sample_g_L[:, 4], 4)
    cn_gen.plot_fits(L_m2_ppi_mc, L_m2_ppi, 5)
   
    #PHSP Fit 
    phsp_mc          = old_model_phsp.MC(100000)
    phsp_m2_ppi_mc   = MLc**2 + Mk**2 + Mp**2 + Mpi**2 - phsp_mc[:, 0] - phsp_mc[:, 1]
    phsp_m2_ppi      = MLc**2 + Mk**2 + Mp**2 + Mpi**2 - sample_g_phsp[:, 0] - sample_g_phsp[:, 1]
    cn_gen.plot_fits(phsp_mc[:, 0], sample_g_phsp[:, 0], 0)
    cn_gen.plot_fits(phsp_mc[:, 1], sample_g_phsp[:, 1], 1)
    cn_gen.plot_fits(phsp_mc[:, 2], sample_g_phsp[:, 2], 2)
    cn_gen.plot_fits(phsp_mc[:, 3], sample_g_phsp[:, 3], 3)
    cn_gen.plot_fits(phsp_mc[:, 4], sample_g_phsp[:, 4], 4)
    cn_gen.plot_fits(phsp_m2_ppi_mc, phsp_m2_ppi, 5)
   
if __name__ == '__main__':
    main()
    

