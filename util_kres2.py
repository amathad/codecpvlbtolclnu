#import few packages
import sys, os
import tensorflow as tf
os.environ["CUDA_VISIBLE_DEVICES"] = ""  # Do not use GPU for tensor flow
import numpy as np
import matplotlib.pyplot as plt
import math

#amplitf
home = os.getenv('HOME')
sys.path.append(home+"/Packages/AmpliTF/")
import amplitf.interface as atfi
import amplitf.kinematics as atfk
import amplitf.dynamics as atfd
from amplitf.phasespace.rectangular_phasespace import RectangularPhaseSpace

#tfa2
sys.path.append(home+"/Packages/TFA2/")
import tfa.optimisation as tfo
import tfa.toymc as tft
import tfa.plotting as tfp

#get current directory
fit_dir=os.path.dirname(os.path.abspath(__file__))

#define the complex
I = atfi.complex(atfi.const(0), atfi.const(1))

########### Make some common functions
def MakeFourVector(pmag, costheta, phi, msq): 
    """Make 4-vec given magnitude, cos(theta), phi and mass"""
    sintheta = atfi.sqrt(1. - costheta**2) #theta 0 to pi => atfi.sin always pos
    px = pmag * sintheta * atfi.cos(phi)
    py = pmag * sintheta * atfi.sin(phi)
    pz = pmag * costheta
    E  = atfi.sqrt(msq + pmag**2)
    return atfk.lorentz_vector(atfk.vector(px, py, pz), E)

def pvecMag(Msq, m1sq, m2sq): 
    """Momentum mag of (1 or 2) in M rest frame"""
    kallen = atfi.sqrt(Msq**2 + m1sq**2 + m2sq**2 - 2.*(Msq*m1sq + Msq*m2sq + m1sq*m2sq))
    return kallen/(2.*atfi.sqrt(Msq))

def InvRotateAndBoostFromRest(m_p4mom_p1, m_p4mom_p2, gm_phi_m, gm_ctheta_m, gm_p4mom_m):
    """
    Function that gives 4momenta of the particles in the grand mother helicity frame

    m_p4mom_p1: particle p1 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
    m_p4mom_p2: particle p2 4mom in mother m helicity frame (i.e. z points along m momentum in grand mother gm's rest frame)
    gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
    gm_4mom_m: m 4mom in gm's helicity frame
    --------------
    Checks done such as 
        - (lc_p4mom_p+lc_p4mom_k == lc_p4mom_r), 
        - Going in reverse i.e. boost lc_p4mom_p into R rest frame (using lc_p4mom_r) and rotating into R helicity frame gives back r_p4mom_p 
            - i.e. lc_p4mom_p_opp = atfk.rotate_lorentz_vector(atfk.boost_to_rest(lc_p4mom_p, lc_p4mom_r), -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)) where lc_p4mom_p_opp == r_p4mom_p
        - Rotate and boost, instead of boost and rotate should give the same answer, checked (Does not agree with TFA implementation though)
            -i.e. lc_p4mom_prot  = atfk.rotate_lorentz_vector(lc_p4mom_p, -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)
            -i.e. lc_p4mom_krot  = atfk.rotate_lorentz_vector(lc_p4mom_k, -lc_phi_r, -atfi.acos(lc_ctheta_r), lc_phi_r)
            -i.e. lc_p4mom_p2    = atfk.boost_to_rest(lc_p4mom_prot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_p2 == r_p4mom_p
            -i.e. lc_p4mom_k2    = atfk.boost_to_rest(lc_p4mom_krot, lc_p4mom_prot+lc_p4mom_krot) #lc_p4mom_k2 == r_p4mom_k
    """
    #First: Rotate particle p 3mom defined in mother m helicity frame such that z now points along z in grand mother gm helicity frame 
    #(i.e interpreted as gm mom in it's grand-grand-mother ggm rest frame)
    m_p4mom_p1_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p1, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
    m_p4mom_p2_zgmmom      = atfk.rotate_lorentz_vector(m_p4mom_p2, -gm_phi_m, atfi.acos(gm_ctheta_m), gm_phi_m)
    #Second: Boost particle p 4mom from mother m's rest frame to gm helicity frame. This is done using boost vector from gm_p4mom_m  [checked: E(m_p4mom_p1_zmmom + m_p4mom_p2_zmmom) == Mass_m]
    gm_p4mom_p1            = atfk.boost_from_rest(m_p4mom_p1_zgmmom, gm_p4mom_m)
    gm_p4mom_p2            = atfk.boost_from_rest(m_p4mom_p2_zgmmom, gm_p4mom_m)
    return gm_p4mom_p1, gm_p4mom_p2

def make_q2_cthl(dt_np, Lb4moml, Lc4moml, Mu4moml, Nu4moml):
    """Get q^2 and cos(theta) given 4 momenta of all particles"""
    Lb4mom = atfk.lorentz_vector(atfk.vector(dt_np[Lb4moml[1]],dt_np[Lb4moml[2]],dt_np[Lb4moml[3]]), dt_np[Lb4moml[0]])
    Lc4mom = atfk.lorentz_vector(atfk.vector(dt_np[Lc4moml[1]],dt_np[Lc4moml[2]],dt_np[Lc4moml[3]]), dt_np[Lc4moml[0]])
    Mu4mom = atfk.lorentz_vector(atfk.vector(dt_np[Mu4moml[1]],dt_np[Mu4moml[2]],dt_np[Mu4moml[3]]), dt_np[Mu4moml[0]])
    Nu4mom = atfk.lorentz_vector(atfk.vector(dt_np[Nu4moml[1]],dt_np[Nu4moml[2]],dt_np[Nu4moml[3]]), dt_np[Nu4moml[0]])
    #Nu4mom = Nu4mom + (Lb4mom-(Lc4mom+Mu4mom+Nu4mom))
    #Boost Lb frame
    Lb4mom_Lb  = atfk.boost_to_rest(Lb4mom, Lb4mom)
    Lc4mom_Lb  = atfk.boost_to_rest(Lc4mom, Lb4mom)
    Mu4mom_Lb  = atfk.boost_to_rest(Mu4mom, Lb4mom)
    Nu4mom_Lb  = atfk.boost_to_rest(Nu4mom, Lb4mom)
    W4mom_Lb   = atfk.boost_to_rest(Mu4mom+Nu4mom, Lb4mom)
    #Q2
    Q2_var     = tf.square(atfk.mass(Mu4mom+Nu4mom))
    #Cthl
    Lb4mom_W  = atfk.boost_to_rest(Lb4mom_Lb, W4mom_Lb)
    Mu4mom_W  = atfk.boost_to_rest(Mu4mom_Lb, W4mom_Lb)
    Cthl_var  =-atfk.scalar_product(atfk.spatial_components(Mu4mom_W), atfk.spatial_components(Lb4mom_W))/(P(Mu4mom_W) * P(Lb4mom_W))
    #clthl    = atfk.scalar_product(atfk.spatial_components(Mu4mom_W), atfk.spatial_components(W4mom_Lb))/(P(Mu4mom_W) * P(W4mom_Lb))
    return Q2_var, Cthl_var

def HelAngles3Body(pa, pb, pc):
    """Get three-body helicity angles"""
    theta_r  = atfi.acos(-atfk.z_component(pc) / atfk.norm(atfk.spatial_components(pc)))
    phi_r    = atfi.atan2(-atfk.y_component(pc), -atfk.x_component(pc))
    pa_prime = atfk.rotate_lorentz_vector(pa, -phi_r, -theta_r, phi_r)
    pb_prime = atfk.rotate_lorentz_vector(pb, -phi_r, -theta_r, phi_r)
    pa_prime2= atfk.boost_to_rest(pa_prime, pa_prime+pb_prime)
    theta_a  = atfi.acos(atfk.z_component(pa_prime2) / atfk.norm(atfk.spatial_components(pa_prime2)))
    phi_a    = atfi.atan2(atfk.y_component(pa_prime2), atfk.x_component(pa_prime2))
    return (theta_r, phi_r, theta_a, phi_a)

def RotateAndBoostToRest(gm_p4mom_p1, gm_p4mom_p2, gm_phi_m, gm_ctheta_m):
    """
    gm_p4mom_p1: particle p1 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
    gm_p4mom_p2: particle p2 4mom in grand mother gm helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame).
    gm_phi_m, gm_ctheta_m: Angles phi and ctheta of m in gm's helicity frame (i.e z points along gm momentum in grand-grand-mother rest frame)
    """
    #First:  Rotate particle p 3mom defined in grand mother gm helicity frame such that z now points along z of mother m in gm rest frame.
    gm_p4mom_p1_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p1, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
    gm_p4mom_p2_zmmom      = atfk.rotate_lorentz_vector(gm_p4mom_p2, -gm_phi_m, -atfi.acos(gm_ctheta_m), gm_phi_m)
    #Second: Boost particle p 4mom to mother m i.e. p1p2 rest frame
    gm_p4mom_m             = gm_p4mom_p1_zmmom+gm_p4mom_p2_zmmom
    m_p4mom_p1             = atfk.boost_to_rest(gm_p4mom_p1_zmmom, gm_p4mom_m)
    m_p4mom_p2             = atfk.boost_to_rest(gm_p4mom_p2_zmmom, gm_p4mom_m)
    return m_p4mom_p1, m_p4mom_p2
########

#define the class for Lb->Lclnu decay
class LbToLclNu_Model:
    """Class that defines the Lb->Lclnu decay"""

    def __init__(self, MLb, MLc, Mlep, Mp, Mk, Mpi, flavour, wc_floated_names = ['CVR'], ff_floated_names = ['None']):
        """initialise some variables"""
        #Masses of particles involved 
        self.MLb     = MLb
        self.MLc     = MLc
        self.Mlep    = Mlep
        self.Mp      = Mp
        self.Mk      = Mk
        self.Mpi     = Mpi
        #print(self.MLb, self.MLc, self.Mlep)

        #define the flavour of the particle
        self.flavour = flavour
    
        #Constants expressed in GeV
        self.GF      = 1.166378e-5 #GeV^-2
        self.Vcb     = 4.22e-2     #avg of incl and excl
        #print(self.GF, self.Vcb)
        
        #Define function defined in lattice QCD here related to form factors
        self.Tf_plus = {} #mf_pole = M_BC + delta_f and Tf_plus = mf_pole**2
        self.Tf_plus['fplus']      = np.power(6.276 + 56e-3         , 2)#GeV
        self.Tf_plus['fperp']      = np.power(6.276 + 56e-3         , 2)#GeV
        self.Tf_plus['f0']         = np.power(6.276 + 449e-3        , 2)#GeV
        self.Tf_plus['gplus']      = np.power(6.276 + 492e-3        , 2)#GeV
        self.Tf_plus['gperp']      = np.power(6.276 + 492e-3        , 2)#GeV
        self.Tf_plus['g0']         = np.power(6.276 + 0.            , 2)#GeV
        self.Tf_plus['hplus']      = np.power(6.276 + 6.332 - 6.276 , 2)#GeV
        self.Tf_plus['hperp']      = np.power(6.276 + 6.332 - 6.276 , 2)#GeV
        self.Tf_plus['htildeplus'] = np.power(6.276 + 6.768 - 6.276 , 2)#GeV
        self.Tf_plus['htildeperp'] = np.power(6.276 + 6.768 - 6.276 , 2)#GeV
        #print(self.Tf_plus)
        
        #Map to relate W* resonance helicity to the sign in the amplitude (used in building the amplitde)
        self.eta    = {'t': 1.,  0 : -1.,  -1: -1., 1 : -1.}
        #print(self.eta)
        
        #Map to relate WC to the sign in the amplitude (used in building the amplitde)
        self.signWC = {'V': 1., 'A': -1., 'S': 1., 'PS': -1., 'T': 1., 'PT': -1.}
        #print(self.signWC)

        #make SM FF mean and covariance values 
        #ff mean
        self.ff_mean  = {}
        f     = open(fit_dir+"/FF_cov/LambdabLambdac_results.dat", "r")
        for l in f.readlines(): self.ff_mean[l.split()[0]] = float(l.split()[1])
        f.close()
        #print(self.ff_mean)

        #ff covariant
        self.ff_cov = {}
        for l in list(self.ff_mean.keys()): self.ff_cov[l] = {}
        f  = open(fit_dir+"/FF_cov/LambdabLambdac_covariance.dat", "r")
        for l in f.readlines(): self.ff_cov[l.split()[0]][l.split()[1]] = float(l.split()[2])
        f.close()
        #print(self.ff_cov)
        
        #Lists that will serve as index for incoherent sum of amplitudes (defined in units of 1/2 except for lw used in leptonic case that does not need this)
        self.lLbs   = [1, -1]
        self.lls    = [1, -1]
        self.lps    = [1, -1]
        #print(self.lLbs, self.lls, self.lps)
        
        #Lists that will serve as index for coherent sum of amplitudes (defined in units of 1/2 except for lw used in leptonic case that does not need this)
        self.lLcs = [1, -1]
        self.lws  = ['t', 0, 1, -1]
        self.WCs  = ['V', 'A', 'S', 'PS', 'T', 'PT']
        self.FFs  = ['a0f0', 'a0fplus', 'a0fperp', 'a0g0', 'a0gplus', 'a0hplus', 'a0hperp', 'a0htildeplus']
        self.FFs += ['a1f0', 'a1fplus', 'a1fperp', 'a1g0', 'a1gplus', 'a1gperp', 'a1hplus', 'a1hperp', 'a1htildeplus', 'a1htildeperp']
        self.FFs += ['a0gperp', 'a0htildeperp']
        #print(self.lws, self.WCs, self.FFs)
        
        #get wilson coefficients (wc)
        self.wc_params     = self.get_wc_params()
        #get form factor (ff) parameters
        self.ff_params     = self.get_ff_params()
        #define limits and rectangular phase space i.e. (qsq, costhmu) space
        phsp_limits  = [(self.Mlep**2, (self.MLb - self.MLc)**2)] 
        phsp_limits += [(0. , atfi.pi())]
        phsp_limits += [((self.Mk + self.Mpi)**2 , (self.MLc - self.Mp)**2 ) ]
        phsp_limits += [(-atfi.pi(), atfi.pi())]
        phsp_limits += [(0. , atfi.pi())]
        phsp_limits += [(-atfi.pi(), atfi.pi())]
        phsp_limits += [(0. , atfi.pi())]
        self.phase_space = RectangularPhaseSpace(ranges=phsp_limits)

    def get_wc_params(self):
        """Make WC parameters"""
        fitp = {}
        fitp['CVR_Mag'] = tfo.FitParameter("CVR_Mag" , 10.0, -2., 2., 0.08)
        fitp['CVR_Phs'] = tfo.FitParameter("CVR_Phs" , math.pi/6., -math.pi, math.pi, 0.08)
        fitp['CVL_Mag'] = tfo.FitParameter("CVL_Mag" , 0.0, -2., 2., 0.08)
        fitp['CVL_Phs'] = tfo.FitParameter("CVL_Phs" , 0.0, -math.pi, math.pi, 0.08)
        fitp['CSR_Mag'] = tfo.FitParameter("CSR_Mag" , 0.0, -2., 2., 0.08)
        fitp['CSR_Phs'] = tfo.FitParameter("CSR_Phs" , 0.0, -math.pi, math.pi, 0.08)
        fitp['CSL_Mag'] = tfo.FitParameter("CSL_Mag" , 0.0, -2., 2., 0.08)
        fitp['CSL_Phs'] = tfo.FitParameter("CSL_Phs" , 0.0, -math.pi, math.pi, 0.08)
        fitp['CT_Mag']  = tfo.FitParameter("CT_Mag"  , 18.0, -2., 2., 0.08)
        fitp['CT_Phs']  = tfo.FitParameter("CT_Phs"  ,-3*math.pi/2, -math.pi, math.pi, 0.08)
    
        Wlcoef = {}
        #Wlcoef['CVL'] = atfi.cast_complex( fitp['CVL_Mag']() ) * atfi.exp( I * atfi.cast_complex(self.flavour) * atfi.cast_complex( fitp['CVL_Phs']() ) )
        Wlcoef['CVR'] = atfi.cast_complex( fitp['CVR_Mag']() ) * atfi.exp( I * atfi.cast_complex(self.flavour) * atfi.cast_complex( fitp['CVR_Phs']() ) )
        #Wlcoef['CSL'] = atfi.cast_complex( fitp['CSL_Mag']() ) * atfi.exp( I * atfi.cast_complex(self.flavour) * atfi.cast_complex( fitp['CSL_Phs']() ) )
        #Wlcoef['CSR'] = atfi.cast_complex( fitp['CSR_Mag']() ) * atfi.exp( I * atfi.cast_complex(self.flavour) * atfi.cast_complex( fitp['CSR_Phs']() ) )
        #Wlcoef['CT']  = atfi.cast_complex( fitp['CT_Mag']()  ) * atfi.exp( I * atfi.cast_complex(self.flavour) * atfi.cast_complex( fitp['CT_Phs']()  ) )
        Wlcoef['CVL'] = atfi.cast_complex( 0 ) * atfi.exp( I * atfi.cast_complex(self.flavour) * atfi.cast_complex( 0. ) )
        #Wlcoef['CVR'] = atfi.cast_complex( 0 ) * atfi.exp( I * atfi.cast_complex(self.flavour) * atfi.cast_complex( 0. ) )
        Wlcoef['CSR'] = atfi.cast_complex( 0 ) * atfi.exp( I * atfi.cast_complex(self.flavour) * atfi.cast_complex( 0 ) )
        Wlcoef['CSL'] = atfi.cast_complex( 0 ) * atfi.exp( I * atfi.cast_complex(self.flavour) * atfi.cast_complex( 0 ) )
        Wlcoef['CT']  = atfi.cast_complex( 0 ) * atfi.exp( I * atfi.cast_complex(self.flavour) * atfi.cast_complex( 0 ) )
        return Wlcoef

    def get_ff_params(self):
        """Make FF parameters"""
        #make dictionary of ff params
        ffact = {}
        print('Setting FF to LQCD central value and allowed to vary b/w [lqcd_val - 100 * lqcd_sigma, lqcd_val + 100 * lqcd_sigma]. So this corresponds to...')
        for FF in self.FFs[:-2]: 
            ff_mn  = self.ff_mean[FF]
            ff_sg  = np.sqrt(self.ff_cov[FF][FF])
            nsigma_ff = 100.
            ff_l   = ff_mn - nsigma_ff*ff_sg
            ff_h   = ff_mn + nsigma_ff*ff_sg
            print('Setting', FF, 'to SM value:', ff_mn, 'with sigma', ff_sg, ', allowed to vary in fit b/w [', ff_l, ff_h, ']')
            ffact[FF] = tfo.FitParameter(FF, ff_mn, ff_l, ff_h, 0.08)
            ffact[FF].fix() #fix all the ff params
            
        return ffact

    def get_freeparams(self, x_SM, x_NP):
        """Make dictionary of free parameters i.e. WC * FF"""

        #define function for change of basis
        def _wc_basis(basis_name):
            """Change the WC basis e.g. 1+CVL+CVR -> V"""
            wcp = None
            if basis_name == 'V':
                wcp =  x_SM * atfi.cast_complex(1.) + self.wc_params['CVL'] + x_NP * self.wc_params['CVR']
            elif basis_name == 'A':
                wcp =  x_SM * atfi.cast_complex(1.) + self.wc_params['CVL'] - x_NP * self.wc_params['CVR']
            elif basis_name == 'S':
                wcp =  self.wc_params['CSL'] + self.wc_params['CSR']
            elif basis_name == 'PS':
                wcp =  self.wc_params['CSL'] - self.wc_params['CSR']
            elif basis_name == 'T' or basis_name == 'PT':
                wcp =  self.wc_params['CT']
            else:
                raise Exception('The passed basis_name', basis_name, 'not recognised')
    
            return wcp

        def _ff_common(ff_name):
            """Set a0gplus = a0gperp and a0htildeplus = a0htildeperp"""
            ffp = None
            if ff_name == 'a0gperp':
                ffp = self.ff_params['a0gplus']()
            elif ff_name == 'a0htildeperp':
                ffp = self.ff_params['a0htildeplus']()
            else:
                ffp = self.ff_params[ff_name]()

            return ffp

        #Define free parameters dictionary
        free_params = {}
        for WC in self.WCs: 
            for FF in self.FFs:
                free_params[( WC, FF)] = _wc_basis(WC) * atfi.cast_complex(_ff_common(FF))
    
        #print(len(free_params.keys()))                        #6*20 = 120
        return free_params

    def get_resonances(self, resns):
        resonances = {}
        for res in resns:
            if res == "Kstar-kpi":
                    resonances["Kstar-kpi"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : atfi.const(0.84), 
                                            "width"     : atfi.const(0.051),  
                                            "spin"      : 2., 
                                            "parity"    : -1., 
                                            "coupl"     : [              
                                                            #tfo.FitParameter(res+"_Mag_1", 1.0    ,   0. , 20. , 0.01),     tfo.FitParameter(res+"_Phs_1", 0.     , -math.pi , math.pi, 0.01), 
                                                            #tfo.FitParameter(res+"_Mag_2", 0.     ,   0. , 20. , 0.01),     tfo.FitParameter(res+"_Phs_2", 0.     , -math.pi , math.pi, 0.01), 
                                                            #tfo.FitParameter(res+"_Mag_3", 0.     ,   0. , 20. , 0.01),     tfo.FitParameter(res+"_Phs_3", 0.     , -math.pi , math.pi, 0.01), 
                                                            #tfo.FitParameter(res+"_Mag_4", 0.     ,   0. , 20. , 0.01),     tfo.FitParameter(res+"_Phs_4", 0.     , -math.pi , math.pi, 0.01), 
                                                            atfi.const(1.0), atfi.const(0.),
                                                            atfi.const(0. ), atfi.const(0.), 
                                                            atfi.const(1.0), atfi.const(0.), 
                                                            atfi.const(0. ), atfi.const(0.), 
                                                          ]   
                    }
            elif res == "Kstar-kpi2":
                    resonances["Kstar-kpi2"] = {
                                            "lineshape" : "BW_lineshape",  
                                            "mass"      : atfi.const(0.892), 
                                            "width"     : atfi.const(0.051),  
                                            "spin"      : 2., 
                                            "parity"    : -1., 
                                            "coupl"     : [              
                                                            #tfo.FitParameter(res+"_Mag_1", 1.0    ,   0. , 20. , 0.01), tfo.FitParameter(res+"_Phs_1", 0., -math.pi , math.pi, 0.01), 
                                                            #tfo.FitParameter(res+"_Mag_2", 0.     ,   0. , 20. , 0.01), tfo.FitParameter(res+"_Phs_2", 0., -math.pi , math.pi, 0.01), 
                                                            #tfo.FitParameter(res+"_Mag_3", 0.     ,   0., 20.  , 0.01), tfo.FitParameter(res+"_Phs_3", 0., -math.pi , math.pi, 0.01), 
                                                            #tfo.FitParameter(res+"_Mag_4", 0.     ,   0. , 20. , 0.01), tfo.FitParameter(res+"_Phs_4", 0., -math.pi , math.pi, 0.01), 
                                                            atfi.const(1.1), atfi.const(math.pi) + self.flavour *  atfi.const(math.pi/2.),
                                                            atfi.const(2.5), atfi.const(-math.pi/6.) + self.flavour *  atfi.const(math.pi/3.),
                                                            atfi.const(3.6), atfi.const(-math.pi/7.) + self.flavour *  atfi.const(math.pi/4.),
                                                            atfi.const(0.8), atfi.const(2.*math.pi/3.) + self.flavour *  atfi.const(math.pi/6.),
                                                          ]   
                    }
        return resonances 

    def prepare_data(self,x):
        """Function that calculates most of the variables for phase space variables"""
        Vars = {}
        #Store Lc phase_space Varsiables and make sure volume element stays as dq2*dcthlc*dcthl*dphl*dm2pk
        q2            = x[:,0]
        w_theta_l     = x[:,1]
        w_ctheta_l    = tf.cos(w_theta_l)
        #w_ctheta_l    = x[:,1] #This because in MC it is like this
        lb_ctheta_lc  = atfi.ones(x[:,0])  #Lb polarisation is zero (shape same as integrating or fixing)
        lb_phi_lc     = atfi.zeros(x[:,0]) #always zeros
        w_phi_l       = atfi.zeros(x[:,0]) #Lb polarisation is zero (shape same as integrating or fixing)
    
        #Store info of other particles
        lb_ctheta_w   = -lb_ctheta_lc
        lb_phi_w      =  lb_phi_lc+atfi.pi()
        w_ctheta_nu   = -w_ctheta_l
        w_phi_nu      =  w_phi_l+atfi.pi()

        #Lc and W 4mom in Lb rest frame     (zLb = pbeam_lab x pLb_lab, yLb = zLb x lb_p3vec_lc, x = y x z => r, theta, phi = lb_p3mag_lc,lb_ctheta_lc, 0)
        lb_p3mag_lc  = pvecMag(atfi.const(self.MLb)**2, atfi.const(self.MLc)**2, q2)
        lb_p4mom_lc  = MakeFourVector(lb_p3mag_lc,     lb_ctheta_lc, lb_phi_lc, atfi.const(self.MLc)**2)
        lb_p4mom_w   = MakeFourVector(lb_p3mag_lc,     lb_ctheta_w,  lb_phi_w , q2)
        #l and nu 4mom in W helicity frame (zw = lb_p3vec_w, yw = yLb = zLb x lb_p3vec_lc , xw = yw x zw => r, theta, phi => w_p3mag_l, w_ctheta_l, w_phl_l)
        w_p3mag_l    = pvecMag(q2, atfi.const(self.Mlep)**2, 0.)
        w_p4mom_l    = MakeFourVector(w_p3mag_l,    w_ctheta_l , w_phi_l , atfi.const(self.Mlep)**2)
        w_p4mom_nu   = MakeFourVector(w_p3mag_l,    w_ctheta_nu, w_phi_nu, 0.)
    
        #Get everything is Lb rest frame
        lb_p4mom_l , lb_p4mom_nu = InvRotateAndBoostFromRest(w_p4mom_l,   w_p4mom_nu, lb_phi_w,  lb_ctheta_w,  lb_p4mom_w)

        #Store Varss
        Vars['q2']            = q2
        Vars['El']            = atfk.time_component(lb_p4mom_l)
        Vars['m_Lbmu']        = atfk.mass(lb_p4mom_lc+lb_p4mom_l)
        #lnu angles
        Vars['lb_ctheta_w']   = lb_ctheta_w
        Vars['lb_phi_w']      = lb_phi_w
        Vars['w_theta_l']     = w_theta_l
        Vars['w_ctheta_l']    = w_ctheta_l
        Vars['w_phi_l']       = w_phi_l
        #3mom mag 
        Vars['lb_p3mag_lc']   = lb_p3mag_lc
        Vars['w_p3mag_l']     = w_p3mag_l

        #add in Lambda_c decay 
        m2_kpi          = x[:,2]
        lc_phi_res      = x[:,3]
        lc_theta_res    = x[:,4]
        #cthr_lc         = tf.cos(lc_theta_res)
        res_phi_k       = x[:,5]
        res_theta_k     = x[:,6]
        #cthk_r          = tf.cos(res_theta_k)

        Vars['m2_kpi']          = m2_kpi
        Vars['lc_phi_res']      = lc_phi_res
        Vars['lc_theta_res']    = lc_theta_res
        Vars['res_phi_k']       = res_phi_k
        Vars['res_theta_k']     = res_theta_k
    
        return Vars

    def d_phi_twobody(self,mijsq, misq, mjsq):
        """Two body phase space element"""
        return 1./(2.**4 * atfi.pi()**2) * pvecMag(mijsq, misq, mjsq)/atfi.sqrt(mijsq)

    def get_phasespace_term(self, Obs):
        """Two body phase space factors along with the element"""
        #d(lb_ctheta_lc) * d(q2) * d(w_ctheta_l) * d(w_phi_l)
        phsspace  = 1./(2. * atfi.const(self.MLb) * 2. * atfi.pi()) * self.d_phi_twobody(atfi.const(self.MLb)**2, atfi.const(self.MLc)**2, Obs['q2'])  #Lb -> Lc W
        phsspace *= self.d_phi_twobody(Obs['q2'], atfi.const(self.Mlep)**2, 0.) * tf.sin(Obs['w_theta_l'])   #W  ->  l nu
        phsspace *= 1./(2. * atfi.pi()) * self.d_phi_twobody(atfi.const(self.MLc)**2, Obs['m2_kpi'], atfi.const(self.Mp)**2) * tf.sin(Obs['lc_theta_res'])    #Lc   ->   K* p 
        phsspace *= 1./(2. * atfi.pi()) * self.d_phi_twobody(Obs['m2_kpi'], atfi.const(self.Mk)**2, atfi.const(self.Mpi)**2) * tf.sin(Obs['res_theta_k'])   #K*   ->   K  pi
        return phsspace

    def get_lb_ampls(self, Obs):
        """
        Some important comments about the hadronic currents:
        V currents:
            - aV, bV and cV are form factor parameter independent (unlike in our paper)
            - When summing over lwd only one term is needed. This term is taken as 't' (b'cos eta[t] = 1). Make sure Leptonic V and A current lwd='t'.
            - The ffterms func return corrent q2 dependent terms pertaining to FF parameter a0 and a1.
            - Hadr(lb, lc, lw, 't', 'V', 'a0') have 16 comb out of which 12 survive and 4 are zero
        S currents:
            - Fake lw and lwd index (see above for reason). Fixed to zero, make sure Leptonic S, PS currents also have lw = lwd = 't'.
            - Hadr(lb, lc, 't', 't', 'S', 'a0') have 4 terms all which are nonzero.
        A currents: 
            - Like in Leptonic current these are NOT equal to V currents.
            - Same comments as V currents.
        PS currents: 
            - Like in Leptonic current these are NOT equal to S currents.
            - Same comments as S currents.
        T currents:
            - Hadr(lb, lc, lw, lwd, 'T', 'a0') have 64 terms out of which 32 are nonzero.
        PT currents:
            - Like in Leptonic current these are NOT equal to T currents.
            - same comments as T currents.
        Func returns:
            Dict with index as tuples Hadr[(lb, lc, lw, lwd, wc, ff)]
        """
        #q2 and angular terms: Used sin(x/2) = sqrt((1-cthlc)/2) and cos(x/2)=sqrt((1+cthlc)/2).Since x/2 ranges from 0 to pi/2 both sine and cosine are pos.
        q2          =  Obs['q2']
        Mpos        = atfi.const(self.MLb) + atfi.const(self.MLc)
        Mneg        = atfi.const(self.MLb) - atfi.const(self.MLc)
        sqrtQp      = atfi.sqrt(atfi.pow(Mpos,2) - q2)
        sqrtQn      = atfi.sqrt(atfi.pow(Mneg,2) - q2)
        sqrtQpMn    = sqrtQp * Mneg
        sqrtQnMp    = sqrtQn * Mpos
        aV          = sqrtQpMn/atfi.sqrt(q2)
        bV          = sqrtQnMp/atfi.sqrt(q2)
        cV          = atfi.sqrt(np.array(2.)) * sqrtQn
        aA          = bV
        bA          = aV
        cA          = atfi.sqrt(np.array(2.)) * sqrtQp
        mb          = atfi.const(4.18       ) #GeV
        mc          = atfi.const(1.275      ) #GeV
        aS          = sqrtQpMn/(mb - mc)
        aP          = sqrtQnMp/(mb + mc)
        aT          = sqrtQn
        bT          = atfi.sqrt(np.array(2.)) * bV
        cT          = atfi.sqrt(np.array(2.)) * aV
        dT          = sqrtQp
        cthlc       = -Obs['lb_ctheta_w'] #ONE Lb polarisation ignored
        costhlchalf = atfi.sqrt((1+cthlc)/2.)  #One Lb polarisation ignored
        sinthlchalf = atfi.sqrt((1-cthlc)/2.)  #ZERO Lb polarisation ignored, so commented out the terms
        t0          = atfi.pow(Mneg,2)
    
        #get only q2 dependent terms in FF expansion i.e. ones pertaining to a0 and a1.
        ffterms = {}
        for ff in ['fplus', 'fperp', 'f0', 'gplus', 'gperp', 'g0', 'hplus', 'hperp', 'htildeperp', 'htildeplus']:
            cf = 1./(1. - q2/atfi.const(self.Tf_plus[ff]))
            zf = (atfi.sqrt(atfi.const(self.Tf_plus[ff]) - q2) - atfi.sqrt(atfi.const(self.Tf_plus[ff]) - t0))/(atfi.sqrt(atfi.const(self.Tf_plus[ff]) - q2) + atfi.sqrt(atfi.const(self.Tf_plus[ff]) - t0))
            ffterms['a0'+ff] =  cf
            ffterms['a1'+ff] =  cf * zf
    
        #define hadronic amplitudes
        Hadr = {}
        #f0 terms: contains V and S currents
        for a in ['a0f0', 'a1f0']:
            Hadr[( 1, 1,'t', 't', 'V', a)] =  costhlchalf * aV * ffterms[a]
            Hadr[(-1,-1,'t', 't', 'V', a)] =  costhlchalf * aV * ffterms[a]
            Hadr[( 1, 1,'t', 't', 'S', a)] =  costhlchalf * aS * ffterms[a]
            Hadr[(-1,-1,'t', 't', 'S', a)] =  costhlchalf * aS * ffterms[a]
            #Hadr[( 1,-1,'t', 't', 'V', a)] = -sinthlchalf * aV * ffterms[a]
            #Hadr[(-1, 1,'t', 't', 'V', a)] =  sinthlchalf * aV * ffterms[a]
            #Hadr[( 1,-1,'t', 't', 'S', a)] = -sinthlchalf * aS * ffterms[a]
            #Hadr[(-1, 1,'t', 't', 'S', a)] =  sinthlchalf * aS * ffterms[a]
        
        #fplus terms
        for a in ['a0fplus', 'a1fplus']:
            Hadr[( 1, 1,  0, 't', 'V', a)] =  costhlchalf  * bV * ffterms[a]
            Hadr[(-1,-1,  0, 't', 'V', a)] =  costhlchalf  * bV * ffterms[a]
            #Hadr[( 1,-1,  0, 't', 'V', a)] = -sinthlchalf  * bV * ffterms[a]
            #Hadr[(-1, 1,  0, 't', 'V', a)] =  sinthlchalf  * bV * ffterms[a]
        
        #fperp terms
        for a in ['a0fperp', 'a1fperp']:
            Hadr[( 1,-1, -1, 't', 'V', a)] = -costhlchalf  * cV * ffterms[a]
            Hadr[(-1, 1,  1, 't', 'V', a)] = -costhlchalf  * cV * ffterms[a]
            #Hadr[(-1,-1, -1, 't', 'V', a)] = -sinthlchalf  * cV * ffterms[a]
            #Hadr[( 1, 1,  1, 't', 'V', a)] =  sinthlchalf  * cV * ffterms[a]
        
        #g0 terms: contains A and PS currents
        for a in ['a0g0','a1g0']:
            Hadr[( 1, 1,'t', 't', 'A', a)] =  costhlchalf * aA * ffterms[a]
            Hadr[(-1,-1,'t', 't', 'A', a)] = -costhlchalf * aA * ffterms[a]
            Hadr[( 1, 1,'t', 't','PS', a)] = -costhlchalf * aP * ffterms[a]
            Hadr[(-1,-1,'t', 't','PS', a)] =  costhlchalf * aP * ffterms[a]
            #Hadr[( 1,-1,'t', 't', 'A', a)] =  sinthlchalf * aA * ffterms[a]
            #Hadr[(-1, 1,'t', 't', 'A', a)] =  sinthlchalf * aA * ffterms[a]
            #Hadr[( 1,-1,'t', 't','PS', a)] = -sinthlchalf * aP * ffterms[a]
            #Hadr[(-1, 1,'t', 't','PS', a)] = -sinthlchalf * aP * ffterms[a]
        
        #gplus terms
        for a in ['a0gplus','a1gplus']:
            Hadr[( 1, 1,  0, 't', 'A', a)] =  costhlchalf * bA * ffterms[a]
            Hadr[(-1,-1,  0, 't', 'A', a)] = -costhlchalf * bA * ffterms[a]
            #Hadr[( 1,-1,  0, 't', 'A', a)] =  sinthlchalf * bA * ffterms[a]
            #Hadr[(-1, 1,  0, 't', 'A', a)] =  sinthlchalf * bA * ffterms[a]
        
        #gperp terms
        for a in ['a0gperp','a1gperp']:
            Hadr[( 1,-1, -1, 't', 'A', a)] =  costhlchalf * cA * ffterms[a]
            Hadr[(-1, 1,  1, 't', 'A', a)] = -costhlchalf * cA * ffterms[a]
            #Hadr[(-1,-1, -1, 't', 'A', a)] =  sinthlchalf * cA * ffterms[a]
            #Hadr[( 1, 1,  1, 't', 'A', a)] =  sinthlchalf * cA * ffterms[a]
        
        #hplus terms: T and PT
        for a in ['a0hplus','a1hplus']:
            Hadr[( 1, 1,'t',  0, 'T', a)] =  costhlchalf * aT * ffterms[a]
            Hadr[(-1,-1,'t',  0, 'T', a)] =  costhlchalf * aT * ffterms[a]
            Hadr[( 1, 1,  0,'t', 'T', a)] = -costhlchalf * aT * ffterms[a]
            Hadr[(-1,-1,  0,'t', 'T', a)] = -costhlchalf * aT * ffterms[a]
            Hadr[( 1, 1,  1, -1, 'PT',a)] =  costhlchalf * aT * ffterms[a]
            Hadr[(-1,-1,  1, -1, 'PT',a)] =  costhlchalf * aT * ffterms[a]
            Hadr[( 1, 1, -1,  1, 'PT',a)] = -costhlchalf * aT * ffterms[a]
            Hadr[(-1,-1, -1,  1, 'PT',a)] = -costhlchalf * aT * ffterms[a]
            #Hadr[( 1,-1,'t',  0, 'T', a)] = -sinthlchalf * aT * ffterms[a]
            #Hadr[(-1, 1,'t',  0, 'T', a)] =  sinthlchalf * aT * ffterms[a]
            #Hadr[( 1,-1,  0,'t', 'T', a)] =  sinthlchalf * aT * ffterms[a]
            #Hadr[(-1, 1,  0,'t', 'T', a)] = -sinthlchalf * aT * ffterms[a]
            #Hadr[( 1,-1,  1, -1, 'PT',a)] = -sinthlchalf * aT * ffterms[a]
            #Hadr[(-1, 1,  1, -1, 'PT',a)] =  sinthlchalf * aT * ffterms[a]
            #Hadr[( 1,-1, -1,  1, 'PT',a)] =  sinthlchalf * aT * ffterms[a]
            #Hadr[(-1, 1, -1,  1, 'PT',a)] = -sinthlchalf * aT * ffterms[a]
        
        #hperp terms: T and PT
        for a in ['a0hperp','a1hperp']:
            Hadr[(-1, 1,'t',  1, 'T', a)] = -costhlchalf * bT *  ffterms[a]
            Hadr[( 1,-1,'t', -1, 'T', a)] = -costhlchalf * bT *  ffterms[a]
            Hadr[(-1, 1,  1,'t', 'T', a)] =  costhlchalf * bT *  ffterms[a]
            Hadr[( 1,-1, -1,'t', 'T', a)] =  costhlchalf * bT *  ffterms[a]
            Hadr[( 1,-1,  0, -1, 'PT',a)] = -costhlchalf * bT *  ffterms[a]
            Hadr[( 1,-1, -1,  0, 'PT',a)] =  costhlchalf * bT *  ffterms[a]
            Hadr[(-1, 1,  0,  1, 'PT',a)] =  costhlchalf * bT *  ffterms[a]
            Hadr[(-1, 1,  1,  0, 'PT',a)] = -costhlchalf * bT *  ffterms[a]
            #Hadr[(-1,-1,'t', -1, 'T', a)] = -sinthlchalf * bT *  ffterms[a]
            #Hadr[( 1, 1,'t',  1, 'T', a)] =  sinthlchalf * bT *  ffterms[a]
            #Hadr[(-1,-1, -1,'t', 'T', a)] =  sinthlchalf * bT *  ffterms[a]
            #Hadr[( 1, 1,  1,'t', 'T', a)] = -sinthlchalf * bT *  ffterms[a]
            #Hadr[(-1,-1,  0, -1, 'PT',a)] = -sinthlchalf * bT *  ffterms[a]
            #Hadr[( 1, 1,  1,  0, 'PT',a)] =  sinthlchalf * bT *  ffterms[a]
            #Hadr[(-1,-1, -1,  0, 'PT',a)] =  sinthlchalf * bT *  ffterms[a]
            #Hadr[( 1, 1,  0,  1, 'PT',a)] = -sinthlchalf * bT *  ffterms[a]
        
        #htildeperp terms: T and PT
        for a in ['a0htildeperp','a1htildeperp']:
            Hadr[(-1, 1,  0,  1, 'T', a)] = -costhlchalf * cT * ffterms[a]
            Hadr[( 1,-1,  0, -1, 'T', a)] = -costhlchalf * cT * ffterms[a]
            Hadr[(-1, 1,  1,  0, 'T', a)] =  costhlchalf * cT * ffterms[a]
            Hadr[( 1,-1, -1,  0, 'T', a)] =  costhlchalf * cT * ffterms[a]
            Hadr[( 1,-1,'t', -1, 'PT',a)] = -costhlchalf * cT * ffterms[a]
            Hadr[(-1, 1,'t',  1, 'PT',a)] =  costhlchalf * cT * ffterms[a]
            Hadr[( 1,-1, -1,'t', 'PT',a)] =  costhlchalf * cT * ffterms[a]
            Hadr[(-1, 1,  1,'t', 'PT',a)] = -costhlchalf * cT * ffterms[a]
            #Hadr[(-1,-1,  0, -1, 'T', a)] = -sinthlchalf * cT * ffterms[a]
            #Hadr[( 1, 1,  0,  1, 'T', a)] =  sinthlchalf * cT * ffterms[a]
            #Hadr[(-1,-1, -1,  0, 'T', a)] =  sinthlchalf * cT * ffterms[a]
            #Hadr[( 1, 1,  1,  0, 'T', a)] = -sinthlchalf * cT * ffterms[a]
            #Hadr[( 1, 1,'t',  1, 'PT',a)] = -sinthlchalf * cT * ffterms[a]
            #Hadr[(-1,-1,'t', -1, 'PT',a)] = -sinthlchalf * cT * ffterms[a]
            #Hadr[( 1, 1,  1,'t', 'PT',a)] =  sinthlchalf * cT * ffterms[a]
            #Hadr[(-1,-1, -1,'t', 'PT',a)] =  sinthlchalf * cT * ffterms[a]
        
        #htildeplus terms: T and PT
        for a in ['a0htildeplus','a1htildeplus']:
            Hadr[( 1, 1,  1, -1, 'T', a)] = -costhlchalf * dT * ffterms[a]
            Hadr[(-1,-1,  1, -1, 'T', a)] =  costhlchalf * dT * ffterms[a]
            Hadr[( 1, 1, -1,  1, 'T', a)] =  costhlchalf * dT * ffterms[a]
            Hadr[(-1,-1, -1,  1, 'T', a)] = -costhlchalf * dT * ffterms[a]
            Hadr[( 1, 1,'t',  0, 'PT',a)] = -costhlchalf * dT * ffterms[a]
            Hadr[(-1,-1,'t',  0, 'PT',a)] =  costhlchalf * dT * ffterms[a]
            Hadr[( 1, 1,  0,'t', 'PT',a)] =  costhlchalf * dT * ffterms[a]
            Hadr[(-1,-1,  0,'t', 'PT',a)] = -costhlchalf * dT * ffterms[a]
            #Hadr[( 1,-1,  1, -1, 'T', a)] = -sinthlchalf * dT * ffterms[a]
            #Hadr[(-1, 1,  1, -1, 'T', a)] = -sinthlchalf * dT * ffterms[a]
            #Hadr[( 1,-1, -1,  1, 'T', a)] =  sinthlchalf * dT * ffterms[a]
            #Hadr[(-1, 1, -1,  1, 'T', a)] =  sinthlchalf * dT * ffterms[a]
            #Hadr[( 1,-1,'t',  0, 'PT',a)] = -sinthlchalf * dT * ffterms[a]
            #Hadr[(-1, 1,'t',  0, 'PT',a)] = -sinthlchalf * dT * ffterms[a]
            #Hadr[( 1,-1,  0,'t', 'PT',a)] =  sinthlchalf * dT * ffterms[a]
            #Hadr[(-1, 1,  0,'t', 'PT',a)] =  sinthlchalf * dT * ffterms[a]
    
        return Hadr

    def get_w_ampls(self, Obs):
        """
        Some important comments on all the leptonic currents
        V or A current:
            - Terms with WC index V and A are equal.
            - Since only one term appears in the ampl for lwd. We fix lwd to 't' as done in hadronic V, A currents. Index 't' choosen because eta['t'] = 1.
            - Therefore Lept(lw, 't', 'V', ll) has total 8 components out of which 1 is zero.
        S or PS current:
            - Terms with WC index S and PS are equal.
            - In this case both lw and lwd are fake indeces (see above for reason), choosen to be 't' (see above) as done in hadronic S, PS currents.
            - Lept('t', 't', 'S', ll) has total 2 components out of which 1 is zero.
        T or PT current:
            - Terms with WC index T and PT are equal.
            - Lept(lw,lwd,'T',l) has total 32 components out of which 8 are zero.
        Func returns:
            Dict with index as tuples Lept[(lw, lwd, wc, ll)]
        """
        
        #q2 and angular terms
        q2              = Obs['q2'] #real
        cthl            =-Obs['w_ctheta_l'] #real (our pdf is expressed a function of w_ctheta_nu not w_ctheta_l
        phl             = Obs['w_phi_l'] #ZERO Lb polarisation ignored as a result amplitude real
        v               = atfi.sqrt(1. - atfi.pow(atfi.const(self.Mlep),2)/q2) #real
        sinthl          = atfi.sqrt(1. - atfi.pow(cthl,2.)) #real
        expPlusOneIphl  = atfi.cos(phl)    #real and One since Lb polarisation ignored
        expMinusOneIphl = atfi.cos(-phl)   #real and One since Lb polarisation ignored
        expMinusTwoIphl = atfi.cos(-2.*phl)#real and One since Lb polarisation ignored
        OneMinuscthl    = (1. - cthl)
        OnePluscthl     = (1. + cthl)
        al              = 2. * atfi.const(self.Mlep) * v
        bl              = 2. * atfi.sqrt(q2)* v
        complexsqrttwo  = atfi.sqrt(np.array(2.))
        
        Lept = {}
        
        Lept[('t', 't', 'V',  1)] =  expMinusOneIphl * al
        Lept[(  0, 't', 'V',  1)] = -expMinusOneIphl * cthl * al
        Lept[(  1, 't', 'V',  1)] =  expMinusTwoIphl * sinthl * al/complexsqrttwo
        Lept[( -1, 't', 'V',  1)] = -sinthl * al/complexsqrttwo
        Lept[(  0, 't', 'V', -1)] =  sinthl * bl
        Lept[(  1, 't', 'V', -1)] =  expMinusOneIphl * OnePluscthl  * bl/complexsqrttwo
        Lept[( -1, 't', 'V', -1)] =  expPlusOneIphl  * OneMinuscthl * bl/complexsqrttwo
        
        Lept[('t', 't', 'S', 1)]  =  expMinusOneIphl * bl
        
        Lept[('t',  0, 'T', -1)]  =  sinthl * al
        Lept[(  1, -1, 'T', -1)]  =  sinthl * al
        Lept[(  0, -1, 'T', -1)]  =  expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
        Lept[('t', -1, 'T', -1)]  =  expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
        Lept[('t',  1, 'T', -1)]  =  expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
        Lept[(  0,  1, 'T', -1)]  = -expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
        Lept[('t',  0, 'T',  1)]  = -expMinusOneIphl * cthl * bl
        Lept[(  1, -1, 'T',  1)]  = -expMinusOneIphl * cthl * bl
        Lept[('t',  1, 'T',  1)]  =  expMinusTwoIphl * sinthl * bl/complexsqrttwo
        Lept[(  0,  1, 'T',  1)]  = -expMinusTwoIphl * sinthl * bl/complexsqrttwo
        Lept[('t', -1, 'T',  1)]  = -sinthl * bl/complexsqrttwo
        Lept[(  0, -1, 'T',  1)]  = -sinthl * bl/complexsqrttwo
        Lept[(  0,'t', 'T', -1)]  = -sinthl * al
        Lept[( -1,  1, 'T', -1)]  = -sinthl * al
        Lept[( -1,  0, 'T', -1)]  = -expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
        Lept[( -1,'t', 'T', -1)]  = -expPlusOneIphl  * OneMinuscthl * al/complexsqrttwo
        Lept[(  1,'t', 'T', -1)]  = -expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
        Lept[(  1,  0, 'T', -1)]  =  expMinusOneIphl * OnePluscthl  * al/complexsqrttwo
        Lept[(  0,'t', 'T',  1)]  =  expMinusOneIphl * cthl * bl
        Lept[( -1,  1, 'T',  1)]  =  expMinusOneIphl * cthl * bl
        Lept[(  1,'t', 'T',  1)]  = -expMinusTwoIphl * sinthl * bl/complexsqrttwo
        Lept[(  1,  0, 'T',  1)]  =  expMinusTwoIphl * sinthl * bl/complexsqrttwo
        Lept[( -1,'t', 'T',  1)]  =  sinthl * bl/complexsqrttwo
        Lept[( -1,  0, 'T',  1)]  =  sinthl * bl/complexsqrttwo
    
        return Lept

    ''' Lc - pKpi decay , amplitude for 2 resonances K* and K*2 '''
    def get_lc_ampls(self, Obvs, x_K1, x_K2):
        #phase space variables 
        m2_kpi          = Obvs['m2_kpi']
        lc_phi_res      = self.flavour * Obvs['lc_phi_res']
        lc_theta_res    = Obvs['lc_theta_res'] 
        res_phi_k       = self.flavour * Obvs['res_phi_k'] 
        res_theta_k     = Obvs['res_theta_k']

        #Angular momenta constants 
        Jlam_c      = 1.
        Jp          = 1.
        Jk          = 0.
        Jpi         = 0.
        J_k_star    = 2.

        #finite widths - can be varied to find optimal - (Blatt-Weisskopf radii)
        d_k_star    = atfi.const(1.5)  #GeV
        d_Lambda_c  = atfi.const(5.)  #GeV

        #define resonances and factors
        res    = ["Kstar-kpi", "Kstar-kpi2"]
        resn   = self.get_resonances(res)
        res_factor = [x_K1, x_K2]

        """ For decay A to BC - Function to determine the angular momentum of L(BC) """
        # use J(A) = J(B) +/- J(C) +/- L(BC)
        # where total angular momenta are definened in units of 1/2
        def l_bc( ja , jb , jc ):
            l_bc1 = - ( ja/2 - jb/2 - jc/2 )
            l_bc2 = ja/2 - jb/2 + jc/2
            l_bc = np.min( [l_bc1 , l_bc2] )
            return np.max( [l_bc, 0.] )

        l_kstar     = int( l_bc( J_k_star , Jk , Jpi ) )
        l_lamc      = int( l_bc( Jlam_c , J_k_star , Jp ) )

        def Ampl( res_name, J_lam_c_proj , lp, h_res, coupling, m_sq_Kpi, phi_r_lamc, theta_r_lamc, phi_k_res, theta_k_res ):
            e_lp = atfi.cast_complex( ( -1 )**( Jp/2 - lp/2 ) )
            A  = atfd.breit_wigner_lineshape(m_sq_Kpi , resn[res_name]['mass'] , resn[res_name]['width'] , self.Mk , self.Mpi , self.Mp , self.MLc , d_k_star , d_Lambda_c , l_kstar , l_lamc )
            A *= tf.math.conj( atfk.wigner_capital_d( phi_r_lamc , theta_r_lamc , 0 , Jlam_c  , J_lam_c_proj , h_res - lp ) )
            A *= tf.math.conj( atfk.wigner_capital_d( phi_k_res , theta_k_res , 0 , resn[res_name]['spin'] , h_res , 0 ) )
            A *= e_lp
            A *= coupling
            return A

        def A_res(lLc, lp):
            amplf = atfi.complex(atfi.const(0.) , atfi.const(0.))
            #coherently sum over resonances and their helicities
            for indx, r in enumerate(list(resn.keys())):
                if 'kpi' in r:
                    coupling1 = atfi.cast_complex(  resn[r]['coupl'][0] ) * atfi.exp( I * atfi.cast_complex(  resn[r]['coupl'][1] ) )
                    coupling2 = atfi.cast_complex(  resn[r]['coupl'][2] ) * atfi.exp( I * atfi.cast_complex(  resn[r]['coupl'][3] ) )
                    coupling3 = atfi.cast_complex(  resn[r]['coupl'][4] ) * atfi.exp( I * atfi.cast_complex(  resn[r]['coupl'][5] ) )
                    coupling4 = atfi.cast_complex(  resn[r]['coupl'][6] ) * atfi.exp( I * atfi.cast_complex(  resn[r]['coupl'][7] ) )
                    if lp == 1.:
                        ampl1           = Ampl(r, lLc , lp, 0., coupling1, m2_kpi, lc_phi_res, lc_theta_res, res_phi_k, res_theta_k )
                        ampl2           = Ampl(r, lLc , lp, 2., coupling2, m2_kpi, lc_phi_res, lc_theta_res, res_phi_k, res_theta_k )
                    else:
                        ampl1           = Ampl(r,  lLc , lp,  0., coupling3, m2_kpi, lc_phi_res, lc_theta_res, res_phi_k, res_theta_k )
                        ampl2           = Ampl(r,  lLc , lp, -2., coupling4, m2_kpi, lc_phi_res, lc_theta_res, res_phi_k, res_theta_k )
                else:
                    raise Exception('Channel not specified for resonance. Please check!')

                amplf += atfi.cast_complex(res_factor[indx]) * (ampl1 + ampl2)

            return amplf

        #define the amplitudes
        LcAmp = {}
        LcAmp[( 1 ,  1)] = A_res(  1 ,  1)
        LcAmp[( 1 , -1)] = A_res(  1 , -1)
        LcAmp[(-1 ,  1)] = A_res( -1 ,  1)
        LcAmp[(-1 , -1)] = A_res( -1 , -1)
        return LcAmp
    
    def helicities_dict(self, x):
        helicities = {}
        helicities[(1, 1, 1)]       = [1, 1, 1] 
        helicities[(1, 1, -1)]      = [1, 1, -1] 
        helicities[(1, -1, 1)]      = [1, -1, 1] 
        helicities[(1, -1, -1)]     = [1, -1, -1] 
        helicities[(-1, 1, 1)]      = [-1, 1, 1] 
        helicities[(-1, 1, -1)]     = [-1, 1, -1] 
        helicities[(-1, -1, 1)]     = [-1, -1, 1] 
        helicities[(-1, -1, -1)]    = [-1, -1, -1] 

        if x == 1:
            return helicities[(1,1,1)]
        elif x == 2:
            return helicities[(1,1,-1)]
        elif x == 3:
            return helicities[(1,-1,1)]
        elif x == 4:
            return helicities[(1,-1,-1)]
        elif x == 5:
            return helicities[(-1,1,1)]
        elif x == 6:
            return helicities[(-1,1,-1)]
        elif x == 7:
            return helicities[(-1,-1,1)]
        elif x == 8:
            return helicities[(-1,-1,-1)]


    def get_dynamic_term(self, obsv, x_SM, x_NP, x_K1, x_K2, h):
        """Get the dynamic density term in Lb->Lclnu decay"""
    
        #define a helpful function
        def _replace_wc(strg):
            """Helpful function when for leptonic amplitudes in definition of density function"""
            strg_wc = strg.replace('A' , 'V')
            strg_wc = strg_wc.replace('PS', 'S')
            strg_wc = strg_wc.replace('PT', 'T')
            return strg_wc
    
        #get Lb and w amplitudes
        free_params     = self.get_freeparams(x_SM, x_NP)
        Lbampl          = self.get_lb_ampls(obsv)  #LbDecay_lLb_lLc_lw_lwd_WC_FF
        Wampl           = self.get_w_ampls(obsv)   #WDecay_lw_lwd_WC_ll
        Lcampl          = self.get_lc_ampls(obsv, x_K1, x_K2)  #LcDecay_J_Lc_proj, lp
        #print(len(LbDecay_lLb_lLc_lw_lwd_WC_FF.keys()))    #192 reduces to 92 if Lb is unpolarised
        #print(len(WDecay_lw_lwd_WC_ll.keys()))             #32
        #print(len(fp_wc_ff.keys()))                        #6*20 = 120

        if h == 9:
            lLbs    = self.lLbs
            lls     = self.lls
            lps     = self.lps
        else:
            helicity    = self.helicities_dict(h) 
            lLbs        = [helicity[0]]
            lls         = [helicity[1]]
            lps         = [helicity[2]]

        dens = atfi.const(0.)
        for lLb in lLbs: 
            for ll in lls:
                for lp in lps:
                    ampls_coherent = atfi.complex( atfi.const(0.), atfi.const(0.) ) 
                    for lLc in self.lLcs:
                        for lw in self.lws:
                            for lwd in self.lws:
                                for WC in self.WCs:
                                    for FF in self.FFs:
                                        lbindx = (lLb,lLc,lw,lwd,WC,FF)
                                        windx  = (lw,lwd,_replace_wc(WC),ll)
                                        fpindx = (WC, FF)
                                        lcindx = (lLc, lp)
                                        cond   = lbindx not in Lbampl or windx not in Wampl or lcindx not in Lcampl
                                        if cond: continue
                                        amp  = atfi.cast_complex(atfi.sqrt(np.array(2.))*atfi.const(self.GF)*atfi.const(self.Vcb)*atfi.const(self.signWC[WC])*atfi.const(self.eta[lw])*atfi.const(self.eta[lwd]) )
                                        amp *= atfi.cast_complex(Lbampl[lbindx])*atfi.cast_complex(Wampl[windx])*free_params[fpindx] 
                                        amp *= Lcampl[lcindx]
                                        #print(amp)
                                        ampls_coherent += amp

                    #print(ampls_coherent)
                    dens += atfd.density(ampls_coherent)
        return dens
        

    def get_unbinned_model(self, ph, x_SM, x_NP, x_K1, x_K2, h, method= '1' , normalisation_sample = False):
        """Define the unbinned model for Lb->Lclnu"""
        #if method == '1' and normalisation_sample:
        #    raise Exception('Method', method, 'picked, but normalisation_sample is set to True. Check!')

        model = None
        if method == '1':
            obsv              = self.prepare_data(ph) 
            dGdO_dynm         = self.get_dynamic_term(obsv, x_SM, x_NP, x_K1, x_K2, h)
            dGdO_phsp         = self.get_phasespace_term(obsv)
            model             = dGdO_phsp * dGdO_dynm
           
        else:
            raise Exception("Method not recognised")

        return model

    def weights(self, sample, x_SM, x_NP, x_K1, x_K2, h):
        #return weights for SM + K1 + K2 ampl
        w1      = self.get_unbinned_model(sample, 1, 0, 1, 0, h,  method= '1') / self.get_unbinned_model(sample, 1, 1, 1, 1, h,  method= '1')
        #return weights for CVR + K1 + K2 ampl
        w2      = self.get_unbinned_model(sample, 1, 0, 0, 1, h,  method= '1') / self.get_unbinned_model(sample, 1, 1, 1, 1, h,  method= '1')
        #return weights for SM + CVR + K1 ampl
        w3      = self.get_unbinned_model(sample, 0, 1, 1, 0, h,  method= '1') / self.get_unbinned_model(sample, 1, 1, 1, 1, h,  method= '1')
        #return weights for SM + CVR + K2 ampl
        w4      = self.get_unbinned_model(sample, 0, 1, 0, 1, h,  method= '1') / self.get_unbinned_model(sample, 1, 1, 1, 1, h,  method= '1')
        #interference SM
        #d1 = self.get_unbinned_model(sample, 1, 0, 1, 1, 9,  method= '1') - self.get_unbinned_model(sample, 1, 0, 1, 0, 9,  method= '1') - self.get_unbinned_model(sample, 1, 0, 0, 1, 9, method= '1')
        #intf1 = d1 / self.get_unbinned_model(sample, 1, 1, 1, 1, 9,   method= '1')
        #interference NP
        #d2 = self.get_unbinned_model(sample, 0, 1, 1, 1, 9,  method= '1') - self.get_unbinned_model(sample, 0, 1, 1, 0, 9,  method= '1') - self.get_unbinned_model(sample, 0, 1, 0, 1, 9,  method= '1')
        #intf2 = d2 / self.get_unbinned_model(sample, 1, 1, 1, 1, 9,  method= '1')

        if x_SM == 1 and x_NP == 0 and x_K1 ==1 and x_K2 == 0:
            return w1 
        if x_SM == 1 and x_NP == 0 and x_K1 ==0 and x_K2 == 1:
            return w2
        if x_SM == 0 and x_NP == 1 and x_K1 ==1 and x_K2 == 0:
            return w3
        if x_SM == 0 and x_NP == 1 and x_K1 ==0 and x_K2 == 1:
            return w4
        #if x1 == 1 and y1 == 0 and x2 == 1 and y2 == 1:
            #return intf1
        #if x1 == 0 and y1 == 1 and x2 == 1 and y2 == 1:
            #return intf2
        
    def get_interference_term(self, data, h):
        """Get the dynamic density term in Lb->Lclnu decay"""
        obsv              = self.prepare_data(data) 
        #define a helpful function
        def _replace_wc(strg):
            """Helpful function when for leptonic amplitudes in definition of density function"""
            strg_wc = strg.replace('A' , 'V')
            strg_wc = strg_wc.replace('PS', 'S')
            strg_wc = strg_wc.replace('PT', 'T')
            return strg_wc

        Lbampl           = self.get_lb_ampls(obsv)  #LbDecay_lLb_lLc_lw_lwd_WC_FF
        Wampl            = self.get_w_ampls(obsv)   #WDecay_lw_lwd_WC_ll
        Lcamp_k1         = self.get_lc_ampls(obsv, 1, 0) 
        Lcamp_k2         = self.get_lc_ampls(obsv, 0, 1)
        free_params_sm   = self.get_freeparams(1, 0)
        free_params_np   = self.get_freeparams(0, 1)

        if h == 9:
            lLbs    = self.lLbs
            lls     = self.lls
            lps     = self.lps
        else:
            helicity    = self.helicities_dict(h) 
            lLbs        = [helicity[0]]
            lls         = [helicity[1]]
            lps         = [helicity[2]]

        interference1 = atfi.const(0.)
        interference2 = atfi.const(0.)
        interference3 = atfi.const(0.)
        interference4 = atfi.const(0.)
        interference5 = atfi.const(0.)
        interference6 = atfi.const(0.)

        for lLb in lLbs: 
            for ll in lls:
                for lp in lps:
                    
                    amp_sm_k1    = atfi.complex( atfi.const(0.), atfi.const(0.) ) 
                    amp_sm_k2    = atfi.complex( atfi.const(0.), atfi.const(0.) ) 
                    amp_np_k1    = atfi.complex( atfi.const(0.), atfi.const(0.) ) 
                    amp_np_k2    = atfi.complex( atfi.const(0.), atfi.const(0.) )

                    for lLc in self.lLcs:
                        for lw in self.lws:
                            for lwd in self.lws:
                                for WC in self.WCs:
                                    for FF in self.FFs:
                                        lbindx = (lLb,lLc,lw,lwd,WC,FF)
                                        windx  = (lw,lwd,_replace_wc(WC),ll)
                                        fpindx = (WC, FF)
                                        lcindx = (lLc, lp)
                                        cond   = lbindx not in Lbampl or windx not in Wampl
                                        if cond: continue

                                        amp_intf         = atfi.cast_complex(atfi.sqrt(np.array(2.))*atfi.const(self.GF)*atfi.const(self.Vcb)*atfi.const(self.signWC[WC])*atfi.const(self.eta[lw])*atfi.const(self.eta[lwd]) )
                                        print('amp_intf' + str(amp_intf))
                                        amplb            = atfi.cast_complex(Lbampl[lbindx])
                                        print('amplb' + str(amplb))
                                        ampw             = atfi.cast_complex(Wampl[windx])
                                        print('ampw' + str(ampw))
                                        free_params_sm1  = free_params_sm[fpindx]
                                        print('amp_freeparam_sm' + str(free_params_sm1))
                                        free_params_np1  = free_params_np[fpindx]
                                        print('amp_freeparam_np' + str(free_params_np1))
                                        lc_k1            = Lcamp_k1[lcindx]
                                        print('amp_lc_k1' + str(lc_k1))
                                        lc_k2            = Lcamp_k2[lcindx]
                                        print('amp_lc_k2' + str(lc_k2))
                                    
                                        amp_sm_k1       += amp_intf * atfi.cast_complex(Lbampl[lbindx]) * atfi.cast_complex(Wampl[windx]) *free_params_sm[fpindx] * Lcamp_k1[lcindx]    
                                        print(amp_sm_k1)
                                        amp_sm_k2       += amp_intf * atfi.cast_complex(Lbampl[lbindx])*atfi.cast_complex(Wampl[windx])*free_params_sm[fpindx] * Lcamp_k2[lcindx]  
                                        print(amp_sm_k2)
                                        amp_np_k1       += amp_intf * atfi.cast_complex(Lbampl[lbindx])*atfi.cast_complex(Wampl[windx])*free_params_np[fpindx] * Lcamp_k1[lcindx]  
                                        print(amp_np_k1) 
                                        amp_np_k2       += amp_intf * atfi.cast_complex(Lbampl[lbindx])*atfi.cast_complex(Wampl[windx])*free_params_np[fpindx] * Lcamp_k2[lcindx]   
                                        print(amp_np_k2)
                                        

                    interference1  += 2. * tf.math.real(amp_sm_k1) * tf.math.conj(amp_np_k2) 
                    interference2  += 2. * tf.math.real(amp_sm_k2) * tf.math.conj(amp_np_k1) 
                    interference3  += 2. * tf.math.real(amp_sm_k1) * tf.math.conj(amp_np_k1) 
                    interference4  += 2. * tf.math.real(amp_sm_k2) * tf.math.conj(amp_np_k2) 
                    interference5  += 2. * tf.math.real(amp_sm_k1) * tf.math.conj(amp_sm_k2) 
                    interference6  += 2. * tf.math.real(amp_np_k1) * tf.math.conj(amp_np_k2) 
                    #print(ampls_coherent)
    
    
        return interference1, interference2, interference3, interference4, interference5, interference6
    
    def intf_weight(self, data, h, j):

        obsv              = self.prepare_data(data) 
        dGdO_phsp         = self.get_phasespace_term(obsv)
        interf1           = self.get_interference_term(data, h)[0]
        interf2           = self.get_interference_term(data, h)[1]
        interf3           = self.get_interference_term(data, h)[2]
        interf4           = self.get_interference_term(data, h)[3]
        interf5           = self.get_interference_term(data, h)[4]
        interf6           = self.get_interference_term(data, h)[5]

        interference_smk1_npk2 = dGdO_phsp * interf1 / self.get_unbinned_model(data, 1, 1, 1, 1, h, method= '1')
        interference_smk2_npk1 = dGdO_phsp * interf2 / self.get_unbinned_model(data, 1, 1, 1, 1, h, method= '1')
        interference_smk1_npk1 = dGdO_phsp * interf3 / self.get_unbinned_model(data, 1, 1, 1, 1, h, method= '1')
        interference_smk2_npk2 = dGdO_phsp * interf4 / self.get_unbinned_model(data, 1, 1, 1, 1, h, method= '1')
        interference_smk1_smk2 = dGdO_phsp * interf5 / self.get_unbinned_model(data, 1, 1, 1, 1, h, method= '1')
        interference_npk1_npk2 = dGdO_phsp * interf6 / self.get_unbinned_model(data, 1, 1, 1, 1, h, method= '1')

        if j == 'smk1_npk2':
            return interference_smk1_npk2
        if j == 'smk2_npk1':
            return  interference_smk2_npk1
        if j == 'smk1_npk1':
            return  interference_smk1_npk1
        if j == 'smk2_npk2':
            return  interference_smk2_npk2
        if j == 'smk1_smk2':
            return  interference_smk1_smk2
        if j == 'npk1_npk2':
            return  interference_npk1_npk2
    

    def plot_intf_sm_np(self, data, data_bar, i):
        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure
        label = [r"$q^2$", r"$\theta_\mu)$", r"$m^2(K\pi)$", "phi", "theta", "phi", r"$\theta_{res}$", r"$\cos(\theta_{res})$", r"$\cos(\theta_\mu)$"]

        #tfp.plot_distr1d(
            #data[:, i],
            #bins=100,
            #range=(np.min(data[:, i]), np.max(data[:, i])),
            #ax=ax,
            #label = label[i],
            #units="",
            #weights = None,
            #color = 'k',
            #)
            
        tfp.plot_distr1d(
            data[:, i],
            bins=100,
            range=(np.min(data[:, i]), np.max(data[:, i])),
            ax=ax,
            label = label[i],
            units="",
            weights = self.intf_weight( data, 8, 'smk1_npk2'),
            color = 'r',
            )

        tfp.plot_distr1d(
            data[:, i],
            bins=100,
            range=(np.min(data[:, i]), np.max(data[:, i])),
            ax=ax,
            label = label[i],
            units="",
            weights = self.intf_weight( data, 8, 'smk2_npk1'),
            color = 'b',
            )
        
        tfp.plot_distr1d(
            data_bar[:, i],
            bins=100,
            range=(np.min(data[:, i]), np.max(data[:, i])),
            ax=ax,
            label = label[i],
            units="",
            weights = self.intf_weight( data, 8, 'smk1_npk2'),
            color = 'g',
            )

        tfp.plot_distr1d(
            data_bar[:, i],
            bins=100,
            range=(np.min(data[:, i]), np.max(data[:, i])),
            ax=ax,
            label = label[i],
            units="",
            weights = self.intf_weight( data, 8, 'smk2_npk1'),
            color = 'y',
            )
        '''
        tfp.plot_distr1d(
            data[:, i],
            bins=100,
            range=(np.min(data[:, i]), np.max(data[:, i])),
            ax=ax,
            label = label[i],
            units="",
            weights = self.intf_weight( data, 'smk1_npk1'),
            color = 'g',
            )

        tfp.plot_distr1d(
            data[:, i],
            bins=100,
            range=(np.min(data[:, i]), np.max(data[:, i])),
            ax=ax,
            label = label[i],
            units="",
            weights = self.intf_weight( data, 'smk2_npk2'),
            color = 'y',
            )
        '''
            
        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
        #plt.legend(['full', 'SM-K1 and NP-K2', 'SM-K2 and NP-K1', 'SM-K1 and NP-K1', 'SM-K2 and NP-K2'], fontsize =  'xx-small')
        plt.legend(['SM-K1 and NP-K2 - data', 'SM-K2 and NP-K1 - data', 'SM-K1 and NP-K2 - data_bar', 'SM-K2 and NP-K1 - data_bar'], fontsize =  'xx-small')

        return plt.show()
        
    '''
    def weights_helicities(self, sample, x1):  
        #return weights for helicities: 1, 1, 1
        w1     = self.get_unbinned_model(sample, 1, 1, 1, 0, 1, method= '1') / self.get_unbinned_model(sample, 1, 1, 1, 0, 9, method= '1')
        #return weights for helicities: 1, 1, -1
        w2     = self.get_unbinned_model(sample, 1, 1, 1, 0, 2, method= '1') / self.get_unbinned_model(sample, 1, 1, 1, 0, 9, method= '1')
        #return weights for helicities: 1, -1, 1
        w3     = self.get_unbinned_model(sample, 1, 1, 1, 0, 3, method= '1') / self.get_unbinned_model(sample, 1, 1, 1, 0, 9, method= '1')
        #return weights for helicities: 1, -1, -1
        w4     = self.get_unbinned_model(sample, 1, 1, 1, 0, 4, method= '1') / self.get_unbinned_model(sample, 1, 1, 1, 0, 9, method= '1')
        #return weights for helicities: -1, 1, 1
        w5     = self.get_unbinned_model(sample, 1, 1, 1, 0, 5, method= '1') / self.get_unbinned_model(sample, 1, 1, 1, 0, 9, method= '1')
        #return weights for helicities: -1, 1, -1
        w6     = self.get_unbinned_model(sample, 1, 1, 1, 0, 6, method= '1') / self.get_unbinned_model(sample, 1, 1, 1, 0, 9, method= '1')
        #return weights for helicities: -1, -1, 1
        w7     = self.get_unbinned_model(sample, 1, 1, 1, 0, 7, method= '1') / self.get_unbinned_model(sample, 1, 1, 1, 0, 9, method= '1')
        #return weights for helicities: -1, -1, -1
        w8     = self.get_unbinned_model(sample, 1, 1, 1, 0, 8, method= '1') / self.get_unbinned_model(sample, 1, 1, 1, 0, 9, method= '1')

        if x1 == 1:
            return w1 
        if x1 == 2:
            return w2
        if x1 == 3:
            return w3
        if x1 == 4:
            return w4
        if x1 == 5:
            return w5
        if x1 == 6:
            return w6
        if x1 == 7:
            return w7
        if x1 == 8:
            return w8
    '''

    """ MC generation for 7 phase space variables """
    def model_MC(self, x):
        return self.get_unbinned_model( x, 1., 1., 1., 1., 8, method= '1')
    
    def MC(self, size): 
        atfi.set_seed(1)
        #set seed for reproducibility 
        maximum = tft.maximum_estimator(self.model_MC , self.phase_space, 1000000) * 1.5
        print(maximum)
        toy_sample = tft.run_toymc( self.model_MC , self.phase_space , size , maximum, chunk=50000, components = False)
        print(toy_sample)
        return toy_sample
    
    #def A_CP( self, data1 , data2__CP_cong ):
    #    #get the weights for each bin 
    #    def norm_weight(m):
    #        sum_weights = float( len(m) )
    #        numerators = np.ones_like( m )
    #        return( numerators / sum_weights )

    #    """ Mass Distribution """
    #    def m_k_pi(data1 , data2__CP_cong):
    #        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
    #        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure

    #        hist1       = np.histogram( data1[:, 2], bins = 50, weights = norm_weight(data1[:, 2]) )
    #        hist2       = np.histogram( data2__CP_cong[:, 2], bins = 50, weights = norm_weight(data2__CP_cong[:, 2]) )

    #        distrib1    = (hist2[0] - hist1[0]) / (hist1[0] + hist2[0])
    #        print(distrib1)

    #        x_arr   = np.asarray(hist1[1] + 0.5 * (hist1[1][1] - hist1[1][0]))
    #    

    #        plt.axhline(y = 0, color = 'k', linestyle = '-')
    #        plt.scatter(x_arr[:-1] , distrib1, marker = 'x')
    #        plt.title(r"$m(K\pi)$ - A_CP")
    #        plt.xlabel(r"$m(K\pi)$ [GeV]")
    #        plt.ylabel("A_CP")
    #        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)

    #        return plt.show()
    #    
    #    """ Mass Distribution """
    #    def theta(data1 , data2__CP_cong):

    #        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
    #        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure

    #        hist1 = np.histogram( data1[:, 6], bins = 50, range=(0, np.pi), weights = norm_weight(data1[:, 6]) )
    #        hist2 = np.histogram( data2__CP_cong[:, 6], bins = 50,  range=(0, np.pi), weights = norm_weight(data2__CP_cong[:, 6]) )

    #        distrib1 = (hist2[0] - hist1[0]) / (hist1[0] + hist2[0])
    #        print(distrib1)
    #       
    #        x_arr = np.asarray(hist1[1] + 0.5 * (hist1[1][1] - hist1[1][0]))
    #    
    #        plt.axhline(y = 0, color = 'k', linestyle = '-')
    #        plt.scatter(x_arr[:-1] , distrib1, marker = 'x')
    #        plt.title(r"$\theta_{res}$ - A_CP")
    #        plt.xlabel(r"$\theta_{res}$ [Radians]")
    #        plt.ylabel("A_CP")
    #        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)

    #        return plt.show()
    #    
    #    return m_k_pi(data1 , data2__CP_cong), theta(data1 , data2__CP_cong)

    def plot(self, data, data_bar, i):
        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure
        label = [r"$q^2$", r"$\theta_\mu)$", r"$m^2(K\pi)$", "phi", "theta", "phi", r"$\theta_{res}$", r"$\cos(\theta_{res})$", r"$\cos(\theta_\mu)$"]
        
        if i == 7:

            tfp.plot_distr1d(
            tf.cos(data[:, 6]),
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights = None,
            color = 'k',
            )

            tfp.plot_distr1d(
            tf.cos(data[:, 6]),
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights = self.weights(data, 1, 0, 1, 0),
            color = 'r',
            )

            tfp.plot_distr1d(
            tf.cos(data[:, 6]),
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights = self.weights(data, 1, 0, 0, 1),
            color = 'b',
            )

            tfp.plot_distr1d(
            tf.cos(data[:, 6]),
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights = self.weights(data, 0, 1, 1, 0),
            color = 'g',
            )

            tfp.plot_distr1d(
            tf.cos(data[:, 6]),
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights = self.weights(data, 0, 1, 0, 1),
            color = 'y',
            )
        
        elif i == 8:
         
            tfp.plot_distr1d(
            tf.cos(data[:, 1]),
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights = None,
            color = 'k',
            )

            tfp.plot_distr1d(
            tf.cos(data[:, 1]),
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights = self.weights(data, 1, 0, 1, 0),
            color = 'r',
            )

            tfp.plot_distr1d(
            tf.cos(data[:, 1]),
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights = self.weights(data, 1, 0, 0, 1),
            color = 'b',
            )

            tfp.plot_distr1d(
            tf.cos(data[:, 1]),
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights = self.weights(data, 0, 1, 1, 0),
            color = 'g',
            )

            tfp.plot_distr1d(
            tf.cos(data[:, 1]),
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights = self.weights(data, 0, 1, 0, 1),
            color = 'y',
            )

            tfp.plot_distr1d(
            tf.cos(data[:, 1]),
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights = self.weights(data, 1, 0, 1, 1),
            color = 'm',
            )
            
            tfp.plot_distr1d(
            tf.cos(data[:, 1]),
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights = self.weights(data, 0, 1, 1, 1),
            color = 'c',
            )


        else: 
            #'''
            tfp.plot_distr1d(
            data[:, i],
            bins=100,
            range=(np.min(data[:, i]), np.max(data[:, i])),
            ax=ax,
            label = label[i],
            units="",
            weights = None,
            color = 'k',
            )

            tfp.plot_distr1d(
            data_bar[:, i],
            bins=100,
            range=(np.min(data[:, i]), np.max(data[:, i])),
            ax=ax,
            label = label[i],
            units="",
            weights = None,
            color = 'r',
            )
            
            #tfp.plot_distr1d(
            #data[:, i],
            #bins=100,
            #range=(np.min(data[:, i]), np.max(data[:, i])),
            #ax=ax,
            #label = label[i],
            #units="",
            #weights = self.weights(data, 1, 0, 1, 0),
            #color = 'r',
            #)

            #tfp.plot_distr1d(
            #data[:, i],
            #bins=100,
            #range=(np.min(data[:, i]), np.max(data[:, i])),
            #ax=ax,
            #label = label[i],
            #units="",
            #weights = self.weights(data, 1, 0, 0, 1),
            #color = 'b',
            #)

            #tfp.plot_distr1d(
            #data[:, i],
            #bins=100,
            #range=(np.min(data[:, i]), np.max(data[:, i])),
            #ax=ax,
            #label = label[i],
            #units="",
            #weights = self.weights(data, 0, 1, 1, 0),
            #color = 'g',
            #)

            #tfp.plot_distr1d(
            #data[:, i],
            #bins=100,
            #range=(np.min(data[:, i]), np.max(data[:, i])),
            #ax=ax,
            #label = label[i],
            #units="",
            #weights = self.weights(data, 0, 1, 0, 1),
            #color = 'y',
            #)
            ##'''

            #tfp.plot_distr1d(
            #data[:, i],
            #bins=100,
            #range=(np.min(data[:, i]), np.max(data[:, i])),
            #ax=ax,
            #label = label[i],
            #units="",
            #weights = self.weights(data, 1, 0, 1, 1),
            #color = 'm',
            #)

            #tfp.plot_distr1d(
            #data[:, i],
            #bins=100,
            #range=(np.min(data[:, i]), np.max(data[:, i])),
            #ax=ax,
            #label = label[i],
            #units="",
            #weights = self.weights(data, 0, 1, 1, 1),
            #color = 'c',
            #)
            
        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
        #plt.legend(['full', 'SM-K1', 'SM-K2', 'NP-K1', 'NP-K2', 'SM-Interf', 'NP-Interf'], fontsize =  'x-small')
        plt.legend(['Lb', 'Lbbar'], fontsize =  'x-small')
        #plt.legend(['SM-Interf', 'NP-Interf'], fontsize =  'x-small')

        return plt.show()
    '''
    def plot_helicity_dependence(self, data, i):
        tfp.set_lhcb_style(size=12, usetex=False)  # Adjust plotting style for LHCb papers
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4, 3))  # Single subplot on the figure
        label = [r"$q^2$", r"$\theta_\mu)$", r"$m^2(K\pi)$", "phi", "theta", "phi", r"$\theta_{res}$", r"$\cos(\theta_{res})$", r"$\cos(\theta_\mu)$"]

        if i == 8:

            cth = tf.cos(data[:, 1])
            
            tfp.plot_distr1d(
            cth,
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights = None,
            color = 'k',
            )
            
            tfp.plot_distr1d(
            cth,
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights =  self.weights_helicities(data, 1),
            color = 'r',
            )
            
            
            tfp.plot_distr1d(
            cth,
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights =  self.weights_helicities(data, 2),
            color = 'b',
            )
           

            tfp.plot_distr1d(
            cth,
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights =  self.weights_helicities(data, 3),
            color = 'g',
            )
            
            tfp.plot_distr1d(
            cth,
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights =  self.weights_helicities(data, 4),
            color = 'm',
            )
            
            tfp.plot_distr1d(
            cth,
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights =  self.weights_helicities(data, 5),
            color = 'c',
            )
            
            tfp.plot_distr1d(
            cth,
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights =  self.weights_helicities(data, 6),
            color = 'y',
            )

            tfp.plot_distr1d(
            cth,
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights =  self.weights_helicities(data, 7),
            color = 'orange',
            )

            tfp.plot_distr1d(
            cth,
            bins=100,
            range=(-1, 1),
            ax=ax,
            label = label[i],
            units="",
            weights =  self.weights_helicities(data, 8),
            color = 'blueviolet',
            )
            
        else:
            
            tfp.plot_distr1d(
            data[:, i],
            bins=100,
            range=(np.min(data[:, i]), np.max(data[:, i])),
            ax=ax,
            label = label[i],
            units="",
            weights = None,
            color = 'k',
            )
            
            tfp.plot_distr1d(
            data[:, i],
            bins=100,
            range=(np.min(data[:, i]), np.max(data[:, i])),
            ax=ax,
            label = label[i],
            units="",
            weights =  self.weights_helicities(data, 1),
            color = 'r',
            )
            
            
            tfp.plot_distr1d(
            data[:, i],
            bins=100,
            range=(np.min(data[:, i]), np.max(data[:, i])),
            ax=ax,
            label = label[i],
            units="",
            weights =  self.weights_helicities(data, 2),
            color = 'b',
            )
            
            tfp.plot_distr1d(
            data[:, i],
            bins=100,
            range=(np.min(data[:, i]), np.max(data[:, i])),
            ax=ax,
            label = label[i],
            units="",
            weights =  self.weights_helicities(data, 3),
            color = 'g',
            )
            
            tfp.plot_distr1d(
            data[:, i],
            bins=100,
            range=(np.min(data[:, i]), np.max(data[:, i])),
            ax=ax,
            label = label[i],
            units="",
            weights =  self.weights_helicities(data, 4),
            color = 'm',
            )
            
            tfp.plot_distr1d(
            data[:, i],
            bins=100,
            range=(np.min(data[:, i]), np.max(data[:, i])),
            ax=ax,
            label = label[i],
            units="",
            weights =  self.weights_helicities(data, 5),
            color = 'c',
            )
            
            tfp.plot_distr1d(
            data[:, i],
            bins=100,
            range=(np.min(data[:, i]), np.max(data[:, i])),
            ax=ax,
            label = label[i],
            units="",
            weights =  self.weights_helicities(data, 6),
            color = 'y',
            )

            tfp.plot_distr1d(
            data[:, i],
            bins=100,
            range=(np.min(data[:, i]), np.max(data[:, i])),
            ax=ax,
            label = label[i],
            units="",
            weights =  self.weights_helicities(data, 7),
            color = 'orange',
            )

            tfp.plot_distr1d(
            data[:, i],
            bins=100,
            range=(np.min(data[:, i]), np.max(data[:, i])),
            ax=ax,
            label = label[i],
            units="",
            weights =  self.weights_helicities(data, 8),
            color = 'blueviolet',
            )
            

        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad= 1.0)
        #plt.legend(['Sum over all lLb, ll, lp'], fontsize =  'xx-small')
        #plt.legend(['lLb: 1, ll: 1, lp:1', '1, -1, 1'], fontsize =  'xx-small')

        plt.legend(['Sum over all lLb, ll, lp','lLb: 1, ll: 1, lp:1', '1, 1, -1', '1, -1, 1', '1, -1, -1', '-1, 1, 1', '-1, 1, -1', '-1, -1, 1', '-1, -1, -1'], fontsize =  'xx-small')

        return plt.show()
    '''


########### tests lam_c added
MLb     = 5619.49997776e-3    #GeV
MLc     = 2286.45992749e-3    #GeV
Mlep    = 105.6583712e-3      #GeV Mu
Mp      = 0.938      
Mk      = 0.497    
Mpi     = 0.140 

md      = LbToLclNu_Model(MLb, MLc, Mlep, Mp, Mk, Mpi,  1.)
toy_sample = md.MC(10000)
md_bar  = LbToLclNu_Model(MLb, MLc, Mlep, Mp, Mk, Mpi, -1.)
toy_sample_bar = md_bar.MC(10000)

##### check sumof weights
#sum  = md.weights(toy_sample, 1, 0, 1, 0) + md.weights(toy_sample, 1, 0, 0, 1) + md.weights(toy_sample, 0, 1, 1, 0) + md.weights(toy_sample, 0, 1, 0, 1)
#sum += md.intf_weight(toy_sample, 'smk1_smk2')
#sum += md.intf_weight(toy_sample, 'smk1_npk2')
#sum += md.intf_weight(toy_sample, 'npk1_npk2')
#sum += md.intf_weight(toy_sample, 'smk2_npk1') 
#sum += md.intf_weight(toy_sample, 'smk1_npk1') 
#sum += md.intf_weight(toy_sample, 'smk2_npk2')
#print(sum)
#print(tf.math.reduce_sum(sum))

#sum_bar  = md_bar.weights(toy_sample_bar, 1, 0, 1, 0) + md_bar.weights(toy_sample_bar, 1, 0, 0, 1) + md_bar.weights(toy_sample_bar, 0, 1, 1, 0) + md_bar.weights(toy_sample_bar, 0, 1, 0, 1)
#sum_bar += md_bar.intf_weight(toy_sample_bar, 'smk1_smk2')
#sum_bar += md_bar.intf_weight(toy_sample_bar, 'npk1_npk2')
#sum_bar += md_bar.intf_weight(toy_sample_bar, 'smk1_npk2')
#sum_bar += md_bar.intf_weight(toy_sample_bar, 'smk2_npk1') 
#sum_bar += md_bar.intf_weight(toy_sample_bar, 'smk1_npk1') 
#sum_bar += md_bar.intf_weight(toy_sample_bar, 'smk2_npk2')
#print(sum_bar)
#print(tf.math.reduce_sum(sum_bar))
#######
#print(md.intf_weight(toy_sample, 'smk1_npk2'))
#print(md.intf_weight(toy_sample, 'smk2_npk1'))
#print(md_bar.intf_weight(toy_sample_bar, 'smk2_npk1'))
#print(md_bar.intf_weight(toy_sample_bar, 'smk1_npk2'))

md.get_interference_term(toy_sample[0:5, ], 9)
md_bar.get_interference_term(toy_sample_bar[0:5, ], 9)
#md.get_interference_term(toy_sample[0:5, ], 8)
#md_bar.get_interference_term(toy_sample_bar[0:5, ], 8)

#md.plot_intf_sm_np( toy_sample, toy_sample_bar , 2)

#md.plot(toy_sample, toy_sample_bar, 2)
#md.plot(toy_sample, toy_sample_bar, 6)
#md.plot_intf_sm_np(toy_sample_bar, 2)
#md.plot_intf_sm_np(toy_sample, 2)


#md.A_CP(toy_sample1, toy_sample2)

#md.plot_helicity_dependence(toy_sample, 0)
#md.plot_helicity_dependence(toy_sample, 8)

#md.plot(toy_sample, 0)
#md.plot(toy_sample, 1)
#md.plot(toy_sample1, 2)
#md.plot(toy_sample1, 6)
#md.plot(toy_sample, 7)
#md.plot(toy_sample, 8)



#obv = {}
#obv['m2_kpi'] = atfi.const(0.8**2)
#obv['lc_phi_res'] = atfi.const(0.6)
#obv['lc_theta_res'] = atfi.const(0.5)
#obv['res_phi_k'] = atfi.const(0.4)
#obv['res_theta_k'] = atfi.const(0.3)
#print(md.get_lc_ampls(obv))
#print(md.get_freeparams() )
########### 
